package ru.itsavant.ofdukm.comparator.ofd.rest.json.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftInfoDetailJson {

    private String fnFactoryNumber;
    private int shiftNumber;
    private String openDateTime;
    private int openFdNumber;
    private String closeDateTime;

    private int closeFdNumber;
    private String cashier;
    private ShiftStateJson state;
    private ShiftIncomeJson income;
    private ShiftIncomeReturnJson incomeReturn;

    private ShiftIncomeCorrectionJson incomeCorrection;
    private ShiftExpenditureJson expenditure;
    private ShiftExpenditureReturnJson expenditureReturn;
    private ShiftExpenditureCorrectionJson expenditureCorrection;
}
