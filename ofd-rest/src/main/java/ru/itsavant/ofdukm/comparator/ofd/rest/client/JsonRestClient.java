package ru.itsavant.ofdukm.comparator.ofd.rest.client;

import ch.qos.logback.classic.Logger;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import okhttp3.*;
import org.slf4j.LoggerFactory;
import ru.itsavant.ofdukm.comparator.common.logger.LoggerUtil;
import ru.itsavant.ofdukm.comparator.common.stacktrace.StackTracePrinter;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestPostRequest;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.Buffer;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Objects;

public class JsonRestClient<TRequest, TResponse> {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(JsonRestClient.class);

    private final ObjectMapper mapper = new ObjectMapper().enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
    private final OkHttpClient httpClient = new OkHttpClient();

    private final Logger logger;

    private String url;
    private Class<TRequest> requestClass;
    private Class<TResponse> responseClass;

    public static class Builder<TRequest, TResponse> {
        private String url;
        private Class<TRequest> requestClass;
        private Class<TResponse> responseClass;

        public Builder<TRequest, TResponse> url(final String url) {
            this.url = url;
            return this;
        }

        public Builder<TRequest, TResponse> requestClass(final Class<TRequest> requestClass) {
            this.requestClass = requestClass;
            return this;
        }

        public Builder<TRequest, TResponse> responseClass(final Class<TResponse> responseClass) {
            this.responseClass = responseClass;
            return this;
        }

        public JsonRestClient<TRequest, TResponse> build() {
            return new JsonRestClient<>(url, requestClass, responseClass);
        }
    }

    private JsonRestClient(final String url, final Class<TRequest> requestClass, final Class<TResponse> responseClass) {
        this.url = url;
        this.requestClass = requestClass;
        this.responseClass = responseClass;
        this.logger = LoggerUtil.createLoggerFor("ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient", "ofdukm-comparator.log");
    }

    private JsonRestClient(final String url) {
        this(url, null, null);
    }

    public TResponse get(final RestGetRequest getRequest) throws IOException {
        try {
            final Headers headers = Headers.of(getRequest.headers());
            final HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(url)).newBuilder();
            getRequest.body().forEach(urlBuilder::addQueryParameter);

            final Request request = new Request.Builder()
                    .url(urlBuilder.build())
                    .get()
                    .headers(headers)
                    .build();

            logger.info("Send HTTP GET-request with url {} with headers {}", urlBuilder.build().toString(), headers.toString());

            final Response response = httpClient.newCall(request).execute();
            response.header("Connection", "close");

//        final String s = response.body().string();
//        final byte[] bytes = new byte[1027 * 1024];
//        int size = response.body().byteStream().read(bytes);
//
//        final byte[] read = Arrays.copyOf(bytes, size);

//        final ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
//        final Type type = parameterizedType.getActualTypeArguments()[1];
//        final Class<?> klass = Class.forName(type.getTypeName());


            final String bodyString = response.body().string();

            logger.info("Receive response from {} with content: {}", url, bodyString);

            System.out.println();
            System.out.println("get: " + bodyString);

            response.body().close();

            final TResponse result = mapper.readValue(bodyString, responseClass);

            if (result == null) {
                return null;
            }

            return result;
        } catch (final Throwable throwable) {
            logger.error("{}", throwable.getMessage());
            logger.error(StackTracePrinter.print(throwable));
            throw new RuntimeException(throwable);
        }
    }

    public TResponse post(final RestPostRequest<TRequest> postRequest) throws IOException, ClassNotFoundException {
        try {
            final String json = mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(postRequest.body());
            final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);
            final Headers headers = Headers.of(postRequest.headers());
            final Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .headers(headers)
                    .build();

            logger.info("Send HTTP POST-request with url {} and with data: {}", url, requestBody.toString());

            final Response response = httpClient.newCall(request).execute();

            if (response.body() == null) {
                return null;
            }

//        final ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
//        final Type type = parameterizedType.getActualTypeArguments()[1];
//        final Class<?> klass = Class.forName(type.getTypeName());

            final String bodyString = response.body().string();

            logger.info("Receive response from url {} with content: {}", url, bodyString);

            System.out.println("put: " + bodyString);

            response.body().close();

            final TResponse result = mapper.readValue(bodyString, responseClass);

            if (result == null) {
                return null;
            }

            return result;
        } catch (final Throwable throwable) {
            logger.error("{}", throwable.getMessage());
            logger.error(StackTracePrinter.print(throwable));
            throw new RuntimeException(throwable);
        }
    }
}
