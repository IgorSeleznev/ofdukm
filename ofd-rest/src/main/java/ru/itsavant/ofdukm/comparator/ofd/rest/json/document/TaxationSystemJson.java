package ru.itsavant.ofdukm.comparator.ofd.rest.json.document;

public enum TaxationSystemJson {

    OSN("OSN"),
    USNINCOME("USNIncome"),
    USNINCOMEEXPENDITURE("USNIncomeExpenditure"),
    ENVD("ENVD"),
    ESN("ESN"),
    PATENT("Patent");

    private String value;

    TaxationSystemJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
