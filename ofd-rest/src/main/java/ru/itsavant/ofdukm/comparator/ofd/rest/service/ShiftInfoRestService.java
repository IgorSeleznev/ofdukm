package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet.OutletListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet.OutletListResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftInfoRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftInfoResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.OUTLETS_LIST;
import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.SHIFT_INFO;

public class ShiftInfoRestService implements OfdRestService {

    private final JsonRestClient<ShiftInfoRequestJson, ShiftInfoResponseJson> restClient;

    public ShiftInfoRestService() {
        this.restClient = new JsonRestClient.Builder<ShiftInfoRequestJson, ShiftInfoResponseJson>()
                .url(SHIFT_INFO)
                .requestClass(ShiftInfoRequestJson.class)
                .responseClass(ShiftInfoResponseJson.class)
                .build();
    }

    public ShiftInfoResponseJson shiftInfo(final ShiftInfoRequestJson infoRequest)
            throws IOException, ClassNotFoundException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", infoRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("fn", infoRequest.getFn())
                                        .entry("shift", infoRequest.getShift())
                                        .map()
                        )
        );
    }
}
