package ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.CountsJson;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class OutletListResponseJson {

    private String reportDate;
    private CountsJson counts;
    private List<OutletListItemResponseJson> records;
}
