package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

public enum FnStateJson {

    ACTIVE("Active"),
    EXPIRES("Expires"),
    EXPIRED("Expired");

    private String value;

    FnStateJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
