package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentListResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.DEPARTMENTS_LIST;
import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.DOCUMENT_LIST;

public class DocumentListRestService implements OfdRestService {

    private final JsonRestClient<DocumentListRequestJson, DocumentListResponseJson> restClient;

    public DocumentListRestService() {
        this.restClient = new JsonRestClient.Builder<DocumentListRequestJson, DocumentListResponseJson>()
                .url(DOCUMENT_LIST)
                .requestClass(DocumentListRequestJson.class)
                .responseClass(DocumentListResponseJson.class)
                .build();
    }

    public DocumentListResponseJson documentList(final DocumentListRequestJson listRequest)
            throws IOException, ClassNotFoundException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", listRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("fn", listRequest.getFn())
                                        .entry("shift", listRequest.getShift())
                                        .entry("pn", Integer.toString(listRequest.getPageNumber()))
                                        .entry("ps", Integer.toString(listRequest.getPageSize()))
                                        .map()
                        )
        );
    }
}
