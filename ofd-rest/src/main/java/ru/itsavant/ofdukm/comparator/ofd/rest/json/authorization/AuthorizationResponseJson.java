package ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class AuthorizationResponseJson {

    private String sessionToken;
}
