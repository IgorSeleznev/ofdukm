package ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class OutletDepartmentListItemResponseJson {

    private String id;
    private String name;
    private String code;
}
