package ru.itsavant.ofdukm.comparator.ofd.rest.service;

public interface RestClientHostConstants {

    String AUTHORIZATION = "https://api-lk-ofd.taxcom.ru/API/v2/Login";
    String DEPARTMENTS_LIST = "https://api-lk-ofd.taxcom.ru/API/v2/DepartmentList";
    String OUTLETS_LIST = "https://api-lk-ofd.taxcom.ru/API/v2/OutletList";
    String KKT_LIST = "https://api-lk-ofd.taxcom.ru/API/v2/KktList";
    String KKT_FN_HISTORY = "https://api-lk-ofd.taxcom.ru/API/v2/FnHistory";
    String KKT_INFO = "https://api-lk-ofd.taxcom.ru/API/v2/KktInfo";
    String SHIFT_LIST = "https://api-lk-ofd.taxcom.ru/API/v2/ShiftList";
    String SHIFT_INFO = "https://api-lk-ofd.taxcom.ru/API/v2/ShiftInfo";
    String DOCUMENT_LIST = "https://api-lk-ofd.taxcom.ru/API/v2/DocumentList";
    String DOCUMENT_INFO = "https://api-lk-ofd.taxcom.ru/API/v2/DocumentInfo";
}
