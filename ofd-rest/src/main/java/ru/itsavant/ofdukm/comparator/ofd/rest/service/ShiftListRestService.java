package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftInfoRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftInfoResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftListResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.OUTLETS_LIST;
import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.SHIFT_LIST;

public class ShiftListRestService implements OfdRestService {

    private final JsonRestClient<ShiftListRequestJson, ShiftListResponseJson> restClient;

    public ShiftListRestService() {
        this.restClient = new JsonRestClient.Builder<ShiftListRequestJson, ShiftListResponseJson>()
                .url(SHIFT_LIST)
                .requestClass(ShiftListRequestJson.class)
                .responseClass(ShiftListResponseJson.class)
                .build();
    }

    public ShiftListResponseJson shiftList(final ShiftListRequestJson listRequest)
            throws IOException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", listRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("fn", listRequest.getFn())
                                        .entry("begin", listRequest.getBegin())
                                        .entry("end", listRequest.getEnd())
                                        .entry("pn", Integer.toString(listRequest.getPageNumber()))
                                        .entry("ps", Integer.toString(listRequest.getPageSize()))
                                        .map()
                        )
        );
    }
}
