package ru.itsavant.ofdukm.comparator.ofd.rest.json.department;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DepartmentParentListItemResponseJson {

    private String id;
    private String name;
    private String code;
}
