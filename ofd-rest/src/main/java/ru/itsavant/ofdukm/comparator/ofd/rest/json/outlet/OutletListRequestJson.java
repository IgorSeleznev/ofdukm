package ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class OutletListRequestJson {

    private String sessionToken;
    private String departmentId;
    private int pageNumber;
    private int pageSize;
}
