package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient.Builder;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktListResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.KKT_LIST;

public class KktListRestService implements OfdRestService {

    private final JsonRestClient<KktListRequestJson, KktListResponseJson> restClient;

    public KktListRestService() {
        this.restClient = new Builder<KktListRequestJson, KktListResponseJson>()
                .url(KKT_LIST)
                .requestClass(KktListRequestJson.class)
                .responseClass(KktListResponseJson.class)
                .build();
    }

    public KktListResponseJson kktList(final KktListRequestJson listRequest)
            throws IOException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", listRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("id", listRequest.getOutletId())
                                        .entry("pn", Integer.toString(listRequest.getPageNumber()))
                                        .entry("ps", Integer.toString(listRequest.getPageSize()))
                                        .entry("type", "3")
                                        .map()
                        )
        );
    }
}
