package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestPostRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization.AuthorizationRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization.AuthorizationResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization.IntegratorAuthorizationRequestJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient.*;
import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.AUTHORIZATION;

public class AuthorizationRestService implements OfdRestService {

    private final JsonRestClient<AuthorizationRequestJson, AuthorizationResponseJson> restClient;

    public AuthorizationRestService() {
        this.restClient = new Builder<AuthorizationRequestJson, AuthorizationResponseJson>()
                .url(AUTHORIZATION)
                .requestClass(AuthorizationRequestJson.class)
                .responseClass(AuthorizationResponseJson.class)
                .build();
    }

    public AuthorizationResponseJson authorize(final IntegratorAuthorizationRequestJson authorizationRequest)
            throws IOException, ClassNotFoundException {
        return restClient.post(
                new RestPostRequest<AuthorizationRequestJson>()
                        .header("Integrator-ID", authorizationRequest.getIntegratorId())
                        .body(authorizationRequest.getAuthorization())
        );
    }
}
