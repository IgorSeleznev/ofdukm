package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

public enum StatusFnJson {

    ACTIVE("Active"),
    NOTACTIVE("NotActive");

    private String value;

    StatusFnJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
