package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

public enum LastDocumentStateJson {

    OK("OK"),
    WARNING("Warning"),
    PROBLEM("Problem");

    private String value;

    LastDocumentStateJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
