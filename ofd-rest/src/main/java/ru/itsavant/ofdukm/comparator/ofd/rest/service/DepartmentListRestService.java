package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.DEPARTMENTS_LIST;

public class DepartmentListRestService implements OfdRestService {

    private final JsonRestClient<DepartmentListRequestJson, DepartmentListResponseJson> restClient;

    public DepartmentListRestService() {
        this.restClient = new JsonRestClient.Builder<DepartmentListRequestJson, DepartmentListResponseJson>()
                .url(DEPARTMENTS_LIST)
                .requestClass(DepartmentListRequestJson.class)
                .responseClass(DepartmentListResponseJson.class)
                .build();
    }

    public DepartmentListResponseJson departmentList(final DepartmentListRequestJson listRequest)
            throws IOException, ClassNotFoundException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", listRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("pn", Integer.toString(listRequest.getPageNumber()))
                                        .entry("ps", Integer.toString(listRequest.getPageSize()))
                                        .map()
                        )
        );
    }
}
