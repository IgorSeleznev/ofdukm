package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktInfoRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktInfoResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.DEPARTMENTS_LIST;
import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.KKT_INFO;

public class KktInfoRestService implements OfdRestService {

    private final JsonRestClient<KktInfoRequestJson, KktInfoResponseJson> restClient;

    public KktInfoRestService() {
        this.restClient = new JsonRestClient.Builder<KktInfoRequestJson, KktInfoResponseJson>()
                .url(KKT_INFO)
                .requestClass(KktInfoRequestJson.class)
                .responseClass(KktInfoResponseJson.class)
                .build();
    }

    public KktInfoResponseJson kktInfo(final KktInfoRequestJson infoRequest)
            throws IOException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", infoRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("fn", infoRequest.getFn())
                                        .map()
                        )
        );
    }
}
