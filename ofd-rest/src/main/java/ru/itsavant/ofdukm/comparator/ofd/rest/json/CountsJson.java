package ru.itsavant.ofdukm.comparator.ofd.rest.json;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CountsJson {

    private int recordCount;
    private int recordFilteredCount;
    private int recordInResponceCount;
}
