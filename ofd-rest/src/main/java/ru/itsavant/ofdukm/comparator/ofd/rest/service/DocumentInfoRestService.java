package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.StringJsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentInfoRequestJson;

import java.io.IOException;
import java.util.Map;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.DOCUMENT_INFO;

public class DocumentInfoRestService implements OfdRestService {

    private final ObjectMapper mapper = new ObjectMapper();
    private final StringJsonRestClient<DocumentInfoRequestJson> restClient;

    public DocumentInfoRestService() {
        this.restClient = new StringJsonRestClient<>(
                DOCUMENT_INFO
        );
    }

    public Map<String, Object> documentInfo(final DocumentInfoRequestJson infoRequest)
            throws IOException, ClassNotFoundException {

        final String response = this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", infoRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("fn", infoRequest.getFn())
                                        .entry("fd", infoRequest.getFd())
                                        .map()
                        )
        );

        return mapper.readValue(response, new TypeReference<Map<String, Object>>() {
        });
    }
}
