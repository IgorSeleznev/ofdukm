package ru.itsavant.ofdukm.comparator.ofd.rest.json.department;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.ProblemIndicatorJson;

@Getter
@Setter
@Accessors(chain = true)
public class DepartmentListItemResponseJson {

    private String id;
    private String name;
    private String code;
    private DepartmentParentListItemResponseJson parent;
    private ProblemIndicatorJson problemIndicator;
}
