package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient.Builder;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet.OutletListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet.OutletListResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.OUTLETS_LIST;

public class OutletListRestService implements OfdRestService {

    private final JsonRestClient<OutletListRequestJson, OutletListResponseJson> restClient;

    public OutletListRestService() {
        this.restClient = new Builder<OutletListRequestJson, OutletListResponseJson>()
                .url(OUTLETS_LIST)
                .requestClass(OutletListRequestJson.class)
                .responseClass(OutletListResponseJson.class)
                .build();
    }

    public OutletListResponseJson outletList(final OutletListRequestJson listRequest)
            throws IOException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", listRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entryIfValueNotNull("id", listRequest.getDepartmentId())
                                        .entry("pn", Integer.toString(listRequest.getPageNumber()))
                                        .entry("ps", Integer.toString(listRequest.getPageSize()))
                                        .map()
                        )
        );
    }
}
