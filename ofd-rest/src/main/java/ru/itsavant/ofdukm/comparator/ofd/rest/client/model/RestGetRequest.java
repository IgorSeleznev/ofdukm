package ru.itsavant.ofdukm.comparator.ofd.rest.client.model;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

public class RestGetRequest {

    private Map<String, String> headers = newHashMap();
    private Map<String, String> body = newHashMap();

    public RestGetRequest header(final String name, final String value) {
        headers.put(name, value);
        return this;
    }

    public Map<String, String> headers() {
        return headers;
    }

    public RestGetRequest headers(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public Map<String, String> body() {
        return body;
    }

    public RestGetRequest body(Map<String, String> body) {
        this.body = body;
        return this;
    }

    @Override
    public String toString() {
        return "RestGetRequest{" +
                "headers=" + headers +
                ", body=" + body +
                '}';
    }
}
