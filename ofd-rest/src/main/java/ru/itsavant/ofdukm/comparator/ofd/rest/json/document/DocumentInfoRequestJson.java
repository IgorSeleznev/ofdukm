package ru.itsavant.ofdukm.comparator.ofd.rest.json.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentInfoRequestJson {

    private String sessionToken;
    private String fn;
    private String fd;
}
