package ru.itsavant.ofdukm.comparator.ofd.rest.json.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentInfoResponseJson {

    private String sessionToken;
    private String fn;
    private String shift;
    private String pageNumber;
}
