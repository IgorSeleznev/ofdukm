package ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class IntegratorAuthorizationRequestJson {

    private String integratorId;
    private AuthorizationRequestJson authorization;
}
