package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class KktCashdeskPartnerContactJson {

    private String name;
    private String phone;
    private String email;
}
