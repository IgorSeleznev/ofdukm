package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class KktCashdeskPartnerJson {

    private String name;
    private String inn;
    private KktCashdeskPartnerContactJson contact;
}
