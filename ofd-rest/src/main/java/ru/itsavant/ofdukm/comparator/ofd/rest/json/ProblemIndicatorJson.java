package ru.itsavant.ofdukm.comparator.ofd.rest.json;

public enum ProblemIndicatorJson {

    OK("OK"),
    WARNING("Warning"),
    PROBLEM("Problem");

    private String value;

    ProblemIndicatorJson(final String value) {
        this.value = value;
    }

    public static ProblemIndicatorJson getEnum(final String value) {
        for(ProblemIndicatorJson v : values())
            if(v.getValue().equalsIgnoreCase(value)) return v;
        throw new IllegalArgumentException();
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
