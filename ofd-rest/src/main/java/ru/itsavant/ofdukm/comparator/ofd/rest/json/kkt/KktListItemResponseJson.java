package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.ProblemIndicatorJson;

@Getter
@Setter
@Accessors(chain = true)
public class KktListItemResponseJson {

    private String name;
    private String kktRegNumber;
    private String kktFactoryNumber;
    private String fnFactoryNumber;
    private CashdeskStateJson cashdeskState;

    private ProblemIndicatorJson problemIndicator;
}
