package ru.itsavant.ofdukm.comparator.ofd.rest.json.shift;

public enum ShiftStatusJson {

    OPEN("Open"),
    CLOSE("Close"),
    NODATA("NoDate");

    private String value;

    ShiftStatusJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
