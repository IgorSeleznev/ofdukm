package ru.itsavant.ofdukm.comparator.ofd.rest.json.document;

public enum DocumentAccountingTypeJson {

    INCOME("Income"),
    INCOMERETURN("IncomeReturn"),
    EXPENDITURE("Expenditure"),
    EXPENDITURERETURN("ExpenditureReturn");

    private String value;

    DocumentAccountingTypeJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
