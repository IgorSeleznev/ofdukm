package ru.itsavant.ofdukm.comparator.ofd.rest.context;

public class RestClientContext {

    private static final RestClientContext CONTEXT = new RestClientContext();

    private RestClientContext() {

    }

    public RestClientContext context() {
        return CONTEXT;
    }
}
