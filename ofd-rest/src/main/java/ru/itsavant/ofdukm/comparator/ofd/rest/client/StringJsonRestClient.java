package ru.itsavant.ofdukm.comparator.ofd.rest.client;

import ch.qos.logback.classic.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import ru.itsavant.ofdukm.comparator.common.logger.LoggerUtil;
import ru.itsavant.ofdukm.comparator.common.stacktrace.StackTracePrinter;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestPostRequest;

import java.io.IOException;
import java.util.Objects;

public class StringJsonRestClient<TRequest> {

    private final ObjectMapper mapper = new ObjectMapper();
    private final OkHttpClient httpClient = new OkHttpClient();
    private final String url;
    private final Logger logger;

    public StringJsonRestClient(final String url) {
        this.url = url;
        this.logger = LoggerUtil.createLoggerFor("ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient", "ofdukm-comparator.log");
    }

    public String get(final RestGetRequest getRequest) throws IOException {
        try {
            final Headers headers = Headers.of(getRequest.headers());
            final HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(url)).newBuilder();
            getRequest.body().forEach(urlBuilder::addQueryParameter);

            final Request request = new Request.Builder()
                    .url(urlBuilder.build())
                    .get()
                    .headers(headers)
                    .build();

            final Response response = httpClient.newCall(request).execute();

            if (response.body() == null) {
                return null;
            }

            return response.body().string();
        } catch (final Throwable throwable) {
            logger.error("{}", throwable.getMessage());
            logger.error(StackTracePrinter.print(throwable));
            throw new RuntimeException(throwable);
        }
    }

    public String post(final RestPostRequest<TRequest> postRequest) throws IOException {
        try {
            final String json = mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(postRequest.body());
            final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);
            final Headers headers = Headers.of(postRequest.headers());
            final Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .headers(headers)
                    .build();
            final Response response = httpClient.newCall(request).execute();

            if (response.body() == null) {
                return null;
            }

            return response.body().string();
        } catch (final Throwable throwable) {
            logger.error("{}", throwable.getMessage());
            logger.error(StackTracePrinter.print(throwable));
            throw new RuntimeException(throwable);
        }
    }
}
