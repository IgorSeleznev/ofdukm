package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.CountsJson;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class KktListResponseJson {

    private String reportDate;
    private CountsJson counts;
    private KktOutletListItemResponseJson outlet;
    private List<KktListItemResponseJson> records;
}
