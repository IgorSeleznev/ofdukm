package ru.itsavant.ofdukm.comparator.ofd.rest.json.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.CountsJson;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentListResponseJson {

    private String reportDate;
    private CountsJson counts;
    private List<DocumentListItemResponseJson> records;
}
