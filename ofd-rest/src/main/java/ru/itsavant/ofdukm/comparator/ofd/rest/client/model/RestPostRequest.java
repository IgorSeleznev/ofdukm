package ru.itsavant.ofdukm.comparator.ofd.rest.client.model;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

public class RestPostRequest<TRequest> {

    private Map<String, String> headers = newHashMap();
    private TRequest body;

    public RestPostRequest<TRequest> header(final String name, final String value) {
        headers.put(name, value);
        return this;
    }

    public Map<String, String> headers() {
        return headers;
    }

    public RestPostRequest<TRequest> headers(final Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public TRequest body() {
        return body;
    }

    public RestPostRequest<TRequest> body(final TRequest body) {
        this.body = body;
        return this;
    }

    @Override
    public String toString() {
        return "RestPostRequest{" +
                "headers=" + headers +
                ", body=" + body +
                '}';
    }
}
