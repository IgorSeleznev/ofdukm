package ru.itsavant.ofdukm.comparator.ofd.rest.json.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.CountsJson;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftListResponseJson {

    private String reportDate;
    private CountsJson counts;
    private List<ShiftListItemResponseJson> records;
}
