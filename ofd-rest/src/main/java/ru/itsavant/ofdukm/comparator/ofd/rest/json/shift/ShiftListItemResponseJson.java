package ru.itsavant.ofdukm.comparator.ofd.rest.json.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftListItemResponseJson {

    private String fnFactoryNumber;
    private int shiftNumber;
    private String openDateTime;
    private String closeDateTime;
    private int receiptCount;
}
