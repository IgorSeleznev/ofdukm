package ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.ProblemIndicatorJson;

@Getter
@Setter
@Accessors(chain = true)
public class OutletListItemResponseJson {

    private String id;
    private String name;
    private String code;
    private String address;
    private ProblemIndicatorJson problemIndicator;

    private OutletDepartmentListItemResponseJson department;
}
