package ru.itsavant.ofdukm.comparator.ofd.rest.json.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftExpenditureCorrectionJson {

    private int total;
    private int cash;
    private int electronic;
    private int receiptCount;
    private int nds0Total;

    private int noNDSTotal;
    private int nds10;
    private int nds18;
    private int nds20;
    private int nds10_110;

    private int nds18_118;
    private int nds20_120;
    private int prepayment;
    private int credit;
    private int exchange;
}
