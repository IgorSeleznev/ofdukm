package ru.itsavant.ofdukm.comparator.ofd.rest.json.department;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DepartmentListRequestJson {

    private String sessionToken;
    private int pageNumber;
    private int pageSize;
}
