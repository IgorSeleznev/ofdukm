package ru.itsavant.ofdukm.comparator.ofd.rest.json.department;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.CountsJson;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class DepartmentListResponseJson {

    private String responseDate;
    private CountsJson counts;
    private List<DepartmentListItemResponseJson> records;
}
