package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftStatusJson;

@Getter
@Setter
@Accessors(chain = true)
public class KktCashdeskResponseJson {

    private String name;
    private String kktRegNumber;
    private String kktFactoryNumber;
    private String kktModelName;
    private String fnFactoryNumber;

    private String fnRegDateTime;
    private String fnDuration;
    private KktShiftStatusJson shiftStatus;
    private CashdeskStateJson cashdeskState;
    private String cashdeskEndDateTime;

    private FnStateJson fnState;
    private String fnEndDateTime;
    private LastDocumentStateJson lastDocumentState;
    private String lastDocumentDateTime;
    private KktInfoOutletResponseJson outlet;
    private KktCashdeskPartnerJson partner;
}
