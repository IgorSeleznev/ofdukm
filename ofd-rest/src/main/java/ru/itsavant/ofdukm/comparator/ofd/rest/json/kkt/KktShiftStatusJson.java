package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

public enum KktShiftStatusJson {

    OPEN("Open"),
    CLOSE("Close"),
    NODATA("NoDate");

    private String value;

    KktShiftStatusJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
