package ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt;

public enum CashdeskStateJson {

    ACTIVE("Active"),
    EXPIRES("Expires"),
    EXPIRED("Expired"),
    INACTIVE("Inactive"),
    ACTIVATION("Activation"),
    DEACTIVATION("Deactivation"),
    FNCHANGE("FNChange"),
    FNSREGISTRATION("FNSRegistration"),
    FNSRegistrationError("FNSRegistrationError");

    private String value;

    CashdeskStateJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
