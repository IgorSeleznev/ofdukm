package ru.itsavant.ofdukm.comparator.ofd.rest.service;

import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.JsonRestClient;
import ru.itsavant.ofdukm.comparator.ofd.rest.client.model.RestGetRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktFnListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktFnListResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktListResponseJson;

import java.io.IOException;

import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.DEPARTMENTS_LIST;
import static ru.itsavant.ofdukm.comparator.ofd.rest.service.RestClientHostConstants.KKT_FN_HISTORY;

public class KktFnListRestService implements OfdRestService {

    private final JsonRestClient<KktFnListRequestJson, KktFnListResponseJson> restClient;

    public KktFnListRestService() {
        this.restClient = new JsonRestClient.Builder<KktFnListRequestJson, KktFnListResponseJson>()
                .url(KKT_FN_HISTORY)
                .requestClass(KktFnListRequestJson.class)
                .responseClass(KktFnListResponseJson.class)
                .build();
    }

    public KktFnListResponseJson kktFnList(final KktFnListRequestJson listRequest)
            throws IOException, ClassNotFoundException {
        return this.restClient.get(
                new RestGetRequest()
                        .header("Session-Token", listRequest.getSessionToken())
                        .body(
                                new MapUtil.Builder<String, String>()
                                        .entry("kktRegNumber", listRequest.getKktRegNumber())
                                        .map()
                        )
        );
    }
}
