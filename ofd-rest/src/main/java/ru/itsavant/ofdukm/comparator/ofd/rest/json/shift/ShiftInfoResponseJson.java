package ru.itsavant.ofdukm.comparator.ofd.rest.json.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftInfoResponseJson {

    private String reportDate;
    private ShiftInfoDetailJson shift;
}
