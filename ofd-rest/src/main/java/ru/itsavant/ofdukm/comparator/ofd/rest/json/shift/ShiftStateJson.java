package ru.itsavant.ofdukm.comparator.ofd.rest.json.shift;

public enum ShiftStateJson {

    OPEN("Open"),
    CLOSE("Close");

    private String value;

    ShiftStateJson(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
