package ru.itsavant.ofdukm.comparator.ofd.model.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentListResponse implements OfdResponse<DocumentListItem> {

    private String reportDate;
    private Counts counts;
    private List<DocumentListItem> records;
}
