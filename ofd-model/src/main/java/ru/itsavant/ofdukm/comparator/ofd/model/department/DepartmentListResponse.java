package ru.itsavant.ofdukm.comparator.ofd.model.department;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class DepartmentListResponse implements OfdResponse<DepartmentListItem> {

    private String responseDate;
    private Counts counts;
    private List<DepartmentListItem> records;
}
