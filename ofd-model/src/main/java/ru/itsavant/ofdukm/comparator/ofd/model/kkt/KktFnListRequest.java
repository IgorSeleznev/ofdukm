package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class KktFnListRequest {

    private String sessionToken;
    private String kktRegNumber;
}
