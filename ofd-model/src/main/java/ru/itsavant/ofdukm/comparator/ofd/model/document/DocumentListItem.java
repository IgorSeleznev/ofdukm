package ru.itsavant.ofdukm.comparator.ofd.model.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentListItem {

    private String fnFactoryNumber;
    private int shiftNumber;
    private String documentType;
    private String dateTime;
    private int fdNumber;

    private int numberInShift;
    private String fpd;
    private String cashier;
    private String taxationSystem;
    private String accountingType;

    private int sum;
    private int cash;
    private int electronic;
    private int nondsSum;
    private int nds0Sum;

    private int nds10;
    private int nds18;
    private int nds20;
    private int ndsC10;
    private int ndsC18;

    private int ndsC20;
}
