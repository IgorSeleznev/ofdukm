package ru.itsavant.ofdukm.comparator.ofd.model.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftListItem {

    private String fnFactoryNumber;
    private int shiftNumber;
    private String openDateTime;
    private String closeDateTime;
    private int receiptCount;
    private boolean tag1188 = false;
}
