package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class KktCashdesk {

    private String name;
    private String kktRegNumber;
    private String kktFactoryNumber;
    private String kktModelName;
    private String fnFactoryNumber;

    private String fnRegDateTime;
    private String fnDuration;
    private KktShiftStatus shiftStatus;
    private CashdeskState cashdeskState;
    private String cashdeskEndDateTime;

    private FnState fnState;
    private String fnEndDateTime;
    private LastDocumentState lastDocumentState;
    private String lastDocumentDateTime;
    private KktInfoOutlet outlet;
    private KktCashdeskPartner partner;
}
