package ru.itsavant.ofdukm.comparator.ofd.model.outlet;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class OutletListRequest {

    private String sessionToken;
    private String departmentId;
    private int pageNumber;
    private int pageSize;
}
