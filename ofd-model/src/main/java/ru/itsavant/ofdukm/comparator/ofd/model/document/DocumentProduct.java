package ru.itsavant.ofdukm.comparator.ofd.model.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class DocumentProduct {

    private List<DocumentProperty> properties;
}
