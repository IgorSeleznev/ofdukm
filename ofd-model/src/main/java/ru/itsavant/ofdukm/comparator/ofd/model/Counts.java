package ru.itsavant.ofdukm.comparator.ofd.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Counts {

    private int recordCount;
    private int recordFilteredCount;
    private int recordInResponceCount;
}
