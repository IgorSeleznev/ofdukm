package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class KktListResponse implements OfdResponse<KktListItem> {

    private String reportDate;
    private Counts counts;
    private KktOutletListItemResponse outlet;
    private List<KktListItem> records;
}
