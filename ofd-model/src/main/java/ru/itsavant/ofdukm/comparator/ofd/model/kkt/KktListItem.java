package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.ProblemIndicator;

@Getter
@Setter
@Accessors(chain = true)
public class KktListItem {

    private String name;
    private String kktRegNumber;
    private String kktFactoryNumber;
    private String fnFactoryNumber;
    private CashdeskState cashdeskState;

    private ProblemIndicator problemIndicator;
}
