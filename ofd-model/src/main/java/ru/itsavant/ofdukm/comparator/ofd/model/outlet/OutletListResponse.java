package ru.itsavant.ofdukm.comparator.ofd.model.outlet;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class OutletListResponse implements OfdResponse<OutletListItem> {

    private String reportDate;
    private Counts counts;
    private List<OutletListItem> records;
}
