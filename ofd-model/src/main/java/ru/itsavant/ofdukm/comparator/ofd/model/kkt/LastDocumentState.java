package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

public enum LastDocumentState {

    OK("OK"),
    WARNING("Warning"),
    PROBLEM("Problem");

    private String value;

    LastDocumentState(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
