package ru.itsavant.ofdukm.comparator.ofd.model.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import static ru.itsavant.ofdukm.comparator.ofd.model.constants.OfdDocumentPropertyKeyConstants.OFD_DOCUMENT_ITEM_KEY;
import static ru.itsavant.ofdukm.comparator.ofd.model.constants.OfdDocumentPropertyKeyConstants.OFD_DOCUMENT_ITEM_NAME_KEY;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class DocumentProperty {

    private String fn;
    private String fd;
    private String itemName;
    private int numberOfNestedItemInDocument;
    private String code;
    private Object value;

    public static DocumentProperty property(final DocumentProperty property, final String newCode, final Object newValue) {
        return new DocumentProperty()
                .fn(property.fn())
                .fd(property.fd())
                .numberOfNestedItemInDocument(property.numberOfNestedItemInDocument)
                .itemName(property.itemName)
                .code(newCode)
                .value(newValue);
    }

    public static DocumentProperty property(final DocumentProperty property, final Object newValue) {
        return new DocumentProperty()
                .fn(property.fn())
                .fd(property.fd())
                .numberOfNestedItemInDocument(property.numberOfNestedItemInDocument)
                .itemName(property.itemName)
                .code(property.code())
                .value(newValue);
    }

    public static DocumentProperty stringify(final DocumentProperty property) {
        return new DocumentProperty()
                .fn(property.fn())
                .fd(property.fd())
                .numberOfNestedItemInDocument(property.numberOfNestedItemInDocument)
                .itemName(property.itemName)
                .code(property.code())
                .value(String.valueOf(property.value));
    }

    public DocumentProperty itemName(final String key, final Object value) {
        if (OFD_DOCUMENT_ITEM_NAME_KEY.equals(key)
                || (OFD_DOCUMENT_ITEM_KEY + "." + OFD_DOCUMENT_ITEM_NAME_KEY).equals(key)) {
            this.itemName = String.valueOf(value);
        }

        return this;
    }
}
