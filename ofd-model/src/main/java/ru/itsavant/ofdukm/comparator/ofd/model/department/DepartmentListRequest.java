package ru.itsavant.ofdukm.comparator.ofd.model.department;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DepartmentListRequest {

    private String sessionToken;
    private int pageNumber;
    private int pageSize;
}
