package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class KktListRequest {

    private String sessionToken;
    private String outletId;
    private int pageNumber;
    private int pageSize;
}
