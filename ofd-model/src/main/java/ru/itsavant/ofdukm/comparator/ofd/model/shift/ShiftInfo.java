package ru.itsavant.ofdukm.comparator.ofd.model.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftInfo {

    private String fnFactoryNumber;
    private int shiftNumber;
    private String openDateTime;
    private int openFdNumber;
    private String closeDateTime;

    private int closeFdNumber;
    private String cashier;
    private ShiftState state;
    private ShiftIncome income;
    private ShiftIncomeReturn incomeReturn;

    private ShiftIncomeCorrection incomeCorrection;
    private ShiftExpenditure expenditure;
    private ShiftExpenditureReturn expenditureReturn;
    private ShiftExpenditureCorrection expenditureCorrection;
    private boolean tag1188 = false;
}
