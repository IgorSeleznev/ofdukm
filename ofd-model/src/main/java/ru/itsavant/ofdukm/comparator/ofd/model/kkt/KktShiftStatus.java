package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

public enum KktShiftStatus {

    OPEN("Open"),
    CLOSE("Close"),
    NODATA("NoDate");

    private String value;

    KktShiftStatus(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
