package ru.itsavant.ofdukm.comparator.ofd.model.department;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.ProblemIndicator;

@Getter
@Setter
@Accessors(chain = true)
public class DepartmentListItem {

    private String id;
    private String name;
    private String code;
    private DepartmentParentListItem parent;
    private ProblemIndicator problemIndicator;
}
