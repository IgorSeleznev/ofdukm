package ru.itsavant.ofdukm.comparator.ofd.model.shift;

public enum ShiftState {

    OPEN("Open"),
    CLOSE("Close");

    private String value;

    ShiftState(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
