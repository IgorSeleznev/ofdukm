package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class KktOutletListItemResponse {

    private String id;
    private String name;
    private String code;
    private String address;
}
