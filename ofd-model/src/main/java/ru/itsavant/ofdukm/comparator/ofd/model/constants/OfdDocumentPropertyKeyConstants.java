package ru.itsavant.ofdukm.comparator.ofd.model.constants;

public interface OfdDocumentPropertyKeyConstants {

    String OFD_DOCUMENT_ITEM_KEY = "1059";
    String OFD_DOCUMENT_ITEM_CODE_KEY = "1162";
    String OFD_DOCUMENT_ITEM_NAME_KEY = "1030";
    String OFD_DOCUMENT_ITEM_PRICE_KEY = "1079";
    String OFD_DOCUMENT_ITEM_QUANTITY_KEY = "1023";
    String OFD_DOCUMENT_ITEM_SUM_KEY = "1043";
}
