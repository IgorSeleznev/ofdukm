package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

public enum CashdeskState {

    ACTIVE("Active"),
    EXPIRES("Expires"),
    EXPIRED("Expired"),
    INACTIVE("Inactive"),
    ACTIVATION("Activation"),
    DEACTIVATION("Deactivation"),
    FNCHANGE("FNChange"),
    FNSREGISTRATION("FNSRegistration"),
    FNSREGISTRATIONERROR("FNSRegistrationError");

    private String value;

    CashdeskState(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
