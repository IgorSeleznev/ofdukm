package ru.itsavant.ofdukm.comparator.ofd.model.document;

public enum DocumentAccountingType {

    INCOME("Income", "0"),
    INCOMERETURN("IncomeReturn", "4"),
    EXPENDITURE("Expenditure", ""),
    EXPENDITURERETURN("ExpenditureReturn", "");

    private String value;
    private String ukmType;

    DocumentAccountingType(final String value, final String ukmType) {
        this.value = value;
        this.ukmType = ukmType;
    }

    @Override
    public String toString() {
        return value;
    }

    public String ukmType() {
        return ukmType;
    }

    public static boolean isValue(final String value) {
        return (
                INCOME.value.equals(value)
                        || INCOMERETURN.value.equals(value)
                        || EXPENDITURE.value.equals(value)
                        || EXPENDITURERETURN.value.equals(value)
        );
    }
}
