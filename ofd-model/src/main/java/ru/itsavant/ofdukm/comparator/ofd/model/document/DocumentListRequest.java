package ru.itsavant.ofdukm.comparator.ofd.model.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentListRequest {

    private String sessionToken;
    private String fn;
    private String shift;
    private int pageNumber;
    private int pageSize;
}
