package ru.itsavant.ofdukm.comparator.ofd.model.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftListResponse implements OfdResponse<ShiftListItem> {

    private String reportDate;
    private Counts counts;
    private List<ShiftListItem> records;
}
