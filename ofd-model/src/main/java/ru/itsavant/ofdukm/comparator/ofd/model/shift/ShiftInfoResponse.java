package ru.itsavant.ofdukm.comparator.ofd.model.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftInfoResponse {

    private String reportDate;
    private ShiftInfo shift;
}
