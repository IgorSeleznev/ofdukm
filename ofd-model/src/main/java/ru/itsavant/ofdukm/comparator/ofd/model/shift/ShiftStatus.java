package ru.itsavant.ofdukm.comparator.ofd.model.shift;

public enum ShiftStatus {

    OPEN("Open"),
    CLOSE("Close"),
    NODATA("NoDate");

    private String value;

    ShiftStatus(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
