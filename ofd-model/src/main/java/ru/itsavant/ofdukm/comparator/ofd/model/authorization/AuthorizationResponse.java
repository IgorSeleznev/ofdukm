package ru.itsavant.ofdukm.comparator.ofd.model.authorization;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class AuthorizationResponse {

    private String sessionToken;
}
