package ru.itsavant.ofdukm.comparator.ofd.model;

public enum ProblemIndicator {

    OK("OK"),
    WARNING("Warning"),
    PROBLEM("Problem");

    private String value;

    ProblemIndicator(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
