package ru.itsavant.ofdukm.comparator.ofd.model.document;

public enum TaxationSystem {

    OSN("OSN"),
    USNINCOME("USNIncome"),
    USNIncome_Expenditure("USNIncomeExpenditure"),
    ENVD("ENVD"),
    ESN("ESN"),
    PATENT("Patent");

    private String value;

    TaxationSystem(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
