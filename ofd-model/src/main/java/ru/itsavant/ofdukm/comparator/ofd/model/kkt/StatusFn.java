package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

public enum StatusFn {

    ACTIVE("Active"),
    NOTACTIVE("NotActive");

    private String value;

    StatusFn(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
