package ru.itsavant.ofdukm.comparator.ofd.model.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentCombinedResponse {

    private Counts counts;
    private List<DocumentCombined> records;
}
