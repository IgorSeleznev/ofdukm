package ru.itsavant.ofdukm.comparator.ofd.model.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftListRequest {

    private String sessionToken;
    private String fn;
    private String begin;
    private String end;
    private int pageNumber;
    private int pageSize;
}
