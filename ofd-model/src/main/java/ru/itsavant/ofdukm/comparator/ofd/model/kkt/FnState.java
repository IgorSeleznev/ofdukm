package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

public enum FnState {

    ACTIVE("Active"),
    EXPIRES("Expires"),
    EXPIRED("Expired");

    private String value;

    FnState(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
