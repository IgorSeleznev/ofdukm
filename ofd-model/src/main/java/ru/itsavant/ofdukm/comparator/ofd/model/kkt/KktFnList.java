package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class KktFnList implements OfdResponse<KktFn> {

    private String reportDate;
    private Counts counts;
    private List<KktFn> records;
}
