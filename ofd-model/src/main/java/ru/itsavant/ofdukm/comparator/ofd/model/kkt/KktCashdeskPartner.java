package ru.itsavant.ofdukm.comparator.ofd.model.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class KktCashdeskPartner {

    private String name;
    private String inn;
    private KktCashdeskPartnerContact contact;
}
