package ru.itsavant.ofdukm.comparator.ofd.model.document;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentCombined {

    private DocumentListItem head;
    private List<DocumentProperty> property;
    private List<DocumentProduct> products;
}
