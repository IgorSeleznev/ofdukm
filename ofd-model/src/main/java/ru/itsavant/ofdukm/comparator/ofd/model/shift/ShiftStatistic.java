package ru.itsavant.ofdukm.comparator.ofd.model.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktInfo;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftStatistic {

    private ShiftListItem shiftHead;
    private ShiftInfo shiftInfo;
    private int receiptCount;
    private KktInfo kkt;
    private long amount;
    private long loginId;
    private long ukmShiftId;
}
