package ru.itsavant.ofdukm.comparator.ofd.model.response;

import ru.itsavant.ofdukm.comparator.ofd.model.Counts;

import java.util.List;

public interface OfdResponse<R> {

    Counts getCounts();
    List<R> getRecords();
}
