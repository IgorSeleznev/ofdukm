package ru.itsavant.ofdukm.comparator.ofd.model.outlet;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.ProblemIndicator;

@Getter
@Setter
@Accessors(chain = true)
public class OutletListItem {

    private String id;
    private String name;
    private String code;
    private String address;
    private ProblemIndicator problemIndicator;

    private OutletDepartmentListItemResponse department;
}
