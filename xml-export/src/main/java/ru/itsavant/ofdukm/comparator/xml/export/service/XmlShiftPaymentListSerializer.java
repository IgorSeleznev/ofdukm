package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShift;

public class XmlShiftPaymentListSerializer {

    private final XmlShiftPaymentSerializer shiftPaymentSerializer = new XmlShiftPaymentSerializer();

    public String serialize(final XmlShift shift) {
        final StringBuilder builder = new StringBuilder();

        shift.getPayment().forEach(
                payment -> builder.append(
                        shiftPaymentSerializer.serialize(payment)
                ).append("\r\n")
        );

        return builder.toString();
    }
}
