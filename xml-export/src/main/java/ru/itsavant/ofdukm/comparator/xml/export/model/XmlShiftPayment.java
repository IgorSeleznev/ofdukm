package ru.itsavant.ofdukm.comparator.xml.export.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class XmlShiftPayment {

    private String paymentId;
    private String paymentName;
    private String paymentAmountSale;
    private String paymentAmountReturn;

    @JacksonXmlProperty(localName = "kkm_paymentAmountSale")
    private String kkmPaymentAmountSale;

    @JacksonXmlProperty(localName = "kkm_paymentAmountReturn")
    private String kkmPaymentAmountReturn;
}
