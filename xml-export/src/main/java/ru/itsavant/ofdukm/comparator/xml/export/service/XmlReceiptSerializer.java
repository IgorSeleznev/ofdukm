package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceipt;

public class XmlReceiptSerializer {

    private final XmlReceiptItemListSerializer itemListSerializer = new XmlReceiptItemListSerializer();
    private final XmlReceiptPaymentListSerializer paymentListSerializer = new XmlReceiptPaymentListSerializer();

    public String serialize(final XmlReceipt receipt) {
        return String.format(
                "    <receipt storeId=\"%s\" posNum=\"%s\" shiftNum=\"%s\" receiptNum=\"%s\">\r\n",
                receipt.getStoreId(),
                receipt.getPosNum(),
                receipt.getShiftNum(),
                receipt.getReceiptNum()
        ) +
                String.format("        <receiptDateTime>%s</receiptDateTime>\r\n", receipt.getReceiptDateTime()) +
                String.format("        <receiptOpenDateTime>%s</receiptOpenDateTime>\r\n", receipt.getReceiptOpenDateTime()) +
                String.format("        <receiptSubtotalDateTime>%s</receiptSubtotalDateTime>\r\n", receipt.getReceiptSubtotalDateTime()) +
                String.format("        <cashierName>%s</cashierName>\r\n", receipt.getCashierName()) +
                String.format("        <cashierCode>%s</cashierCode>\r\n", receipt.getCashierCode()) +
                String.format("        <type>%s</type>\r\n", receipt.getType()) +
                String.format("        <amount>%s</amount>\r\n", receipt.getAmount()) +
                itemListSerializer.serialize(receipt) +
                paymentListSerializer.serialize(receipt) +
                "    </receipt>\r\n";
    }
}