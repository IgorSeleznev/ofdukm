package ru.itsavant.ofdukm.comparator.xml.export.converter;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.common.numeric.Rubbles;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentAccountingType;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfo;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShift;
import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShiftPayment;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static ru.itsavant.ofdukm.comparator.common.counter.Counter.basedByZero;
import static ru.itsavant.ofdukm.comparator.common.numeric.Rubbles.kopeeks;
import static ru.itsavant.ofdukm.comparator.common.numeric.Rubbles.rubbles;
import static ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentAccountingType.INCOME;
import static ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentAccountingType.INCOMERETURN;
import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASH;
import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASHLESS;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;


@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class XmlShiftBuilder {

    private UkmPos ukmPos;
    private ShiftInfo shiftInfo;
    private List<OfdReceiptStatistic> receipts;
    private KktCombined kkt;
    private String ukmShiftId;
    private String cashierCode;

    public XmlShift build() {
        final Rubbles saleAmount = kopeeks(0);
        final Rubbles returnAmount = kopeeks(0);

        final Counter saleCount = basedByZero();
        final Counter returnCount = basedByZero();

        final Rubbles cashSaleAmount = kopeeks(0);
        final Rubbles cashlessSaleAmount = kopeeks(0);

        final Rubbles cashReturnAmount = kopeeks(0);
        final Rubbles cashlessReturnAmount = kopeeks(0);

        receipts.forEach(
                receipt -> {
                    if ("Income".equals(receipt.getHeader().getType())) {
                        saleAmount.add(rubbles(receipt.getHeader().getAmount()));
                        saleCount.next();
//                        if (rubbles(defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")).compareTo(0) != 0) {
                        if (rubbles(defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")).compareTo(0) != 0) {
                            cashlessSaleAmount.add(
                                    rubbles(
                                            defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")
                                    )
                            );
                        }
                        if (rubbles(defaultIfNull(receipt.getHeader().getAmount(), "0")).compareTo(0) != 0) {
                            cashSaleAmount.add(
                                    rubbles(
                                            receipt.getHeader().getAmount()
                                    ).subtract(
                                            rubbles(
                                                    defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")
                                            )
                                    )
                            );
                        }
                    }
                    if ("IncomeReturn".equals(receipt.getHeader().getType())) {
                        returnAmount.add(rubbles(receipt.getHeader().getAmount()));
                        returnCount.next();
//                        if (rubbles(defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")).compareTo(0) != 0) {
                        if (rubbles(defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")).compareTo(0) != 0) {
//                            cashlessReturnAmount.add(rubbles(receipt.getHeader().getCashlessAmount()));
                            cashlessReturnAmount.add(
                                    rubbles(
                                            defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")
                                    )
                            );
                        }
//                        } else {
                        if (rubbles(defaultIfNull(receipt.getHeader().getAmount(), "0")).compareTo(0) != 0) {
//                            cashReturnAmount.add(rubbles(receipt.getHeader().getAmount()));
                            cashReturnAmount.add(
                                    rubbles(
                                            receipt.getHeader().getAmount()
                                    ).subtract(
                                            rubbles(
                                                    defaultIfNull(receipt.getHeader().getCashlessAmount(), "0")
                                            )
                                    )
                            );
                        }
//                        }
                    }
                }
        );

        final Counter receiptNum = new Counter(1);

        final XmlReceiptConverter receiptConverter = new XmlReceiptConverter(cashierCode);

        return new XmlShift()
                .setKkmShiftNumber(String.valueOf(shiftInfo.getShiftNumber()))
                .setShiftNum(ukmContext().exportShiftId())
                .setCashierCode(cashierCode)
                .setCashierName(shiftInfo.getCashier())
                .setDateClose(shiftInfo.getCloseDateTime())
                .setKkmInsertion("0")
                .setKkmModelName(kkt.getInfo().getCashdesk().getKktModelName())
                .setKkmRegistrationNumber(kkt.getInfo().getCashdesk().getKktRegNumber())
                .setKkmReturnAmount(returnAmount.toString())
                .setKkmReturnCount(String.valueOf(returnCount.current()))
                .setKkmSaleAmount(saleAmount.toString())
                .setKkmSaleCount(String.valueOf(saleCount.current()))
                .setKkmSerialNumber(kkt.getInfo().getCashdesk().getKktFactoryNumber())
                .setKkmWithdrawal((saleAmount.value().subtract(returnAmount.value())).toString())
                .setPayment(
                        newArrayList(
                                new XmlShiftPayment()
                                        .setKkmPaymentAmountReturn(cashReturnAmount.toString())
                                        .setKkmPaymentAmountSale(cashSaleAmount.toString())
                                        .setPaymentAmountReturn(cashReturnAmount.toString())
                                        .setPaymentAmountSale(cashSaleAmount.toString())
                                        .setPaymentId(String.valueOf(ukmContext().payments().get(CASH).getId()))
                                        .setPaymentName(ukmContext().payments().get(CASH).getName()),
                                new XmlShiftPayment()
                                        .setKkmPaymentAmountReturn(cashlessReturnAmount.toString())
                                        .setKkmPaymentAmountSale(cashlessSaleAmount.toString())
                                        .setPaymentAmountReturn(cashlessReturnAmount.toString())
                                        .setPaymentAmountSale(cashlessSaleAmount.toString())
                                        .setPaymentId(String.valueOf(ukmContext().payments().get(CASHLESS).getId()))
                                        .setPaymentName(ukmContext().payments().get(CASHLESS).getName())
                        )
                )
                .setPosNum(ukmPos.getNumber())
                .setReturnAmount(returnAmount.toString())
                .setReturnCount(String.valueOf(returnCount.current()))
                .setSaleAmount(saleAmount.toString())
                .setSaleCount(String.valueOf(saleCount.current()))
                .setStoreId(
                        ukmContext().storePlaceId()
                )
                .setVersion("0")
                .setReceipt(
                        receipts
                                .stream()
                                .map(receiptConverter::convert)
                                .filter(
                                        receipt ->
                                                INCOME.ukmType().equals(receipt.getType())
                                                        || INCOMERETURN.ukmType().equals(receipt.getType())
                                )
                                .map(receipt -> receipt.setReceiptNum(String.valueOf(receiptNum.currentAndNext())))
                                .collect(Collectors.toList())
                );
    }
}
