package ru.itsavant.ofdukm.comparator.xml.export.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class XmlReceipt {

    @JacksonXmlProperty(isAttribute = true)
    private String storeId;

    @JacksonXmlProperty(isAttribute = true)
    private String posNum;

    @JacksonXmlProperty(isAttribute = true)
    private String shiftNum;

    @JacksonXmlProperty(isAttribute = true)
    private String receiptNum;

    private String receiptDateTime;
    private String receiptOpenDateTime;
    private String receiptSubtotalDateTime;
    private String cashierName;
    private String cashierCode;

    private String type;

    private String shiftNumSale;
    @JacksonXmlProperty(localName = "POSsale")
    private String posSale;
    private String receiptNumSale;

    private String amount;

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<XmlReceiptItem> item;

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<XmlReceiptPayment> payment;
}
