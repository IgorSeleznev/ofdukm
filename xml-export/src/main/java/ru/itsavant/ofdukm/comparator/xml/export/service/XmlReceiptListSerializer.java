package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShift;

public class XmlReceiptListSerializer {

    private final XmlReceiptSerializer receiptSerializer = new XmlReceiptSerializer();

    public String serialize(final XmlShift shift) {
        final StringBuilder builder = new StringBuilder();

        shift.getReceipt().forEach(
                receipt -> builder.append(
                        receiptSerializer.serialize(receipt)
                ).append("\r\n")
        );

        return builder.toString();
    }
}
