package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceipt;

public class XmlReceiptPaymentListSerializer {

    private final XmlReceiptPaymentSerializer paymentSerializer = new XmlReceiptPaymentSerializer();

    public String serialize(final XmlReceipt receipt) {

            final StringBuilder builder = new StringBuilder();

            receipt.getPayment().forEach(
                    payment -> builder.append(
                            paymentSerializer.serialize(payment)
                    ).append("\r\n")
            );

            return builder.toString();
    }
}
