package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceiptItem;

public class XmlReceiptItemSerializer {

    public String serialize(final XmlReceiptItem item) {
        return "        <item>\r\n" +
                String.format("            <position>%s</position>\r\n", item.getPosition()) +
                String.format("            <article>%s</article>\r\n", item.getArticle()) +
                String.format("            <quantity>%s</quantity>\r\n", item.getQuantity()) +
                String.format("            <price>%s</price>\r\n", item.getPrice()) +
                String.format("            <total>%s</total>\r\n", item.getTotal()) +
                String.format("            <stockId>%s</stockId>\r\n", item.getStockId()) +
                String.format("            <enterType>%s</enterType>\r\n", item.getEnterType()) +
                "        </item>\r\n";
    }
}
