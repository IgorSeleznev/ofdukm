package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShiftPayment;

public class XmlShiftPaymentSerializer {

    public String serialize(final XmlShiftPayment payment) {

        return "    <payment>\r\n" +
                String.format("        <paymentId>%s</paymentId>\r\n", payment.getPaymentId()) +
                String.format("        <paymentName>%s</paymentName>\r\n", payment.getPaymentName()) +
                String.format("        <paymentAmountSale>%s</paymentAmountSale>\r\n", payment.getPaymentAmountSale()) +
                String.format("        <paymentAmountReturn>%s</paymentAmountReturn>\r\n", payment.getPaymentAmountReturn()) +
                String.format("        <kkm_paymentAmountSale>%s</kkm_paymentAmountSale>\r\n", payment.getKkmPaymentAmountSale()) +
                String.format("        <kkm_paymentAmountReturn>%s</kkm_paymentAmountReturn>\r\n", payment.getKkmPaymentAmountReturn()) +
                "    </payment>\r\n";
    }
}
