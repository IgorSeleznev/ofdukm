package ru.itsavant.ofdukm.comparator.xml.export.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class XmlReceiptPayment {

    private String paymentId;
    private String paymentName;
    private String paymentAmount;
}
