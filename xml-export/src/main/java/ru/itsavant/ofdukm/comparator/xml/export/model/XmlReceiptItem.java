package ru.itsavant.ofdukm.comparator.xml.export.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class XmlReceiptItem {

    private String position;
    private String article;
    private String quantity;
    private String price;
    private String total;

    private String stockId;
    private String enterType;
}
