package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceiptPayment;

public class XmlReceiptPaymentSerializer {

    public String serialize(final XmlReceiptPayment payment) {
        return "        <payment>\r\n" +
                String.format("            <paymentId>%s</paymentId>\r\n", payment.getPaymentId()) +
                String.format("            <paymentName>%s</paymentName>\r\n", payment.getPaymentName()) +
                String.format("            <paymentAmount>%s</paymentAmount>\r\n", payment.getPaymentAmount()) +
                "        </payment>\r\n";
    }
}
