package ru.itsavant.ofdukm.comparator.xml.export.converter;

import ru.itsavant.ofdukm.comparator.common.numeric.Rubbles;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPayment;
import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceiptPayment;

import java.math.BigDecimal;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.common.numeric.Rubbles.rubbles;
import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASH;
import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASHLESS;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class XmlReceiptPaymentBuilder {

    public List<XmlReceiptPayment> build(final OfdReceiptStatistic receipt) {
        final List<XmlReceiptPayment> result = newArrayList();
        if (Rubbles.kopeeks(receipt.getHeader().getCashlessAmount()).value().compareTo(BigDecimal.valueOf(0)) != 0) {
            final UkmPayment cashlessPayment = ukmContext().payments().get(CASHLESS);
            result.add(
                    new XmlReceiptPayment()
                            .setPaymentId(String.valueOf(cashlessPayment.getId()))
                            .setPaymentName(cashlessPayment.getName())
                            .setPaymentAmount(receipt.getHeader().getCashlessAmount())
            );
        }
        if (rubbles(receipt.getHeader().getAmount()).subtract(receipt.getHeader().getCashlessAmount()).compareTo("0") > 0) {
            final UkmPayment cashPayment = ukmContext().payments().get(CASH);
            result.add(
                    new XmlReceiptPayment()
                            .setPaymentId(String.valueOf(cashPayment.getId()))
                            .setPaymentName(cashPayment.getName())
                            .setPaymentAmount(
                                    rubbles(receipt.getHeader().getAmount())
                                            .subtract(receipt.getHeader().getCashlessAmount())
                                            .toString()
                            )
            );
        }
        return result;
    }
}
