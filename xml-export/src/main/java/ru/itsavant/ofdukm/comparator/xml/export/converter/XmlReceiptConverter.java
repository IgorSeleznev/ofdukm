package ru.itsavant.ofdukm.comparator.xml.export.converter;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentAccountingType;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceipt;

import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.common.numeric.Rubbles.rubbles;
import static ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentAccountingType.INCOMERETURN;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class XmlReceiptConverter implements Converter<OfdReceiptStatistic, XmlReceipt> {

    private XmlReceiptItemConverter itemConverter = new XmlReceiptItemConverter();
    private XmlReceiptPaymentBuilder paymentBuilder = new XmlReceiptPaymentBuilder();
    private XmlReceiptTypeResolver typeResolver = new XmlReceiptTypeResolver();

    private final String cashierCode;

    public XmlReceiptConverter(final String cashierCode) {
        this.cashierCode = cashierCode;
    }

    @Override
    public XmlReceipt convert(final OfdReceiptStatistic source) {
        final String receiptType = typeResolver.resolve(source.getHeader().getType());
        String amount = source.getHeader().getAmount();
//        if ("1".equals(receiptType) && !rubbles(amount).equalsTo(0)) {
//            amount = rubbles(amount).multiply(-1).toString();
//        }
        final XmlReceipt xmlReceipt = new XmlReceipt()
                .setStoreId(ukmContext().storePlaceId())
                .setPosNum(source.getUkmPos().getNumber())
                .setShiftNum(String.valueOf(ukmContext().exportShiftId()))
                .setReceiptNum(String.valueOf(source.getHeader().getNumberInShift()))
                .setReceiptDateTime(source.getHeader().getReceiptDate())

                .setReceiptOpenDateTime(source.getHeader().getReceiptDate())
                .setReceiptSubtotalDateTime(source.getHeader().getReceiptDate())
                .setCashierName(source.getHeader().getCashier())
                .setCashierCode(cashierCode)
                .setType(receiptType)
                .setAmount(amount)
                .setItem(
                        source.getItems().stream().map(
                                item -> itemConverter.convert(item)
                        ).collect(Collectors.toList())
                )
                .setPayment(
                        newArrayList(
                                paymentBuilder.build(source)
                        )
                );

        if (INCOMERETURN.ukmType().equals(receiptType)) {
            xmlReceipt.setPosSale(xmlReceipt.getPosNum());
            xmlReceipt.setShiftNumSale(xmlReceipt.getShiftNum());
        }

        return xmlReceipt;
    }
}
