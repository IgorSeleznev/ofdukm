package ru.itsavant.ofdukm.comparator.xml.export.converter;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;
import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceiptItem;

import static ru.itsavant.ofdukm.comparator.common.numeric.Rubbles.rubbles;

public class XmlReceiptItemConverter implements Converter<OfdReceiptItem, XmlReceiptItem> {

    @Override
    public XmlReceiptItem convert(final OfdReceiptItem source) {
        return new XmlReceiptItem()
                .setArticle(source.getItemCode())
                .setEnterType("2")
                .setPosition(String.valueOf(source.getNumberInDocument() + 1))
                .setPrice(source.getPrice())
                .setQuantity(source.getQuantity())
                .setStockId("0")
                .setTotal(source.getAmount());
    }
}
