package ru.itsavant.ofdukm.comparator.xml.export.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@JacksonXmlRootElement(localName = "shift")
public class XmlShift {

    @JacksonXmlProperty(isAttribute = true)
    private String storeId;

    @JacksonXmlProperty(isAttribute = true)
    private String posNum;

    @JacksonXmlProperty(isAttribute = true)
    private String shiftNum;

    private String version;
    private String dateClose;

    @JacksonXmlProperty(localName = "kkm_shift_number")
    private String kkmShiftNumber;

    @JacksonXmlProperty(localName = "kkm_serial_number")
    private String kkmSerialNumber;

    @JacksonXmlProperty(localName = "kkm_registration_number")
    private String kkmRegistrationNumber;

    @JacksonXmlProperty(localName = "kkm_model_name")
    private String kkmModelName;

    private String cashierName;
    private String cashierCode;
    private String saleAmount;
    private String returnAmount;

    private String saleCount;
    private String returnCount;

    @JacksonXmlProperty(localName = "kkm_saleAmount")
    private String kkmSaleAmount;

    @JacksonXmlProperty(localName = "kkm_returnAmount")
    private String kkmReturnAmount;

    @JacksonXmlProperty(localName = "kkm_saleCount")
    private String kkmSaleCount;

    @JacksonXmlProperty(localName = "kkm_returnCount")
    private String kkmReturnCount;

    @JacksonXmlProperty(localName = "kkm_withdrawal")
    private String kkmWithdrawal;

    @JacksonXmlProperty(localName = "kkm_insertion")
    private String kkmInsertion;

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<XmlShiftPayment> payment;

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<XmlReceipt> receipt;
}
