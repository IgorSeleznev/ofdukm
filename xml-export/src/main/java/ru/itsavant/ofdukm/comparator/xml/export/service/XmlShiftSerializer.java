package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShift;

public class XmlShiftSerializer {

    private final XmlShiftPaymentListSerializer paymentListSerializer = new XmlShiftPaymentListSerializer();
    private final XmlReceiptListSerializer receiptListSerializer = new XmlReceiptListSerializer();

    public String serialize(final XmlShift xmlShift) {

        return String.format("<shift storeId=\"%s\" posNum=\"%s\" shiftNum=\"%s\">\r\n", xmlShift.getStoreId(), xmlShift.getPosNum(), xmlShift.getShiftNum()) +
                String.format("    <version>%s</version>\r\n", xmlShift.getVersion()) +
                String.format("    <dateClose>%s</dateClose>\r\n", xmlShift.getDateClose()) +
                String.format("    <cashierName>%s</cashierName>\r\n", xmlShift.getCashierName()) +
                String.format("    <cashierCode>%s</cashierCode>\r\n", xmlShift.getCashierCode()) +
                String.format("    <saleAmount>%s</saleAmount>\r\n", xmlShift.getSaleAmount()) +
                String.format("    <returnAmount>%s</returnAmount>\r\n", xmlShift.getReturnAmount()) +
                String.format("    <saleCount>%s</saleCount>\r\n", xmlShift.getSaleCount()) +
                String.format("    <returnCount>%s</returnCount>\r\n", xmlShift.getReturnCount()) +
                String.format("    <kkm_shift_number>%s</kkm_shift_number>\r\n", xmlShift.getKkmShiftNumber()) +
                String.format("    <kkm_serial_number>%s</kkm_serial_number>\r\n", xmlShift.getKkmSerialNumber()) +
                String.format("    <kkm_registration_number>%s</kkm_registration_number>\r\n", xmlShift.getKkmRegistrationNumber()) +
                String.format("    <kkm_model_name>%s</kkm_model_name>\r\n", xmlShift.getKkmModelName()) +
                String.format("    <kkm_saleAmount>%s</kkm_saleAmount>\r\n", xmlShift.getSaleAmount()) +
                String.format("    <kkm_returnAmount>%s</kkm_returnAmount>\r\n", xmlShift.getReturnAmount()) +
                String.format("    <kkm_saleCount>%s</kkm_saleCount>\r\n", xmlShift.getSaleCount()) +
                String.format("    <kkm_returnCount>%s</kkm_returnCount>\r\n", xmlShift.getReturnCount()) +
                String.format("    <kkm_withdrawal>%s</kkm_withdrawal>\r\n", xmlShift.getKkmWithdrawal()) +
                String.format("    <kkm_insertion>%s</kkm_insertion>\r\n", xmlShift.getKkmInsertion()) +
                paymentListSerializer.serialize(xmlShift) +
                receiptListSerializer.serialize(xmlShift) +
                "</shift>";
    }
}
