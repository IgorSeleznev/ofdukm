package ru.itsavant.ofdukm.comparator.xml.export.service;

import ru.itsavant.ofdukm.comparator.xml.export.model.XmlReceipt;

public class XmlReceiptItemListSerializer {

    private final XmlReceiptItemSerializer itemSerializer = new XmlReceiptItemSerializer();

    public String serialize(final XmlReceipt receipt) {
        final StringBuilder builder = new StringBuilder();

        receipt.getItem().forEach(
                item -> builder.append(
                        itemSerializer.serialize(item)
                ).append("\r\n")
        );

        return builder.toString();
    }
}
