package ru.itsavant.ofdukm.comparator.xml.export.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.util.DefaultXmlPrettyPrinter;
import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShift;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class XmlSaveService {

    private XmlShiftSerializer shiftSerializer = new XmlShiftSerializer();
    private final XmlMapper xmlMapper = new XmlMapper();

    public XmlSaveService() {
        xmlMapper.writer(
                new DefaultPrettyPrinter()
                        .withObjectIndenter(new DefaultIndenter().withLinefeed("\n"))
        );
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public void save(final String path, final XmlShift shift) {
        try {
            xmlMapper.writeValue(new File(path + ".xml"), shift);
            final String xml = shiftSerializer.serialize(shift);
            Files.write(Paths.get(path + ".prettified.xml"), xml.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
