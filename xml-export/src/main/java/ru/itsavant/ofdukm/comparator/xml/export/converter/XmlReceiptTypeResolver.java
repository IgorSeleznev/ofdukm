package ru.itsavant.ofdukm.comparator.xml.export.converter;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentAccountingType;

public class XmlReceiptTypeResolver {

    public String resolve(final String ofdType) {
        if (DocumentAccountingType.isValue(ofdType)) {
            return DocumentAccountingType.valueOf(ofdType.toUpperCase()).ukmType();
        }

        return "";
    }
}
