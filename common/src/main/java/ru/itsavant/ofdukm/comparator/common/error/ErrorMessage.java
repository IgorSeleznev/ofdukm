package ru.itsavant.ofdukm.comparator.common.error;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorMessage {

    private final String title;
    private final String message;
    private final Throwable exception;
}
