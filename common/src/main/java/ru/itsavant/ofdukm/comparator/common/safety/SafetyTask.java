package ru.itsavant.ofdukm.comparator.common.safety;

import org.slf4j.Logger;
import ru.itsavant.ofdukm.comparator.common.function.Applier;
import ru.itsavant.ofdukm.comparator.common.logger.LoggerUtil;
import ru.itsavant.ofdukm.comparator.common.stacktrace.StackTracePrinter;

public class SafetyTask {

    public static void safety(final Applier applier) {
        new SafetyTask().run(applier);
    }

    private final Logger logger;

    public SafetyTask() {
        this.logger = LoggerUtil.createLoggerFor("ru.itsavant.ofdukm.comparator.common.safety.SafetyTask", "ofdukm-comparator.log");
    }

    public void run(final Applier consumer) {
        try {
            consumer.apply();
        } catch (final Throwable throwable) {
            System.out.println(
                    StackTracePrinter.print(throwable)
            );
            logger.error("Ошибка во время безопасного выполнения метода:", throwable);
        }
    }
}
