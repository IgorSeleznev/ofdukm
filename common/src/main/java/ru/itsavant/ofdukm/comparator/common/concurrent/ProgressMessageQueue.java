package ru.itsavant.ofdukm.comparator.common.concurrent;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ProgressMessageQueue {

    private final ConcurrentLinkedQueue<ProgressMessage> queue = new ConcurrentLinkedQueue<>();

    public ProgressMessageQueue push(final ProgressMessage message) {
        queue.offer(message);
        return this;
    }

    public ProgressMessageQueue clear() {
        queue.clear();
        return this;
    }

    public ProgressMessage poll() {
        return queue.poll();
    }

    public ProgressMessage head() {
        return queue.peek();
    }
}
