package ru.itsavant.ofdukm.comparator.common.counter;

public class Counter {

    private int initializationValue = 0;
    private int current = 0;

    public static Counter of(final int value) {
        return new Counter(value);
    }

    public static Counter basedByZero() {
        return of(0);
    }

    public Counter(final int start) {
        this.initializationValue = start;
        this.current = start;
    }

    public int next() {
        this.current++;

        return current;
    }

    public int next(final int step) {
        this.current += step;

        return current;
    }

    public int currentAndNext() {
        final int currentForReturn = this.current;
        this.current++;

        return currentForReturn;
    }

    public int currentAndNext(final int step) {
        final int currentForReturn = this.current;
        this.current += step;

        return currentForReturn;
    }

    public void setValue(final int value) {
        this.current = value;
    }

    public void initialize(final int value) {
        this.initializationValue = value;
        reset();
    }

    public void reset() {
        this.current = initializationValue;
    }

    public int current() {
        return current;
    }
}