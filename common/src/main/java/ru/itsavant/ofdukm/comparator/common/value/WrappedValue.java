package ru.itsavant.ofdukm.comparator.common.value;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

import static ru.itsavant.ofdukm.comparator.common.value.WrappedValueType.*;

public class WrappedValue {

    private WrappedValueType type;
    private Object value;

    public static WrappedValue emptyValue() {
        return new WrappedValue().setEmpty();
    }

    public static WrappedValue stringValue(final String value) {
        return new WrappedValue().setString(value);
    }

    public static WrappedValue integerValue(final Integer value) {
        return new WrappedValue().setInteger(value);
    }

    public static WrappedValue doubleValue(final Double value) {
        return new WrappedValue().setDouble(value);
    }

    public static WrappedValue booleanValue(final Boolean value) {
        return new WrappedValue().setBoolean(value);
    }

    public static WrappedValue listValue(final List<WrappedValue> list) {
        return new WrappedValue().setList(list);
    }

    public static WrappedValue mapValue(final Map<String, WrappedValue> map) {
        return new WrappedValue().setMap(map);
    }

    public WrappedValueType getType() {
        return type;
    }

    public WrappedValue setType(WrappedValueType type) {
        this.type = type;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public WrappedValue setValue(Object value) {
        this.value = value;
        return this;
    }

    public boolean isEmpty() {
        return value == null || NULL.equals(type);
    }

    public String getEmpty() {
        if (NULL.equals(type)) {
            return null;
        }
        return String.valueOf(value);
    }

    public WrappedValue setEmpty() {
        this.type = NULL;
        this.value = null;

        return this;
    }

    public String getString() {
        if (STRING.equals(type)) {
            return (String) value;
        }
        return String.valueOf(value);
    }

    public WrappedValue setString(final String value) {
        this.value = value;
        this.type = STRING;
        return this;
    }

    public Integer getInteger() {
        if (INTEGER.equals(type)) {
            return (Integer) value;
        }
        return Integer.valueOf(String.valueOf(value));
    }

    public WrappedValue setInteger(final Integer value) {
        this.value = value;
        this.type = INTEGER;
        return this;
    }

    public Double getDouble() {
        if (DOUBLE.equals(type)) {
            return (Double) value;
        }
        return Double.valueOf(String.valueOf(value));
    }

    public WrappedValue setDouble(final Double value) {
        this.value = value;
        this.type = DOUBLE;
        return this;
    }

    public Boolean getBoolean() {
        if (BOOLEAN.equals(type)) {
            return (Boolean) value;
        }
        return Boolean.valueOf(String.valueOf(value));
    }

    public WrappedValue setBoolean(final Boolean value) {
        this.value = value;
        this.type = BOOLEAN;
        return this;
    }

    public List<WrappedValue> getList() {
        if (WrappedValueType.LIST.equals(type)) {
            return (List<WrappedValue>) value;
        }
        return ImmutableList.of();
    }

    public WrappedValue setList(final List<WrappedValue> value) {
        this.value = value;
        this.type = LIST;
        return this;
    }

    public Map<String, WrappedValue> getMap() {
        if (WrappedValueType.MAP.equals(type)) {
            return (Map<String, WrappedValue>) value;
        }
        return ImmutableMap.of();
    }

    public WrappedValue setMap(final Map<String, WrappedValue> value) {
        this.value = value;
        this.type = MAP;
        return this;
    }

    @Override
    public String toString() {
        return "WrappedValue{" +
                "type=" + type +
                ", value=" + value +
                '}';
    }
}
