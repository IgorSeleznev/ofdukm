package ru.itsavant.ofdukm.comparator.common.concurrent;

public interface Managable extends Startable, Stoppable {
}
