package ru.itsavant.ofdukm.comparator.common.concurrent;

public interface Stoppable {

    boolean stopped();
    void stop();
}
