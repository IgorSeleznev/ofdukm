package ru.itsavant.ofdukm.comparator.common.context;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessageQueue;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;

import java.util.concurrent.ConcurrentLinkedQueue;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class CommonContext {

    private static final CommonContext CONTEXT = new CommonContext();

    private CommonContext() {

    }

    public static CommonContext commonContext() {
        return CONTEXT;
    }

    private final ProgressMessageQueue messageQueue = new ProgressMessageQueue();
    private final ConcurrentLinkedQueue<ErrorMessage> errorsQueue = new ConcurrentLinkedQueue<>();
    private volatile boolean stopped = false;

    public void stop() {
        this.stopped = true;
    }

    public void init() {
        this.stopped = false;
    }
}
