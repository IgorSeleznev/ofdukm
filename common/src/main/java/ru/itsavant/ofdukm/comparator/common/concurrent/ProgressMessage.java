package ru.itsavant.ofdukm.comparator.common.concurrent;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class ProgressMessage {

    private Class<?> messageSource;
    private String title;
    private int current;
    private int size;
}
