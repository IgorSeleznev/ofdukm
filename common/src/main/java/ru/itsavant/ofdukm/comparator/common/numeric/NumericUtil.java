package ru.itsavant.ofdukm.comparator.common.numeric;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumericUtil {

    public static BigDecimal stringRubbles(final String amount) {
        return BigDecimal.valueOf(Long.valueOf(amount));
    }

    public static BigDecimal roundStringRubbles(final String amount) {
        return roundStringRubbles(amount, 4);
    }

    public static BigDecimal roundStringRubbles(final String amount, final int scale) {
        return BigDecimal.valueOf(Long.valueOf(amount)).divide(BigDecimal.valueOf(100), scale, RoundingMode.HALF_EVEN);
    }
}
