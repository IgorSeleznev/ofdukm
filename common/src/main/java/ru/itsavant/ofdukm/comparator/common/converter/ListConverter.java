package ru.itsavant.ofdukm.comparator.common.converter;

import java.util.List;

public interface ListConverter<TSource, TTarget> extends Converter<List<TSource>, List<TTarget>> {

}
