package ru.itsavant.ofdukm.comparator.common.function;

public interface Applier {

    void apply();
}
