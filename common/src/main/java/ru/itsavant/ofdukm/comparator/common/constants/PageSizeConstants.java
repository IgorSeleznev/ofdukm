package ru.itsavant.ofdukm.comparator.common.constants;

public interface PageSizeConstants {

    int MAX_SMALL_LIST_PAGE_SIZE = 100;
    int MAX_LARGE_LIST_PAGE_SIZE = 1000;

    static int limitTop(final int value, final int range) {
        if (value > range) {
            return range;
        }
        return value;
    }
}
