package ru.itsavant.ofdukm.comparator.common.concurrent;

import static ru.itsavant.ofdukm.comparator.common.context.CommonContext.commonContext;

public class OfdukmConcurrentUtil {

    public ProgressMessageQueue progressMessageQueue() {
        return commonContext().messageQueue();
    }

    public void stop() {
        commonContext().stop();
    }

    public void init() {
        commonContext().init();
    }
}
