package ru.itsavant.ofdukm.comparator.common.value;

public enum WrappedValueType {

    NULL,
    STRING,
    INTEGER,
    DOUBLE,
    BOOLEAN,
    LIST,
    MAP
}
