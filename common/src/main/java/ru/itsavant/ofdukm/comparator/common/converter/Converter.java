package ru.itsavant.ofdukm.comparator.common.converter;

public interface Converter<TSource, TTarget> {

    TTarget convert(final TSource source);
}
