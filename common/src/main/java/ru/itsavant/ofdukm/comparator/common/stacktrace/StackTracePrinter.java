package ru.itsavant.ofdukm.comparator.common.stacktrace;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StackTracePrinter {

    private StackTracePrinter() {
    }

    public static String print(Throwable exception) {
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);

        return stringWriter.toString();
    }
}
