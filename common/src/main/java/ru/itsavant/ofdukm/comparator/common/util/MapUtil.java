package ru.itsavant.ofdukm.comparator.common.util;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

public class MapUtil {

    private MapUtil() {

    }

    public static class Builder<K, V> {

        private Map<K, V> map = newHashMap();

        public Builder<K, V> entry(final K key, final V value) {
            map.put(key, value);
            return this;
        }

        public Builder<K, V> entryIfValueNotNull(final K key, final V value) {
            if (value != null) {
                return entry(key, value);
            }
            return this;
        }

        public Map<K, V> map() {
            return map;
        }

        public Map<K, V> build() {
            return map;
        }
    }
}
