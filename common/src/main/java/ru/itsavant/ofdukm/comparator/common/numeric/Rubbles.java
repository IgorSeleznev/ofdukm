package ru.itsavant.ofdukm.comparator.common.numeric;

import lombok.Getter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Getter
@Accessors(fluent = true)
public class Rubbles {

    private BigDecimal value;

    public static Rubbles kopeeks(final String value) {
        return new Rubbles(defaultIfNull(value, "0"), 2);
    }

    public static Rubbles kopeeks(final int value) {
        return new Rubbles(String.valueOf(value), 2);
    }

    public static Rubbles rubbles(final String value) {
        return new Rubbles(defaultIfNull(value, "0"));
    }

    public static Rubbles rubbles(final int value) {
        return new Rubbles(String.valueOf(value));
    }

    public static Rubbles scale(final String value, final int scale) {
        return new Rubbles(value, scale);
    }

    private Rubbles(final String value, final int scale) {
        this.value = new BigDecimal(defaultIfNull(value, "0")).divide(BigDecimal.valueOf(100), scale, RoundingMode.HALF_EVEN);
    }

    private Rubbles(final String value) {
        if (isNullOrEmpty(value)) {
            this.value = new BigDecimal(0);
        } else {
            this.value = new BigDecimal(defaultIfNull(value, "0"));
        }
    }

    public Rubbles add(final Rubbles addValue) {
        this.value = value.add(addValue.value());
        return this;
    }

    public Rubbles add(final String addValue) {
        this.value = value.add(rubbles(addValue).value());
        return this;
    }

    public Rubbles subtract(final Rubbles substractValue) {
        this.value = value.subtract(substractValue.value());
        return this;
    }

    public Rubbles subtract(final String substractValue) {
        this.value = value.subtract(rubbles(substractValue).value());
        return this;
    }

    public Rubbles divide(final Rubbles divideValue, final int scale) {
        this.value = value.divide(divideValue.value(), scale, BigDecimal.ROUND_HALF_EVEN);
        return this;
    }

    public Rubbles divide(final Rubbles divideValue) {
        this.value = value.divide(divideValue.value(), BigDecimal.ROUND_HALF_EVEN);
        return this;
    }

    public Rubbles multiply(final Rubbles multiplyValue) {
        this.value = value.multiply(multiplyValue.value());
        return this;
    }

    public Rubbles multiply(final int multiplyValue) {
        this.value = multiply(rubbles(multiplyValue)).value();
        return this;
    }

    public int compareTo(final int value) {
        return this.value.compareTo(BigDecimal.valueOf(value));
    }

    public boolean equalsTo(final int value) {
        return this.value.compareTo(BigDecimal.valueOf(value)) == 0;
    }

    public int compareTo(final String value) {
        return this.value.compareTo(new BigDecimal(value));
    }

    public boolean equalsTo(final String value) {
        return this.value.compareTo(new BigDecimal(value)) == 0;
    }

    public int compareTo(final BigDecimal value) {
        return this.value.compareTo(value);
    }

    public boolean equalsTo(final BigDecimal value) {
        return this.value.compareTo(value) == 0;
    }

    public String toString() {
        return value.toPlainString();
    }
}
