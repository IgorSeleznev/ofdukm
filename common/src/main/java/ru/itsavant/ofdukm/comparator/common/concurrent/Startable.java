package ru.itsavant.ofdukm.comparator.common.concurrent;

public interface Startable {

    void setStarted();
}
