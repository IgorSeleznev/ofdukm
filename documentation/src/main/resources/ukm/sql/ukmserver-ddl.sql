create schema if not exists ukmserver collate utf8_general_ci;

create table if not exists auth_bpc_journal
(
  id bigint unsigned auto_increment
    primary key,
  cash_id bigint unsigned default 0 not null,
  equaring_id int default 0 not null,
  op_type int null,
  amount decimal(20,4) default 0.0000 not null,
  cardno varchar(19) null,
  expdate varchar(7) null,
  expdate_formated varchar(20) null,
  authcode varchar(20) null,
  reference varchar(20) null,
  postransnumber varchar(20) null,
  entrymode char null
);

create index i_cash_id_equaring_id
  on auth_bpc_journal (cash_id, equaring_id);

create table if not exists auth_gpb_journal
(
  id bigint unsigned auto_increment
    primary key,
  cash_id bigint unsigned default 0 not null,
  shift_id bigint default 0 not null,
  equaring_id int default 0 not null,
  pos_number varchar(10) null,
  op_type char default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  transaction_time datetime default '0000-00-00 00:00:00' not null,
  auth_code varchar(20) null,
  message_number varchar(20) null,
  settled tinyint(1) default 0 null
);

create table if not exists auth_internal_journal
(
  guid varchar(40) default '' not null
    primary key,
  account_id varchar(40) default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  op_code int(1) default 0 not null,
  deleted int(1) default 0 not null
);

create index account_id
  on auth_internal_journal (account_id, date);

create table if not exists auth_ucs_journal
(
  cash_id bigint unsigned default 0 not null,
  shift_id bigint default 0 not null,
  term_id smallint(6) default 0 not null,
  card_no varchar(20) default '' not null,
  exp_date varchar(4) default '0000' not null,
  amount decimal(20,4) default 0.0000 not null,
  auth_code varchar(10) default '' not null,
  msg_no smallint(6) default 0 not null,
  trn_ts datetime default '0000-00-00 00:00:00' not null,
  trn_type smallint(6) default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, shift_id, trn_ts)
);

create table if not exists bottom_counts
(
  name varchar(50) default '' not null
    primary key,
  count int default 0 not null
);

create table if not exists bottom_tables
(
  name varchar(50) not null,
  from_version int default 0 not null,
  primary key (name, from_version)
);

create table if not exists cashdoc
(
  store_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  number varchar(100) default '' not null,
  date datetime default '0000-00-00 00:00:00' not null,
  user_id varchar(40) default '{00000000-0000-0000-0000-000000000000}' not null,
  blocked tinyint(1) default 0 not null,
  comment varchar(100) null,
  primary key (store_id, id)
);

create index blocked
  on cashdoc (store_id, blocked);

create table if not exists cashdoc_date
(
  store_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  primary key (store_id, id, date)
);

create table if not exists cashdoc_items
(
  store_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  type_sale tinyint(1) default 0 not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  total decimal(20,4) default 0.0000 not null,
  discount decimal(20,4) default 0.0000 not null,
  meanprice decimal(20,4) default 0.0000 not null,
  quantity_return decimal(20,4) default 0.0000 not null,
  total_return decimal(20,4) default 0.0000 not null,
  discount_return decimal(20,4) default 0.0000 not null,
  meanprice_return decimal(20,4) default 0.0000 not null,
  primary key (store_id, id, type_sale, item)
);

create table if not exists cashdoc_number
(
  store_id int default 0 not null
    primary key,
  prefix varchar(40) default '' not null,
  count bigint unsigned null,
  suffix varchar(40) default '' not null,
  date_from datetime null
);

create table if not exists cashdoc_payments
(
  store_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  type_sale tinyint(1) default 0 not null,
  payment int default 0 not null,
  count int(11) unsigned default 0 not null,
  sale decimal(20,4) default 0.0000 not null,
  `return` decimal(20,4) default 0.0000 not null,
  pinsert decimal(20,4) default 0.0000 not null,
  pextract decimal(20,4) default 0.0000 not null,
  primary key (store_id, id, type_sale, payment)
);

create table if not exists cashdoc_shifts
(
  store_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  cash_id int default 0 not null,
  shift_id bigint unsigned default 0 not null,
  primary key (store_id, id, cash_id, shift_id),
  constraint cash_id
    unique (cash_id, shift_id)
);

create table if not exists cashdoc_statistic
(
  store_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  type_sale tinyint(1) default 0 not null,
  sale decimal(20,4) default 0.0000 not null,
  `return` decimal(20,4) default 0.0000 not null,
  cancel decimal(20,4) default 0.0000 not null,
  cancel_return decimal(20,4) default 0.0000 not null,
  discount decimal(20,4) default 0.0000 not null,
  discount_return decimal(20,4) default 0.0000 not null,
  primary key (store_id, id, type_sale)
);

create table if not exists cashdoc_taxes
(
  store_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  type_sale tinyint(1) default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  amount_return decimal(20,4) default 0.0000 not null,
  tax_id int default 0 not null,
  percent varchar(20) default '0' not null,
  primary key (store_id, id, type_sale, tax_id, percent)
);

create table if not exists cnv_casegroup_classif_clients
(
  dcode_dcontr varchar(60) default '' not null
    primary key,
  classif_id int default 0 not null,
  constraint classif_id
    unique (classif_id)
)
  engine=MyISAM;

create table if not exists cnv_casegroup_pricelist
(
  pricelist_id int(11) unsigned not null,
  nomenclature_id int not null,
  casegroup_pricelist varchar(60) not null,
  primary key (pricelist_id, nomenclature_id)
)
  engine=MyISAM;

create index casegroup_pricelist
  on cnv_casegroup_pricelist (casegroup_pricelist);

create table if not exists cnv_crocus_sap_classif_map
(
  sap_id varchar(10) not null
    primary key,
  ukm_id int not null
)
  engine=MyISAM;

create table if not exists cnv_crocus_sap_discount_map
(
  sap_id int not null
    primary key,
  ukm_id int not null
)
  engine=MyISAM;

create index ukm_id
  on cnv_crocus_sap_discount_map (ukm_id);

create table if not exists cnv_exp_account_journal
(
  cnv_id varchar(40) not null,
  record_id int not null,
  cnv_date datetime default '0000-00-00 00:00:00' not null,
  primary key (cnv_id, record_id)
);

create index cnv_date
  on cnv_exp_account_journal (cnv_id, cnv_date);

create table if not exists cnv_exp_clm
(
  cnv_id varchar(40) default '0' not null,
  name varchar(255) default '' not null,
  version int default 0 not null,
  primary key (cnv_id, name)
);

create table if not exists cnv_exp_clm_classif_map
(
  cnv_id varchar(40) default '0' not null,
  ukm_classif varchar(40) default '' not null,
  clm_classif int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cnv_id, ukm_classif)
);

create table if not exists cnv_oebs_operations
(
  oebs_id varchar(20) default '' not null
    primary key,
  ukm_id int default 0 not null
);

create table if not exists cnv_oebs_store_params
(
  store_id int default 0 not null
    primary key,
  cfo varchar(50) default '' not null,
  beznal_not_allowed tinyint(1) default 0 not null
);

create table if not exists cnv_params
(
  name varchar(50) default '' not null
    primary key,
  value tinytext null
);

create table if not exists cnv_pricelist_id_map
(
  nomenclature_id int default 0 not null,
  ukm_pricelist_id int(11) unsigned default 0 not null
    primary key,
  other_pricelist_id int default 0 not null,
  cnv_code varchar(40) null
);

create table if not exists cnv_sm25_classif_price_limit
(
  nomenclature_id int default 0 not null,
  pricelist_id int default 0 not null,
  classif_id varchar(40) default '0' not null,
  percent double default 0 not null,
  primary key (nomenclature_id, pricelist_id, classif_id)
)
  engine=MyISAM;

create table if not exists cnv_sm25_classif_tax
(
  nomenclature_id int default 0 not null,
  classif varchar(40) default '0' not null,
  tax_group int default 0 not null,
  primary key (nomenclature_id, classif)
)
  engine=MyISAM;

create table if not exists cnv_sm25_items_price_limit
(
  nomenclature_id int default 0 not null,
  pricelist_id int default 0 not null,
  item_id varchar(40) default '0' not null,
  percent double default 0 not null,
  primary key (nomenclature_id, pricelist_id, item_id)
)
  engine=MyISAM;

create table if not exists cnv_sm25_items_tax
(
  nomenclature_id int default 0 not null,
  item varchar(40) default '0' not null,
  tax_group int default 0 not null,
  changed tinyint(1) default 0 not null,
  primary key (nomenclature_id, item)
)
  engine=MyISAM;

create table if not exists cnv_sm25m_classif_map
(
  tree varchar(40) default '#' not null
    primary key,
  classif_id int default 0 not null,
  constraint classif_id
    unique (classif_id)
)
  engine=MyISAM;

create table if not exists cnv_smxml_disc_map
(
  type varchar(40) not null,
  sm_price int(11) unsigned default 0 not null,
  disc_id int default 0 not null,
  primary key (type, sm_price)
);

create table if not exists cnv_standart_sap_taxgroup_map
(
  sap_id varchar(10) default '' not null
    primary key,
  ukm_id int not null
)
  engine=MyISAM;

create table if not exists cnv_table2_deleted
(
  mysterious_id bigint unsigned default 0 not null,
  table_name varchar(40) default '' not null,
  fpk1 varchar(40) default '' not null,
  fpk2 varchar(40) default '' not null,
  fpk3 varchar(40) default '' not null,
  fpk4 varchar(40) default '' not null,
  fpk5 varchar(40) default '' not null,
  version bigint unsigned default 0 not null,
  primary key (mysterious_id, table_name, fpk1, fpk2, fpk3, fpk4, fpk5)
);

create index version
  on cnv_table2_deleted (mysterious_id, table_name, version);

create table if not exists cnv_table2_versions
(
  mysterious_id bigint unsigned default 0 not null,
  table_name varchar(40) default '' not null,
  mysterious_type tinyint default 0 not null,
  latest_version bigint unsigned default 0 not null,
  oldest_version bigint unsigned default 0 not null,
  primary key (mysterious_id, table_name)
);

create table if not exists cnv_table_client_versions
(
  client varchar(40) default '' not null,
  mysterious_id bigint unsigned default 0 not null,
  table_name varchar(40) default '' not null,
  latest_version bigint unsigned default 0 not null,
  primary key (client, mysterious_id, table_name)
);

create table if not exists cnv_table_deleted
(
  mysterious_id bigint unsigned default 0 not null,
  table_name varchar(40) default '' not null,
  fpk1 varchar(40) default '' not null,
  fpk2 varchar(40) default '' not null,
  fpk3 varchar(40) default '' not null,
  fpk4 varchar(40) default '' not null,
  fpk5 varchar(40) default '' not null,
  version bigint unsigned default 0 not null,
  primary key (mysterious_id, table_name, fpk1, fpk2, fpk3, fpk4, fpk5)
);

create index version
  on cnv_table_deleted (mysterious_id, table_name, version);

create table if not exists cnv_table_versions
(
  mysterious_id bigint unsigned default 0 not null,
  table_name varchar(40) default '' not null,
  mysterious_type tinyint default 0 not null,
  latest_version bigint unsigned default 0 not null,
  oldest_version bigint unsigned default 0 not null,
  primary key (mysterious_id, table_name)
);

create table if not exists cnv_unload_state
(
  cash_id bigint unsigned default 0 not null,
  cnv_id char(40) default '' not null,
  obj_type tinyint default 0 not null,
  last_unload_obj_id int default 0 not null,
  primary key (cash_id, cnv_id, obj_type)
)
  engine=MyISAM;

create table if not exists document_status
(
  mysterious_id int default 0 not null,
  table_id int default 0 not null,
  table_name varchar(40) default '' not null,
  id int default 0 not null,
  status int default 0 not null,
  user_id varchar(40) default '{00000000-0000-0000-0000-000000000000}' not null,
  date datetime default '0000-00-00 00:00:00' not null,
  primary key (mysterious_id, table_id, table_name, id)
);

create index table_id
  on document_status (table_id, table_name);

create table if not exists gds_bs_delivery_proc
(
  store_id int default 0 not null,
  delivery_id varchar(40) default '' not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, delivery_id, item)
);

create index version
  on gds_bs_delivery_proc (store_id, version, deleted);

create table if not exists gds_bs_goods
(
  store_id int default 0 not null,
  article varchar(40) default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, article)
);

create index version
  on gds_bs_goods (store_id, version, deleted);

create table if not exists gds_bs_goods_journal
(
  store_id int default 0 not null,
  id int default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  item varchar(40) default '' not null,
  comment varchar(100) null,
  amount decimal(20,4) default 0.0000 not null,
  amount_before decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on gds_bs_goods_journal (store_id, version, deleted);

create table if not exists gds_reserve
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  position bigint unsigned default 0 not null,
  article varchar(40) default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id, position)
);

create index by_article
  on gds_reserve (cash_id, article);

create index version
  on gds_reserve (cash_id, version, deleted);

create table if not exists gds_struckoff
(
  cash_id bigint unsigned default 0 not null,
  article varchar(40) default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, article)
);

create index version
  on gds_struckoff (cash_id, version, deleted);

create table if not exists invoice_number
(
  store_id int default 0 not null
    primary key,
  prefix varchar(40) default '' not null,
  count bigint unsigned null,
  suffix varchar(40) default '' not null
);

create table if not exists local_accounting_transaction
(
  accounting_guid varchar(40) default '0' not null
    primary key,
  amount decimal(20,4) default 0.0000 not null,
  action_date datetime default '0000-00-00 00:00:00' not null,
  deleted tinyint(1) default 0 not null
);

create table if not exists local_analyze_cash_day
(
  cash_id bigint unsigned default 0 not null,
  year varchar(20) default '' not null,
  month varchar(20) default '' not null,
  day varchar(20) default '' not null,
  sum_sale decimal(20,4) null,
  sum_sale_nonfiscal decimal(20,4) default 0.0000 null,
  sum_return decimal(20,4) null,
  sum_return_nonfiscal decimal(20,4) default 0.0000 null,
  receipt_count bigint default 0 null,
  items_receipt_count bigint default 0 null,
  receipt_handling_time_duration bigint default 0 null,
  primary key (cash_id, year, month, day)
);

create table if not exists local_analyze_cash_month
(
  cash_id bigint unsigned default 0 not null,
  year varchar(20) default '' not null,
  month varchar(20) default '' not null,
  sum_sale decimal(20,4) null,
  sum_sale_nonfiscal decimal(20,4) default 0.0000 null,
  sum_return decimal(20,4) null,
  sum_return_nonfiscal decimal(20,4) default 0.0000 null,
  receipt_count bigint default 0 null,
  items_receipt_count bigint default 0 null,
  receipt_handling_time_duration bigint default 0 null,
  primary key (cash_id, year, month)
);

create table if not exists local_analyze_cash_year
(
  cash_id bigint unsigned default 0 not null,
  year varchar(20) default '' not null,
  sum_sale decimal(20,4) null,
  sum_sale_nonfiscal decimal(20,4) default 0.0000 null,
  sum_return decimal(20,4) null,
  sum_return_nonfiscal decimal(20,4) default 0.0000 null,
  receipt_count bigint default 0 null,
  items_receipt_count bigint default 0 null,
  receipt_handling_time_duration bigint default 0 null,
  primary key (cash_id, year)
);

create table if not exists local_analyze_client
(
  client varchar(40) not null,
  date date not null,
  count int default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  max decimal(20,4) default 0.0000 not null,
  primary key (client, date)
);

create table if not exists local_analyze_save_config
(
  id int default 0 not null
    primary key,
  name_config varchar(40) default '' not null,
  chart_params text not null
);

create table if not exists local_analyze_store_day
(
  store_id int default 0 not null,
  year varchar(20) default '' not null,
  month varchar(20) default '' not null,
  day varchar(20) default '' not null,
  sum_sale decimal(20,4) null,
  sum_sale_nonfiscal decimal(20,4) default 0.0000 null,
  sum_return decimal(20,4) null,
  sum_return_nonfiscal decimal(20,4) default 0.0000 null,
  receipt_count bigint default 0 null,
  items_receipt_count bigint default 0 null,
  receipt_handling_time_duration bigint default 0 null,
  primary key (store_id, year, month, day)
);

create table if not exists local_analyze_store_month
(
  store_id int default 0 not null,
  year varchar(20) default '' not null,
  month varchar(20) default '' not null,
  sum_sale decimal(20,4) null,
  sum_sale_nonfiscal decimal(20,4) default 0.0000 null,
  sum_return decimal(20,4) null,
  sum_return_nonfiscal decimal(20,4) default 0.0000 null,
  receipt_count bigint default 0 null,
  items_receipt_count bigint default 0 null,
  receipt_handling_time_duration bigint default 0 null,
  primary key (store_id, year, month)
);

create table if not exists local_analyze_store_year
(
  store_id int default 0 not null,
  year varchar(20) default '' not null,
  sum_sale decimal(20,4) null,
  sum_sale_nonfiscal decimal(20,4) default 0.0000 null,
  sum_return decimal(20,4) null,
  sum_return_nonfiscal decimal(20,4) default 0.0000 null,
  receipt_count bigint default 0 null,
  items_receipt_count bigint default 0 null,
  receipt_handling_time_duration bigint default 0 null,
  primary key (store_id, year)
);

create table if not exists local_aoo_incomplete
(
  auth_code varchar(40) default '' not null
    primary key
);

create table if not exists local_auth_account
(
  id int auto_increment
    primary key,
  account_type_id int(11) unsigned default 0 not null,
  name varchar(128) default '' not null,
  credit decimal(20,2) default 0.00 not null,
  params varchar(255) null,
  closed datetime null,
  constraint name
    unique (name)
);

create index account_type_id
  on local_auth_account (account_type_id, params);

create definer = ukm_server@localhost trigger local_auth_account_after_del_tr
  after DELETE on local_auth_account
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_account_after_ins_tr
  after INSERT on local_auth_account
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_account_after_upd_tr
  after UPDATE on local_auth_account
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_account_before_upd_tr
  before UPDATE on local_auth_account
  for each row
-- missing source code
;

create table if not exists local_auth_account_journal
(
  id int auto_increment
    primary key,
  account_id int default 0 not null,
  amount decimal(20,2) null,
  date datetime default '0000-00-00 00:00:00' not null,
  source_type smallint(6) unsigned default 0 not null,
  source_id varchar(40) default '0' not null,
  cash_id int default 0 not null,
  comment text null,
  balance decimal(20,2) null,
  action_date datetime default '0000-00-00 00:00:00' not null,
  constraint date
    unique (date, id),
  constraint local_auth_account_journal_fk
    foreign key (account_id) references local_auth_account (id)
)
  comment 'Проводки по счетам';

create index account_id_date
  on local_auth_account_journal (account_id, date);

create index account_id_id
  on local_auth_account_journal (account_id, id);

create index local_auth_account_journal_FK1
  on local_auth_account_journal (account_id);

create index source_type
  on local_auth_account_journal (account_id, source_type);

create index source_type_date
  on local_auth_account_journal (account_id, source_type, date);

create definer = ukm_server@localhost trigger local_auth_account_journal_after_ins_tr
  after INSERT on local_auth_account_journal
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_account_journal_after_upd_tr
  after UPDATE on local_auth_account_journal
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_account_journal_before_ins_tr
  before INSERT on local_auth_account_journal
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_account_journal_before_upd_tr
  before UPDATE on local_auth_account_journal
  for each row
-- missing source code
;

create table if not exists local_auth_certificate_account
(
  account_id int default 0 not null
    primary key,
  active tinyint(1) default 0 null,
  date_from date null,
  date_to date null,
  days_from_after_activate int null,
  days_to_after_activate int null,
  pin_code varchar(20) null,
  store_id int null,
  constraint local_auth_certificate_account_fk
    foreign key (account_id) references local_auth_account (id)
)
  comment 'Параметры сертификатов';

create table if not exists local_auth_journal
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned auto_increment,
  server_type int null,
  server_id int null,
  terminal_id varchar(20) null,
  transaction_id varchar(20) null,
  operation_type varchar(20) default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  transaction_time datetime null,
  settled tinyint(1) default 0 null,
  primary key (cash_id, id)
);

create index id
  on local_auth_journal (id);

create table if not exists local_auth_operation
(
  id int auto_increment
    primary key,
  account_id int default 0 not null,
  account_id_link int null,
  number varchar(100) default '' not null,
  date datetime default '0000-00-00 00:00:00' not null,
  user_id varchar(40) default '' not null,
  operation_date datetime default '0000-00-00 00:00:00' not null,
  operation_type int default 0 not null,
  operation_type_version int default 0 not null,
  description text null,
  amount decimal(20,4) default 0.0000 not null,
  comment text null,
  constraint local_auth_operation_fk
    foreign key (account_id) references local_auth_account (id)
);

create index account_id
  on local_auth_operation (account_id, date);

create index local_auth_operation_FK1
  on local_auth_operation (account_id);

create definer = ukm_server@localhost trigger local_auth_operation_before_ins_tr
  before INSERT on local_auth_operation
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_operation_before_upd_tr
  before UPDATE on local_auth_operation
  for each row
-- missing source code
;

create table if not exists local_auth_operation_number
(
  account_type_id int default 0 not null,
  operation_type_type tinyint(1) default 0 not null,
  prefix varchar(40) default '' not null,
  count bigint unsigned null,
  suffix varchar(40) default '' not null,
  primary key (account_type_id, operation_type_type)
);

create table if not exists local_auth_operation_properties
(
  operation_id int default 0 not null,
  property_id int default 0 not null,
  property_value text null,
  primary key (operation_id, property_id)
);

create definer = ukm_server@localhost trigger local_auth_operation_properties_after_del_tr
  after DELETE on local_auth_operation_properties
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_operation_properties_after_ins_tr
  after INSERT on local_auth_operation_properties
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger local_auth_operation_properties_after_upd_tr
  after UPDATE on local_auth_operation_properties
  for each row
-- missing source code
;

create table if not exists local_cashbook
(
  id int not null
    primary key,
  account_id int default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  user_id varchar(40) default '' not null,
  created datetime not null,
  constraint account_date
    unique (account_id, date)
);

create table if not exists local_cashbook_autocreate
(
  account_id int default 0 not null
    primary key,
  set_number bigint unsigned null,
  set_date datetime null
);

create table if not exists local_certificate_rules
(
  id int auto_increment
    primary key,
  account_type_id int default 0 not null,
  prefix varchar(40) not null,
  pad_zero int null,
  code_start int default 0 not null,
  code_stop int default 0 not null,
  suffix varchar(40) not null,
  operation_in int null,
  apply_nominal tinyint(1) default 0 null,
  gen_pincode tinyint(1) default 0 not null,
  pincode_len int null,
  date_from date null,
  date_to date null,
  days_from_after_activate int null,
  days_to_after_activate int null,
  store_id int null
);

create table if not exists local_clm_confirm
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  discount_id bigint unsigned default 0 not null,
  confirm_cmd mediumblob null,
  send_status tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id, discount_id)
);

create index send_status
  on local_clm_confirm (send_status);

create index versions
  on local_clm_confirm (cash_id);

create table if not exists local_config_oper_day
(
  store_id int default 0 not null,
  id varchar(50) default '' not null,
  value text null,
  primary key (store_id, id)
);

create table if not exists local_data_change_log
(
  store_id int default 0 not null,
  id int unsigned default 0 not null,
  order_no int unsigned default 0 not null,
  create_moment datetime default '0000-00-00 00:00:00' not null,
  creator_user_id varchar(40) default '{00000000-0000-0000-0000-000000000000}' not null,
  status_change_user_id varchar(40) default '{00000000-0000-0000-0000-000000000000}' not null,
  status_change_moment datetime default '0000-00-00 00:00:00' not null,
  working_life datetime null,
  primary key (store_id, id),
  constraint order_no
    unique (store_id, order_no)
);

create index creator_user_id
  on local_data_change_log (creator_user_id);

create index status_change_user_id
  on local_data_change_log (status_change_user_id);

create table if not exists local_data_change_log_entities
(
  store_id int default 0 not null,
  change_log_id int unsigned default 0 not null,
  number bigint unsigned default 0 not null,
  item_id varchar(40) default '' not null,
  command tinyint unsigned default 0 not null,
  primary key (store_id, change_log_id, number)
);

create index command
  on local_data_change_log_entities (store_id, change_log_id, command);

create index entity_code
  on local_data_change_log_entities (store_id, change_log_id, item_id);

create index entity_id
  on local_data_change_log_entities (store_id, item_id);

create table if not exists local_data_change_log_entity_prices
(
  store_id int default 0 not null,
  change_log_id int unsigned default 0 not null,
  number bigint unsigned default 0 not null,
  property_id int not null,
  id int default 0 not null,
  pricelist_name varchar(100) default '' not null,
  old_value text null,
  new_value text null,
  primary key (store_id, change_log_id, number, property_id, id)
);

create table if not exists local_data_change_log_entity_printed
(
  store_id int default 0 not null,
  change_log_id int unsigned default 0 not null,
  pricetag_id int default 0 not null,
  primary key (store_id, change_log_id, pricetag_id)
);

create table if not exists local_data_change_log_entity_properties
(
  store_id int default 0 not null,
  change_log_id int unsigned default 0 not null,
  number bigint unsigned default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  old_value text null,
  new_value text null,
  primary key (store_id, change_log_id, number, id)
);

create table if not exists local_data_change_log_param
(
  store_id int default 0 not null
    primary key,
  conv_mask text null
);

create table if not exists local_data_change_log_status
(
  store_id int default 0 not null,
  change_log_id int unsigned default 0 not null,
  status int unsigned default 0 not null,
  cookies text null,
  primary key (store_id, change_log_id),
  constraint cash_line_id
    unique (store_id)
);

create table if not exists local_discount_withdrawal
(
  cash_id bigint unsigned default 0 not null,
  receipt_header_id bigint unsigned default 0 not null,
  efts int unsigned default 0 not null,
  primary key (cash_id, efts)
);

create table if not exists local_documents_to_print
(
  cash_id bigint unsigned default 0 not null,
  id int(20) auto_increment,
  printer_id int(3) default 0 null,
  print_text text null,
  misc_info varchar(200) null,
  primary key (cash_id, id)
);

create index id
  on local_documents_to_print (id);

create table if not exists local_external_service
(
  global_id int default 0 not null,
  service_id int not null,
  service_params varchar(20000) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, service_id)
);

create table if not exists local_integration
(
  id int default 0 not null
    primary key,
  name varchar(100) default '' not null,
  efts int default 0 not null,
  param text null
);

create table if not exists local_last_selected_price
(
  cash_id bigint unsigned default 0 not null,
  id varchar(40) default '' not null,
  type int(11) unsigned default 0 not null,
  amount decimal(20,4) null,
  primary key (cash_id, id, type)
);

create table if not exists local_list_rule
(
  nomenclature_id int default 0 not null,
  list int default 0 not null,
  id int default 0 not null,
  name varchar(128) default '' not null,
  primary key (nomenclature_id, list, id)
);

create table if not exists local_list_rule_condition
(
  nomenclature_id int not null,
  list int default 0 not null,
  rule int default 0 not null,
  id int default 0 not null,
  type varchar(60) not null,
  param text null,
  negative tinyint(1) default 0 not null,
  primary key (list, rule, id, nomenclature_id)
);

create table if not exists local_manzana_ds_clients_form_register
(
  rec_id int not null
    primary key,
  card_num varchar(50) not null,
  form_number varchar(13) not null,
  register_date date not null,
  store_id int not null
);

create index client_date
  on local_manzana_ds_clients_form_register (card_num, register_date);

create index form_number
  on local_manzana_ds_clients_form_register (form_number);

create table if not exists local_mng_pos
(
  cash_id bigint unsigned not null
    primary key,
  mon_guid varchar(40) null,
  version bigint unsigned null
);

create table if not exists local_mng_store
(
  store_id int not null
    primary key,
  version bigint unsigned null
);

create table if not exists local_mrp_pricelist
(
  pricelist_id int(11) unsigned default 0 not null
    primary key,
  owner int(11) unsigned default 0 not null,
  name_suffix int default 0 not null
);

create table if not exists local_oper_day_document_notes
(
  store_id bigint not null,
  document_id int default 0 not null,
  nominal int(11) unsigned not null,
  nominal_type tinyint(1) default 0 not null,
  currency_id int not null,
  count int default 0 not null,
  primary key (store_id, document_id, nominal, nominal_type)
);

create index cur_id
  on local_oper_day_document_notes (currency_id);

create table if not exists local_oper_day_documents
(
  store_id bigint not null,
  document_id int default 0 not null,
  cash_id bigint unsigned null,
  moneyoperation_id bigint unsigned null,
  oper_day date default '0000-00-00' not null,
  type tinyint(1) default 0 not null,
  number bigint unsigned null,
  primary key (store_id, document_id)
);

create index moneyoperation
  on local_oper_day_documents (cash_id, moneyoperation_id);

create index oper_day
  on local_oper_day_documents (store_id, oper_day);

create index oper_type_number
  on local_oper_day_documents (store_id, type, number);

create table if not exists local_oper_day_numbers
(
  cashbook_id int default 0 not null,
  type tinyint(1) default 0 not null,
  count bigint unsigned null,
  primary key (cashbook_id, type)
);

create table if not exists local_oper_day_shift
(
  store_id bigint unsigned default 0 not null,
  oper_day date default '0000-00-00' not null,
  cash_id bigint unsigned default 0 not null,
  shift_id bigint unsigned default 0 not null,
  checked tinyint(1) default 0 not null,
  comment text null,
  have_warnings tinyint(1) default 0 not null,
  primary key (store_id, oper_day, cash_id, shift_id)
);

create index shift
  on local_oper_day_shift (cash_id, shift_id);

create table if not exists local_order
(
  id int(11) unsigned default 0 not null,
  cash_id bigint unsigned default 0 not null,
  blocking_cash_id int null,
  user_id int default 0 not null,
  table_num int null,
  order_data mediumblob null,
  primary key (id, cash_id)
);

create table if not exists local_prepare_install_data
(
  type tinyint(1) not null,
  object_id int not null,
  state tinyint(1) not null,
  date date null,
  primary key (type, object_id)
);

create table if not exists local_satellite_service_offline_receipts
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  primary key (cash_id, id)
);

create table if not exists local_satellite_service_refresh_date
(
  client_id varchar(40) default '' not null
    primary key,
  date datetime default '0000-00-00 00:00:00' not null
);

create table if not exists local_satellite_service_transactions
(
  accounting_guid varchar(40) default '0' not null
    primary key,
  client_id varchar(40) default '' not null,
  account_type int(11) unsigned default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  type tinyint(1) default 0 not null
);

create index client
  on local_satellite_service_transactions (client_id);

create table if not exists local_server_param
(
  id varchar(255) default '' not null
    primary key,
  value text null
);

create table if not exists local_server_task
(
  id int(11) unsigned auto_increment
    primary key,
  server_guid varchar(40) not null,
  type tinyint(1) not null,
  params mediumtext null,
  status tinyint(1) not null,
  status_text varchar(255) not null,
  created datetime not null,
  updated datetime null,
  last_check datetime null,
  user varchar(40) null
);

create index local_server_task_server_guid
  on local_server_task (server_guid);

create table if not exists local_servers
(
  id int default 0 not null
    primary key,
  guid varchar(40) null,
  name varchar(80) null,
  host varchar(80) default '' not null,
  allow int(1) default 0 not null,
  allow_import int(1) default 0 not null,
  version int unsigned default 0 null,
  avalable_update int unsigned null,
  last_export datetime null,
  last_import datetime null
);

create table if not exists local_siebel_transactions
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  cheque_id varchar(40) not null,
  card_number varchar(40) null,
  auth_by_phone_number tinyint(1) default 0 not null,
  card_use tinyint(1) default 0 not null,
  social_card_number varchar(40) null,
  offline_type tinyint(1) default 0 not null,
  calculate_order_response text null,
  list_of_gifts text null,
  create_order_request text null,
  state tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id)
);

create table if not exists local_srt_storage
(
  shipment varchar(40) not null
    primary key,
  number varchar(40) null,
  supplier varchar(100) null,
  datetime datetime null,
  title varchar(100) null,
  plan_gate int null,
  plan_pallet_count int null,
  handle_type varchar(40) null,
  stream varchar(40) null,
  command_client varchar(100) null
);

create table if not exists local_srv_clean_params
(
  cash_id bigint unsigned default 0 not null,
  param varchar(50) default '' not null,
  value int default 0 not null,
  primary key (cash_id, param)
);

create table if not exists local_state
(
  cash_id bigint unsigned default 0 not null,
  id varchar(50) default '' not null,
  value text not null,
  primary key (cash_id, id)
);

create table if not exists local_table_status
(
  id int default 0 not null
    primary key,
  status int default 2 not null,
  last_change_time datetime default '0000-00-00 00:00:00' not null,
  user_login bigint null,
  guests int null,
  link_table int null
);

create table if not exists local_tsd_pricetag
(
  id int auto_increment,
  store_id int not null,
  item_barcode varchar(80) null,
  code39 varchar(6) null,
  code39_full varchar(16) null,
  pricetype tinyint(1) default 0 null,
  template_id int not null,
  status tinyint(1) not null,
  operator_barcode varchar(32) null,
  timestamp datetime not null,
  primary key (id, store_id)
);

create table if not exists local_web_log
(
  id int not null
    primary key,
  store_id int null,
  user_id varchar(40) null,
  user_name varchar(255) default '' null,
  action tinyint(1) null,
  section varchar(255) null,
  ip varchar(16) null,
  action_time datetime not null,
  action_microtime varchar(4) not null,
  additional varchar(255) null
);

create table if not exists local_web_user_view_type
(
  user_id varchar(40) default '' not null,
  form_key varchar(255) default '' not null,
  view_type tinyint(1) default 0 not null,
  primary key (user_id, form_key)
);

create table if not exists mon_categories
(
  id bigint unsigned auto_increment
    primary key,
  parent_id bigint unsigned null,
  code varchar(40) default '' not null,
  name varchar(40) default '' not null,
  description varchar(100) default '' not null,
  show_order smallint unsigned default 0 not null,
  constraint idx_parent_id
    unique (parent_id, code),
  constraint fk_parent_id
    foreign key (parent_id) references mon_categories (id)
      on delete cascade
);

create table if not exists mon_values
(
  cat_id bigint unsigned default 0 not null,
  code varchar(20) default '' not null,
  name varchar(40) default '' not null,
  description varchar(100) default '' not null,
  value varchar(200) default '' not null,
  show_order smallint unsigned default 0 not null,
  primary key (cat_id, code),
  constraint fk_cat_id
    foreign key (cat_id) references mon_categories (id)
      on delete cascade
);

create table if not exists operation_committer
(
  cash_id bigint unsigned default 0 not null,
  owner varchar(70) default '' not null,
  oper_id bigint unsigned default 0 not null,
  primary key (cash_id, owner)
);

create table if not exists receipt_committer
(
  cash_id bigint unsigned default 0 not null,
  owner varchar(70) default '' not null,
  receipt_id bigint unsigned default 0 not null,
  shift_id bigint unsigned default 0 not null,
  primary key (cash_id, owner)
);

create table if not exists sequences
(
  mysterious_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  name char(50) not null,
  primary key (mysterious_id, name)
);

create table if not exists srv_assortment_group_auto_classif
(
  store_id int default 0 not null,
  ag_id int default 0 not null,
  classif varchar(40) default '0' not null,
  status int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, ag_id, classif)
);

create index version
  on srv_assortment_group_auto_classif (store_id, version, deleted);

create table if not exists srv_assortment_group_items
(
  store_id int default 0 not null,
  ag_id int default 0 not null,
  var varchar(40) default '' not null,
  plu int(11) unsigned null,
  hot_key varchar(40) null,
  exp_date1 datetime null,
  exp_date2 int null,
  status int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, ag_id, var)
);

create index version
  on srv_assortment_group_items (store_id, version, deleted);

create table if not exists srv_assortment_group_scales
(
  store_id int default 0 not null,
  ag_id int default 0 not null,
  id int default 0 not null,
  name varchar(80) default '' not null,
  type int(2) default 0 not null,
  xml text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, ag_id, id)
);

create index version
  on srv_assortment_group_scales (store_id, version, deleted);

create table if not exists srv_assortment_group_scales_parameters
(
  store_id int default 0 not null,
  ag_id int default 0 not null,
  type int(2) default 0 not null,
  xml mediumtext null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, ag_id, type)
);

create index version
  on srv_assortment_group_scales_parameters (store_id, version, deleted);

create table if not exists srv_assortment_groups
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(80) default '' not null,
  can_sale_non_weight tinyint(1) default 0 not null,
  delete_plu tinyint(1) default 0 not null,
  order_mode int default 0 not null,
  plu_mode int default 0 not null,
  exp_date int null,
  load_descr tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on srv_assortment_groups (store_id, version, deleted);

create table if not exists srv_configuration_hardware
(
  config_hardware_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (config_hardware_id, id)
);

create index version
  on srv_configuration_hardware (config_hardware_id, version, deleted);

create table if not exists srv_converters
(
  server_id bigint default 0 not null,
  cnv_id varchar(40) default '0' not null,
  cnv_name varchar(40) default '' not null,
  cnv_code varchar(40) default '' not null,
  cnv_pr_xml mediumtext null,
  cnv_sleep int default 0 not null,
  cnv_target int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (server_id, cnv_id)
);

create index version
  on srv_converters (server_id, version, deleted);

create table if not exists srv_converters_start_from
(
  server_id bigint default 0 not null,
  cash_id bigint unsigned default 0 not null,
  cnv_id char(40) default '' not null,
  obj_type tinyint default 0 not null,
  last_unload_obj_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (server_id, cash_id, cnv_id, obj_type)
);

create index version
  on srv_converters_start_from (server_id, version, deleted);

create table if not exists srv_discount_adder_classifclients
(
  global_id int default 0 not null,
  adder_account_type_id int default 0 not null,
  classif_id int default 0 not null,
  amount decimal(20,2) default 0.00 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, adder_account_type_id, classif_id)
);

create index version
  on srv_discount_adder_classifclients (global_id, version, deleted);

create table if not exists srv_discount_adder_clients
(
  global_id int default 0 not null,
  adder_account_type_id int default 0 not null,
  client_id varchar(40) default '' not null,
  amount decimal(20,2) default 0.00 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, adder_account_type_id, client_id)
);

create index version
  on srv_discount_adder_clients (global_id, version, deleted);

create table if not exists srv_discount_csv_upload_param
(
  global_id int default 0 not null,
  efts int default 0 not null
    primary key,
  csv_separator varchar(1) default ',' not null,
  codepage varchar(20) default '' not null,
  columns text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on srv_discount_csv_upload_param (global_id, version, deleted);

create table if not exists srv_domain_repl
(
  server_id bigint unsigned default 0 not null,
  domain_id int unsigned default 0 not null,
  replicate tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (server_id, domain_id)
);

create index version
  on srv_domain_repl (server_id, version, deleted);

create table if not exists srv_ext_param_values
(
  global_id int default 0 not null,
  id int not null,
  ext_param int not null,
  position int not null,
  value varchar(20) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create table if not exists srv_ext_params
(
  global_id int default 0 not null,
  id int not null,
  position int not null,
  name varchar(20) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create table if not exists srv_external_links
(
  global_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  descr text null,
  url text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on srv_external_links (global_id, version, deleted);

create table if not exists srv_keyboard_label
(
  keyboard_layout_id int(10) default 0 not null,
  p_code int default 0 not null,
  label text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, p_code)
);

create table if not exists srv_keyboard_layouts
(
  keyboard_layout_id int default 0 not null,
  id int default 0 not null,
  keyboard_type int(10) default 0 not null,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, id)
);

create index version
  on srv_keyboard_layouts (keyboard_layout_id, version, deleted);

create table if not exists srv_offline_account
(
  global_id int default 0 not null,
  id int not null,
  account_type_id int(11) unsigned default 0 not null,
  name varchar(128) default '' not null,
  balance decimal(20,2) default 0.00 not null,
  credit decimal(20,2) default 0.00 not null,
  params varchar(255) null,
  closed datetime null,
  active tinyint(1) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index account_type_id
  on srv_offline_account (account_type_id, params);

create index version
  on srv_offline_account (global_id, version, deleted);

create table if not exists srv_operation_type
(
  global_id int default 0 not null,
  id int default 0 not null,
  rec_version int default 0 not null,
  account_type int default 0 not null,
  account_type_link int null,
  type tinyint(1) default 0 not null,
  name varchar(100) default '' not null,
  archival tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id, rec_version)
)
  comment 'Типы операций';

create index version
  on srv_operation_type (global_id, version, deleted);

create table if not exists srv_operation_type_properties
(
  global_id int default 0 not null,
  oper_type_id int default 0 not null,
  property_id int default 0 not null,
  sort int null,
  defvalue varchar(2000) null,
  version int default 0 not null,
  deleted varchar(20) default '0' not null,
  primary key (global_id, oper_type_id, property_id, deleted),
  constraint oper_type_id
    unique (oper_type_id, sort, deleted)
)
  comment 'Соответствие типов операций и custom-property';

create index version
  on srv_operation_type_properties (global_id, version, deleted);

create definer = ukm_server@localhost trigger srv_operation_type_properties_before_ins_tr
  before INSERT on srv_operation_type_properties
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger srv_operation_type_properties_before_upd_tr
  before UPDATE on srv_operation_type_properties
  for each row
-- missing source code
;

create table if not exists srv_pricetags_classif
(
  nomenclature_id int default 0 not null,
  classif_id varchar(40) default '0' not null,
  pricetag_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, classif_id, pricetag_id)
);

create index pricetag
  on srv_pricetags_classif (pricetag_id);

create index version
  on srv_pricetags_classif (nomenclature_id, version, deleted);

create table if not exists srv_pricetags_item
(
  nomenclature_id int default 0 not null,
  item_id varchar(40) default '' not null,
  pricetag_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item_id, pricetag_id)
);

create index pricetag
  on srv_pricetags_item (pricetag_id);

create index version
  on srv_pricetags_item (nomenclature_id, version, deleted);

create table if not exists srv_pricetags_var
(
  nomenclature_id int default 0 not null,
  var_id varchar(40) default '' not null,
  pricetag_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, var_id, pricetag_id)
);

create index pricetag
  on srv_pricetags_var (pricetag_id);

create index version
  on srv_pricetags_var (nomenclature_id, version, deleted);

create table if not exists srv_properties
(
  global_id int default 0 not null,
  id int auto_increment,
  code varchar(20) default '' not null,
  title varchar(50) default '' not null,
  defvalue varchar(2000) default '' null,
  visible tinyint null,
  max_len int null,
  rows int default 1 null,
  version int not null,
  deleted varchar(20) default '0' not null,
  primary key (global_id, id)
)
  comment 'Custom-property операций';

create index id_ai
  on srv_properties (id);

create index version
  on srv_properties (global_id, version, deleted);

create index visible
  on srv_properties (visible);

create table if not exists srv_rd_template
(
  global_id int not null,
  id int not null,
  type int not null,
  file longblob null,
  description varchar(150) null,
  default_printer varchar(100) null,
  version int not null,
  deleted tinyint not null,
  primary key (global_id, id)
)
  comment 'Шаблоны отчетов';

create index Type
  on srv_rd_template (type);

create index version
  on srv_rd_template (global_id, version, deleted);

create definer = ukm_server@localhost trigger srv_rd_template_after_del_tr
  after DELETE on srv_rd_template
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger srv_rd_template_after_ins_tr
  after INSERT on srv_rd_template
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger srv_rd_template_after_upd_tr
  after UPDATE on srv_rd_template
  for each row
-- missing source code
;

create table if not exists srv_report_tags
(
  global_id int default 0 not null,
  template_id int(11) unsigned default 0 not null,
  account_type int default 0 not null,
  operation_type int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, template_id, account_type, operation_type)
)
  comment 'Соответствие отчетов типам счетов и операций';

create index version
  on srv_report_tags (global_id, version, deleted);

create definer = ukm_server@localhost trigger srv_report_tags_after_ins_tr
  after INSERT on srv_report_tags
  for each row
-- missing source code
;

create table if not exists srv_repricing_act
(
  store_id int not null,
  id bigint unsigned not null,
  date_start datetime not null,
  date_stop datetime null,
  state tinyint(1) default 0 not null,
  priority int not null,
  description varchar(255) null,
  date_invalid datetime null,
  nomenclature_id int(11) unsigned not null,
  version int(11) unsigned default 0 not null,
  deleted tinyint(1) unsigned default 0 not null,
  primary key (store_id, id)
)
  comment 'Акты переоценки';

create index stopped_ind
  on srv_repricing_act (date_stop, state);

create table if not exists srv_repricing_item
(
  item varchar(40) not null,
  store_id int not null,
  repricing_act_id bigint unsigned not null,
  number int(11) unsigned not null,
  state tinyint(1) default 0 not null,
  price decimal(20,4) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (item, store_id, repricing_act_id),
  constraint store_id
    unique (store_id, repricing_act_id, number)
)
  comment 'Товары в актах переоценки';

create index act_index
  on srv_repricing_item (store_id, repricing_act_id, deleted);

create table if not exists srv_role
(
  global_id int default 0 not null,
  id varchar(40) default '' not null
    primary key,
  title varchar(100) null,
  version int default 0 not null,
  deleted tinyint default 0 not null
);

create index version
  on srv_role (global_id, version, deleted);

create table if not exists srv_role_permission
(
  global_id int default 0 not null,
  entry_id varchar(40) default '' not null,
  role_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint default 0 not null,
  primary key (entry_id, role_id)
);

create index role_id
  on srv_role_permission (role_id);

create index version
  on srv_role_permission (global_id, version, deleted);

create table if not exists srv_store_ext_param_values
(
  global_id int default 0 not null,
  store_id int not null,
  ext_param int not null,
  ext_param_value int not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, store_id, ext_param, ext_param_value)
);

create table if not exists srv_store_topology_nodes
(
  global_id int default 0 not null,
  id int(11) unsigned not null,
  owner int(11) unsigned null,
  name varchar(100) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create table if not exists srv_terminal_menus
(
  terminal_menu_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (terminal_menu_id, id)
);

create index version
  on srv_terminal_menus (terminal_menu_id, version, deleted);

create table if not exists srv_trm_domain_tables
(
  global_id int default 0 not null,
  domain_id int unsigned default 0 not null,
  name varchar(40) default '' not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, name)
);

create index global_id
  on srv_trm_domain_tables (global_id, version, deleted);

create table if not exists srv_trm_domains
(
  global_id int default 0 not null,
  id int unsigned default 0 not null,
  name varchar(40) default '' not null,
  export tinyint(1) default 0 not null,
  import tinyint(1) default 0 not null,
  unit tinyint(1) default 0 not null,
  mode tinyint default 0 not null,
  hidden tinyint default 0 not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index global_id
  on srv_trm_domains (global_id, version, deleted);

create table if not exists srv_user
(
  global_id int default 0 not null,
  id varchar(40) default '' not null
    primary key,
  role_id varchar(40) default '' not null,
  username varchar(32) default '' not null,
  password varchar(32) default '' not null,
  title text null,
  properties varchar(40) default '1' not null,
  version int default 0 not null,
  deleted tinyint default 0 not null,
  constraint username
    unique (username)
);

create index role_id
  on srv_user (role_id);

create index version
  on srv_user (global_id, version, deleted);

create table if not exists srv_users_servers
(
  global_id int default 0 not null,
  user_id varchar(40) default '' not null,
  server_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint default 0 not null,
  primary key (global_id, user_id, server_id)
);

create index version
  on srv_users_servers (global_id, version, deleted);

create table if not exists srv_users_stores
(
  global_id int default 0 not null,
  user_id varchar(40) default '' not null,
  store_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint default 0 not null,
  primary key (global_id, user_id, store_id)
);

create index version
  on srv_users_stores (global_id, version, deleted);

create table if not exists trm_auth_local_storage
(
  cash_id bigint unsigned default 0 not null,
  payment int default 0 not null,
  user int default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, payment, user)
);

create index version
  on trm_auth_local_storage (cash_id, version, deleted);

create table if not exists trm_coupon_data
(
  global_id tinyint(1) not null,
  id varchar(20) not null,
  date_from date not null,
  date_to date not null,
  amount decimal(20,2) not null,
  status tinyint(1) null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (global_id, id)
);

create table if not exists trm_discount_card_stop_list_pos
(
  cash_id bigint unsigned default 0 not null,
  id varchar(40) default '' not null,
  type_id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index code
  on trm_discount_card_stop_list_pos (cash_id, type_id, start_card_code, stop_card_code);

create index single_card
  on trm_discount_card_stop_list_pos (start_card_code, deleted);

create index version
  on trm_discount_card_stop_list_pos (cash_id, version, deleted);

create table if not exists trm_in2_account_type
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  name varchar(40) null,
  increase_type int default 0 not null,
  online tinyint(1) default 0 not null,
  type int default 1 not null,
  params varchar(40) default '' not null,
  integration_id int null,
  print_in_receipt tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_account_type (global_id, version, deleted);

create table if not exists trm_in2_account_type_certificates
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  nominal decimal(20,4) null,
  mono_account tinyint(1) default 0 null,
  check_underpay tinyint(1) default 0 null,
  multi_sell tinyint(1) default 0 null,
  allow_return tinyint(1) default 1 null,
  use_pincode tinyint(1) default 0 not null,
  return_money tinyint(1) default 0 not null,
  check_store tinyint(1) default 0 not null,
  fixed_nominal tinyint(1) default 1 not null,
  min_nominal decimal(20,4) null,
  max_nominal decimal(20,4) null,
  nominal_multiplicity decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_account_type_certificates (global_id, version, deleted);

create table if not exists trm_in2_account_type_certificates_items
(
  nomenclature_id int default 0 not null,
  account_type_id int(11) unsigned default 0 not null,
  item_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, account_type_id, item_id)
);

create index version
  on trm_in2_account_type_certificates_items (nomenclature_id, version, deleted);

create table if not exists trm_in2_additive_additivitygroup
(
  nomenclature_id int default 0 not null,
  additivitygroup int default 0 not null,
  additive varchar(40) default '' not null,
  show_order int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, additivitygroup, additive)
);

create index additivitygroup
  on trm_in2_additive_additivitygroup (additivitygroup, show_order);

create index version
  on trm_in2_additive_additivitygroup (nomenclature_id, version, deleted);

create table if not exists trm_in2_additivitygroups
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in2_additivitygroups (nomenclature_id, version, deleted);

create table if not exists trm_in2_advertising_campaign
(
  global_id tinyint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  marketing_effort bigint unsigned default 0 not null,
  title varchar(100) default '' not null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id),
  constraint id
    unique (id)
);

create index marketing_effort
  on trm_in2_advertising_campaign (marketing_effort);

create index version
  on trm_in2_advertising_campaign (global_id, version, deleted);

create table if not exists trm_in2_agent
(
  global_id int default 0 not null,
  tag_1226 bigint not null,
  tag_1222 tinyint(3) null,
  tag_1073 varchar(25) null,
  tag_1044 varchar(25) null,
  tag_1016 bigint null,
  tag_1026 varchar(70) null,
  tag_1075 varchar(25) null,
  tag_1005 varchar(256) null,
  tag_1074 varchar(25) null,
  tag_1225 varchar(256) null,
  tag_1171 varchar(25) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, tag_1226)
);

create index versions
  on trm_in2_agent (global_id, version, deleted);

create table if not exists trm_in2_appeal_reason
(
  nomenclature_id int default 0 not null,
  code varchar(40) default '' not null,
  title varchar(100) default '' not null,
  can_comment tinyint(1) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, code)
);

create index version
  on trm_in2_appeal_reason (nomenclature_id, version, deleted);

create table if not exists trm_in2_auth_servers
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  efts int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_auth_servers (store_id, version, deleted);

create table if not exists trm_in2_available_receipt_item_properties
(
  nomenclature_id int default 0 not null,
  property_code varchar(20) default '' not null,
  value_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, property_code, value_id)
);

create index version
  on trm_in2_available_receipt_item_properties (nomenclature_id, version, deleted);

create table if not exists trm_in2_bp
(
  store_id int default 0 not null,
  bp varchar(40) default '' not null,
  title varchar(100) default '' not null,
  remains tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, bp)
);

create index version
  on trm_in2_bp (store_id, version, deleted);

create table if not exists trm_in2_bp_role
(
  store_id int default 0 not null,
  bp varchar(40) default '' not null,
  role_id bigint unsigned not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, bp, role_id)
);

create index version
  on trm_in2_bp_role (store_id, version, deleted);

create table if not exists trm_in2_cancellation_reason
(
  global_id int default 0 not null,
  id int default 0 not null,
  message text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_cancellation_reason (global_id, version, deleted);

create table if not exists trm_in2_card_client
(
  global_id int default 0 not null,
  card int(11) unsigned default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, card)
);

create index client
  on trm_in2_card_client (client, deleted);

create index version
  on trm_in2_card_client (global_id, version, deleted);

create table if not exists trm_in2_card_stoplist
(
  global_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in2_card_stoplist (start_card_code, stop_card_code);

create index version
  on trm_in2_card_stoplist (global_id, version, deleted);

create table if not exists trm_in2_card_stoplist_client
(
  global_id int default 0 not null,
  card_stoplist int(11) unsigned default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, card_stoplist)
);

create index client
  on trm_in2_card_stoplist_client (client);

create index version
  on trm_in2_card_stoplist_client (global_id, version, deleted);

create table if not exists trm_in2_cards
(
  global_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) null,
  date_from datetime null,
  date_till datetime null,
  active tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in2_cards (start_card_code, stop_card_code);

create index version
  on trm_in2_cards (global_id, version, deleted);

create table if not exists trm_in2_cash_auth_server
(
  cash_id bigint unsigned default 0 not null,
  auth_server_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, auth_server_id)
);

create index version
  on trm_in2_cash_auth_server (cash_id, version, deleted);

create table if not exists trm_in2_cash_equarings
(
  cash_id bigint unsigned default 0 not null,
  equaring_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, equaring_id)
);

create index version
  on trm_in2_cash_equarings (cash_id, version, deleted);

create table if not exists trm_in2_cash_messages
(
  store_id int default 0 not null,
  type tinyint(1) default 0 not null,
  id int default 0 not null,
  message text not null,
  date_from datetime null,
  date_to datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, type, id)
);

create index version
  on trm_in2_cash_messages (store_id, version, deleted);

create table if not exists trm_in2_cash_transferings
(
  cash_id bigint unsigned default 0 not null,
  transfering_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, transfering_id)
);

create index version
  on trm_in2_cash_transferings (cash_id, version, deleted);

create table if not exists trm_in2_classif
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '0' not null,
  owner varchar(40) default '0' not null,
  name varchar(80) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index name
  on trm_in2_classif (name);

create index owner
  on trm_in2_classif (nomenclature_id, owner, deleted);

create index version
  on trm_in2_classif (nomenclature_id, version, deleted);

create table if not exists trm_in2_classif_remains_config
(
  store_id int default 0 not null,
  classif varchar(40) default '0' not null,
  nomenclature_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, classif)
);

create table if not exists trm_in2_classif_stocks
(
  store_id int default 0 not null,
  classif varchar(40) default '0' not null,
  stock int default 0 not null,
  nomenclature_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, classif)
);

create index version
  on trm_in2_classif_stocks (store_id, version, deleted);

create table if not exists trm_in2_classifclients
(
  global_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  owner int(11) unsigned default 0 not null,
  name varchar(255) default '' not null,
  pricetype varchar(38) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index owner
  on trm_in2_classifclients (owner);

create index version
  on trm_in2_classifclients (global_id, version, deleted);

create table if not exists trm_in2_client_items
(
  nomenclature_id int default 0 not null,
  item varchar(40) default '' not null,
  client varchar(40) default '' not null,
  client_item varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item, client)
);

create index version
  on trm_in2_client_items (nomenclature_id, version, deleted);

create table if not exists trm_in2_client_marketingeffort
(
  global_id int default 0 not null,
  client varchar(40) default '' not null,
  marketingeffort int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, client)
);

create index marketingeffort
  on trm_in2_client_marketingeffort (marketingeffort);

create index version
  on trm_in2_client_marketingeffort (global_id, version, deleted);

create table if not exists trm_in2_clientclassif_marketingeffort
(
  global_id int default 0 not null,
  clientclassif int(11) unsigned default 0 not null,
  marketingeffort int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, clientclassif)
);

create index marketingeffort
  on trm_in2_clientclassif_marketingeffort (marketingeffort);

create index version
  on trm_in2_clientclassif_marketingeffort (global_id, version, deleted);

create table if not exists trm_in2_cliententerprise
(
  global_id int default 0 not null,
  client varchar(40) default '' not null,
  enterprisename varchar(100) null,
  okpo varchar(100) null,
  okdp varchar(100) null,
  kpp varchar(100) null,
  baddress varchar(100) null,
  address varchar(100) null,
  phone varchar(100) null,
  bank varchar(100) null,
  register varchar(100) null,
  consignee_name varchar(100) null,
  consignee_address varchar(100) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, client)
);

create index enterprisename
  on trm_in2_cliententerprise (enterprisename);

create index version
  on trm_in2_cliententerprise (global_id, version, deleted);

create table if not exists trm_in2_clients
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  classifclient int(11) unsigned default 0 not null,
  sur_name varchar(100) default '' not null,
  name varchar(100) default '' not null,
  patronymic varchar(100) default '' null,
  birthday datetime null,
  inn varchar(100) default '' null,
  passport varchar(100) null,
  pricetype varchar(38) null,
  type tinyint(1) default 0 not null,
  allow_paycash tinyint(1) default 0 not null,
  structure varchar(100) null,
  active tinyint(1) default 0 not null,
  save_change tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index cl_i
  on trm_in2_clients (active, classifclient, deleted);

create index classifclient
  on trm_in2_clients (classifclient, sur_name, active, deleted);

create index inn
  on trm_in2_clients (inn);

create index sur_name
  on trm_in2_clients (sur_name);

create index version
  on trm_in2_clients (global_id, version, deleted);

create table if not exists trm_in2_clients_properties
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index global_id
  on trm_in2_clients_properties (global_id, version, deleted);

create index name
  on trm_in2_clients_properties (name);

create table if not exists trm_in2_clients_properties_values
(
  global_id int default 0 not null,
  property varchar(40) default '' not null,
  client varchar(40) default '' not null,
  value varchar(100) default '' null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, property, client)
);

create index version
  on trm_in2_clients_properties_values (global_id, version, deleted);

create table if not exists trm_in2_complex_dish
(
  nomenclature_id int default 0 not null,
  owner_dish varchar(40) default '' not null,
  dish varchar(40) default '' not null,
  quantity decimal(20,4) default 1.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, owner_dish, dish)
);

create index version
  on trm_in2_complex_dish (nomenclature_id, version, deleted);

create table if not exists trm_in2_config_cashline
(
  store_id int default 0 not null,
  id varchar(50) default '' not null,
  value text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_config_cashline (store_id, version, deleted);

create table if not exists trm_in2_config_global
(
  global_id int default 0 not null,
  id varchar(50) default '' not null,
  value text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_config_global (global_id, version, deleted);

create table if not exists trm_in2_config_hardware
(
  config_hardware_id bigint unsigned default 0 not null,
  id bigint default 0 not null,
  _object bigint unsigned default 0 not null,
  object_property varchar(200) default '' not null,
  port varchar(50) default '' not null,
  port_property varchar(200) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (config_hardware_id, _object, port, id)
);

create index version
  on trm_in2_config_hardware (config_hardware_id, version, deleted);

create table if not exists trm_in2_config_pos
(
  cash_id bigint unsigned default 0 not null,
  id varchar(50) default '' not null,
  value text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index version
  on trm_in2_config_pos (cash_id, version, deleted);

create table if not exists trm_in2_configuration_groups
(
  global_id int default 0 not null,
  id int default 0 not null,
  config_hardware_id int(10) default 0 null,
  keyboard_layout_id int(10) default 0 null,
  terminal_menu_id int(10) null,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_configuration_groups (global_id, version, deleted);

create table if not exists trm_in2_coupon_data_pos
(
  cash_id bigint unsigned not null,
  id varchar(20) not null,
  date_from date not null,
  date_to date not null,
  amount decimal(20,2) not null,
  status tinyint(1) null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (cash_id, id)
);

create table if not exists trm_in2_coupon_ranges
(
  global_id tinyint(1) not null,
  type_id int not null,
  id int not null,
  range_from varchar(7) not null,
  range_to varchar(7) not null,
  creation_date datetime not null,
  created_by varchar(40) not null,
  version int default 0 null,
  deleted int(1) default 0 null,
  primary key (global_id, type_id, id)
);

create table if not exists trm_in2_coupon_type
(
  global_id tinyint(1) not null,
  id int auto_increment,
  name varchar(100) not null,
  print_type tinyint(1) not null,
  prefix int(6) null,
  nominal decimal(20,2) null,
  date_from date null,
  date_to date null,
  days_from int null,
  days_to int null,
  version int default 0 null,
  deleted int(1) default 0 null,
  primary key (global_id, id)
);

create index id
  on trm_in2_coupon_type (id);

create table if not exists trm_in2_currency
(
  global_id int default 0 not null,
  currency_id int default 0 not null,
  name varchar(100) default '' not null,
  shortname varchar(100) default '' not null,
  loose_change varchar(100) default '' not null,
  multiplier text null,
  monetary_unit_multiplier text null,
  rate decimal(20,4) default 1.0000 not null,
  facevalues varchar(1000) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, currency_id)
);

create index version
  on trm_in2_currency (global_id, version, deleted);

create table if not exists trm_in2_currency_nominal
(
  global_id int not null,
  currency_id int not null,
  nominal int(11) unsigned not null,
  nominal_type tinyint(1) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, currency_id, nominal, nominal_type)
);

create index version
  on trm_in2_currency_nominal (global_id, version, deleted);

create table if not exists trm_in2_custom_text_codecs
(
  global_id int default 0 not null,
  name varchar(40) default '' not null,
  cnv_table blob not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, name)
);

create index version
  on trm_in2_custom_text_codecs (global_id, version, deleted);

create table if not exists trm_in2_customer_screen
(
  global_id int default 0 not null,
  type tinyint not null,
  form text not null,
  profile tinyint not null,
  profile_name text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, profile, type)
);

create index versions
  on trm_in2_customer_screen (global_id, version, deleted);

create table if not exists trm_in2_defect
(
  nomenclature_id int default 0 not null,
  defect_code varchar(40) default '' not null,
  title varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, defect_code)
);

create index version
  on trm_in2_defect (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std2classif
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  classif_id varchar(40) default '0' not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, classif_id)
);

create index version
  on trm_in2_disc_std2classif (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std2item_quantity
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '0' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, item, quantity)
);

create index version
  on trm_in2_disc_std2item_quantity (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std2perscard
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  card_code varchar(40) default '' not null,
  type tinyint(1) unsigned default 0 not null,
  card_type smallint(5) unsigned default 0 not null,
  name varchar(40) null,
  receipt_amount decimal(20,4) default 0.0000 not null,
  classif varchar(40) default '0' not null,
  modificator varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, type, card_code, receipt_amount, classif)
);

create index card_code
  on trm_in2_disc_std2perscard (nomenclature_id, discount_type, card_code);

create index dt_cl_md
  on trm_in2_disc_std2perscard (nomenclature_id, discount_type, classif, modificator, deleted);

create index version
  on trm_in2_disc_std2perscard (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std2perscard_stoplist
(
  nomenclature_id int default 0 not null,
  start varchar(40) default '' not null,
  stop varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, start)
);

create index version
  on trm_in2_disc_std2perscard_stoplist (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_adders
(
  global_id tinyint unsigned default 0 not null,
  id int default 0 not null,
  discount_type bigint default 0 not null,
  adder_type int(11) unsigned default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index discount_type
  on trm_in2_disc_std_adders (discount_type, adder_type, amount);

create index version
  on trm_in2_disc_std_adders (global_id, version, deleted);

create table if not exists trm_in2_disc_std_classif
(
  nomenclature_id int default 0 not null,
  discount_type int default 0 not null,
  classif_id varchar(40) default '0' not null,
  amount decimal(20,3) default 0.000 not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, classif_id, amount, quantity)
);

create index version
  on trm_in2_disc_std_classif (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_classif_n_plus_m
(
  nomenclature_id int default 0 not null,
  discount_type int default 0 not null,
  classif_id varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, classif_id)
);

create index version
  on trm_in2_disc_std_classif_n_plus_m (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_client_property
(
  global_id int default 0 not null,
  id int default 0 not null,
  discount_type int default 0 not null,
  client varchar(40) null,
  code varchar(20) default '' not null,
  value int default 0 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint default 0 not null,
  primary key (global_id, id),
  constraint client
    unique (global_id, discount_type, client, code, value)
);

create index version
  on trm_in2_disc_std_client_property (global_id, version, deleted);

create table if not exists trm_in2_disc_std_clients
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  client varchar(40) default '' not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, client)
);

create index version
  on trm_in2_disc_std_clients (global_id, version, deleted);

create table if not exists trm_in2_disc_std_groupclassifs
(
  nomenclature_id int default 0 not null,
  discount_id bigint default 0 not null,
  classif_id varchar(40) default '0' not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_id, classif_id)
);

create index version
  on trm_in2_disc_std_groupclassifs (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_groupitems
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  itemgroup int default 0 not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, itemgroup, item)
);

create index item
  on trm_in2_disc_std_groupitems (nomenclature_id, discount_type, item);

create index version
  on trm_in2_disc_std_groupitems (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_item_prop
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  property_code_amount varchar(40) default '' not null,
  property_code_quantity varchar(40) default '' not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type)
);

create index version
  on trm_in2_disc_std_item_prop (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_item_prop_amount
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  property_value_const varchar(100) default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, property_value_const, amount)
);

create index version
  on trm_in2_disc_std_item_prop_amount (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_item_prop_quantity
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  property_value_const varchar(100) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, property_value_const, quantity)
);

create index version
  on trm_in2_disc_std_item_prop_quantity (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_items_n_plus_m
(
  nomenclature_id int default 0 not null,
  item varchar(40) default '' not null,
  discount_type bigint default 0 not null,
  quantity_paid decimal(20,4) default 0.0000 not null,
  quantity_without_paid decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item, discount_type)
);

create index version
  on trm_in2_disc_std_items_n_plus_m (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_itemsgroup
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  modificator varchar(40) default '' not null,
  sequence smallint(5) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, id)
);

create index version
  on trm_in2_disc_std_itemsgroup (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_std_list_basket
(
  nomenclature_id int default 0 not null,
  discount_id bigint default 0 not null,
  list_id int default 0 not null,
  quantity decimal(20,4) null,
  and_more tinyint(1) default 0 null,
  limit_summ decimal(20,4) null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_id, list_id)
);

create index version
  on trm_in2_disc_std_list_basket (nomenclature_id, version, deleted);

create table if not exists trm_in2_disc_store_items
(
  store_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  type tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, discount_type, item)
);

create index version
  on trm_in2_disc_store_items (store_id, version, deleted);

create table if not exists trm_in2_discount_adder_type
(
  global_id tinyint unsigned default 0 not null,
  account_type_id int(11) unsigned default 0 not null,
  automatic tinyint(1) default 0 not null,
  calculated datetime null,
  must_calculated datetime null,
  start datetime default '0000-00-00 00:00:00' not null,
  launch_type int default 0 not null,
  interval_algorithm int default 0 not null,
  period_type int(11) unsigned default 0 not null,
  period_value int(11) unsigned default 0 not null,
  range_type int(11) unsigned default 0 not null,
  range_value int(11) unsigned default 0 not null,
  delay_type int(11) unsigned default 0 not null,
  delay_value int(11) unsigned default 0 not null,
  function_type int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, account_type_id)
);

create index automatic
  on trm_in2_discount_adder_type (automatic);

create index version
  on trm_in2_discount_adder_type (global_id, version, deleted);

create table if not exists trm_in2_discount_card
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  type_id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) default '' not null,
  name varchar(40) null,
  date_from datetime null,
  date_till datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in2_discount_card (global_id, type_id, start_card_code, stop_card_code);

create index single_card
  on trm_in2_discount_card (start_card_code, deleted);

create index version
  on trm_in2_discount_card (global_id, version, deleted);

create table if not exists trm_in2_discount_card_personal
(
  global_id tinyint unsigned default 0 not null,
  discount_type bigint not null,
  card_number varchar(40) not null,
  modificator varchar(40) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, card_number)
);

create index version
  on trm_in2_discount_card_personal (global_id, version, deleted);

create table if not exists trm_in2_discount_card_siebel
(
  global_id tinyint unsigned default 0 not null,
  id varchar(40) not null,
  card_status varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_discount_card_siebel (global_id, version, deleted);

create table if not exists trm_in2_discount_card_stop_list
(
  global_id tinyint unsigned default 0 not null,
  id varchar(40) default '' not null,
  type_id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in2_discount_card_stop_list (global_id, type_id, start_card_code, stop_card_code);

create index single_card
  on trm_in2_discount_card_stop_list (start_card_code, deleted);

create index version
  on trm_in2_discount_card_stop_list (global_id, version, deleted);

create table if not exists trm_in2_discount_card_type
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  system_id int(11) unsigned default 0 not null,
  name varchar(40) null,
  numberlen int(11) unsigned default 0 not null,
  single_use tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index `system`
  on trm_in2_discount_card_type (global_id, system_id);

create index version
  on trm_in2_discount_card_type (global_id, version, deleted);

create table if not exists trm_in2_discount_system
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  name varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_discount_system (global_id, version, deleted);

create table if not exists trm_in2_discount_types
(
  global_id int default 0 not null,
  id bigint default 0 not null,
  advertising_campaign_id bigint default 0 not null,
  property bigint unsigned default 0 not null,
  name varchar(40) default '' not null,
  efts int default 0 not null,
  param mediumtext null,
  sequence int default 0 not null,
  group_type int null,
  card_type int(11) unsigned null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  account_type int null,
  account_summ decimal(20,2) null,
  receipt_summ decimal(20,2) null,
  in_birth_month tinyint(1) default 0 null,
  days_before int null,
  days_after int null,
  application_condition text not null,
  balance_condition tinyint(1) default 0 not null,
  receipt_amount_condition tinyint(1) default 0 not null,
  use_action_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index advertising_campaign_id
  on trm_in2_discount_types (advertising_campaign_id);

create index global_id
  on trm_in2_discount_types (global_id, version, deleted);

create table if not exists trm_in2_discount_types_classifclient
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  classif_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, classif_id)
);

create index version
  on trm_in2_discount_types_classifclient (global_id, version, deleted);

create table if not exists trm_in2_discount_types_client
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, client)
);

create index deleted
  on trm_in2_discount_types_client (discount_type, global_id, deleted);

create index version
  on trm_in2_discount_types_client (global_id, version, deleted);

create table if not exists trm_in2_discount_types_gifts
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '' not null,
  amount decimal(20,3) default 0.000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, item)
);

create index version
  on trm_in2_discount_types_gifts (nomenclature_id, version, deleted);

create table if not exists trm_in2_discount_types_items
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '' not null,
  amount decimal(20,3) default 0.000 not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, item, amount, quantity)
);

create index version
  on trm_in2_discount_types_items (nomenclature_id, version, deleted);

create table if not exists trm_in2_discount_types_lists
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  list int default 0 not null,
  amount decimal(20,3) default 0.000 not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, list, amount, quantity)
);

create index version
  on trm_in2_discount_types_lists (nomenclature_id, version, deleted);

create table if not exists trm_in2_discount_types_prods
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  list int default 0 not null,
  begin_offset int(4) default 0 null,
  finish_offset int(4) default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, list)
);

create index version
  on trm_in2_discount_types_prods (nomenclature_id, version, deleted);

create table if not exists trm_in2_discount_types_results
(
  global_id int default 0 not null,
  discount_type_id bigint default 0 not null,
  efts varchar(38) default '' not null,
  params text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type_id, efts)
);

create table if not exists trm_in2_discount_types_vars
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  discount_type bigint default 0 not null,
  var varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index discount_type
  on trm_in2_discount_types_vars (nomenclature_id, discount_type, var);

create index version
  on trm_in2_discount_types_vars (nomenclature_id, version, deleted);

create table if not exists trm_in2_discount_types_where
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  store_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, store_id)
);

create index version
  on trm_in2_discount_types_where (global_id, version, deleted);

create table if not exists trm_in2_dishes
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  short_name varchar(40) default '' not null,
  comment varchar(128) default '' not null,
  recipe varchar(128) default '' not null,
  show_additives tinyint(1) default 1 not null,
  min_additives int null,
  max_free_additives int null,
  max_additives int null,
  printer tinyint unsigned null,
  is_complex tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in2_dishes (nomenclature_id, version, deleted);

create table if not exists trm_in2_equarings
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  efts int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_equarings (store_id, version, deleted);

create table if not exists trm_in2_external_order_items
(
  store_id int not null comment 'Идентификатор магазина',
  order_id bigint not null comment 'Уникальный номер заказа, выданный внешней системой',
  item varchar(40) not null comment 'Артикул товара',
  quantity decimal(20,4) not null comment 'Количество',
  price decimal(20,4) not null comment 'Цена',
  min_price decimal(20,4) not null comment 'Мин. цена',
  changeable tinyint(1) not null comment 'Изменяемая позиция',
  fix_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, order_id, item)
);

create table if not exists trm_in2_external_order_refusals
(
  global_id int default 0 not null,
  id int default 0 not null,
  title varchar(255) not null,
  can_comment tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_external_order_refusals (global_id, version, deleted);

create table if not exists trm_in2_external_orders
(
  store_id int not null comment 'Идентификатор магазина',
  order_id bigint not null comment 'Уникальный номер заказа, выданный внешней системой',
  ext_order_type varchar(100) not null comment 'Название системы, в которой заказ был сформирован',
  ext_order_id varchar(100) not null comment 'Номер заказа, сформированный системой, в которой делался заказ',
  order_date datetime null comment 'Дата заказа',
  status tinyint(5) not null comment 'Статус заказа',
  status_change_date datetime null comment 'Дата перехода в последний статус',
  sale_type tinyint(1) not null comment 'Оплата заказа на кассе/отложенная оплата',
  wholesale tinyint(1) not null comment 'Оптовый/розничный',
  client_name varchar(255) not null comment 'ФИО клиента',
  check_structure tinyint(1) not null comment 'Флаг необходимость проверки содержания заказа при оплате',
  changeable tinyint(1) not null comment 'Флаг - разрешено на кассе изменять содержание заказа или не разрешено',
  amount decimal(20,4) not null comment 'Cумма заказа',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, order_id),
  constraint external_orders_ext_uniq
    unique (ext_order_type, ext_order_id)
);

create index store_id_ext_order_id
  on trm_in2_external_orders (store_id, ext_order_id, status, deleted);

create table if not exists trm_in2_external_system
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  type varchar(20) default '0' not null,
  param mediumtext null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_external_system (store_id, version, deleted);

create table if not exists trm_in2_goods_search_stores
(
  store_id int default 0 not null
    primary key,
  search_store_id text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in2_goods_search_stores (store_id, version, deleted);

create table if not exists trm_in2_input_template_fields
(
  global_id int default 0 not null,
  input_template bigint unsigned default 0 not null,
  field bigint unsigned default 0 not null,
  value text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, input_template, field)
);

create index version
  on trm_in2_input_template_fields (global_id, version, deleted);

create table if not exists trm_in2_input_template_ranges
(
  global_id int default 0 not null,
  id int default 0 not null,
  input_template bigint unsigned default 0 not null,
  value varchar(255) default '' not null,
  start varchar(255) default '' not null,
  stop varchar(255) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id),
  constraint k1
    unique (input_template, value, start, stop)
);

create index version
  on trm_in2_input_template_ranges (global_id, version, deleted);

create table if not exists trm_in2_input_template_stores
(
  store_id bigint unsigned default 0 not null,
  input_template_id bigint default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, input_template_id)
);

create index version
  on trm_in2_input_template_stores (store_id, version, deleted);

create table if not exists trm_in2_input_templates
(
  global_id int default 0 not null,
  id bigint unsigned default 0 not null,
  name varchar(40) default '' not null,
  template text not null,
  keyboard tinyint(1) unsigned default 0 not null,
  mcr tinyint(1) unsigned default 0 not null,
  scanner tinyint(1) unsigned default 0 not null,
  event bigint unsigned default 0 not null,
  priority bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index priority
  on trm_in2_input_templates (priority);

create index version
  on trm_in2_input_templates (global_id, version, deleted);

create table if not exists trm_in2_inquirer
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  name varchar(40) default '' not null,
  show_inq_on tinyint(1) default 0 not null,
  question text not null,
  date_from date null,
  date_to date null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index global_id
  on trm_in2_inquirer (global_id, version, deleted);

create table if not exists trm_in2_inquirer_answer
(
  global_id int default 0 not null,
  inquirer_id varchar(40) default '' not null,
  id varchar(40) default '' not null,
  sequence int default 0 not null,
  answer varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, inquirer_id, id)
);

create index global_id
  on trm_in2_inquirer_answer (global_id, version, deleted);

create table if not exists trm_in2_inquirer_store
(
  store_id int default 0 not null,
  inquirer_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, inquirer_id)
);

create index version
  on trm_in2_inquirer_store (store_id, version, deleted);

create table if not exists trm_in2_item_agent
(
  nomenclature_id int not null,
  item_id varchar(40) not null,
  tag_1226 bigint not null,
  tax_group_id int null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item_id)
);

create index versions
  on trm_in2_item_agent (nomenclature_id, version, deleted);

create table if not exists trm_in2_item_cc
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  gtd varchar(40) null,
  country varchar(40) null,
  certification varchar(40) null,
  certification_date varchar(40) null,
  producer_marking varchar(100) null,
  summary varchar(100) null,
  structure text null,
  substructure varchar(100) null,
  exp_date datetime null,
  realization_time int null,
  qdoc_org varchar(40) null,
  qdoc_id varchar(40) null,
  qdoc_date datetime null,
  qdoc_exp datetime null,
  hdoc_org varchar(40) null,
  hdoc_id varchar(40) null,
  hdoc_date datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in2_item_cc (nomenclature_id, version, deleted);

create table if not exists trm_in2_item_classif_egais
(
  nomenclature_id int not null,
  id varchar(40) not null,
  excise tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index versions
  on trm_in2_item_classif_egais (nomenclature_id, version, deleted);

create table if not exists trm_in2_item_properties
(
  nomenclature_id int default 0 not null,
  item_id varchar(40) default '' not null,
  code varchar(20) default '' not null,
  id int(11) unsigned default 0 not null,
  sequence smallint(5) unsigned default 0 not null,
  description text not null,
  flags int unsigned default 0 not null,
  cookies int unsigned null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item_id, code)
);

create index sequence
  on trm_in2_item_properties (nomenclature_id, item_id, sequence);

create index version
  on trm_in2_item_properties (nomenclature_id, version, deleted);

create table if not exists trm_in2_item_properties_type
(
  nomenclature_id int default 0 not null,
  item_id varchar(40) default '' not null,
  property_code varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item_id, property_code)
);

create index version
  on trm_in2_item_properties_type (nomenclature_id, version, deleted);

create table if not exists trm_in2_item_property_values
(
  nomenclature_id int default 0 not null,
  unique_item_id bigint not null,
  property_code varchar(40) default '' not null,
  property_id int unsigned default 0 not null,
  sequence smallint(5) unsigned default 0 not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, unique_item_id, property_code)
);

create index group_property
  on trm_in2_item_property_values (nomenclature_id, property_code, property_id);

create index version
  on trm_in2_item_property_values (nomenclature_id, version, deleted);

create table if not exists trm_in2_item_protection
(
  nomenclature_id int default 0 not null,
  item varchar(40) default '' not null,
  protection_code varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item, protection_code)
);

create index version
  on trm_in2_item_protection (nomenclature_id, version, deleted);

create table if not exists trm_in2_items
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  unique_item_id bigint not null,
  name varchar(255) default '' not null,
  descr text not null,
  measure varchar(40) default '' not null,
  measure_iso varchar(5) null,
  measprec int default 0 not null,
  classif varchar(40) default '0' not null,
  prop int default 0 not null,
  tax int null,
  weight decimal(20,4) null,
  length int(11) unsigned null,
  height int(11) unsigned null,
  width int(11) unsigned null,
  quantity_shipping decimal(20,4) null,
  sku decimal(20,4) null,
  alcohol tinyint(3) null,
  country varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index classif
  on trm_in2_items (nomenclature_id, classif, deleted);

create index id
  on trm_in2_items (classif, id);

create index name
  on trm_in2_items (classif, name);

create index nomenclature_id
  on trm_in2_items (nomenclature_id, id, deleted);

create index unique_item_id
  on trm_in2_items (unique_item_id);

create index version
  on trm_in2_items (nomenclature_id, version, deleted);

create table if not exists trm_in2_items_egais
(
  nomenclature_id int not null,
  id varchar(40) not null,
  excise tinyint(1) default 0 not null,
  version int not null,
  deleted tinyint(1) not null,
  primary key (nomenclature_id, id)
);

create index versions
  on trm_in2_items_egais (nomenclature_id, version, deleted);

create table if not exists trm_in2_items_images
(
  nomenclature_id int not null,
  id varchar(40) not null,
  image mediumblob not null,
  version int not null,
  deleted tinyint(1) not null,
  primary key (nomenclature_id, id)
);

create table if not exists trm_in2_items_sets
(
  nomenclature_id int default 0 not null,
  set_id varchar(40) default '' not null,
  item_id varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, set_id, item_id)
);

create index version
  on trm_in2_items_sets (nomenclature_id, version, deleted);

create table if not exists trm_in2_items_stocks
(
  store_id int default 0 not null,
  item varchar(40) default '' not null,
  stock int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, item, stock)
);

create index version
  on trm_in2_items_stocks (store_id, version, deleted);

create table if not exists trm_in2_keyboard
(
  keyboard_layout_id int(10) default 0 not null,
  id int default 0 not null,
  ace bigint unsigned default 0 not null,
  command int default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, id)
);

create index version
  on trm_in2_keyboard (keyboard_layout_id, version, deleted);

create table if not exists trm_in2_keyboard_msrs
(
  keyboard_layout_id int default 0 not null,
  id int default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, id)
);

create index version
  on trm_in2_keyboard_msrs (keyboard_layout_id, version, deleted);

create table if not exists trm_in2_keyboard_scanners
(
  keyboard_layout_id int default 0 not null,
  id int default 0 not null,
  scanner_type int(10) default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, id)
);

create index version
  on trm_in2_keyboard_scanners (keyboard_layout_id, version, deleted);

create table if not exists trm_in2_keylock
(
  config_hardware_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  command bigint unsigned default 0 not null,
  param varchar(50) default '' not null,
  version int(11) unsigned default 0 not null,
  deleted tinyint(1) unsigned default 0 not null,
  primary key (config_hardware_id, id)
);

create index version
  on trm_in2_keylock (config_hardware_id, version, deleted);

create table if not exists trm_in2_limitation
(
  nomenclature_id int default 0 not null,
  id int(11) unsigned auto_increment,
  name varchar(50) default '' not null,
  type int default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index id
  on trm_in2_limitation (id);

create table if not exists trm_in2_limitation_and_alcohol
(
  nomenclature_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  alcohol tinyint(3) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, limitation_id, alcohol)
);

create table if not exists trm_in2_limitation_in_classif
(
  nomenclature_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  classif_id varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, limitation_id, classif_id)
);

create table if not exists trm_in2_limitation_in_item
(
  nomenclature_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  item_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, limitation_id, item_id)
);

create table if not exists trm_in2_limitation_in_pos
(
  cash_id bigint unsigned not null,
  limitation_id int(11) unsigned not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, limitation_id)
);

create table if not exists trm_in2_limitation_in_store
(
  store_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, limitation_id)
);

create table if not exists trm_in2_list
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  type tinyint(1) default 0 not null,
  name varchar(128) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in2_list (nomenclature_id, version, deleted);

create table if not exists trm_in2_list_items
(
  nomenclature_id int default 0 not null,
  list int default 0 not null,
  item varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, list, item)
);

create index item
  on trm_in2_list_items (nomenclature_id, item);

create index version
  on trm_in2_list_items (nomenclature_id, version, deleted);

create table if not exists trm_in2_luaukm
(
  global_id int default 0 not null,
  profile_id int default 1 not null,
  name varchar(50) default '' not null,
  value mediumtext not null,
  edit tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, name, profile_id)
);

create index version
  on trm_in2_luaukm (global_id, version, deleted);

create table if not exists trm_in2_luaukm_profile
(
  global_id int default 0 not null comment 'глобальный id',
  profile_id int(11) unsigned default 1 not null comment 'id профиля',
  profile_name tinytext not null comment 'имя профиля',
  active tinyint(1) default 0 not null comment 'текущий активный профиль. может быть только один',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, profile_id)
);

create index version
  on trm_in2_luaukm_profile (global_id, profile_id, version, deleted);

create table if not exists trm_in2_marketing_effort
(
  global_id tinyint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  title varchar(100) default '' not null,
  date_from datetime null,
  date_to datetime null,
  matrix mediumtext null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id),
  constraint id
    unique (id)
);

create index version
  on trm_in2_marketing_effort (global_id, version, deleted);

create table if not exists trm_in2_marketing_effort_in_cashline
(
  store_id int default 0 not null,
  marketing_effort_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, marketing_effort_id)
);

create index cash_line_id
  on trm_in2_marketing_effort_in_cashline (store_id);

create index marketing_effort_id
  on trm_in2_marketing_effort_in_cashline (marketing_effort_id);

create index version
  on trm_in2_marketing_effort_in_cashline (store_id, version, deleted);

create table if not exists trm_in2_measure_iso
(
  nomenclature_id int default 0 not null,
  id varchar(5) default '' not null,
  code varchar(40) default '' not null,
  `desc` varchar(40) default '' not null,
  category int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in2_measure_iso (nomenclature_id, version, deleted);

create table if not exists trm_in2_media_picture
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  data mediumblob not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_media_picture (global_id, version, deleted);

create table if not exists trm_in2_media_playlist
(
  global_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  date_from date null,
  date_to date null,
  timeout_sec int default 10 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_media_playlist (global_id, version, deleted);

create table if not exists trm_in2_media_playlist_picture
(
  global_id int default 0 not null,
  playlist_id int default 0 not null,
  picture_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, playlist_id, picture_id)
);

create index version
  on trm_in2_media_playlist_picture (global_id, version, deleted);

create table if not exists trm_in2_media_playlist_store
(
  store_id int default 0 not null,
  playlist_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, playlist_id)
);

create index version
  on trm_in2_media_playlist_store (store_id, version, deleted);

create table if not exists trm_in2_menu
(
  terminal_menu_id int(10) default 0 not null,
  id int default 0 not null,
  owner int default 0 not null,
  name varchar(80) default '' not null,
  ace bigint unsigned default 0 not null,
  command int default 0 not null,
  param text not null,
  hide_on_no_permission tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (terminal_menu_id, id)
);

create index version
  on trm_in2_menu (terminal_menu_id, version, deleted);

create table if not exists trm_in2_nomenclature
(
  nomenclature_id int default 0 not null
    primary key,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in2_nomenclature (nomenclature_id, version, deleted);

create table if not exists trm_in2_offline_account
(
  global_id tinyint unsigned default 0 not null,
  id int default 0 not null,
  account_type_id int(11) unsigned default 0 not null,
  name varchar(128) default '' not null,
  balance decimal(20,2) default 0.00 not null,
  credit decimal(20,2) default 0.00 not null,
  params varchar(255) null,
  closed datetime null,
  active tinyint(1) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index account_type_params
  on trm_in2_offline_account (account_type_id, params);

create index version
  on trm_in2_offline_account (global_id, version, deleted);

create table if not exists trm_in2_order_login
(
  store_id int default 0 not null,
  id int default 0 not null,
  user_id varchar(40) default '' not null,
  date datetime default '0000-00-00 00:00:00' not null,
  address varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_order_login (store_id, version, deleted);

create table if not exists trm_in2_order_order
(
  store_id int default 0 not null,
  id varchar(20) default '' not null,
  login_id int default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  property_code varchar(20) default '0' not null,
  changeable tinyint(1) default 1 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_order_order (store_id, version, deleted);

create table if not exists trm_in2_order_var
(
  store_id int default 0 not null,
  order_id varchar(20) default '' not null,
  position int default 0 not null,
  var varchar(40) default '' not null,
  manual tinyint(1) default 0 not null,
  quantity decimal(20,4) default 0.0000 not null,
  stock int null,
  price decimal(20,4) null,
  minprice decimal(20,4) null,
  fix_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, order_id, position)
);

create index version
  on trm_in2_order_var (store_id, version, deleted);

create table if not exists trm_in2_pallet
(
  nomenclature_id int default 0 not null,
  pallet_code varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, pallet_code)
);

create index version
  on trm_in2_pallet (nomenclature_id, version, deleted);

create table if not exists trm_in2_payment_range_param
(
  store_id int default 0 not null,
  range_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, range_id)
);

create index version
  on trm_in2_payment_range_param (store_id, version, deleted);

create table if not exists trm_in2_payments
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  param text null,
  equaring_id int default 0 not null,
  old_efts int default 0 not null,
  is_cash tinyint(1) default 1 not null,
  kkt_payment_form tinyint(1) default 0 not null,
  limitation_disc_type_id bigint null,
  allow_edit_limitation tinyint(1) default 1 not null,
  return_by_cash_forbidden tinyint(1) default 0 not null,
  payments_allowed tinyint(1) default 0 not null,
  increment_account text null,
  discount_disc_type_id bigint null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_payments (store_id, version, deleted);

create table if not exists trm_in2_payments_classifclient
(
  store_id int default 0 not null,
  payment int default 0 not null,
  classif_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, payment, classif_id)
);

create index version
  on trm_in2_payments_classifclient (store_id, version, deleted);

create table if not exists trm_in2_payments_client
(
  store_id int default 0 not null,
  payment int default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, payment, client)
);

create index version
  on trm_in2_payments_client (store_id, version, deleted);

create table if not exists trm_in2_pch_serv
(
  pch_serv_id int default 0 not null,
  id int default 0 not null,
  guid varchar(40) default '' not null,
  number int default 0 not null,
  name varchar(80) default '' not null,
  store_id int default 0 not null,
  register_date datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pch_serv_id, id)
);

create index version
  on trm_in2_pch_serv (pch_serv_id, version, deleted);

create table if not exists trm_in2_pch_types
(
  pch_serv_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  xml text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pch_serv_id, id)
);

create index version
  on trm_in2_pch_types (pch_serv_id, version, deleted);

create table if not exists trm_in2_pixmap
(
  global_id int default 0 not null,
  id int default 0 not null,
  data mediumblob not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_pixmap (global_id, version, deleted);

create table if not exists trm_in2_pos
(
  cash_id bigint unsigned default 0 not null
    primary key,
  guid varchar(40) default '' not null,
  pos_version int unsigned default 0 not null,
  number int default 0 not null,
  kkm_number varchar(40) null,
  register_number varchar(40) null,
  name varchar(80) default '' not null,
  store_id int default 0 not null,
  config_group_id int(10) null,
  enable_auto_update int default 0 null,
  active tinyint(1) default 1 not null,
  register_date datetime null,
  pos_type int(2) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in2_pos (cash_id, version, deleted);

create table if not exists trm_in2_pos_cw77f_regparams
(
  cash_id int not null
    primary key,
  `values` varchar(1024) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in2_pos_cw77f_regparams (cash_id, version, deleted);

create table if not exists trm_in2_pricelist
(
  pricelist_id int(11) unsigned default 0 not null
    primary key,
  nomenclature_id int default 0 not null,
  name varchar(100) default '' not null,
  date_from datetime null,
  date_to datetime null,
  active tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  constraint nomenclature
    unique (nomenclature_id, pricelist_id)
);

create index version
  on trm_in2_pricelist (pricelist_id, version, deleted);

create table if not exists trm_in2_pricelist_items
(
  pricelist_id int(11) unsigned default 0 not null,
  item varchar(40) default '' not null,
  nomenclature_id int default 0 not null,
  price decimal(20,4) default 0.0000 not null,
  minprice decimal(20,4) default 0.0000 not null,
  is_promo_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pricelist_id, item, nomenclature_id),
  constraint nomenclature
    unique (nomenclature_id, pricelist_id, item)
);

create index version
  on trm_in2_pricelist_items (pricelist_id, version, deleted);

create table if not exists trm_in2_pricelist_items_ext
(
  pricelist_id int(11) unsigned not null,
  item varchar(40) not null,
  nomenclature_id int not null,
  pos int not null,
  price decimal(20,4) not null,
  minprice decimal(20,4) null,
  descr varchar(100) default '' null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pricelist_id, nomenclature_id, item, pos)
);

create index version
  on trm_in2_pricelist_items_ext (pricelist_id, version, deleted);

create table if not exists trm_in2_pricelist_var
(
  pricelist_id int(11) unsigned default 0 not null,
  var varchar(40) default '' not null,
  nomenclature_id int default 0 not null,
  price decimal(20,4) default 0.0000 not null,
  min_price decimal(20,4) default 0.0000 not null,
  is_promo_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pricelist_id, var, nomenclature_id),
  constraint nomenclature
    unique (nomenclature_id, pricelist_id, var)
);

create index var
  on trm_in2_pricelist_var (nomenclature_id, var);

create index version
  on trm_in2_pricelist_var (pricelist_id, version, deleted);

create table if not exists trm_in2_pricetype
(
  global_id int default 0 not null,
  id varchar(38) default '' not null,
  name varchar(100) default '' not null,
  active tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_pricetype (global_id, version, deleted);

create table if not exists trm_in2_pricetype_pricelist
(
  store_id int default 0 not null,
  pricetype varchar(38) default '' not null,
  pricelist int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, pricetype, pricelist)
);

create index version
  on trm_in2_pricetype_pricelist (store_id, version, deleted);

create table if not exists trm_in2_properties
(
  nomenclature_id int default 0 not null,
  code varchar(40) default '' not null,
  name varchar(40) default '' not null,
  flags int unsigned default 0 not null,
  description text null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, code)
);

create index version
  on trm_in2_properties (nomenclature_id, version, deleted);

create table if not exists trm_in2_property_values
(
  nomenclature_id int default 0 not null,
  property_code varchar(40) default '' not null,
  id int unsigned default 0 not null,
  const varchar(100) null,
  description text not null,
  comment text null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, property_code, id)
);

create index property_values
  on trm_in2_property_values (nomenclature_id, property_code, const, deleted);

create index version
  on trm_in2_property_values (nomenclature_id, version, deleted);

create table if not exists trm_in2_rdish
(
  nomenclature_id int default 0 not null,
  rmenu bigint default 0 not null,
  dish varchar(40) default '' not null,
  additivitygroup int default 0 not null,
  show_order int null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, rmenu, dish, additivitygroup)
);

create index rmenu_dish_show_order
  on trm_in2_rdish (rmenu, dish, show_order);

create index version
  on trm_in2_rdish (nomenclature_id, version, deleted);

create table if not exists trm_in2_restaurant_tables
(
  store_id int default 0 not null,
  id int default 0 not null,
  description varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_restaurant_tables (store_id, version, deleted);

create table if not exists trm_in2_rmenus
(
  nomenclature_id int default 0 not null,
  id bigint default 0 not null,
  name varchar(40) default '' not null,
  role bigint unsigned null,
  type tinyint default 0 not null,
  owner bigint null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  show_at int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index owner
  on trm_in2_rmenus (owner, type);

create index version
  on trm_in2_rmenus (nomenclature_id, version, deleted);

create table if not exists trm_in2_rmenus_images
(
  nomenclature_id int not null,
  id bigint not null,
  image mediumblob not null,
  version int not null,
  deleted tinyint(1) not null,
  primary key (nomenclature_id, id)
);

create table if not exists trm_in2_role
(
  global_id tinyint unsigned default 0 not null,
  id bigint unsigned auto_increment,
  title varchar(100) default '' not null,
  lillo_screen varchar(38) null,
  order_panel varchar(38) null,
  role_entry text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index id
  on trm_in2_role (id);

create index version
  on trm_in2_role (global_id, version, deleted);

create table if not exists trm_in2_role_permission
(
  global_id tinyint unsigned default 0 not null,
  role_id bigint unsigned default 0 not null,
  entry_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, role_id, entry_id)
);

create index role_id
  on trm_in2_role_permission (role_id);

create index version
  on trm_in2_role_permission (global_id, version, deleted);

create table if not exists trm_in2_screen
(
  global_id int default 0 not null,
  id varchar(38) default '' not null,
  content longtext not null,
  friendly_name varchar(255) default '' not null,
  group_id varchar(38) default '' not null,
  screen_type int(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_screen (global_id, version, deleted);

create table if not exists trm_in2_screen_group
(
  global_id int default 0 not null,
  id varchar(38) default '' not null,
  friendly_name varchar(255) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_screen_group (global_id, version, deleted);

create table if not exists trm_in2_sellers
(
  store_id int not null,
  seller_code varchar(50) not null,
  name varchar(50) not null,
  first_date date not null,
  last_date date null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, seller_code)
);

create index name
  on trm_in2_sellers (store_id, name);

create index version
  on trm_in2_sellers (store_id, version, deleted);

create table if not exists trm_in2_services
(
  store_id int default 0 not null,
  id varchar(40) default '' not null,
  name varchar(80) default '' not null,
  transfering_id int default 0 not null,
  check_request tinyint(1) default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_services (store_id, version, deleted);

create table if not exists trm_in2_social_moscow_items
(
  nomenclature_id int not null,
  id varchar(40) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in2_social_moscow_items (nomenclature_id, version, deleted);

create table if not exists trm_in2_sound_messages
(
  global_id int default 0 not null,
  id varchar(50) default '' not null,
  message text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in2_sound_messages (global_id, version, deleted);

create table if not exists trm_in2_stocks
(
  store_id int default 0 not null,
  id int default 0 not null,
  owner int default 0 not null,
  name varchar(80) default '' not null,
  address varchar(100) default '' not null,
  enterprisename varchar(100) default '' not null,
  inn varchar(40) default '' not null,
  fiscal tinyint(1) default 1 not null,
  printer int default 0 not null,
  taxsystem int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_stocks (store_id, version, deleted);

create table if not exists trm_in2_storage_place
(
  store_id int default 0 not null,
  id int default 0 not null,
  code varchar(40) default '' not null,
  description text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id),
  constraint code
    unique (code)
);

create index version
  on trm_in2_storage_place (store_id, version, deleted);

create table if not exists trm_in2_storage_place_items
(
  store_id int default 0 not null,
  nomenclature_id int default 0 not null,
  item_id varchar(40) not null,
  storage_place_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, nomenclature_id, item_id, storage_place_id)
);

create index version
  on trm_in2_storage_place_items (store_id, version, deleted);

create table if not exists trm_in2_store
(
  store_id int default 0 not null
    primary key,
  name varchar(80) default '' not null,
  server_id int default 0 not null,
  enterprise_name varchar(100) null,
  code_subdivision varchar(40) null,
  inn varchar(40) null,
  okpo varchar(100) null,
  okdp varchar(100) null,
  kpp varchar(100) null,
  bank varchar(100) null,
  static_rrn varchar(40) null,
  registration varchar(100) null,
  director varchar(100) null,
  accountant varchar(100) null,
  address varchar(255) null,
  phone varchar(100) null,
  enterprise_address varchar(100) null,
  remains_efts int null,
  remains_param text null,
  video_control_type varchar(40) null,
  area decimal(20,4) default 1.0000 not null,
  nomenclature_id int default 0 not null,
  pricetype_id varchar(38) default '' not null,
  currency_id int default 0 not null,
  lat decimal(10,6) null,
  lng decimal(10,6) null,
  director_position varchar(150) null,
  accountant_position varchar(150) null,
  topology_node int(11) unsigned not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in2_store (store_id, version, deleted);

create table if not exists trm_in2_store_images
(
  store_id int default 0 not null,
  id int default 0 not null,
  icon blob null,
  icon_fname varchar(40) null,
  photo mediumblob null,
  photo_fname varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_store_images (store_id, version, deleted);

create table if not exists trm_in2_store_rmenus
(
  nomenclature_id int default 0 not null,
  store int default 0 not null,
  rmenu bigint default 0 not null,
  default_settings tinyint(1) default 1 not null,
  role bigint null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, store, rmenu)
);

create index version
  on trm_in2_store_rmenus (nomenclature_id, version, deleted);

create table if not exists trm_in2_store_rmenus_active
(
  nomenclature_id int not null,
  store_id int not null,
  rmenu bigint not null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (nomenclature_id, store_id, rmenu)
);

create table if not exists trm_in2_store_rmenus_settings
(
  nomenclature_id int not null,
  store_id int not null,
  use_defaults tinyint(1) default 1 not null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (nomenclature_id, store_id)
);

create table if not exists trm_in2_tare
(
  nomenclature_id int not null,
  id int(11) unsigned not null,
  name varchar(26) not null,
  weight int(4) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create table if not exists trm_in2_taxes
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  name char(40) default '' not null,
  priority tinyint default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index i1
  on trm_in2_taxes (id, priority);

create index i2
  on trm_in2_taxes (priority, id);

create index priority
  on trm_in2_taxes (priority);

create index version
  on trm_in2_taxes (nomenclature_id, version, deleted);

create table if not exists trm_in2_taxgroup
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  tax_id int default 0 not null,
  percent varchar(20) default '0' not null,
  fp_code varchar(20) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id, tax_id)
);

create index tax_id
  on trm_in2_taxgroup (tax_id, id);

create index version
  on trm_in2_taxgroup (nomenclature_id, version, deleted);

create table if not exists trm_in2_transferings
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  epts int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in2_transferings (store_id, version, deleted);

create table if not exists trm_in2_users
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  user_inn varchar(20) null,
  password varchar(48) default '' not null,
  role_id bigint unsigned null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index role_id
  on trm_in2_users (role_id);

create index version
  on trm_in2_users (store_id, version, deleted);

create table if not exists trm_in2_var
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  tare_weight decimal(20,4) default 0.0000 not null,
  feat1 varchar(40) default '' not null,
  feat2 varchar(40) default '' not null,
  feat3 varchar(40) default '' not null,
  stock int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index item
  on trm_in2_var (nomenclature_id, item, deleted);

create index versions
  on trm_in2_var (nomenclature_id, version, deleted);

create table if not exists trm_in2_var_fur_identity
(
  nomenclature_id int default 0 not null,
  var_id varchar(40) default '' not null,
  fur_identity varchar(40) default '' not null,
  version bigint unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, var_id, fur_identity)
);

create index version
  on trm_in2_var_fur_identity (nomenclature_id, version, deleted);

create table if not exists trm_in2_var_property_values
(
  nomenclature_id int default 0 not null,
  var_id varchar(40) default '' not null,
  property_code varchar(40) default '' not null,
  property_id int unsigned default 0 not null,
  sequence smallint(5) unsigned default 0 not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, var_id, property_code)
);

create index group_property
  on trm_in2_var_property_values (nomenclature_id, property_code, property_id);

create index version
  on trm_in2_var_property_values (nomenclature_id, version, deleted);

create table if not exists trm_in2_working_life
(
  store_id int default 0 not null,
  id int unsigned default 0 not null,
  working_life datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id),
  constraint cash_line_id
    unique (store_id)
);

create index version
  on trm_in2_working_life (store_id, version, deleted);

create table if not exists trm_in_account_type
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  name varchar(40) null,
  increase_type int default 0 not null,
  online tinyint(1) default 0 not null,
  type int default 1 not null,
  params varchar(40) default '' not null,
  integration_id int null,
  print_in_receipt tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_account_type (global_id, version, deleted);

create table if not exists trm_in_account_type_certificates
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  nominal decimal(20,4) null,
  mono_account tinyint(1) default 0 null,
  check_underpay tinyint(1) default 0 null,
  multi_sell tinyint(1) default 0 null,
  allow_return tinyint(1) default 1 null,
  use_pincode tinyint(1) default 0 not null,
  return_money tinyint(1) default 0 not null,
  check_store tinyint(1) default 0 not null,
  fixed_nominal tinyint(1) default 1 not null,
  min_nominal decimal(20,4) null,
  max_nominal decimal(20,4) null,
  nominal_multiplicity decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_account_type_certificates (global_id, version, deleted);

create table if not exists trm_in_account_type_certificates_items
(
  nomenclature_id int default 0 not null,
  account_type_id int(11) unsigned default 0 not null,
  item_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, account_type_id, item_id)
);

create index version
  on trm_in_account_type_certificates_items (nomenclature_id, version, deleted);

create table if not exists trm_in_additive_additivitygroup
(
  nomenclature_id int default 0 not null,
  additivitygroup int default 0 not null,
  additive varchar(40) default '' not null,
  show_order int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, additivitygroup, additive)
);

create index additivitygroup
  on trm_in_additive_additivitygroup (additivitygroup, show_order);

create index version
  on trm_in_additive_additivitygroup (nomenclature_id, version, deleted);

create table if not exists trm_in_additivitygroups
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in_additivitygroups (nomenclature_id, version, deleted);

create table if not exists trm_in_advertising_campaign
(
  global_id tinyint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  marketing_effort bigint unsigned default 0 not null,
  title varchar(100) default '' not null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id),
  constraint id
    unique (id)
);

create index marketing_effort
  on trm_in_advertising_campaign (marketing_effort);

create index version
  on trm_in_advertising_campaign (global_id, version, deleted);

create table if not exists trm_in_agent
(
  global_id int default 0 not null,
  tag_1226 bigint not null,
  tag_1222 tinyint(3) null,
  tag_1073 varchar(25) null,
  tag_1044 varchar(25) null,
  tag_1016 bigint null,
  tag_1026 varchar(70) null,
  tag_1075 varchar(25) null,
  tag_1005 varchar(256) null,
  tag_1074 varchar(25) null,
  tag_1225 varchar(256) null,
  tag_1171 varchar(25) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, tag_1226)
);

create index versions
  on trm_in_agent (global_id, version, deleted);

create table if not exists trm_in_appeal_reason
(
  nomenclature_id int default 0 not null,
  code varchar(40) default '' not null,
  title varchar(100) default '' not null,
  can_comment tinyint(1) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, code)
);

create index version
  on trm_in_appeal_reason (nomenclature_id, version, deleted);

create table if not exists trm_in_auth_servers
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  efts int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_auth_servers (store_id, version, deleted);

create table if not exists trm_in_available_receipt_item_properties
(
  nomenclature_id int default 0 not null,
  property_code varchar(20) default '' not null,
  value_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, property_code, value_id)
);

create index version
  on trm_in_available_receipt_item_properties (nomenclature_id, version, deleted);

create table if not exists trm_in_bp
(
  store_id int default 0 not null,
  bp varchar(40) default '' not null,
  title varchar(100) default '' not null,
  remains tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, bp)
);

create index version
  on trm_in_bp (store_id, version, deleted);

create table if not exists trm_in_bp_role
(
  store_id int default 0 not null,
  bp varchar(40) default '' not null,
  role_id bigint unsigned not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, bp, role_id)
);

create index version
  on trm_in_bp_role (store_id, version, deleted);

create table if not exists trm_in_cancellation_reason
(
  global_id int default 0 not null,
  id int default 0 not null,
  message text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_cancellation_reason (global_id, version, deleted);

create definer = ukm_server@localhost trigger trm_in_cancellation_reason_before_ins_tr
  before INSERT on trm_in_cancellation_reason
  for each row
-- missing source code
;

create table if not exists trm_in_card_client
(
  global_id int default 0 not null,
  card int(11) unsigned default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, card)
);

create index client
  on trm_in_card_client (client, deleted);

create index version
  on trm_in_card_client (global_id, version, deleted);

create table if not exists trm_in_card_stoplist
(
  global_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in_card_stoplist (start_card_code, stop_card_code);

create index version
  on trm_in_card_stoplist (global_id, version, deleted);

create table if not exists trm_in_card_stoplist_client
(
  global_id int default 0 not null,
  card_stoplist int(11) unsigned default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, card_stoplist)
);

create index client
  on trm_in_card_stoplist_client (client);

create index version
  on trm_in_card_stoplist_client (global_id, version, deleted);

create table if not exists trm_in_cards
(
  global_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) null,
  date_from datetime null,
  date_till datetime null,
  active tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in_cards (start_card_code, stop_card_code);

create index version
  on trm_in_cards (global_id, version, deleted);

create table if not exists trm_in_cash_auth_server
(
  cash_id bigint unsigned default 0 not null,
  auth_server_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, auth_server_id)
);

create index version
  on trm_in_cash_auth_server (cash_id, version, deleted);

create table if not exists trm_in_cash_equarings
(
  cash_id bigint unsigned default 0 not null,
  equaring_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, equaring_id)
);

create index version
  on trm_in_cash_equarings (cash_id, version, deleted);

create table if not exists trm_in_cash_messages
(
  store_id int default 0 not null,
  type tinyint(1) default 0 not null,
  id int default 0 not null,
  message text not null,
  date_from datetime null,
  date_to datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, type, id)
);

create index version
  on trm_in_cash_messages (store_id, version, deleted);

create table if not exists trm_in_cash_transferings
(
  cash_id bigint unsigned default 0 not null,
  transfering_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, transfering_id)
);

create index version
  on trm_in_cash_transferings (cash_id, version, deleted);

create table if not exists trm_in_classif
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '0' not null,
  owner varchar(40) default '0' not null,
  name varchar(80) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index name
  on trm_in_classif (name);

create index owner
  on trm_in_classif (nomenclature_id, owner, deleted);

create index version
  on trm_in_classif (nomenclature_id, version, deleted);

create table if not exists trm_in_classif_remains_config
(
  store_id int default 0 not null,
  classif varchar(40) default '0' not null,
  nomenclature_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, classif)
);

create table if not exists trm_in_classif_stocks
(
  store_id int default 0 not null,
  classif varchar(40) default '0' not null,
  stock int default 0 not null,
  nomenclature_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, classif)
);

create index version
  on trm_in_classif_stocks (store_id, version, deleted);

create table if not exists trm_in_classifclients
(
  global_id int default 0 not null,
  id int(11) unsigned default 0 not null,
  owner int(11) unsigned default 0 not null,
  name varchar(255) default '' not null,
  pricetype varchar(38) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index owner
  on trm_in_classifclients (owner);

create index version
  on trm_in_classifclients (global_id, version, deleted);

create table if not exists trm_in_client_items
(
  nomenclature_id int default 0 not null,
  item varchar(40) default '' not null,
  client varchar(40) default '' not null,
  client_item varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item, client)
);

create index version
  on trm_in_client_items (nomenclature_id, version, deleted);

create table if not exists trm_in_client_marketingeffort
(
  global_id int default 0 not null,
  client varchar(40) default '' not null,
  marketingeffort int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, client)
);

create index marketingeffort
  on trm_in_client_marketingeffort (marketingeffort);

create index version
  on trm_in_client_marketingeffort (global_id, version, deleted);

create table if not exists trm_in_clientclassif_marketingeffort
(
  global_id int default 0 not null,
  clientclassif int(11) unsigned default 0 not null,
  marketingeffort int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, clientclassif)
);

create index marketingeffort
  on trm_in_clientclassif_marketingeffort (marketingeffort);

create index version
  on trm_in_clientclassif_marketingeffort (global_id, version, deleted);

create table if not exists trm_in_cliententerprise
(
  global_id int default 0 not null,
  client varchar(40) default '' not null,
  enterprisename varchar(100) null,
  okpo varchar(100) null,
  okdp varchar(100) null,
  kpp varchar(100) null,
  baddress varchar(100) null,
  address varchar(100) null,
  phone varchar(100) null,
  bank varchar(100) null,
  register varchar(100) null,
  consignee_name varchar(100) null,
  consignee_address varchar(100) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, client)
);

create index enterprisename
  on trm_in_cliententerprise (enterprisename);

create index version
  on trm_in_cliententerprise (global_id, version, deleted);

create table if not exists trm_in_clients
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  classifclient int(11) unsigned default 0 not null,
  sur_name varchar(100) default '' not null,
  name varchar(100) default '' not null,
  patronymic varchar(100) default '' null,
  birthday datetime null,
  inn varchar(100) default '' null,
  passport varchar(100) null,
  pricetype varchar(38) null,
  type tinyint(1) default 0 not null,
  allow_paycash tinyint(1) default 0 not null,
  structure varchar(100) null,
  active tinyint(1) default 0 not null,
  save_change tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index cl_i
  on trm_in_clients (active, classifclient, deleted);

create index classifclient
  on trm_in_clients (classifclient, sur_name, active, deleted);

create index inn
  on trm_in_clients (inn);

create index sur_name
  on trm_in_clients (sur_name);

create index version
  on trm_in_clients (global_id, version, deleted);

create table if not exists trm_in_clients_properties
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index global_id
  on trm_in_clients_properties (global_id, version, deleted);

create index name
  on trm_in_clients_properties (name);

create table if not exists trm_in_clients_properties_values
(
  global_id int default 0 not null,
  property varchar(40) default '' not null,
  client varchar(40) default '' not null,
  value varchar(100) default '' null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, property, client)
);

create index version
  on trm_in_clients_properties_values (global_id, version, deleted);

create table if not exists trm_in_complex_dish
(
  nomenclature_id int default 0 not null,
  owner_dish varchar(40) default '' not null,
  dish varchar(40) default '' not null,
  quantity decimal(20,4) default 1.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, owner_dish, dish)
);

create index version
  on trm_in_complex_dish (nomenclature_id, version, deleted);

create table if not exists trm_in_config_cashline
(
  store_id int default 0 not null,
  id varchar(50) default '' not null,
  value text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_config_cashline (store_id, version, deleted);

create table if not exists trm_in_config_global
(
  global_id int default 0 not null,
  id varchar(50) default '' not null,
  value text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_config_global (global_id, version, deleted);

create table if not exists trm_in_config_hardware
(
  config_hardware_id bigint unsigned default 0 not null,
  id bigint default 0 not null,
  _object bigint unsigned default 0 not null,
  object_property varchar(200) default '' not null,
  port varchar(50) default '' not null,
  port_property varchar(200) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (config_hardware_id, _object, port, id)
);

create index version
  on trm_in_config_hardware (config_hardware_id, version, deleted);

create table if not exists trm_in_config_pos
(
  cash_id bigint unsigned default 0 not null,
  id varchar(50) default '' not null,
  value text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index version
  on trm_in_config_pos (cash_id, version, deleted);

create table if not exists trm_in_configuration_groups
(
  global_id int default 0 not null,
  id int default 0 not null,
  config_hardware_id int(10) default 0 null,
  keyboard_layout_id int(10) default 0 null,
  terminal_menu_id int(10) null,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_configuration_groups (global_id, version, deleted);

create table if not exists trm_in_coupon_data_pos
(
  cash_id bigint unsigned not null,
  id varchar(20) not null,
  date_from date not null,
  date_to date not null,
  amount decimal(20,2) not null,
  status tinyint(1) null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (cash_id, id)
);

create table if not exists trm_in_coupon_ranges
(
  global_id tinyint(1) not null,
  type_id int not null,
  id int not null,
  range_from varchar(7) not null,
  range_to varchar(7) not null,
  creation_date datetime not null,
  created_by varchar(40) not null,
  version int default 0 null,
  deleted int(1) default 0 null,
  primary key (global_id, type_id, id)
);

create table if not exists trm_in_coupon_type
(
  global_id tinyint(1) not null,
  id int auto_increment,
  name varchar(100) not null,
  print_type tinyint(1) not null,
  prefix int(6) null,
  nominal decimal(20,2) null,
  date_from date null,
  date_to date null,
  days_from int null,
  days_to int null,
  version int default 0 null,
  deleted int(1) default 0 null,
  primary key (global_id, id)
);

create index id
  on trm_in_coupon_type (id);

create table if not exists trm_in_currency
(
  global_id int default 0 not null,
  currency_id int default 0 not null,
  name varchar(100) default '' not null,
  shortname varchar(100) default '' not null,
  loose_change varchar(100) default '' not null,
  multiplier text null,
  monetary_unit_multiplier text null,
  rate decimal(20,4) default 1.0000 not null,
  facevalues varchar(1000) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, currency_id)
);

create index version
  on trm_in_currency (global_id, version, deleted);

create table if not exists trm_in_currency_nominal
(
  global_id int not null,
  currency_id int not null,
  nominal int(11) unsigned not null,
  nominal_type tinyint(1) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, currency_id, nominal, nominal_type)
);

create index version
  on trm_in_currency_nominal (global_id, version, deleted);

create table if not exists trm_in_custom_text_codecs
(
  global_id int default 0 not null,
  name varchar(40) default '' not null,
  cnv_table blob not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, name)
);

create index version
  on trm_in_custom_text_codecs (global_id, version, deleted);

create table if not exists trm_in_customer_screen
(
  global_id int default 0 not null,
  type tinyint not null,
  form text not null,
  profile tinyint not null,
  profile_name text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, profile, type)
);

create index versions
  on trm_in_customer_screen (global_id, version, deleted);

create table if not exists trm_in_defect
(
  nomenclature_id int default 0 not null,
  defect_code varchar(40) default '' not null,
  title varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, defect_code)
);

create index version
  on trm_in_defect (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std2classif
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  classif_id varchar(40) default '0' not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, classif_id)
);

create index version
  on trm_in_disc_std2classif (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std2item_quantity
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '0' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, item, quantity)
);

create index version
  on trm_in_disc_std2item_quantity (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std2perscard
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  card_code varchar(40) default '' not null,
  type tinyint(1) unsigned default 0 not null,
  card_type smallint(5) unsigned default 0 not null,
  name varchar(40) null,
  receipt_amount decimal(20,4) default 0.0000 not null,
  classif varchar(40) default '0' not null,
  modificator varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, type, card_code, receipt_amount, classif)
);

create index card_code
  on trm_in_disc_std2perscard (nomenclature_id, discount_type, card_code);

create index dt_cl_md
  on trm_in_disc_std2perscard (nomenclature_id, discount_type, classif, modificator, deleted);

create index version
  on trm_in_disc_std2perscard (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std2perscard_stoplist
(
  nomenclature_id int default 0 not null,
  start varchar(40) default '' not null,
  stop varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, start)
);

create index version
  on trm_in_disc_std2perscard_stoplist (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_adders
(
  global_id tinyint unsigned default 0 not null,
  id int default 0 not null,
  discount_type bigint default 0 not null,
  adder_type int(11) unsigned default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index discount_type
  on trm_in_disc_std_adders (discount_type, adder_type, amount);

create index version
  on trm_in_disc_std_adders (global_id, version, deleted);

create table if not exists trm_in_disc_std_classif
(
  nomenclature_id int default 0 not null,
  discount_type int default 0 not null,
  classif_id varchar(40) default '0' not null,
  amount decimal(20,3) default 0.000 not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, classif_id, amount, quantity)
);

create index version
  on trm_in_disc_std_classif (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_classif_n_plus_m
(
  nomenclature_id int default 0 not null,
  discount_type int default 0 not null,
  classif_id varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, classif_id)
);

create index version
  on trm_in_disc_std_classif_n_plus_m (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_client_property
(
  global_id int default 0 not null,
  id int default 0 not null,
  discount_type int default 0 not null,
  client varchar(40) null,
  code varchar(20) default '' not null,
  value int default 0 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint default 0 not null,
  primary key (global_id, id),
  constraint client
    unique (global_id, discount_type, client, code, value)
);

create index version
  on trm_in_disc_std_client_property (global_id, version, deleted);

create table if not exists trm_in_disc_std_clients
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  client varchar(40) default '' not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, client)
);

create index version
  on trm_in_disc_std_clients (global_id, version, deleted);

create table if not exists trm_in_disc_std_groupclassifs
(
  nomenclature_id int default 0 not null,
  discount_id bigint default 0 not null,
  classif_id varchar(40) default '0' not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_id, classif_id)
);

create index version
  on trm_in_disc_std_groupclassifs (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_groupitems
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  itemgroup int default 0 not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, itemgroup, item)
);

create index item
  on trm_in_disc_std_groupitems (nomenclature_id, discount_type, item);

create index version
  on trm_in_disc_std_groupitems (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_item_prop
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  property_code_amount varchar(40) default '' not null,
  property_code_quantity varchar(40) default '' not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type)
);

create index version
  on trm_in_disc_std_item_prop (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_item_prop_amount
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  property_value_const varchar(100) default '' not null,
  amount decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, property_value_const, amount)
);

create index version
  on trm_in_disc_std_item_prop_amount (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_item_prop_quantity
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  property_value_const varchar(100) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, property_value_const, quantity)
);

create index version
  on trm_in_disc_std_item_prop_quantity (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_items_n_plus_m
(
  nomenclature_id int default 0 not null,
  item varchar(40) default '' not null,
  discount_type bigint default 0 not null,
  quantity_paid decimal(20,4) default 0.0000 not null,
  quantity_without_paid decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item, discount_type)
);

create index version
  on trm_in_disc_std_items_n_plus_m (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_itemsgroup
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  modificator varchar(40) default '' not null,
  sequence smallint(5) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, id)
);

create index version
  on trm_in_disc_std_itemsgroup (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_std_list_basket
(
  nomenclature_id int default 0 not null,
  discount_id bigint default 0 not null,
  list_id int default 0 not null,
  quantity decimal(20,4) null,
  and_more tinyint(1) default 0 null,
  limit_summ decimal(20,4) null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_id, list_id)
);

create index version
  on trm_in_disc_std_list_basket (nomenclature_id, version, deleted);

create table if not exists trm_in_disc_store_items
(
  store_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  type tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, discount_type, item)
);

create index version
  on trm_in_disc_store_items (store_id, version, deleted);

create table if not exists trm_in_discount_adder_type
(
  global_id tinyint unsigned default 0 not null,
  account_type_id int(11) unsigned default 0 not null,
  automatic tinyint(1) default 0 not null,
  calculated datetime null,
  must_calculated datetime null,
  start datetime default '0000-00-00 00:00:00' not null,
  launch_type int default 0 not null,
  interval_algorithm int default 0 not null,
  period_type int(11) unsigned default 0 not null,
  period_value int(11) unsigned default 0 not null,
  range_type int(11) unsigned default 0 not null,
  range_value int(11) unsigned default 0 not null,
  delay_type int(11) unsigned default 0 not null,
  delay_value int(11) unsigned default 0 not null,
  function_type int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, account_type_id)
);

create index automatic
  on trm_in_discount_adder_type (automatic);

create index version
  on trm_in_discount_adder_type (global_id, version, deleted);

create table if not exists trm_in_discount_card
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  type_id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) default '' not null,
  name varchar(40) null,
  date_from datetime null,
  date_till datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in_discount_card (global_id, type_id, start_card_code, stop_card_code);

create index single_card
  on trm_in_discount_card (start_card_code, deleted);

create index version
  on trm_in_discount_card (global_id, version, deleted);

create table if not exists trm_in_discount_card_personal
(
  global_id tinyint unsigned default 0 not null,
  discount_type bigint not null,
  card_number varchar(40) not null,
  modificator varchar(40) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, card_number)
);

create index version
  on trm_in_discount_card_personal (global_id, version, deleted);

create table if not exists trm_in_discount_card_siebel
(
  global_id tinyint unsigned default 0 not null,
  id varchar(40) not null,
  card_status varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_discount_card_siebel (global_id, version, deleted);

create table if not exists trm_in_discount_card_stop_list
(
  global_id tinyint unsigned default 0 not null,
  id varchar(40) default '' not null,
  type_id int(11) unsigned default 0 not null,
  start_card_code varchar(40) default '' not null,
  stop_card_code varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index code
  on trm_in_discount_card_stop_list (global_id, type_id, start_card_code, stop_card_code);

create index single_card
  on trm_in_discount_card_stop_list (start_card_code, deleted);

create index version
  on trm_in_discount_card_stop_list (global_id, version, deleted);

create table if not exists trm_in_discount_card_type
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  system_id int(11) unsigned default 0 not null,
  name varchar(40) null,
  numberlen int(11) unsigned default 0 not null,
  single_use tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index `system`
  on trm_in_discount_card_type (global_id, system_id);

create index version
  on trm_in_discount_card_type (global_id, version, deleted);

create table if not exists trm_in_discount_system
(
  global_id tinyint unsigned default 0 not null,
  id int(11) unsigned default 0 not null,
  name varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_discount_system (global_id, version, deleted);

create table if not exists trm_in_discount_types
(
  global_id int default 0 not null,
  id bigint default 0 not null,
  advertising_campaign_id bigint default 0 not null,
  property bigint unsigned default 0 not null,
  name varchar(40) default '' not null,
  efts int default 0 not null,
  param mediumtext null,
  sequence int default 0 not null,
  group_type int null,
  card_type int(11) unsigned null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  account_type int null,
  account_summ decimal(20,2) null,
  receipt_summ decimal(20,2) null,
  in_birth_month tinyint(1) default 0 null,
  days_before int null,
  days_after int null,
  application_condition text not null,
  balance_condition tinyint(1) default 0 not null,
  receipt_amount_condition tinyint(1) default 0 not null,
  use_action_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index advertising_campaign_id
  on trm_in_discount_types (advertising_campaign_id);

create index global_id
  on trm_in_discount_types (global_id, version, deleted);

create table if not exists trm_in_discount_types_classifclient
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  classif_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, classif_id)
);

create index version
  on trm_in_discount_types_classifclient (global_id, version, deleted);

create table if not exists trm_in_discount_types_client
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, client)
);

create index deleted
  on trm_in_discount_types_client (discount_type, global_id, deleted);

create index version
  on trm_in_discount_types_client (global_id, version, deleted);

create table if not exists trm_in_discount_types_gifts
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '' not null,
  amount decimal(20,3) default 0.000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, item)
);

create index version
  on trm_in_discount_types_gifts (nomenclature_id, version, deleted);

create table if not exists trm_in_discount_types_items
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  item varchar(40) default '' not null,
  amount decimal(20,3) default 0.000 not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, item, amount, quantity)
);

create index version
  on trm_in_discount_types_items (nomenclature_id, version, deleted);

create table if not exists trm_in_discount_types_lists
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  list int default 0 not null,
  amount decimal(20,3) default 0.000 not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, list, amount, quantity)
);

create index version
  on trm_in_discount_types_lists (nomenclature_id, version, deleted);

create table if not exists trm_in_discount_types_prods
(
  nomenclature_id int default 0 not null,
  discount_type bigint default 0 not null,
  list int default 0 not null,
  begin_offset int(4) default 0 null,
  finish_offset int(4) default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, discount_type, list)
);

create index version
  on trm_in_discount_types_prods (nomenclature_id, version, deleted);

create table if not exists trm_in_discount_types_results
(
  global_id int default 0 not null,
  discount_type_id bigint default 0 not null,
  efts varchar(38) default '' not null,
  params text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type_id, efts)
);

create table if not exists trm_in_discount_types_vars
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  discount_type bigint default 0 not null,
  var varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  modificator varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index discount_type
  on trm_in_discount_types_vars (nomenclature_id, discount_type, var);

create index version
  on trm_in_discount_types_vars (nomenclature_id, version, deleted);

create table if not exists trm_in_discount_types_where
(
  global_id int default 0 not null,
  discount_type bigint default 0 not null,
  store_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, discount_type, store_id)
);

create index version
  on trm_in_discount_types_where (global_id, version, deleted);

create table if not exists trm_in_dishes
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  short_name varchar(40) default '' not null,
  comment varchar(128) default '' not null,
  recipe varchar(128) default '' not null,
  show_additives tinyint(1) default 1 not null,
  min_additives int null,
  max_free_additives int null,
  max_additives int null,
  printer tinyint unsigned null,
  is_complex tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in_dishes (nomenclature_id, version, deleted);

create table if not exists trm_in_equarings
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  efts int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_equarings (store_id, version, deleted);

create table if not exists trm_in_external_order_items
(
  store_id int not null comment 'Идентификатор магазина',
  order_id bigint not null comment 'Уникальный номер заказа, выданный внешней системой',
  item varchar(40) not null comment 'Артикул товара',
  quantity decimal(20,4) not null comment 'Количество',
  price decimal(20,4) not null comment 'Цена',
  min_price decimal(20,4) not null comment 'Мин. цена',
  changeable tinyint(1) not null comment 'Изменяемая позиция',
  fix_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, order_id, item)
);

create table if not exists trm_in_external_order_refusals
(
  global_id int default 0 not null,
  id int default 0 not null,
  title varchar(255) not null,
  can_comment tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_external_order_refusals (global_id, version, deleted);

create table if not exists trm_in_external_orders
(
  store_id int not null comment 'Идентификатор магазина',
  order_id bigint not null comment 'Уникальный номер заказа, выданный внешней системой',
  ext_order_type varchar(100) not null comment 'Название системы, в которой заказ был сформирован',
  ext_order_id varchar(100) not null comment 'Номер заказа, сформированный системой, в которой делался заказ',
  order_date datetime null comment 'Дата заказа',
  status tinyint(5) not null comment 'Статус заказа',
  status_change_date datetime null comment 'Дата перехода в последний статус',
  sale_type tinyint(1) not null comment 'Оплата заказа на кассе/отложенная оплата',
  wholesale tinyint(1) not null comment 'Оптовый/розничный',
  client_name varchar(255) not null comment 'ФИО клиента',
  check_structure tinyint(1) not null comment 'Флаг необходимость проверки содержания заказа при оплате',
  changeable tinyint(1) not null comment 'Флаг - разрешено на кассе изменять содержание заказа или не разрешено',
  amount decimal(20,4) not null comment 'Cумма заказа',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, order_id),
  constraint external_orders_ext_uniq
    unique (ext_order_type, ext_order_id)
);

create index store_id_ext_order_id
  on trm_in_external_orders (store_id, ext_order_id, status, deleted);

create definer = ukm_server@localhost trigger trm_in_external_orders_upd_tr
  before INSERT on trm_in_external_orders
  for each row
-- missing source code
;

create table if not exists trm_in_external_system
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  type varchar(20) default '0' not null,
  param mediumtext null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_external_system (store_id, version, deleted);

create table if not exists trm_in_goods_search_stores
(
  store_id int default 0 not null
    primary key,
  search_store_id text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in_goods_search_stores (store_id, version, deleted);

create table if not exists trm_in_input_template_fields
(
  global_id int default 0 not null,
  input_template bigint unsigned default 0 not null,
  field bigint unsigned default 0 not null,
  value text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, input_template, field)
);

create index version
  on trm_in_input_template_fields (global_id, version, deleted);

create table if not exists trm_in_input_template_ranges
(
  global_id int default 0 not null,
  id int default 0 not null,
  input_template bigint unsigned default 0 not null,
  value varchar(255) default '' not null,
  start varchar(255) default '' not null,
  stop varchar(255) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id),
  constraint k1
    unique (input_template, value, start, stop)
);

create index version
  on trm_in_input_template_ranges (global_id, version, deleted);

create table if not exists trm_in_input_template_stores
(
  store_id bigint unsigned default 0 not null,
  input_template_id bigint default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, input_template_id)
);

create index version
  on trm_in_input_template_stores (store_id, version, deleted);

create table if not exists trm_in_input_templates
(
  global_id int default 0 not null,
  id bigint unsigned default 0 not null,
  name varchar(40) default '' not null,
  template text not null,
  keyboard tinyint(1) unsigned default 0 not null,
  mcr tinyint(1) unsigned default 0 not null,
  scanner tinyint(1) unsigned default 0 not null,
  event bigint unsigned default 0 not null,
  priority bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index priority
  on trm_in_input_templates (priority);

create index version
  on trm_in_input_templates (global_id, version, deleted);

create table if not exists trm_in_inquirer
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  name varchar(40) default '' not null,
  show_inq_on tinyint(1) default 0 not null,
  question text not null,
  date_from date null,
  date_to date null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index global_id
  on trm_in_inquirer (global_id, version, deleted);

create table if not exists trm_in_inquirer_answer
(
  global_id int default 0 not null,
  inquirer_id varchar(40) default '' not null,
  id varchar(40) default '' not null,
  sequence int default 0 not null,
  answer varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, inquirer_id, id)
);

create index global_id
  on trm_in_inquirer_answer (global_id, version, deleted);

create table if not exists trm_in_inquirer_store
(
  store_id int default 0 not null,
  inquirer_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, inquirer_id)
);

create index version
  on trm_in_inquirer_store (store_id, version, deleted);

create table if not exists trm_in_item_agent
(
  nomenclature_id int not null,
  item_id varchar(40) not null,
  tag_1226 bigint not null,
  tax_group_id int null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item_id)
);

create index versions
  on trm_in_item_agent (nomenclature_id, version, deleted);

create table if not exists trm_in_item_cc
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  gtd varchar(40) null,
  country varchar(40) null,
  certification varchar(40) null,
  certification_date varchar(40) null,
  producer_marking varchar(100) null,
  summary varchar(100) null,
  structure text null,
  substructure varchar(100) null,
  exp_date datetime null,
  realization_time int null,
  qdoc_org varchar(40) null,
  qdoc_id varchar(40) null,
  qdoc_date datetime null,
  qdoc_exp datetime null,
  hdoc_org varchar(40) null,
  hdoc_id varchar(40) null,
  hdoc_date datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in_item_cc (nomenclature_id, version, deleted);

create table if not exists trm_in_item_classif_egais
(
  nomenclature_id int not null,
  id varchar(40) not null,
  excise tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index versions
  on trm_in_item_classif_egais (nomenclature_id, version, deleted);

create table if not exists trm_in_item_properties
(
  nomenclature_id int default 0 not null,
  item_id varchar(40) default '' not null,
  code varchar(20) default '' not null,
  id int(11) unsigned default 0 not null,
  sequence smallint(5) unsigned default 0 not null,
  description text not null,
  flags int unsigned default 0 not null,
  cookies int unsigned null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item_id, code)
);

create index sequence
  on trm_in_item_properties (nomenclature_id, item_id, sequence);

create index version
  on trm_in_item_properties (nomenclature_id, version, deleted);

create table if not exists trm_in_item_properties_type
(
  nomenclature_id int default 0 not null,
  item_id varchar(40) default '' not null,
  property_code varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item_id, property_code)
);

create index version
  on trm_in_item_properties_type (nomenclature_id, version, deleted);

create table if not exists trm_in_item_property_values
(
  nomenclature_id int default 0 not null,
  unique_item_id bigint not null,
  property_code varchar(40) default '' not null,
  property_id int unsigned default 0 not null,
  sequence smallint(5) unsigned default 0 not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, unique_item_id, property_code)
);

create index group_property
  on trm_in_item_property_values (nomenclature_id, property_code, property_id);

create index version
  on trm_in_item_property_values (nomenclature_id, version, deleted);

create table if not exists trm_in_item_protection
(
  nomenclature_id int default 0 not null,
  item varchar(40) default '' not null,
  protection_code varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, item, protection_code)
);

create index version
  on trm_in_item_protection (nomenclature_id, version, deleted);

create table if not exists trm_in_items
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  unique_item_id bigint not null,
  name varchar(255) default '' not null,
  descr text not null,
  measure varchar(40) default '' not null,
  measure_iso varchar(5) null,
  measprec int default 0 not null,
  classif varchar(40) default '0' not null,
  prop int default 0 not null,
  tax int null,
  weight decimal(20,4) null,
  length int(11) unsigned null,
  height int(11) unsigned null,
  width int(11) unsigned null,
  quantity_shipping decimal(20,4) null,
  sku decimal(20,4) null,
  alcohol tinyint(3) null,
  country varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index classif
  on trm_in_items (nomenclature_id, classif, deleted);

create index id
  on trm_in_items (classif, id);

create index name
  on trm_in_items (classif, name);

create index nomenclature_id
  on trm_in_items (nomenclature_id, id, deleted);

create index unique_item_id
  on trm_in_items (unique_item_id);

create index version
  on trm_in_items (nomenclature_id, version, deleted);

create table if not exists trm_in_items_egais
(
  nomenclature_id int not null,
  id varchar(40) not null,
  excise tinyint(1) default 0 not null,
  version int not null,
  deleted tinyint(1) not null,
  primary key (nomenclature_id, id)
);

create index versions
  on trm_in_items_egais (nomenclature_id, version, deleted);

create table if not exists trm_in_items_images
(
  nomenclature_id int not null,
  id varchar(40) not null,
  image mediumblob not null,
  version int not null,
  deleted tinyint(1) not null,
  primary key (nomenclature_id, id)
);

create table if not exists trm_in_items_sets
(
  nomenclature_id int default 0 not null,
  set_id varchar(40) default '' not null,
  item_id varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, set_id, item_id)
);

create index version
  on trm_in_items_sets (nomenclature_id, version, deleted);

create table if not exists trm_in_items_stocks
(
  store_id int default 0 not null,
  item varchar(40) default '' not null,
  stock int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, item, stock)
);

create index version
  on trm_in_items_stocks (store_id, version, deleted);

create table if not exists trm_in_keyboard
(
  keyboard_layout_id int(10) default 0 not null,
  id int default 0 not null,
  ace bigint unsigned default 0 not null,
  command int default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, id)
);

create index version
  on trm_in_keyboard (keyboard_layout_id, version, deleted);

create table if not exists trm_in_keyboard_msrs
(
  keyboard_layout_id int default 0 not null,
  id int default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, id)
);

create index version
  on trm_in_keyboard_msrs (keyboard_layout_id, version, deleted);

create table if not exists trm_in_keyboard_scanners
(
  keyboard_layout_id int default 0 not null,
  id int default 0 not null,
  scanner_type int(10) default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (keyboard_layout_id, id)
);

create index version
  on trm_in_keyboard_scanners (keyboard_layout_id, version, deleted);

create table if not exists trm_in_keylock
(
  config_hardware_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  command bigint unsigned default 0 not null,
  param varchar(50) default '' not null,
  version int(11) unsigned default 0 not null,
  deleted tinyint(1) unsigned default 0 not null,
  primary key (config_hardware_id, id)
);

create index version
  on trm_in_keylock (config_hardware_id, version, deleted);

create table if not exists trm_in_limitation
(
  nomenclature_id int default 0 not null,
  id int(11) unsigned auto_increment,
  name varchar(50) default '' not null,
  type int default 0 not null,
  param text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index id
  on trm_in_limitation (id);

create table if not exists trm_in_limitation_and_alcohol
(
  nomenclature_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  alcohol tinyint(3) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, limitation_id, alcohol)
);

create table if not exists trm_in_limitation_in_classif
(
  nomenclature_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  classif_id varchar(40) default '0' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, limitation_id, classif_id)
);

create table if not exists trm_in_limitation_in_item
(
  nomenclature_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  item_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, limitation_id, item_id)
);

create table if not exists trm_in_limitation_in_pos
(
  cash_id bigint unsigned not null,
  limitation_id int(11) unsigned not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, limitation_id)
);

create table if not exists trm_in_limitation_in_store
(
  store_id int default 0 not null,
  limitation_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, limitation_id)
);

create table if not exists trm_in_list
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  type tinyint(1) default 0 not null,
  name varchar(128) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in_list (nomenclature_id, version, deleted);

create table if not exists trm_in_list_items
(
  nomenclature_id int default 0 not null,
  list int default 0 not null,
  item varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, list, item)
);

create index item
  on trm_in_list_items (nomenclature_id, item);

create index version
  on trm_in_list_items (nomenclature_id, version, deleted);

create table if not exists trm_in_luaukm
(
  global_id int default 0 not null,
  profile_id int default 1 not null,
  name varchar(50) default '' not null,
  value mediumtext not null,
  edit tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, name, profile_id)
);

create index version
  on trm_in_luaukm (global_id, version, deleted);

create table if not exists trm_in_luaukm_profile
(
  global_id int default 0 not null comment 'глобальный id',
  profile_id int(11) unsigned default 1 not null comment 'id профиля',
  profile_name tinytext not null comment 'имя профиля',
  active tinyint(1) default 0 not null comment 'текущий активный профиль. может быть только один',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, profile_id)
);

create index version
  on trm_in_luaukm_profile (global_id, profile_id, version, deleted);

create table if not exists trm_in_marketing_effort
(
  global_id tinyint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  title varchar(100) default '' not null,
  date_from datetime null,
  date_to datetime null,
  matrix mediumtext null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id),
  constraint id
    unique (id)
);

create index version
  on trm_in_marketing_effort (global_id, version, deleted);

create table if not exists trm_in_marketing_effort_in_cashline
(
  store_id int default 0 not null,
  marketing_effort_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, marketing_effort_id)
);

create index cash_line_id
  on trm_in_marketing_effort_in_cashline (store_id);

create index marketing_effort_id
  on trm_in_marketing_effort_in_cashline (marketing_effort_id);

create index version
  on trm_in_marketing_effort_in_cashline (store_id, version, deleted);

create table if not exists trm_in_measure_iso
(
  nomenclature_id int default 0 not null,
  id varchar(5) default '' not null,
  code varchar(40) default '' not null,
  `desc` varchar(40) default '' not null,
  category int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in_measure_iso (nomenclature_id, version, deleted);

create table if not exists trm_in_media_picture
(
  global_id int default 0 not null,
  id varchar(40) default '' not null,
  data mediumblob not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_media_picture (global_id, version, deleted);

create table if not exists trm_in_media_playlist
(
  global_id int default 0 not null,
  id int default 0 not null,
  name varchar(100) default '' not null,
  date_from date null,
  date_to date null,
  timeout_sec int default 10 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_media_playlist (global_id, version, deleted);

create table if not exists trm_in_media_playlist_picture
(
  global_id int default 0 not null,
  playlist_id int default 0 not null,
  picture_id varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, playlist_id, picture_id)
);

create index version
  on trm_in_media_playlist_picture (global_id, version, deleted);

create table if not exists trm_in_media_playlist_store
(
  store_id int default 0 not null,
  playlist_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, playlist_id)
);

create index version
  on trm_in_media_playlist_store (store_id, version, deleted);

create table if not exists trm_in_menu
(
  terminal_menu_id int(10) default 0 not null,
  id int default 0 not null,
  owner int default 0 not null,
  name varchar(80) default '' not null,
  ace bigint unsigned default 0 not null,
  command int default 0 not null,
  param text not null,
  hide_on_no_permission tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (terminal_menu_id, id)
);

create index version
  on trm_in_menu (terminal_menu_id, version, deleted);

create table if not exists trm_in_nomenclature
(
  nomenclature_id int default 0 not null
    primary key,
  name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in_nomenclature (nomenclature_id, version, deleted);

create table if not exists trm_in_offline_account
(
  global_id tinyint unsigned default 0 not null,
  id int default 0 not null,
  account_type_id int(11) unsigned default 0 not null,
  name varchar(128) default '' not null,
  balance decimal(20,2) default 0.00 not null,
  credit decimal(20,2) default 0.00 not null,
  params varchar(255) null,
  closed datetime null,
  active tinyint(1) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index account_type_params
  on trm_in_offline_account (account_type_id, params);

create index version
  on trm_in_offline_account (global_id, version, deleted);

create table if not exists trm_in_order_login
(
  store_id int default 0 not null,
  id int default 0 not null,
  user_id varchar(40) default '' not null,
  date datetime default '0000-00-00 00:00:00' not null,
  address varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_order_login (store_id, version, deleted);

create table if not exists trm_in_order_order
(
  store_id int default 0 not null,
  id varchar(20) default '' not null,
  login_id int default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  property_code varchar(20) default '0' not null,
  changeable tinyint(1) default 1 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_order_order (store_id, version, deleted);

create table if not exists trm_in_order_var
(
  store_id int default 0 not null,
  order_id varchar(20) default '' not null,
  position int default 0 not null,
  var varchar(40) default '' not null,
  manual tinyint(1) default 0 not null,
  quantity decimal(20,4) default 0.0000 not null,
  stock int null,
  price decimal(20,4) null,
  minprice decimal(20,4) null,
  fix_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, order_id, position)
);

create index version
  on trm_in_order_var (store_id, version, deleted);

create table if not exists trm_in_pallet
(
  nomenclature_id int default 0 not null,
  pallet_code varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, pallet_code)
);

create index version
  on trm_in_pallet (nomenclature_id, version, deleted);

create table if not exists trm_in_payment_range_param
(
  store_id int default 0 not null,
  range_id int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, range_id)
);

create index version
  on trm_in_payment_range_param (store_id, version, deleted);

create table if not exists trm_in_payments
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  param text null,
  equaring_id int default 0 not null,
  old_efts int default 0 not null,
  is_cash tinyint(1) default 1 not null,
  kkt_payment_form tinyint(1) default 0 not null,
  limitation_disc_type_id bigint null,
  allow_edit_limitation tinyint(1) default 1 not null,
  return_by_cash_forbidden tinyint(1) default 0 not null,
  payments_allowed tinyint(1) default 0 not null,
  increment_account text null,
  discount_disc_type_id bigint null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_payments (store_id, version, deleted);

create table if not exists trm_in_payments_classifclient
(
  store_id int default 0 not null,
  payment int default 0 not null,
  classif_id int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, payment, classif_id)
);

create index version
  on trm_in_payments_classifclient (store_id, version, deleted);

create table if not exists trm_in_payments_client
(
  store_id int default 0 not null,
  payment int default 0 not null,
  client varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, payment, client)
);

create index version
  on trm_in_payments_client (store_id, version, deleted);

create table if not exists trm_in_pch_serv
(
  pch_serv_id int default 0 not null,
  id int default 0 not null,
  guid varchar(40) default '' not null,
  number int default 0 not null,
  name varchar(80) default '' not null,
  store_id int default 0 not null,
  register_date datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pch_serv_id, id)
);

create index version
  on trm_in_pch_serv (pch_serv_id, version, deleted);

create table if not exists trm_in_pch_types
(
  pch_serv_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  xml text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pch_serv_id, id)
);

create index version
  on trm_in_pch_types (pch_serv_id, version, deleted);

create table if not exists trm_in_pixmap
(
  global_id int default 0 not null,
  id int default 0 not null,
  data mediumblob not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_pixmap (global_id, version, deleted);

create table if not exists trm_in_pos
(
  cash_id bigint unsigned default 0 not null
    primary key,
  guid varchar(40) default '' not null,
  pos_version int unsigned default 0 not null,
  number int default 0 not null,
  kkm_number varchar(40) null,
  register_number varchar(40) null,
  name varchar(80) default '' not null,
  store_id int default 0 not null,
  config_group_id int(10) null,
  enable_auto_update int default 0 null,
  active tinyint(1) default 1 not null,
  register_date datetime null,
  pos_type int(2) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in_pos (cash_id, version, deleted);

create table if not exists trm_in_pos_cw77f_regparams
(
  cash_id int not null
    primary key,
  `values` varchar(1024) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in_pos_cw77f_regparams (cash_id, version, deleted);

create table if not exists trm_in_pricelist
(
  pricelist_id int(11) unsigned default 0 not null
    primary key,
  nomenclature_id int default 0 not null,
  name varchar(100) default '' not null,
  date_from datetime null,
  date_to datetime null,
  active tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  constraint nomenclature
    unique (nomenclature_id, pricelist_id)
);

create index version
  on trm_in_pricelist (pricelist_id, version, deleted);

create table if not exists trm_in_pricelist_items
(
  pricelist_id int(11) unsigned default 0 not null,
  item varchar(40) default '' not null,
  nomenclature_id int default 0 not null,
  price decimal(20,4) default 0.0000 not null,
  minprice decimal(20,4) default 0.0000 not null,
  is_promo_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pricelist_id, item, nomenclature_id),
  constraint nomenclature
    unique (nomenclature_id, pricelist_id, item)
);

create index version
  on trm_in_pricelist_items (pricelist_id, version, deleted);

create table if not exists trm_in_pricelist_items_ext
(
  pricelist_id int(11) unsigned not null,
  item varchar(40) not null,
  nomenclature_id int not null,
  pos int not null,
  price decimal(20,4) not null,
  minprice decimal(20,4) null,
  descr varchar(100) default '' null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pricelist_id, nomenclature_id, item, pos)
);

create index version
  on trm_in_pricelist_items_ext (pricelist_id, version, deleted);

create table if not exists trm_in_pricelist_var
(
  pricelist_id int(11) unsigned default 0 not null,
  var varchar(40) default '' not null,
  nomenclature_id int default 0 not null,
  price decimal(20,4) default 0.0000 not null,
  min_price decimal(20,4) default 0.0000 not null,
  is_promo_price tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (pricelist_id, var, nomenclature_id),
  constraint nomenclature
    unique (nomenclature_id, pricelist_id, var)
);

create index var
  on trm_in_pricelist_var (nomenclature_id, var);

create index version
  on trm_in_pricelist_var (pricelist_id, version, deleted);

create table if not exists trm_in_pricetype
(
  global_id int default 0 not null,
  id varchar(38) default '' not null,
  name varchar(100) default '' not null,
  active tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_pricetype (global_id, version, deleted);

create table if not exists trm_in_pricetype_pricelist
(
  store_id int default 0 not null,
  pricetype varchar(38) default '' not null,
  pricelist int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, pricetype, pricelist)
);

create index version
  on trm_in_pricetype_pricelist (store_id, version, deleted);

create table if not exists trm_in_properties
(
  nomenclature_id int default 0 not null,
  code varchar(40) default '' not null,
  name varchar(40) default '' not null,
  flags int unsigned default 0 not null,
  description text null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, code)
);

create index version
  on trm_in_properties (nomenclature_id, version, deleted);

create table if not exists trm_in_property_values
(
  nomenclature_id int default 0 not null,
  property_code varchar(40) default '' not null,
  id int unsigned default 0 not null,
  const varchar(100) null,
  description text not null,
  comment text null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, property_code, id)
);

create index property_values
  on trm_in_property_values (nomenclature_id, property_code, const, deleted);

create index version
  on trm_in_property_values (nomenclature_id, version, deleted);

create table if not exists trm_in_rdish
(
  nomenclature_id int default 0 not null,
  rmenu bigint default 0 not null,
  dish varchar(40) default '' not null,
  additivitygroup int default 0 not null,
  show_order int null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, rmenu, dish, additivitygroup)
);

create index rmenu_dish_show_order
  on trm_in_rdish (rmenu, dish, show_order);

create index version
  on trm_in_rdish (nomenclature_id, version, deleted);

create table if not exists trm_in_restaurant_tables
(
  store_id int default 0 not null,
  id int default 0 not null,
  description varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_restaurant_tables (store_id, version, deleted);

create table if not exists trm_in_rmenus
(
  nomenclature_id int default 0 not null,
  id bigint default 0 not null,
  name varchar(40) default '' not null,
  role bigint unsigned null,
  type tinyint default 0 not null,
  owner bigint null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  show_at int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index owner
  on trm_in_rmenus (owner, type);

create index version
  on trm_in_rmenus (nomenclature_id, version, deleted);

create table if not exists trm_in_rmenus_images
(
  nomenclature_id int not null,
  id bigint not null,
  image mediumblob not null,
  version int not null,
  deleted tinyint(1) not null,
  primary key (nomenclature_id, id)
);

create table if not exists trm_in_role
(
  global_id tinyint unsigned default 0 not null,
  id bigint unsigned auto_increment,
  title varchar(100) default '' not null,
  lillo_screen varchar(38) null,
  order_panel varchar(38) null,
  role_entry text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index id
  on trm_in_role (id);

create index version
  on trm_in_role (global_id, version, deleted);

create table if not exists trm_in_role_permission
(
  global_id tinyint unsigned default 0 not null,
  role_id bigint unsigned default 0 not null,
  entry_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, role_id, entry_id)
);

create index role_id
  on trm_in_role_permission (role_id);

create index version
  on trm_in_role_permission (global_id, version, deleted);

create table if not exists trm_in_screen
(
  global_id int default 0 not null,
  id varchar(38) default '' not null,
  content longtext not null,
  friendly_name varchar(255) default '' not null,
  group_id varchar(38) default '' not null,
  screen_type int(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_screen (global_id, version, deleted);

create table if not exists trm_in_screen_group
(
  global_id int default 0 not null,
  id varchar(38) default '' not null,
  friendly_name varchar(255) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_screen_group (global_id, version, deleted);

create table if not exists trm_in_sellers
(
  store_id int not null,
  seller_code varchar(50) not null,
  name varchar(50) not null,
  first_date date not null,
  last_date date null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, seller_code)
);

create index name
  on trm_in_sellers (store_id, name);

create index version
  on trm_in_sellers (store_id, version, deleted);

create table if not exists trm_in_services
(
  store_id int default 0 not null,
  id varchar(40) default '' not null,
  name varchar(80) default '' not null,
  transfering_id int default 0 not null,
  check_request tinyint(1) default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_services (store_id, version, deleted);

create table if not exists trm_in_social_moscow_items
(
  nomenclature_id int not null,
  id varchar(40) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index version
  on trm_in_social_moscow_items (nomenclature_id, version, deleted);

create table if not exists trm_in_sound_messages
(
  global_id int default 0 not null,
  id varchar(50) default '' not null,
  message text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (global_id, id)
);

create index version
  on trm_in_sound_messages (global_id, version, deleted);

create table if not exists trm_in_stocks
(
  store_id int default 0 not null,
  id int default 0 not null,
  owner int default 0 not null,
  name varchar(80) default '' not null,
  address varchar(100) default '' not null,
  enterprisename varchar(100) default '' not null,
  inn varchar(40) default '' not null,
  fiscal tinyint(1) default 1 not null,
  printer int default 0 not null,
  taxsystem int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_stocks (store_id, version, deleted);

create table if not exists trm_in_storage_place
(
  store_id int default 0 not null,
  id int default 0 not null,
  code varchar(40) default '' not null,
  description text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id),
  constraint code
    unique (code)
);

create index version
  on trm_in_storage_place (store_id, version, deleted);

create table if not exists trm_in_storage_place_items
(
  store_id int default 0 not null,
  nomenclature_id int default 0 not null,
  item_id varchar(40) not null,
  storage_place_id int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, nomenclature_id, item_id, storage_place_id)
);

create index version
  on trm_in_storage_place_items (store_id, version, deleted);

create table if not exists trm_in_store
(
  store_id int default 0 not null
    primary key,
  name varchar(80) default '' not null,
  server_id int default 0 not null,
  enterprise_name varchar(100) null,
  code_subdivision varchar(40) null,
  inn varchar(40) null,
  okpo varchar(100) null,
  okdp varchar(100) null,
  kpp varchar(100) null,
  bank varchar(100) null,
  static_rrn varchar(40) null,
  registration varchar(100) null,
  director varchar(100) null,
  accountant varchar(100) null,
  address varchar(255) null,
  phone varchar(100) null,
  enterprise_address varchar(100) null,
  remains_efts int null,
  remains_param text null,
  video_control_type varchar(40) null,
  area decimal(20,4) default 1.0000 not null,
  nomenclature_id int default 0 not null,
  pricetype_id varchar(38) default '' not null,
  currency_id int default 0 not null,
  lat decimal(10,6) null,
  lng decimal(10,6) null,
  director_position varchar(150) null,
  accountant_position varchar(150) null,
  topology_node int(11) unsigned not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_in_store (store_id, version, deleted);

create table if not exists trm_in_store_images
(
  store_id int default 0 not null,
  id int default 0 not null,
  icon blob null,
  icon_fname varchar(40) null,
  photo mediumblob null,
  photo_fname varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_store_images (store_id, version, deleted);

create table if not exists trm_in_store_rmenus
(
  nomenclature_id int default 0 not null,
  store int default 0 not null,
  rmenu bigint default 0 not null,
  default_settings tinyint(1) default 1 not null,
  role bigint null,
  date_from datetime null,
  date_to datetime null,
  time_from varchar(5) null,
  time_to varchar(5) null,
  monday tinyint(1) default 1 not null,
  tuesday tinyint(1) default 1 not null,
  wednesday tinyint(1) default 1 not null,
  thursday tinyint(1) default 1 not null,
  friday tinyint(1) default 1 not null,
  saturday tinyint(1) default 1 not null,
  sunday tinyint(1) default 1 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, store, rmenu)
);

create index version
  on trm_in_store_rmenus (nomenclature_id, version, deleted);

create table if not exists trm_in_store_rmenus_active
(
  nomenclature_id int not null,
  store_id int not null,
  rmenu bigint not null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (nomenclature_id, store_id, rmenu)
);

create table if not exists trm_in_store_rmenus_settings
(
  nomenclature_id int not null,
  store_id int not null,
  use_defaults tinyint(1) default 1 not null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (nomenclature_id, store_id)
);

create table if not exists trm_in_tare
(
  nomenclature_id int not null,
  id int(11) unsigned not null,
  name varchar(26) not null,
  weight int(4) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create table if not exists trm_in_taxes
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  name char(40) default '' not null,
  priority tinyint default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index i1
  on trm_in_taxes (id, priority);

create index i2
  on trm_in_taxes (priority, id);

create index priority
  on trm_in_taxes (priority);

create index version
  on trm_in_taxes (nomenclature_id, version, deleted);

create table if not exists trm_in_taxgroup
(
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  tax_id int default 0 not null,
  percent varchar(20) default '0' not null,
  fp_code varchar(20) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id, tax_id)
);

create index tax_id
  on trm_in_taxgroup (tax_id, id);

create index version
  on trm_in_taxgroup (nomenclature_id, version, deleted);

create table if not exists trm_in_transferings
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  epts int default 0 not null,
  param text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_in_transferings (store_id, version, deleted);

create table if not exists trm_in_users
(
  store_id int default 0 not null,
  id int default 0 not null,
  name varchar(40) default '' not null,
  user_inn varchar(20) null,
  password varchar(48) default '' not null,
  role_id bigint unsigned null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index role_id
  on trm_in_users (role_id);

create index version
  on trm_in_users (store_id, version, deleted);

create table if not exists trm_in_var
(
  nomenclature_id int default 0 not null,
  id varchar(40) default '' not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  tare_weight decimal(20,4) default 0.0000 not null,
  feat1 varchar(40) default '' not null,
  feat2 varchar(40) default '' not null,
  feat3 varchar(40) default '' not null,
  stock int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, id)
);

create index item
  on trm_in_var (nomenclature_id, item, deleted);

create index versions
  on trm_in_var (nomenclature_id, version, deleted);

create table if not exists trm_in_var_fur_identity
(
  nomenclature_id int default 0 not null,
  var_id varchar(40) default '' not null,
  fur_identity varchar(40) default '' not null,
  version bigint unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, var_id, fur_identity)
);

create index version
  on trm_in_var_fur_identity (nomenclature_id, version, deleted);

create table if not exists trm_in_var_property_values
(
  nomenclature_id int default 0 not null,
  var_id varchar(40) default '' not null,
  property_code varchar(40) default '' not null,
  property_id int unsigned default 0 not null,
  sequence smallint(5) unsigned default 0 not null,
  version int unsigned default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (nomenclature_id, var_id, property_code)
);

create index group_property
  on trm_in_var_property_values (nomenclature_id, property_code, property_id);

create index version
  on trm_in_var_property_values (nomenclature_id, version, deleted);

create table if not exists trm_in_working_life
(
  store_id int default 0 not null,
  id int unsigned default 0 not null,
  working_life datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id),
  constraint cash_line_id
    unique (store_id)
);

create index version
  on trm_in_working_life (store_id, version, deleted);

create table if not exists trm_line_acceptance_item_defect
(
  store_id int default 0 not null comment 'id магазина (trm_line_delivery.store_id)',
  delivery varchar(40) default '' not null comment 'id документа (trm_line_delivery.id)',
  attempt int default 0 not null comment 'итерация приемки',
  item varchar(40) default '' not null comment 'артикул товара',
  defect_code varchar(40) default '' not null comment 'код брака (trm_in_defect.defect_code)',
  quantity decimal(20,4) default 0.0000 not null comment 'количество брака',
  comments varchar(100) null comment 'комментарий',
  sequence int default 0 not null comment 'порядковый номер',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, delivery, attempt, item, defect_code)
)
  comment 'непринятые товары';

create index version
  on trm_line_acceptance_item_defect (store_id, version, deleted);

create table if not exists trm_line_acceptance_items
(
  store_id int default 0 not null comment 'id магазина (trm_line_delivery.store_id)',
  delivery varchar(40) default '' not null comment 'id документа (trm_line_delivery.id)',
  attempt int default 0 not null comment 'итерация приемки',
  item varchar(40) default '' not null comment 'артикул товара',
  var varchar(40) null comment 'штрихкод товара',
  quantity decimal(20,4) default 0.0000 not null comment 'принятое количество',
  tax decimal(20,4) null comment 'налог',
  amount decimal(20,4) null comment 'сумма',
  title varchar(100) null comment 'примечание',
  appeal_reason varchar(40) null comment 'причина отказа',
  sequence int default 0 not null comment 'порядковый номер',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, delivery, attempt, item)
)
  comment 'фактически притые товары';

create index version
  on trm_line_acceptance_items (store_id, version, deleted);

create table if not exists trm_line_delivery
(
  store_id int default 0 not null comment 'id магазина (store_id.store_id)',
  id varchar(40) default '' not null comment 'id документа',
  number varchar(40) default '' not null comment 'номер приходной накладной (asn_nbr)',
  shipment varchar(40) null comment 'номер огрузки',
  ref_shipment varchar(40) null comment 'отгрузки',
  title varchar(100) default '' null comment 'название документа',
  barcode varchar(100) null comment 'штрихкод (пропуска)',
  date_from datetime null comment 'ожидаемая дата (начало диапазона)',
  date_to datetime null comment 'ожидаемая дата (конец диапазона)',
  operation_type int null comment 'бизнес процесс (приход товара, возврат от покупателя и т.д.)',
  operation_date datetime null comment 'дата и время создания операции в управляющей системе',
  consignee varchar(40) null comment 'грузополучатель',
  supplier varchar(40) null comment 'поставщик',
  supplier_transport varchar(100) null comment 'номер машины поставщика',
  shipper varchar(40) null comment 'грузоотправитель',
  payer varchar(40) null comment 'плательщик',
  reason_number varchar(40) null comment 'номер договора или заказа поставщику',
  reason_date datetime null comment 'дата договора или заказа поставщику',
  container int null,
  alt_type varchar(40) null,
  plan_gate int null comment 'ожидаемый номер ворот',
  gate int null comment 'реальный номер ворот',
  plan_pallet_count int null comment 'ожидаемое количество паллет',
  pallet_count int null comment 'реальное количество паллет',
  pallet_capacity decimal(20,1) null comment 'количество паллетомест',
  invoice_number varchar(40) null comment 'номер накладной',
  invoice_amount decimal(20,4) null comment 'сумма накладной',
  invoice_date date null comment 'дата накладной',
  agent varchar(100) null comment 'ФИО водителя-экспедитора',
  handle_type varchar(40) null,
  stream varchar(40) null,
  command_client varchar(100) null,
  status int default 0 not null comment 'текущий статус документа',
  status_date datetime default '0000-00-00 00:00:00' not null comment 'дата установки статуса',
  attempt int default 0 not null comment 'номер итерации приемки',
  print_flag char(38) null comment 'признак печати накладной',
  bp varchar(40) default '' not null comment 'id бизнеспроцесса',
  act_type int default 0 not null comment 'тип акта (ревизия/инвентаризация)',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
)
  comment 'документ приемки/инвентаризации/ревизии';

create index number
  on trm_line_delivery (store_id, number);

create index version
  on trm_line_delivery (store_id, version, deleted);

create table if not exists trm_line_delivery_acceptance_pallet
(
  store_id int default 0 not null comment 'id магазина (trm_line_delivery.store_id)',
  delivery_id varchar(40) default '' not null comment 'id документа (trm_line_delivery.id)',
  pallet_code varchar(40) default '' not null comment 'код тары (trm_in_pallet.pallete_code)',
  count int default 0 null comment 'количество',
  defect int default 0 null comment 'брак',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, delivery_id, pallet_code)
)
  comment 'принятая тара';

create index version
  on trm_line_delivery_acceptance_pallet (store_id, version, deleted);

create table if not exists trm_line_delivery_items
(
  store_id int default 0 not null comment 'id магазина (trm_line_delivery.store_id)',
  delivery varchar(40) default '' not null comment 'id документа (trm_line_delivery.id)',
  item varchar(40) default '' not null comment 'артикул товара',
  sequence int default 0 not null comment 'порядковый номер в документе',
  quantity decimal(20,4) default 0.0000 not null comment 'количество',
  price decimal(20,4) null comment 'цена',
  title varchar(100) null comment 'примечание',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, delivery, item)
)
  comment 'запланированные товары в документах приемки (состав)';

create index version
  on trm_line_delivery_items (store_id, version, deleted);

create table if not exists trm_line_delivery_return_pallet
(
  store_id int default 0 not null comment 'id магазина (trm_line_delivery.store_id)',
  delivery_id varchar(40) default '' not null comment 'id документа (trm_line_delivery.id)',
  pallet_code varchar(40) default '' not null comment 'код тары (trm_in_pallete.pallete_code)',
  count int default 0 null comment 'количество',
  defect int default 0 null comment 'дефект',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, delivery_id, pallet_code)
)
  comment 'возвращенная тара';

create index version
  on trm_line_delivery_return_pallet (store_id, version, deleted);

create table if not exists trm_line_delivery_status
(
  store_id int default 0 not null comment 'id магазина (trm_line_delivery.store_id)',
  delivery_id varchar(40) default '' not null comment 'id документа (trm_line_delivery.id)',
  sequence int auto_increment comment 'порядковый номер',
  status int default 0 not null comment 'статус документа',
  user_id varchar(40) default '{00000000-0000-0000-0000-000000000000}' not null comment 'пользователь, установивший статус',
  date datetime default '0000-00-00 00:00:00' not null comment 'дата установки статуса',
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, delivery_id, sequence)
);

create index sequence
  on trm_line_delivery_status (sequence);

create index version
  on trm_line_delivery_status (store_id, version, deleted);

create table if not exists trm_line_inventory
(
  store_id int default 0 not null,
  id int default 0 not null,
  number varchar(40) default '0' not null,
  method tinyint(1) default 0 not null,
  date_inventory datetime null,
  purpose text null,
  comment text null,
  status tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id)
);

create index version
  on trm_line_inventory (store_id, version, deleted);

create table if not exists trm_line_inventory_items
(
  store_id int default 0 not null,
  inventory int default 0 not null,
  item varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, inventory, item)
);

create index version
  on trm_line_inventory_items (store_id, version, deleted);

create table if not exists trm_line_inventory_session
(
  store_id int default 0 not null,
  inventory int default 0 not null,
  id int default 0 not null,
  zone int default 0 not null,
  user char(38) default '' not null,
  opened datetime default '0000-00-00 00:00:00' not null,
  elements int default 0 not null,
  relocation_count int default 0 not null,
  error_count int default 0 not null,
  close_result int null,
  closed datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, inventory, id)
);

create index version
  on trm_line_inventory_session (store_id, version, deleted);

create table if not exists trm_line_inventory_session_item
(
  store_id int default 0 not null,
  inventory int default 0 not null,
  session int default 0 not null,
  element int default 0 not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, inventory, session, element, item)
);

create index version
  on trm_line_inventory_session_item (store_id, version, deleted);

create table if not exists trm_line_inventory_zone
(
  store_id int default 0 not null,
  id int default 0 not null,
  barcode varchar(100) null,
  name varchar(40) default '' not null,
  description text null,
  comment text null,
  elements_count int null,
  relocation_period varchar(5) null,
  status tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, id),
  constraint barcode
    unique (store_id, barcode)
);

create index version
  on trm_line_inventory_zone (store_id, version, deleted);

create definer = ukm_server@localhost trigger trm_line_inventory_zone_after_insert_tr
  after INSERT on trm_line_inventory_zone
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger trm_line_inventory_zone_after_update_tr
  after UPDATE on trm_line_inventory_zone
  for each row
-- missing source code
;

create table if not exists trm_line_inventory_zone_default_users
(
  store_id int default 0 not null,
  zone int default 0 not null,
  user varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, zone, user)
);

create index version
  on trm_line_inventory_zone_default_users (store_id, version, deleted);

create table if not exists trm_line_inventory_zone_users
(
  store_id int default 0 not null,
  inventory int default 0 not null,
  zone int default 0 not null,
  user char(38) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, inventory, zone, user)
);

create index version
  on trm_line_inventory_zone_users (store_id, version, deleted);

create definer = ukm_server@localhost trigger trm_line_inventory_zone_users_after_delete_tr
  after DELETE on trm_line_inventory_zone_users
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger trm_line_inventory_zone_users_after_insert_tr
  after INSERT on trm_line_inventory_zone_users
  for each row
-- missing source code
;

create definer = ukm_server@localhost trigger trm_line_inventory_zone_users_after_update_tr
  after UPDATE on trm_line_inventory_zone_users
  for each row
-- missing source code
;

create table if not exists trm_line_oper_day
(
  store_id bigint unsigned default 0 not null,
  date date default '0000-00-00' not null,
  state int(11) unsigned default 0 not null,
  comment text null,
  responsible_cashier varchar(40) null,
  user varchar(40) null,
  close_date datetime null,
  unloaded tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (store_id, date)
);

create index unload_state
  on trm_line_oper_day (store_id, unloaded);

create index version
  on trm_line_oper_day (store_id, version, deleted);

create table if not exists trm_offline_account_local_transaction
(
  cash_id bigint default 0 not null,
  local_transaction_id varchar(40) default '' not null,
  account_id int not null,
  amount decimal(20,2) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, local_transaction_id)
);

create index cash_account
  on trm_offline_account_local_transaction (cash_id, account_id, deleted);

create index version
  on trm_offline_account_local_transaction (cash_id, version, deleted);

create table if not exists trm_out_access_permissions
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  receipt_item bigint unsigned null,
  user_id bigint(19) default 0 not null,
  user_name varchar(100) default '' not null,
  operation_code int not null,
  oper_date datetime not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, oper_date)
);

create index cash_id
  on trm_out_access_permissions (cash_id, receipt_id);

create index versions
  on trm_out_access_permissions (cash_id, version, deleted);

create table if not exists trm_out_aoo
(
  cash_id bigint unsigned default 0 not null,
  id varchar(40) default '' not null,
  account_type_id bigint(11) unsigned default 0 not null,
  account_type_name varchar(40) default '' not null,
  client_id varchar(40) default '' not null,
  journal tinyint(1) default 0 not null,
  sync_type tinyint(1) default 0 not null,
  type tinyint(1) default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  balance_before decimal(20,4) default 0.0000 not null,
  balance_after decimal(20,4) default 0.0000 not null,
  datetime datetime default '0000-00-00 00:00:00' not null,
  comment text null,
  completed tinyint(1) default 0 not null,
  ver_completed int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index completeds
  on trm_out_aoo (journal, account_type_id, completed, deleted);

create index sync_types
  on trm_out_aoo (journal, sync_type, account_type_id, completed, deleted);

create index versions
  on trm_out_aoo (cash_id, version, deleted);

create table if not exists trm_out_bmsgroup_report
(
  cash_id bigint unsigned not null,
  receipt_id bigint unsigned not null,
  store_id bigint unsigned not null,
  operation_type tinyint(1) default 0 not null,
  rrn varchar(50) not null,
  tr_datetime datetime default '0000-00-00 00:00:00' not null,
  card_num varchar(40) not null,
  AccruedBonuses decimal(20,4) default 0.0000 not null,
  SpentBonuses decimal(20,4) default 0.0000 not null,
  DiscountBonuses decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id)
);

create index report
  on trm_out_bmsgroup_report (operation_type, store_id, tr_datetime);

create index versions
  on trm_out_bmsgroup_report (cash_id, version, deleted);

create table if not exists trm_out_bpm_loyality_offline
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  request mediumblob not null,
  result_code int default 0 not null,
  completed tinyint(1) default 0 not null,
  ver_completed int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index completed
  on trm_out_bpm_loyality_offline (completed, deleted);

create index versions
  on trm_out_bpm_loyality_offline (cash_id, version, deleted);

create table if not exists trm_out_inquirer_answer
(
  cash_id bigint unsigned default 0 not null,
  inquirer_id varchar(40) default '' not null,
  answer_id varchar(40) default '' not null,
  count int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, inquirer_id, answer_id)
);

create index versions
  on trm_out_inquirer_answer (cash_id, version, deleted);

create table if not exists trm_out_inquirer_detailed_answer
(
  cash_id bigint unsigned default 0 not null comment 'id-кассы',
  receipt_id bigint unsigned default 0 not null comment 'id-чека',
  date datetime default '0000-00-00 00:00:00' not null comment 'дата-время ответа',
  inq_id varchar(40) default '' not null comment 'id-опроса',
  inq_name varchar(40) default '' not null comment 'Название опроса',
  inq_question text not null comment 'Текст вопроса',
  answ_id varchar(40) default '' not null comment 'id-ответа',
  answ_text varchar(100) default '' not null comment 'текст ответа',
  version int(11) unsigned default 0 not null,
  deleted tinyint(1) unsigned default 0 not null,
  primary key (cash_id, receipt_id, inq_id)
);

create index version
  on trm_out_inquirer_detailed_answer (cash_id, version, deleted);

create table if not exists trm_out_login
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned auto_increment,
  user_id bigint(19) default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  user_name varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_login (id);

create index user_id
  on trm_out_login (user_id);

create index versions
  on trm_out_login (cash_id, version, deleted);

create table if not exists trm_out_logout
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_logout (cash_id, version, deleted);

create table if not exists trm_out_loyality_award_coupons
(
  cash_id bigint unsigned default 0 not null,
  efts int default 0 not null,
  transaction_id varchar(50) not null,
  coupon_num varchar(40) not null,
  coupon_amount decimal(20,2) null,
  effect_date datetime null,
  expiry_date datetime null,
  comment_txt varchar(255) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, efts, transaction_id, coupon_num)
);

create index versions
  on trm_out_loyality_award_coupons (cash_id, version, deleted);

create table if not exists trm_out_loyality_transactions
(
  cash_id bigint unsigned default 0 not null,
  efts int default 0 not null,
  transaction_id varchar(50) not null,
  receipt_id bigint unsigned default 0 not null,
  transaction_time datetime default '0000-00-00 00:00:00' not null,
  operation_type int(4) default 0 not null,
  operation_mode int(4) default 0 not null,
  card_num varchar(128) default '' not null,
  discount_amount decimal(20,2) default 0.00 not null,
  award_bonuses decimal(20,2) default 0.00 not null,
  used_bonuses decimal(20,2) default 0.00 not null,
  used_coupons int default 0 not null,
  award_coupons int default 0 not null,
  used_gifts int default 0 not null,
  is_canceled tinyint(1) default 0 not null,
  orig_transaction_id varchar(50) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, efts, transaction_id)
);

create index receipt
  on trm_out_loyality_transactions (cash_id, receipt_id, deleted);

create index store_date
  on trm_out_loyality_transactions (cash_id, transaction_time);

create index versions
  on trm_out_loyality_transactions (cash_id, version, deleted);

create table if not exists trm_out_loyality_used_coupons
(
  cash_id bigint unsigned default 0 not null,
  efts int default 0 not null,
  transaction_id varchar(50) not null,
  coupon_num varchar(40) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, efts, transaction_id, coupon_num)
);

create index versions
  on trm_out_loyality_used_coupons (cash_id, version, deleted);

create table if not exists trm_out_loyality_used_gifts
(
  cash_id bigint unsigned default 0 not null,
  efts int default 0 not null,
  transaction_id varchar(50) not null,
  item varchar(40) default '' not null,
  quantity decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, efts, transaction_id, item)
);

create index versions
  on trm_out_loyality_used_gifts (cash_id, version, deleted);

create table if not exists trm_out_manzana_ds2_offline
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  request mediumblob not null,
  result_code int default 0 not null,
  completed tinyint(1) default 0 not null,
  ver_completed int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index completed
  on trm_out_manzana_ds2_offline (completed, deleted);

create index versions
  on trm_out_manzana_ds2_offline (cash_id, version, deleted);

create table if not exists trm_out_manzana_mk_offline
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  request mediumblob not null,
  result_code int default 0 not null,
  completed tinyint(1) default 0 not null,
  ver_completed int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index completed
  on trm_out_manzana_mk_offline (completed, deleted);

create index versions
  on trm_out_manzana_mk_offline (cash_id, version, deleted);

create table if not exists trm_out_manzana_ml_offline
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  request mediumblob not null,
  result_code int default 0 not null,
  completed tinyint(1) default 0 not null,
  ver_completed int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index completed
  on trm_out_manzana_ml_offline (completed, deleted);

create index versions
  on trm_out_manzana_ml_offline (cash_id, version, deleted);

create table if not exists trm_out_moneyoperation
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  global_number bigint unsigned null,
  local_number bigint unsigned null,
  login bigint unsigned null,
  shift_number bigint unsigned null,
  date datetime default '0000-00-00 00:00:00' not null,
  type bigint unsigned null,
  auto tinyint(1) default 0 not null,
  payment_id bigint unsigned null,
  payment_name varchar(100) null,
  amount decimal(20,4) null,
  amount_before decimal(20,4) null,
  cookies text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_moneyoperation (id);

create index login
  on trm_out_moneyoperation (login);

create index payment_id
  on trm_out_moneyoperation (payment_id);

create index shift_number
  on trm_out_moneyoperation (cash_id, shift_number);

create index versions
  on trm_out_moneyoperation (cash_id, version, deleted);

create table if not exists trm_out_moneyoperation_note
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  moneyoperation bigint unsigned null,
  note decimal(20,4) null,
  note_type tinyint(1) default 0 not null,
  count bigint unsigned null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_moneyoperation_note (id);

create index moneyoperation
  on trm_out_moneyoperation_note (cash_id, moneyoperation);

create index versions
  on trm_out_moneyoperation_note (cash_id, version, deleted);

create table if not exists trm_out_moneyoperation_props
(
  cash_id bigint not null,
  moneyoperation_id bigint not null,
  courier_order int null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, moneyoperation_id)
);

create index versions
  on trm_out_moneyoperation_props (cash_id, version, deleted);

create table if not exists trm_out_pos_block_state
(
  cash_id bigint unsigned not null
    primary key,
  fiscal_num varchar(10) null,
  date_first_not_send datetime null,
  date_last_update datetime null,
  is_block tinyint(1) default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_out_pos_block_state (cash_id, version, deleted);

create table if not exists trm_out_pos_state
(
  cash_id bigint unsigned default 0 not null
    primary key,
  state tinyint(1) default 0 not null,
  ip varchar(16) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index version
  on trm_out_pos_state (cash_id, version, deleted);

create table if not exists trm_out_receipt_account_balances
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  acсount_type_id int default 0 not null,
  client_id varchar(128) not null,
  att int default 0 not null,
  name varchar(40) null,
  online tinyint(1) default 0 not null,
  initial_balance decimal(20,4) null,
  credit decimal(20,4) default 0.0000 not null,
  is_open tinyint(1) default 0 not null,
  print_in_receipt tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id, acсount_type_id, client_id)
);

create index versions
  on trm_out_receipt_account_balances (cash_id, version, deleted);

create table if not exists trm_out_receipt_additive
(
  cash_id bigint unsigned default 0 not null,
  id bigint default 0 not null,
  additivitygroup int default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_additive (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_adder
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  discount_id bigint unsigned default 0 not null,
  client_id varchar(40) default '' not null,
  adder_type int(11) unsigned default 0 not null,
  name varchar(40) null,
  amount decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index discount
  on trm_out_receipt_discount_adder (discount_id, client_id, adder_type);

create index id
  on trm_out_receipt_discount_adder (id);

create index versions
  on trm_out_receipt_discount_adder (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_aoo
(
  cash_id bigint unsigned default 0 not null,
  discount_id bigint unsigned default 0 not null,
  operation_id varchar(40) default '' not null,
  receipt_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, discount_id, operation_id)
);

create index operation_id
  on trm_out_receipt_discount_aoo (cash_id, operation_id, deleted);

create index receipt
  on trm_out_receipt_discount_aoo (cash_id, receipt_id, deleted);

create index versioins
  on trm_out_receipt_discount_aoo (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_coupons
(
  cash_id bigint unsigned not null,
  receipt_discount bigint unsigned not null,
  coupon_order int(3) unsigned not null,
  amount decimal(20,2) not null,
  coupon_type int not null,
  coupon_type_name varchar(100) not null,
  number varchar(25) not null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (cash_id, receipt_discount, coupon_order)
);

create table if not exists trm_out_receipt_discount_coupons_accept
(
  cash_id bigint unsigned not null,
  receipt_discount bigint unsigned not null,
  coupon_order int(3) unsigned not null,
  amount decimal(20,2) not null,
  coupon_type int not null,
  coupon_type_name varchar(100) not null,
  number varchar(25) not null,
  version int default 0 null,
  deleted tinyint(1) default 0 null,
  primary key (cash_id, receipt_discount, coupon_order)
);

create table if not exists trm_out_receipt_discount_eps_offline
(
  cash_id bigint unsigned default 0 not null,
  doc_nr varchar(30) default '' not null,
  receipt_id bigint unsigned default 0 not null,
  discount_id bigint unsigned default 0 not null,
  doc_status int default 0 not null,
  doc_closed_cmd mediumblob null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, doc_nr)
);

create index discount
  on trm_out_receipt_discount_eps_offline (cash_id, discount_id);

create index receipt
  on trm_out_receipt_discount_eps_offline (cash_id, receipt_id);

create index versions
  on trm_out_receipt_discount_eps_offline (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_gifts
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_discount bigint unsigned default 0 not null,
  item varchar(40) default '' not null,
  item_name varchar(40) default '' not null,
  amount decimal(20,3) default 0.000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_discount_gifts (id);

create index versions
  on trm_out_receipt_discount_gifts (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_handler
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  owner_id bigint unsigned default 0 not null,
  epts bigint unsigned default 0 not null,
  status varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_discount_handler (id);

create index owner_id
  on trm_out_receipt_discount_handler (owner_id);

create index receipt_discount_id
  on trm_out_receipt_discount_handler (cash_id, owner_id);

create index versions
  on trm_out_receipt_discount_handler (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_item_properties
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_item_discount bigint unsigned default 0 not null,
  code int default 0 not null,
  value text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_discount_item_properties (id);

create index receipt_item_discount
  on trm_out_receipt_discount_item_properties (cash_id, receipt_item_discount, code);

create index versions
  on trm_out_receipt_discount_item_properties (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_loymax15offline
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  operation_type tinyint(1) default 0 not null,
  request mediumblob null,
  request_status int default 0 not null,
  completed tinyint(1) default 0 not null,
  ver_completed int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id, operation_type)
);

create index completed
  on trm_out_receipt_discount_loymax15offline (completed, deleted);

create index versions
  on trm_out_receipt_discount_loymax15offline (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_loymax_offline
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  operation_type tinyint(1) default 0 not null,
  request mediumblob null,
  request_status int default 0 not null,
  completed tinyint(1) default 0 not null,
  ver_completed int default 0 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id, operation_type)
);

create index completed
  on trm_out_receipt_discount_loymax_offline (completed, deleted);

create index versions
  on trm_out_receipt_discount_loymax_offline (cash_id, version, deleted);

create table if not exists trm_out_receipt_discount_properties
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_discount bigint unsigned default 0 not null,
  code int default 0 not null,
  value text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_discount_properties (id);

create index receipt_discount
  on trm_out_receipt_discount_properties (cash_id, receipt_discount, code);

create index receipt_item
  on trm_out_receipt_discount_properties (receipt_discount, code);

create index versions
  on trm_out_receipt_discount_properties (cash_id, version, deleted);

create table if not exists trm_out_receipt_discounts
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_header bigint unsigned default 0 not null,
  name varchar(100) default '' not null,
  type tinyint default 0 not null,
  efts int unsigned default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  account_amount decimal(20,4) default 0.0000 not null,
  discount_type bigint default 0 not null,
  card_type int(11) unsigned null,
  card_number varchar(40) null,
  link_disccard bigint unsigned null,
  sequence int default 0 not null,
  group_type int null,
  marketing_effort_id bigint default 0 not null,
  marketing_effort_name varchar(100) default '' not null,
  advertising_campaign_id bigint default 0 not null,
  advertising_campaign_name varchar(100) default '' not null,
  application_condition text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index discount_type
  on trm_out_receipt_discounts (discount_type);

create index efts
  on trm_out_receipt_discounts (receipt_header, efts);

create index id
  on trm_out_receipt_discounts (id);

create index receipt_header_id
  on trm_out_receipt_discounts (cash_id, receipt_header);

create index versions
  on trm_out_receipt_discounts (cash_id, version, deleted);

create table if not exists trm_out_receipt_dish
(
  cash_id bigint unsigned default 0 not null,
  id bigint default 0 not null,
  short_name varchar(40) default '' not null,
  comment varchar(128) default '' not null,
  recipe varchar(128) default '' not null,
  show_additives tinyint(1) default 1 not null,
  min_additives int null,
  max_free_additives int null,
  max_additives int null,
  rmenu bigint default 0 not null,
  is_complex tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_dish (cash_id, version, deleted);

create table if not exists trm_out_receipt_egais
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  url text null,
  sign text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_egais (cash_id, version, deleted);

create table if not exists trm_out_receipt_external_order_items
(
  cash_id bigint not null,
  receipt_item_id bigint not null,
  store_id int not null,
  order_id bigint not null,
  item varchar(40) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_item_id)
);

create index versions
  on trm_out_receipt_external_order_items (cash_id, version, deleted);

create table if not exists trm_out_receipt_external_orders
(
  cash_id bigint not null,
  id bigint not null,
  store_id int not null,
  order_id bigint not null,
  ext_order_type varchar(100) not null,
  ext_order_id varchar(100) not null,
  order_date datetime null comment 'Дата заказа',
  sale_type tinyint(1) not null comment 'Оплата заказа на кассе/отложенная оплата',
  wholesale tinyint(1) not null comment 'Оптовый/розничный',
  client_name varchar(255) not null comment 'ФИО клиента',
  check_structure tinyint(1) not null comment 'Флаг необходимость проверки содержания заказа при оплате',
  changeable tinyint(1) not null comment 'Флаг - разрешено на кассе изменять содержание заказа или не разрешено',
  amount decimal(20,4) not null comment 'Cумма заказа',
  status_before tinyint(5) not null,
  status tinyint(5) null,
  appeal_code varchar(40) null,
  appeal_title varchar(255) null,
  appeal_comment varchar(100) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_external_orders (cash_id, version, deleted);

create table if not exists trm_out_receipt_footer
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  result bigint unsigned null,
  date datetime default '0000-00-00 00:00:00' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index date
  on trm_out_receipt_footer (date);

create index result
  on trm_out_receipt_footer (result);

create index versions
  on trm_out_receipt_footer (cash_id, version, deleted);

create table if not exists trm_out_receipt_header
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  global_number bigint unsigned default 0 not null,
  local_number bigint unsigned default 0 not null,
  type bigint unsigned null,
  stock_id bigint unsigned null,
  stock_name varchar(100) null,
  client varchar(40) null,
  card int(11) unsigned null,
  login bigint unsigned null,
  shift_open bigint unsigned null,
  date datetime default '0000-00-00 00:00:00' not null,
  pos int default 0 not null,
  pos_name varchar(80) default '' not null,
  sale_type tinyint(1) unsigned null,
  customer_address varchar(80) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index client
  on trm_out_receipt_header (client);

create index date
  on trm_out_receipt_header (date);

create index global_number
  on trm_out_receipt_header (global_number);

create index id
  on trm_out_receipt_header (id);

create index local_number
  on trm_out_receipt_header (local_number);

create index login
  on trm_out_receipt_header (login);

create index shift_open
  on trm_out_receipt_header (cash_id, shift_open);

create index type
  on trm_out_receipt_header (type);

create index versions
  on trm_out_receipt_header (cash_id, version, deleted);

create table if not exists trm_out_receipt_header_restaurant
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  guests bigint unsigned null,
  table_number bigint unsigned null,
  waiter_id bigint null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_header_restaurant (cash_id, version, deleted);

create table if not exists trm_out_receipt_header_return
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  return_type bigint not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_header_return (id);

create index type
  on trm_out_receipt_header_return (return_type);

create index versions
  on trm_out_receipt_header_return (cash_id, version, deleted);

create table if not exists trm_out_receipt_invoice
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  number varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_invoice (cash_id, version, deleted);

create table if not exists trm_out_receipt_item
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_header bigint unsigned default 0 not null,
  var varchar(40) default '' not null,
  item varchar(40) default '' not null,
  name varchar(255) default '' not null,
  var_quantity decimal(20,4) null,
  var_tare decimal(20,4) default 0.0000 not null,
  quantity decimal(20,4) null,
  total_quantity decimal(20,4) default 0.0000 not null,
  price decimal(20,4) default 0.0000 not null,
  min_price decimal(20,4) default 0.0000 not null,
  blocked_discount tinyint(1) default 0 not null,
  total decimal(20,4) null,
  discount decimal(20,4) default 0.0000 not null,
  account_discount decimal(20,4) default 0.0000 not null,
  stock_id bigint unsigned null,
  stock_name varchar(100) null,
  measurement varchar(100) default '' not null,
  measurement_precision bigint unsigned default 0 not null,
  classif varchar(40) default '0' not null,
  type bigint unsigned default 0 not null,
  weight bigint unsigned null,
  input bigint unsigned null,
  tax int null,
  country varchar(40) null,
  link_item bigint null,
  parent_item bigint null,
  position int default 0 not null,
  remain decimal(20,4) null,
  pricelist int(11) unsigned null,
  cancellation_reason_id int null,
  seller_name varchar(100) default '' null,
  seller_code varchar(50) default '' null,
  blocked_add tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_item (id);

create index link_item
  on trm_out_receipt_item (cash_id, link_item, receipt_header);

create index name
  on trm_out_receipt_item (name);

create index receipt_header
  on trm_out_receipt_item (cash_id, receipt_header, id);

create index seller_code
  on trm_out_receipt_item (seller_code);

create index seller_name
  on trm_out_receipt_item (seller_name);

create index type
  on trm_out_receipt_item (type);

create index versions
  on trm_out_receipt_item (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_addition
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  sold_by_promo_price tinyint(1) default 0 not null,
  init_price decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_item_addition (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_agent
(
  cash_id bigint not null,
  header_id bigint not null,
  receipt_item bigint not null,
  item_agent_data text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, header_id, receipt_item)
);

create index versions
  on trm_out_receipt_item_agent (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_aoo
(
  cash_id bigint unsigned default 0 not null,
  item_id bigint unsigned default 0 not null,
  operation_id varchar(40) default '' not null,
  receipt_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, item_id, operation_id)
);

create index operation_id
  on trm_out_receipt_item_aoo (cash_id, operation_id, deleted);

create index receipt
  on trm_out_receipt_item_aoo (cash_id, receipt_id, deleted);

create index versioins
  on trm_out_receipt_item_aoo (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_discount
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_item bigint unsigned default 0 not null,
  base_total decimal(20,4) default 0.0000 not null,
  increment decimal(20,4) default 0.0000 not null,
  receipt_discount bigint unsigned default 0 not null,
  sequence int unsigned default 0 not null,
  account_increment decimal(20,4) default 0.0000 null,
  matrix_increment decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id),
  constraint receipt_item
    unique (cash_id, receipt_item, sequence)
);

create index discount
  on trm_out_receipt_item_discount (cash_id, receipt_item, receipt_discount);

create index id
  on trm_out_receipt_item_discount (id);

create index versions
  on trm_out_receipt_item_discount (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_egais
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  pos int(2) unsigned default 0 not null,
  egais_barcode varchar(200) null,
  beer_code mediumtext null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id, pos)
);

create index versions
  on trm_out_receipt_item_egais (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_handler
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  owner_id bigint unsigned null,
  epts bigint unsigned default 0 not null,
  status varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index cash_id
  on trm_out_receipt_item_handler (cash_id, owner_id);

create index epts
  on trm_out_receipt_item_handler (epts);

create index id
  on trm_out_receipt_item_handler (id);

create index versions
  on trm_out_receipt_item_handler (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_kiz_marks
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_header bigint unsigned default 0 not null,
  mark_type int default 0 not null,
  var varchar(255) default '' not null,
  barcode varchar(20) default '' not null,
  serial_num varchar(20) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index receipt_header
  on trm_out_receipt_item_kiz_marks (cash_id, receipt_header, id);

create index versions
  on trm_out_receipt_item_kiz_marks (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_property_values
(
  cash_id bigint unsigned default 0 not null,
  id bigint default 0 not null,
  receipt_item bigint default 0 not null,
  property_id bigint default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_item_property_values (id);

create index item_property
  on trm_out_receipt_item_property_values (cash_id, receipt_item, property_id);

create index property
  on trm_out_receipt_item_property_values (property_id);

create index versions
  on trm_out_receipt_item_property_values (cash_id, version, deleted);

create table if not exists trm_out_receipt_item_tax
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_item bigint unsigned default 0 not null,
  receipt_tax bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id),
  constraint tax
    unique (cash_id, receipt_item, receipt_tax)
);

create index id
  on trm_out_receipt_item_tax (id);

create index receipt_item
  on trm_out_receipt_item_tax (receipt_item);

create index versions
  on trm_out_receipt_item_tax (cash_id, version, deleted);

create table if not exists trm_out_receipt_kkm
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  kkm_serial_number varchar(40) default '' not null,
  kkm_owner_number varchar(40) default '' not null,
  kkm_model_name varchar(40) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id)
);

create index kkm_serial_number
  on trm_out_receipt_kkm (kkm_serial_number);

create index versions
  on trm_out_receipt_kkm (cash_id, version, deleted);

create table if not exists trm_out_receipt_link
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_link_header bigint unsigned null,
  link_cash_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index receipt_link_header
  on trm_out_receipt_link (receipt_link_header);

create index versions
  on trm_out_receipt_link (cash_id, version, deleted);

create table if not exists trm_out_receipt_payment
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_header bigint unsigned null,
  type bigint unsigned null,
  payment_id bigint unsigned null,
  payment_name varchar(100) null,
  amount decimal(20,4) null,
  amount_with_change decimal(20,4) null,
  cookies text null,
  link bigint unsigned null,
  card_number varchar(128) null,
  auth_code varchar(100) null,
  ref_number varchar(100) null,
  is_cash tinyint(1) default 1 not null,
  kkt_payment_form tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index amount
  on trm_out_receipt_payment (amount);

create index id
  on trm_out_receipt_payment (id);

create index payment_id
  on trm_out_receipt_payment (payment_id);

create index payment_name
  on trm_out_receipt_payment (payment_name);

create index receipt_header
  on trm_out_receipt_payment (cash_id, receipt_header);

create index type
  on trm_out_receipt_payment (type);

create index versions
  on trm_out_receipt_payment (cash_id, version, deleted);

create table if not exists trm_out_receipt_payment_aoo
(
  cash_id bigint unsigned default 0 not null,
  payment_id bigint unsigned default 0 not null,
  operation_id varchar(40) default '' not null,
  receipt_id bigint unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, payment_id, operation_id)
);

create index operation_id
  on trm_out_receipt_payment_aoo (cash_id, operation_id, deleted);

create index receipt
  on trm_out_receipt_payment_aoo (cash_id, receipt_id, deleted);

create index versioins
  on trm_out_receipt_payment_aoo (cash_id, version, deleted);

create table if not exists trm_out_receipt_payment_currency
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  currency_id int default 0 not null,
  currency_name varchar(40) default '' not null,
  rate decimal(20,4) null,
  amount decimal(20,4) null,
  amount_with_change decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index currency_id
  on trm_out_receipt_payment_currency (currency_id);

create index versions
  on trm_out_receipt_payment_currency (cash_id, version, deleted);

create table if not exists trm_out_receipt_payment_handler
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  owner_id bigint unsigned default 0 not null,
  epts bigint unsigned default 0 not null,
  status varchar(100) default '' not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_payment_handler (id);

create index owner
  on trm_out_receipt_payment_handler (cash_id, owner_id);

create index versions
  on trm_out_receipt_payment_handler (cash_id, version, deleted);

create table if not exists trm_out_receipt_payment_slip
(
  cash_id bigint unsigned default 0 not null,
  payment_id bigint unsigned default 0 not null,
  slip text null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, payment_id)
);

create index version
  on trm_out_receipt_payment_slip (version, deleted);

create table if not exists trm_out_receipt_payments_by_stocks
(
  cash_id bigint unsigned default 0 not null,
  payment_id bigint unsigned default 0 not null,
  stock_id bigint unsigned default 0 not null,
  amount decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, payment_id, stock_id)
);

create index versions
  on trm_out_receipt_payments_by_stocks (cash_id, version, deleted);

create table if not exists trm_out_receipt_properties
(
  cash_id bigint not null,
  header_id bigint not null,
  prop_name varchar(20) not null,
  prop_value text not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, header_id, prop_name)
);

create index version
  on trm_out_receipt_properties (cash_id, version, deleted);

create table if not exists trm_out_receipt_property_values
(
  cash_id bigint unsigned default 0 not null,
  id bigint default 0 not null,
  property_code varchar(20) default '' not null,
  sequence smallint(5) unsigned default 0 not null,
  display_name varchar(40) null,
  value_id int(11) unsigned default 0 not null,
  value varchar(100) default '' not null,
  comment text null,
  flags int unsigned default 0 not null,
  cookies int unsigned null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index cash_id
  on trm_out_receipt_property_values (cash_id, property_code, value_id);

create index id
  on trm_out_receipt_property_values (id);

create index property_code
  on trm_out_receipt_property_values (property_code);

create index versions
  on trm_out_receipt_property_values (cash_id, version, deleted);

create table if not exists trm_out_receipt_subtotal
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  amount decimal(20,4) null,
  discounts_amount decimal(20,4) default 0.0000 not null,
  discounts_account_amount decimal(20,4) default 0.0000 not null,
  items_count bigint unsigned null,
  date datetime default '0000-00-00 00:00:00' not null,
  clear_amount decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_receipt_subtotal (cash_id, version, deleted);

create table if not exists trm_out_receipt_tax
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  taxgroup_id int null,
  tax_id int null,
  name varchar(40) null,
  percent varchar(20) null,
  priority tinyint null,
  amount decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_receipt_tax (id);

create index tax_id
  on trm_out_receipt_tax (tax_id);

create index taxgroup_id
  on trm_out_receipt_tax (taxgroup_id);

create index versions
  on trm_out_receipt_tax (cash_id, version, deleted);

create table if not exists trm_out_sap_crm
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  datetime_gr datetime default '0000-00-00 00:00:00' not null,
  timezone varchar(6) default '' not null,
  store_id_ex varchar(40) default '' not null,
  pos_num int default 0 null,
  card_num varchar(40) null,
  tr_type varchar(10) default '' not null,
  service_code varchar(10) null,
  request text null,
  response text null,
  receipt_id bigint unsigned null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_sap_crm (cash_id, version, deleted);

create table if not exists trm_out_sequence
(
  cash_id bigint not null,
  sequence_name varchar(100) not null,
  value int not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, sequence_name)
);

create table if not exists trm_out_shift_close
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  login bigint unsigned default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  kkm_shift_number bigint unsigned null,
  kkm_serial_number varchar(40) default '' not null,
  kkm_registration_number varchar(40) default '' not null,
  kkm_owner_number varchar(40) default '' not null,
  eklz_number varchar(40) default '' not null,
  eklz_date_activate datetime null,
  eklz_fast_full tinyint(1) null,
  kkm_model_name varchar(40) default '' not null,
  kkt_lifePhase tinyint(1) unsigned null,
  kkt_shiftState tinyint(1) unsigned null,
  kkt_NeedChangeFN tinyint(1) unsigned null,
  kkt_EndingResourceFN tinyint(1) unsigned null,
  kkt_OverflowFN tinyint(1) unsigned null,
  kkt_LongWaitOFD tinyint(1) unsigned null,
  kkt_FN_Number varchar(20) null,
  kkt_fiscalDocNumber int(11) unsigned null,
  kkt_lifeTime date null,
  kkt_status tinyint(1) unsigned null,
  kkt_ffd_version varchar(10) null,
  kkt_ofdQueueLength int(11) unsigned null,
  kkt_firstQueueDocNumber int(11) unsigned null,
  kkt_firstQueueDocDateTime datetime null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index login
  on trm_out_shift_close (login);

create index versions
  on trm_out_shift_close (cash_id, version, deleted);

create table if not exists trm_out_shift_open
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  number bigint unsigned default 0 not null,
  login bigint unsigned default 0 not null,
  date datetime default '0000-00-00 00:00:00' not null,
  sale decimal(20,4) default 0.0000 not null,
  sreturn decimal(20,4) default 0.0000 not null,
  cancel decimal(20,4) default 0.0000 not null,
  cancel_return decimal(20,4) default 0.0000 not null,
  sale_fiscal decimal(20,4) default 0.0000 not null,
  sreturn_fiscal decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index date_idx
  on trm_out_shift_open (cash_id, date);

create index id
  on trm_out_shift_open (id);

create index login
  on trm_out_shift_open (login);

create index number
  on trm_out_shift_open (number);

create index versions
  on trm_out_shift_open (cash_id, version, deleted);

create table if not exists trm_out_shift_payments
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  payment int default 0 not null,
  user int default 0 not null,
  amount decimal(20,4) default 0.0000 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id, payment, user)
);

create index versions
  on trm_out_shift_payments (cash_id, version, deleted);

create table if not exists trm_out_shift_result
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipts_sale_ukm int(11) unsigned default 0 not null,
  receipts_return_ukm int(11) unsigned default 0 not null,
  receipts_cancel_ukm int(11) unsigned default 0 not null,
  receipts_sale_kkm int(11) unsigned default 0 null,
  receipts_return_kkm int(11) unsigned default 0 null,
  receipts_cancel_kkm int(11) unsigned default 0 null,
  insertion_ukm int(11) unsigned default 0 not null,
  withdrawal_ukm int(11) unsigned default 0 not null,
  insertion_kkm int(11) unsigned default 0 null,
  withdrawal_kkm int(11) unsigned default 0 null,
  sale_kkm decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index versions
  on trm_out_shift_result (cash_id, version, deleted);

create table if not exists trm_out_shift_result_payments
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  payment_id bigint unsigned default 0 not null,
  payment_name varchar(100) null,
  is_cash tinyint(1) default 1 not null,
  is_local tinyint(1) default 0 not null,
  sale_ukm decimal(20,4) default 0.0000 null,
  return_ukm decimal(20,4) default 0.0000 null,
  sale_kkm decimal(20,4) null,
  return_kkm decimal(20,4) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id, payment_id)
);

create index versions
  on trm_out_shift_result_payments (cash_id, version, deleted);

create table if not exists trm_out_siebel_offline_card
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  item_id bigint unsigned null,
  cheque_id varchar(40) not null,
  barcode_number int default 0 not null,
  barcode varchar(40) not null,
  card_type tinyint(1) not null,
  card_use tinyint(1) default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id),
  constraint action
    unique (cash_id, receipt_id, barcode)
);

create index version
  on trm_out_siebel_offline_card (cash_id, version, deleted);

create table if not exists trm_out_siebel_offline_certificate
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  item_id bigint unsigned null,
  cheque_id varchar(40) not null,
  barcode varchar(40) not null,
  card_use tinyint(1) default 2 not null,
  summ decimal(20,4) default 0.0000 null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id),
  constraint action
    unique (cash_id, receipt_id, barcode)
);

create index version
  on trm_out_siebel_offline_certificate (cash_id, version, deleted);

create table if not exists trm_out_siebel_offline_discount
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  item_id bigint unsigned default 0 not null,
  cheque_id varchar(40) not null,
  pos_number int default 0 not null,
  disc_id varchar(100) not null,
  disc_summ decimal(20,4) default 0.0000 null,
  disc_quantity decimal(20,4) default 0.0000 not null,
  disc_barcode varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id),
  constraint action
    unique (cash_id, receipt_id, pos_number, disc_id)
);

create index version
  on trm_out_siebel_offline_discount (cash_id, version, deleted);

create table if not exists trm_out_siebel_offline_gifts
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  cheque_id varchar(40) not null,
  gift_action_id varchar(100) not null,
  gift_number int default 0 not null,
  gift_disc_number int default 0 not null,
  item varchar(40) default '' not null,
  gift_quantity int default 1 not null,
  gift_disc_barcode varchar(40) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id),
  constraint dift
    unique (cash_id, receipt_id, gift_action_id, gift_disc_number, gift_number)
);

create index version
  on trm_out_siebel_offline_gifts (cash_id, version, deleted);

create table if not exists trm_out_siebel_offline_receipt
(
  cash_id bigint unsigned default 0 not null,
  receipt_id bigint unsigned default 0 not null,
  cheque_id varchar(40) not null,
  cash_number int not null,
  cheque_number int not null,
  shift_number int not null,
  shop_index varchar(40) default '' not null,
  card_number varchar(40) null,
  auth_by_phone_number tinyint(1) default 0 not null,
  cheque_open_date datetime default '0000-00-00 00:00:00' not null,
  operation tinyint(1) default 0 not null,
  purchase_cheque_id varchar(40) null,
  var_type tinyint(1) not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, receipt_id)
);

create index version
  on trm_out_siebel_offline_receipt (cash_id, version, deleted);

create table if not exists trm_out_table_operations
(
  cash_id bigint unsigned default 0 not null,
  id bigint unsigned default 0 not null,
  table_id int default 0 not null,
  operation int default 0 not null,
  op_time datetime default '0000-00-00 00:00:00' not null,
  login_id bigint default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, id)
);

create index id
  on trm_out_table_operations (id);

create index operation
  on trm_out_table_operations (operation);

create index table_id
  on trm_out_table_operations (table_id);

create index table_operation
  on trm_out_table_operations (table_id, operation);

create index versions
  on trm_out_table_operations (cash_id, version, deleted);

create table if not exists trm_out_updates_download_state
(
  cash_id bigint unsigned default 0 not null
    primary key,
  state int default 0 not null,
  percent smallint(6) default 0 not null,
  description varchar(255) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null
);

create index versions
  on trm_out_updates_download_state (cash_id, version, deleted);

create table if not exists trm_out_webmoney_sequence
(
  cash_id bigint unsigned default 0 not null,
  sequence_name varchar(50) not null,
  prefix varchar(10) not null,
  sequence_value int(11) unsigned default 0 not null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,
  primary key (cash_id, sequence_name, prefix)
);

create index version
  on trm_out_webmoney_sequence (cash_id, version, deleted);

create table if not exists ukm_job_scheduler
(
  ID int unsigned default 0 not null
    primary key,
  JOB_NAME varchar(80) default '' not null,
  JOB_PARAMETERS tinytext null,
  JOB_START_TIME datetime null,
  JOB_END_TIME datetime null,
  JOB_FREQUENCY int(10) default 0 not null,
  JOB_NEXT_START_TIME datetime null,
  ACTIVITY int(1) unsigned default 0 not null
);

create index JOB_NEXT_START_TIME
  on ukm_job_scheduler (JOB_NEXT_START_TIME, ACTIVITY);

create table if not exists ukm_operation_processing
(
  id bigint unsigned auto_increment
    primary key,
  status tinyint null,
  description text not null,
  changed timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP
)
  engine=MyISAM;

create table if not exists ukm_ukmversion
(
  version int unsigned default 0 not null
    primary key,
  full_version varchar(15) default '' not null,
  moment datetime default '0000-00-00 00:00:00' not null,
  cli_updates_version int unsigned default 0 not null
);

create table if not exists wbn_event
(
  Id varchar(50) not null,
  guid varchar(40) default '' not null,
  page varchar(100) default '' not null,
  event varchar(50) default '' not null,
  action smallint default 0 not null,
  params text null,
  ntime datetime default '0000-00-00 00:00:00' not null,
  constraint Id
    unique (Id)
)
  comment 'Уведомления для клиентов WEB';

create index guid_page
  on wbn_event (guid, page);

create index page
  on wbn_event (page);

alter table wbn_event
  add primary key (Id);

create table if not exists wbn_session
(
  guid varchar(40) default '' not null,
  page varchar(100) default '' not null,
  params text null,
  cur_state tinyint(1) default 1 not null,
  last_request datetime default '0000-00-00 00:00:00' not null,
  primary key (guid, page)
)
  comment 'Соединения клиентов WEB';

create definer = ukm_server@localhost trigger wbn_session_after_del_tr
  after DELETE on wbn_session
  for each row
-- missing source code
;

create or replace view ln_currency_name as -- missing source code
;

create or replace view ln_first_name_gd as -- missing source code
;

create or replace view ln_second_name_gd as -- missing source code
;

create or replace view local_auth_account_journal_oper_type as -- missing source code
;

create or replace view trm_line_delivery_items_view as -- missing source code
;

