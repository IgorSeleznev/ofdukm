{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "title": "Сводные данные по ККТ",
  "description": "Сводные данные по ККТ",
  "properties": {
    "reportDate": {
      "type": "string",
      "title": "Дата формирования ответа",
      "description": "Дата формирования ответа в формате: 2017-03-20T10:47:00",
      "format": "date-time"
    },
    "cashdesk": {
      "type": "object",
      "title": "Данные ККТ",
      "properties": {
        "name": {
          "type": "string",
          "title": "Название",
          "maxLength": 255
        },
        "kktRegNumber": {
          "type": "string",
          "title": "Регистрационный номер ККТ",
          "minLength": 1,
          "maxLength": 16
        },
        "kktFactoryNumber": {
          "type": "string",
          "title": "Заводской номер ККТ",
          "minLength": 1,
          "maxLength": 20
        },
        "kktModelName": {
          "type": "string",
          "title": "Модель ККТ",
          "minLength": 1,
          "maxLength": 255
        },
        "fnFactoryNumber": {
          "type": "string",
          "title": "Номер ФН",
          "minLength": 1,
          "maxLength": 16
        },
        "fnRegDateTime": {
          "type": "string",
          "title": "Дата регистрации ФН",
          "description": "Дата регистрации ФН в формате: 2017-03-20T10:47:00",
          "format": "date-time"
        },
        "fnDuration": {
            "type": "string",
            "title": "Срок действия ФН",
            "minLength": 1,
            "maxLength": 20
        },
        "shiftStatus": {
          "type": "string",
          "title": "Статус смены",
          "description": "Open - Смена открыта, Close - Смена закрыта, NoData - Нет данных",
          "enum": [ "Open", "Close", "NoData" ]
        },
        "cashdeskState": {
          "type": "string",
          "title": "Состояние ККТ",
          "description": "Active - Подключена, Expires - Заканчивается оплата, Expired - Не оплачена, Inactive - Отключена пользователем, Activation - Подключение, Deactivation - Отключение, FNChange - Замена ФН, FNSRegistration - Регистрация в ФНС, FNSRegistrationError - Ошибка регистрации в ФНС",
          "enum": [ "Active", "Expires", "Expired", "Inactive", "Activation", "Deactivation", "FNChange", "FNSRegistration", "FNSRegistrationError" ]
        },
        "cashdeskEndDateTime": {
          "type": "string",
          "title": "Оплачена по",
          "description": "Оплачена по в формате: 2017-03-20T10:47:00",
          "format": "date-time"
        },
        "fnState": {
          "type": "string",
          "title": "Состояние ФН",
          "description": "Active - Активен, Expires - Срок истекает, Expired - Срок истек",
          "enum": [ "Active", "Expires", "Expired" ]
        },
        "fnEndDateTime": {
          "type": "string",
          "title": "Дата окончания действия ФН в формате: 2017-03-20T10:47:00",
          "format": "date-time"
        },
        "lastDocumentState": {
          "type": "string",
          "title": "Последний документ",
          "description": "OK - Нет проблем, Warning - Предупреждение, Problem - Проблема",
          "enum": [ "OK", "Warning", "Problem" ]
        },
        "lastDocumentDateTime": {
          "type": "string",
          "title": "Дата последнего документа",
          "description": "Дата последнего документа в формате: 2017-03-20T10:47:00",
          "format": "date-time"
        },
        "outlet": {
          "type": "object",
          "title": "Торговая точка",
          "properties": {
            "id": {
              "type": "string",
              "title": "ИД торговой точки",
              "minLength": 1,
              "maxLength": 36
            },
            "name": {
              "type": "string",
              "title": "Название торговой точки",
              "maxLength": 255
            },
            "code": {
              "type": "string",
              "title": "Код торговой точки",
              "maxLength": 10
            },
            "address": {
              "type": "string",
              "title": "Адрес торговой точки"
            }
          },
          "required": [
            "id",
            "name"
          ]
        },
        "partner": {
          "type": "object",
          "title": "Обслуживающий партнер",
          "properties": {
            "name": {
              "type": "string",
              "title": "Наименование партнера",
              "minLength": 1,
              "maxLength": 450
            },
            "inn": {
              "type": "string",
              "title": "ИНН партнера",
              "minLength": 10,
              "maxLength": 12
            },
            "contact": {
              "type": "object",
              "title": "Контактное лицо партнера",
              "properties": {
                "name": {
                  "type": "string",
                  "title": "ФИО контактного лица партнера",
                  "minLength": 1,
                  "maxLength": 255
                },
                "phone": {
                  "type": "string",
                  "title": "Телефон контактного лица партнера",
                  "maxLength": 50
                },
                "email": {
                  "type": "string",
                  "title": "Электронная почта контактного лица партнера",
                  "format": "email",
                  "maxLength": 255
                }
              },
              "required": [
                "name",
                "phone",
                "email"
              ]
            }
          },
          "required": [
            "name",
            "inn",
            "contact"
          ]
        }
      },
      "required": [
        "name",
        "kktRegNumber",
        "kktFactoryNumber",
        "fnFactoryNumber"
      ]
    }
  },
  "required": [
    "reportDate",
    "cashdesk"
  ]
}