{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "title": "Сводные данные по подразделению",
  "description": "Сводные данные по подразделению",
  "properties": {
    "reportDate": {
      "type": "string",
      "title": "Дата формирования ответа",
      "description": "Дата формирования ответа в формате: 2017-03-20T10:47:00",
      "format": "date-time"
    },
    "department": {
      "type": "object",
      "title": "Данные подразделения",
      "description": "Данные подразделения",
      "properties": {
        "id": {
          "type": "string",
          "title": "ИД подразделения",
          "minLength": 1,
          "maxLength": 36
        },
        "name": {
          "type": "string",
          "title": "Название подразделения",
          "minLength": 0,
          "maxLength": 255
        },
        "code": {
          "type": "string",
          "title": "Код подразделения",
          "minLength": 0,
          "maxLength": 10
        },
        "parent": {
          "type": "object",
          "title": "Данные родительского подразделения",
          "description": "Данные родительского подразделения",
          "properties": {
            "id": {
              "type": "string",
              "title": "ИД родительского подразделения",
              "minLength": 1,
              "maxLength": 36
            },
            "name": {
              "type": "string",
              "title": "Название родительского подразделения",
              "minLength": 0,
              "maxLength": 255
            },
            "code": {
              "type": "string",
              "title": "Код родительского подразделения",
              "minLength": 0,
              "maxLength": 10
            }
          },
          "required": [
            "id",
            "name"
          ]
        }
      },
      "required": [
        "id",
        "name"
      ]
    },
    "outletCount": {
      "type": "integer",
      "title": "Кол-во торговых точек в подразделении и его дочерних подразделениях",
      "minimum": 0
    },
    "cashdeskCount": {
      "type": "integer",
      "title": "Кол-во ККТ в подразделении и его дочерних подразделениях",
      "minimum": 0
    },
    "cashdeskStateSummary": {
      "type": "object",
      "title": "Данные о наличии проблем с ККТ",
      "properties": {
        "kktProblemCount": {
          "type": "integer",
          "title": "Кол-во ККТ с проблемой (состояние ККТ)",
          "minimum": 0
        },
        "kktWarningCount": {
          "type": "integer",
          "title": "Кол-во ККТ с предупреждением (состояние ККТ)",
          "minimum": 0
        },
        "documentProblemCount": {
          "type": "integer",
          "title": "Кол-во ККТ с проблемой (последний документ)",
          "minimum": 0
        },
        "documentWarningCount": {
          "type": "integer",
          "title": "Кол-во ККТ с предупреждением (последний документ)",
          "minimum": 0
        },
        "fnProblemCount": {
          "type": "integer",
          "title": "Кол-во ККТ с проблемой (состояние ФН)",
          "minimum": 0
        },
        "fnWarningCount": {
          "type": "integer",
          "title": "Кол-во ККТ с предупреждением (состояние ФН)",
          "minimum": 0
        }
      },
      "required": [
        "kktProblemCount",
        "kktWarningCount",
        "documentProblemCount",
        "documentWarningCount",
        "fnProblemCount",
        "fnWarningCount"
      ]
    },
    "shiftStateSummary": {
      "type": "object",
      "title": "Данные по сменам",
      "properties": {
        "openedCount": {
          "type": "integer",
          "title": "Кол-во открытых смен",
          "minimum": 0
        },
        "closedCount": {
          "type": "integer",
          "title": "Кол-во закрытых смен",
          "minimum": 0
        }
      },
      "required": [
        "openedCount",
        "closedCount"
      ]
    }
  },
  "required": [
    "reportDate",
    "department",
    "outletCount",
    "cashdeskCount",
    "cashdeskStateSummary",
    "shiftStateSummary"
  ]
}