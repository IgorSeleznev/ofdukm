{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "title": "Список торговых точек по подразделению",
  "description": "Список торговых точек по подразделению",
  "properties": {
    "reportDate": {
      "type": "string",
      "title": "Дата формирования ответа",
      "description": "Дата формирования ответа в формате: 2017-03-20T10:47:00",
      "format": "date-time"
    },
    "counts": {
      "type": "object",
      "title": "Информация о количестве записей",
      "description": "Информация о количестве записей",
      "properties": {
        "recordCount": {
          "type": "integer",
          "title": "Всего записей",
          "minimum": 0
        },
        "recordFilteredCount": {
          "type": "integer",
          "title": "Записей с учетом фильтра",
          "minimum": 0
        },
        "recordInResponceCount": {
          "type": "integer",
          "title": "Записей в ответе",
          "minimum": 0
        }
      },
      "required": [
        "recordCount",
        "recordFilteredCount",
        "recordInResponceCount"
      ]
    },
    "records": {
      "type": "array",
      "title": "Список торговых точек",
      "items": {
        "type": "object",
        "title": "Торговая точка",
        "properties": {
          "id": {
            "type": "string",
            "title": "ИД торговой точки",
            "minLength": 1,
            "maxLength": 36
          },
          "name": {
            "type": "string",
            "title": "Название торговой точки",
            "maxLength": 255
          },
          "code": {
            "type": "string",
            "title": "Код торговой точки",
            "maxLength": 10
          },
          "address": {
            "type": "string",
            "title": "Адрес торговой точки"
          },
          "problemIndicator": {
            "type": "string",
            "title": "Признак наличия проблемы",
            "description": "OK - Нет проблем, Warning - Предупреждение, Problem - Проблема",
            "enum": [ "OK", "Warning", "Problem" ]
          },
          "department": {
            "type": "object",
            "title": "Данные подразделения",
            "properties": {
              "id": {
                "type": "string",
                "title": "ИД подразделения",
                "minLength": 1,
                "maxLength": 36
              },
              "name": {
                "type": "string",
                "title": "Название подразделения",
                "maxLength": 255
              },
              "code": {
                "type": "string",
                "title": "Код подразделения",
                "maxLength": 10
              }
            },
            "required": [
              "id",
              "name"
            ]
          }
        },
        "required": [
          "id",
          "name"
        ]
      }
    }
  },
  "required": [
    "reportDate",
    "counts"
  ]
}