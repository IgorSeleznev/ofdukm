package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktListRequest;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class KktCombinedListService {

    private final KktListService listService = new KktListService();
    private final KktCombinedService combinedService = new KktCombinedService();

    public List<KktCombined> kktList(final String outletId) {
        final List<KktCombined> result = newArrayList();
        final List<KktListItem> kkts = listService.kktList(
                new KktListRequest()
                        .setOutletId(outletId)
                        .setSessionToken(ofdContext().sessionToken())
        ).getRecords();
        kkts.forEach(
                kkt -> result.add(
                        combinedService.kkt(kkt.getKktRegNumber())
                )
        );

        return result;
    }
}
