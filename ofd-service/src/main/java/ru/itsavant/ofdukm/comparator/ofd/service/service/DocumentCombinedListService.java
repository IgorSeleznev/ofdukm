package ru.itsavant.ofdukm.comparator.ofd.service.service;

import lombok.var;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.common.concurrent.StoppedException;
import ru.itsavant.ofdukm.comparator.ofd.model.document.*;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class DocumentCombinedListService {

    private final DocumentListService listService = new DocumentListService();
    private final DocumentInfoService infoService = new DocumentInfoService();
    private final DocumentCombineService combineService = new DocumentCombineService();
    private Stoppable stoppable;

    public DocumentCombinedResponse documents(final DocumentListRequest request) {
        final List<DocumentCombined> result = newArrayList();

        final DocumentListResponse documents = listService.documentList(request);
        for (var head : documents.getRecords()) {
            if (stoppable != null
                    && stoppable.stopped()) {
                throw new StoppedException();
            }

            result.add(
                    combineService.combine(
                            head,
                            infoService.documentInfo(
                                    new DocumentInfoRequest()
                                            .setFn(head.getFnFactoryNumber())
                                            .setFd(String.valueOf(head.getFdNumber()))
                                            .setSessionToken(ofdContext().sessionToken())
                            ))
            );
        }

        return new DocumentCombinedResponse()
                .setCounts(documents.getCounts())
                .setRecords(result);
    }

    public DocumentCombinedListService stoppable(final Stoppable stoppable) {
        this.stoppable = stoppable;
        return this;
    }
}
