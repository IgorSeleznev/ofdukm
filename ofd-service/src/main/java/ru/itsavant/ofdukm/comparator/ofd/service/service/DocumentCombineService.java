package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListItem;
import ru.itsavant.ofdukm.comparator.ofd.service.converter.document.DocumentProductListCollector;
import ru.itsavant.ofdukm.comparator.ofd.service.converter.document.DocumentPropertyListCollector;

import java.util.Map;

public class DocumentCombineService {

    private final DocumentPropertyListCollector propertyCollector = new DocumentPropertyListCollector();
    private final DocumentProductListCollector productCollector = new DocumentProductListCollector();

    public DocumentCombined combine(
            final DocumentListItem head,
            final Map<String, Object> properties
    ) {
        return new DocumentCombined()
                .setHead(head)
                .setProperty(propertyCollector.collect(head, properties))
                .setProducts(productCollector.collect(head, properties));
    }
}
