package ru.itsavant.ofdukm.comparator.ofd.service.converter.document;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProperty;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ofd.model.constants.OfdDocumentPropertyKeyConstants.OFD_DOCUMENT_ITEM_KEY;
import static ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProperty.stringify;

public class DocumentPropertyListCollector {

    public List<DocumentProperty> collect(
            final DocumentListItem head,
            final Map<String, Object> documentMap
    ) {
        final List<DocumentProperty> target = newArrayList();
        final Map<String, Object> properties = (Map<String, Object>) documentMap.get("document");

        properties.forEach(
                (key, value) -> {
                    if (!OFD_DOCUMENT_ITEM_KEY.equals(key)) {
                        addProperty(
                                target, new DocumentProperty()
                                        .fn(head.getFnFactoryNumber())
                                        .fd(String.valueOf(head.getFdNumber()))
                                        .code(key)
                                        .value(value)
                        );
                    }
                }
        );

        return target;
    }

    private void addProperty(final List<DocumentProperty> target, final DocumentProperty property) {
        property.itemName(property.code(), property.value());

        if (property.value() instanceof List) {
            final List<Object> list = (List<Object>) property.value();
            for (final Object item : list) {
                addProperty(target, DocumentProperty.property(property, item));
            }

            return;
        }

        if (property.value() instanceof Map) {
            final Map<String, Object> map = (Map<String, Object>) property.value();
            map.forEach(
                    (nestedKey, nestedValue) -> addProperty(target, DocumentProperty.property(property, nestedKey, nestedValue))
            );
        }

        target.add(stringify(property));
    }
}
