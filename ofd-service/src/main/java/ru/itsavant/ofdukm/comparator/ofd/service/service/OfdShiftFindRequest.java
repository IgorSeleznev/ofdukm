package ru.itsavant.ofdukm.comparator.ofd.service.service;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class OfdShiftFindRequest {

    private String kktRegNumber;
    private String startDate;
    private String finishDate;
}
