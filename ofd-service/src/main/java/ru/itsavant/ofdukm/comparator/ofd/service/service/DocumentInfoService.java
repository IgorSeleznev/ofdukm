package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentInfoRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.DocumentInfoRestService;

import java.util.Map;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class DocumentInfoService {

    private final DocumentInfoRestService restService = new DocumentInfoRestService();

    public Map<String, Object> documentInfo(final DocumentInfoRequest request) {
        try {
            return restService.documentInfo(
                    ofdContext().modelMapper().map(request, DocumentInfoRequestJson.class)
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
