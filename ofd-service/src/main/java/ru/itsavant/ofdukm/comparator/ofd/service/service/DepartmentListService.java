package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.department.DepartmentListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.department.DepartmentListResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.DepartmentListRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class DepartmentListService {

    private final DepartmentListRestService restService = new DepartmentListRestService();

    public DepartmentListResponse departmentList(final DepartmentListRequest request) {
        try {
            return ofdContext().modelMapper().map(
                    restService.departmentList(
                            ofdContext().modelMapper().map(request, DepartmentListRequestJson.class)
                    ), DepartmentListResponse.class
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}