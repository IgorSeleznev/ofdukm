package ru.itsavant.ofdukm.comparator.ofd.service.manager;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftListParameters {

    private String fn;
    private String begin;
    private String end;
}
