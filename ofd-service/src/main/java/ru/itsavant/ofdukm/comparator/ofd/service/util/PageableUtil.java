package ru.itsavant.ofdukm.comparator.ofd.service.util;

import static ru.itsavant.ofdukm.comparator.common.constants.PageSizeConstants.*;

public class PageableUtil {

    private PageableUtil() {

    }

    public static int largePageSize(final int pageSize, final int receivedCount, final int total) {
        return total == -1
                || pageSize * MAX_LARGE_LIST_PAGE_SIZE + receivedCount < total
                ? MAX_LARGE_LIST_PAGE_SIZE
                : limitTop(total - pageSize * MAX_LARGE_LIST_PAGE_SIZE + receivedCount, MAX_LARGE_LIST_PAGE_SIZE);
    }

    public static int smallPageSize(final int pageSize, final int receivedCount, final int total) {
        return total == -1
                || pageSize * MAX_SMALL_LIST_PAGE_SIZE + receivedCount < total
                ? MAX_SMALL_LIST_PAGE_SIZE
                : limitTop(total - pageSize * MAX_SMALL_LIST_PAGE_SIZE + receivedCount, MAX_SMALL_LIST_PAGE_SIZE);
    }
}
