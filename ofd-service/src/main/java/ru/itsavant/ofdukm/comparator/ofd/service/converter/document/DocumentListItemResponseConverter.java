package ru.itsavant.ofdukm.comparator.ofd.service.converter.document;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListItem;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentListItemResponseJson;

public class DocumentListItemResponseConverter implements Converter<DocumentListItemResponseJson, DocumentListItem> {

    @Override
    public DocumentListItem convert(final DocumentListItemResponseJson source) {
        return new DocumentListItem()
                .setAccountingType(source.getAccountingType())
                .setCash(source.getCash())
                .setCashier(source.getCashier())
                .setDateTime(source.getDateTime());
    }
}
