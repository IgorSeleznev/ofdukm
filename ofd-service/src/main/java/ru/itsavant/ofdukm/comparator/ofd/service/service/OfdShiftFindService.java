package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfo;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListRequest;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class OfdShiftFindService {

    private final KktCombinedService kktListService = new KktCombinedService();
    private final ShiftListService shiftListService = new ShiftListService();
    private final ShiftInfoService shiftInfoService = new ShiftInfoService();

    public List<ShiftInfo> find(final OfdShiftFindRequest request) {
        final List<ShiftInfo> result = newArrayList();
        final KktCombined kktCombined = kktListService.kkt(request.getKktRegNumber());
        final List<ShiftListItem> shiftsFound = newArrayList();

        kktCombined.getKktFn().forEach(
                kktFn -> shiftsFound.addAll(
                        shiftListService.shiftList(
                                new ShiftListRequest()
                                        .setSessionToken(ofdContext().sessionToken())
                                        .setFn(kktFn.getFn())
                                        .setBegin(request.getStartDate())
                                        .setEnd(request.getFinishDate())
                        ).getRecords()
                )
        );
        shiftsFound.forEach(
                shiftListItem -> result.add(
                        shiftInfoService.shiftInfo(
                                new ShiftInfoRequest()
                                        .setFn(shiftListItem.getFnFactoryNumber())
                                        .setSessionToken(ofdContext().sessionToken())
                                        .setShift(String.valueOf(shiftListItem.getShiftNumber()))
                        ).getShift()
                )
        );

        return result;
    }
}
