package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfoResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftInfoRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.ShiftInfoRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class ShiftInfoService {

    private final ShiftInfoRestService restService = new ShiftInfoRestService();

    public ShiftInfoResponse shiftInfo(final ShiftInfoRequest request) {
        try {
            return ofdContext().modelMapper().map(
                    restService.shiftInfo(
                            ofdContext().modelMapper().map(request, ShiftInfoRequestJson.class)
                    ), ShiftInfoResponse.class
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
