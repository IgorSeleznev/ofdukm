package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.common.concurrent.Managable;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;

import java.util.List;
import java.util.function.Consumer;

import static ru.itsavant.ofdukm.comparator.common.constants.PageSizeConstants.MAX_SMALL_LIST_PAGE_SIZE;
import static ru.itsavant.ofdukm.comparator.ofd.service.util.PageableUtil.smallPageSize;

public class PageableListApplyService {

    private Managable manager;
    private int pageNumber = 1;

    public PageableListApplyService(final Managable manager) {
        this.manager = manager;
    }

    public <R> void apply(final PageableServiceFunction<OfdResponse<R>> serviceFunction,
                          final Consumer<List<R>> consumer) {
        manager.setStarted();
        boolean finished = false;
        int total = -1;
        int receivedCount = -1;
        int pageSize = MAX_SMALL_LIST_PAGE_SIZE;

        while(!manager.stopped() && !finished) {
            pageSize = smallPageSize(pageSize, receivedCount, total);
            final OfdResponse<R> response = serviceFunction.list(pageNumber, pageSize);

            total = response.getCounts().getRecordCount();
            receivedCount = response.getCounts().getRecordInResponceCount();

            if (!manager.stopped()) {
                final Counts counts = response.getCounts();
                finished = counts.getRecordInResponceCount() + (pageNumber - 1) * MAX_SMALL_LIST_PAGE_SIZE >= counts.getRecordCount();
            }

            consumer.accept(response.getRecords());

            pageNumber++;
        }
    }
}
