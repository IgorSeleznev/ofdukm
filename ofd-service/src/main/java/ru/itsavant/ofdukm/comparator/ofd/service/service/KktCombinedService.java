package ru.itsavant.ofdukm.comparator.ofd.service.service;

import lombok.NonNull;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.*;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class KktCombinedService {

    private final KktInfoService infoService = new KktInfoService();
    private final KktFnListService fnService = new KktFnListService();

    public KktCombined kkt(@NonNull final String regNumber) {
        final KktCombined combined = new KktCombined().setKktFn(newArrayList());

        List<KktFn> fnList = fnService.kktFnList(
                new KktFnListRequest()
                        .setKktRegNumber(regNumber)
                        .setSessionToken(ofdContext().sessionToken())
        ).getRecords();

        for (final KktFn fn : fnList) {
            if (combined.getInfo() == null) {
                final KktInfo kktInfo = infoService.kktInfo(
                        new KktInfoRequest()
                                .setSessionToken(ofdContext().sessionToken())
                                .setFn(fn.getFn())
                );
                combined.setInfo(kktInfo);
            }
            combined.getKktFn().add(fn);
        }

        return combined;
    }
}