package ru.itsavant.ofdukm.comparator.ofd.service.converter.document;

import com.google.common.collect.ImmutableList;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProduct;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProperty;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ofd.model.constants.OfdDocumentPropertyKeyConstants.OFD_DOCUMENT_ITEM_KEY;
import static ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProperty.stringify;

public class DocumentProductListCollector {

    public List<DocumentProduct> collect(
            final DocumentListItem head,
            final Map<String, Object> documentMap
    ) {
        final Map<String, Object> properties = (Map<String, Object>) documentMap.get("document");
        final Object productProperty = properties.get(OFD_DOCUMENT_ITEM_KEY);
        if (productProperty != null && productProperty instanceof List) {
            final List<DocumentProduct> products = newArrayList();
            final List<Object> productPropertyList = (List<Object>) productProperty;
            productPropertyList.forEach(
                    productPropertyItem -> {
                        final List<DocumentProperty> target = newArrayList();
                        if (productPropertyItem instanceof Map) {
                            final Map<String, Object> map = (Map<String, Object>) productPropertyItem;
                            map.forEach(
                                    (key, value) -> {
                                        addProperty(
                                                target, new DocumentProperty()
                                                        .fn(head.getFnFactoryNumber())
                                                        .fd(String.valueOf(head.getFdNumber()))
                                                        .numberOfNestedItemInDocument(products.size())
                                                        .code(key)
                                                        .value(value)
                                        );
                                    }
                            );
                            products.add(
                                    new DocumentProduct()
                                            .properties(target)
                            );
                        }
                    }
            );
            return products;
        }

        if (productProperty == null) {
            return ImmutableList.of();
        }

        throw new IllegalStateException("Found property " + OFD_DOCUMENT_ITEM_KEY + " but type is not map (" + productProperty.getClass().getTypeName() + " type found)");
    }

    private void addProperty(final List<DocumentProperty> target, final DocumentProperty property) {
        property.itemName(property.code(), property.value());

        if (property.value() instanceof List) {
            final List<Object> list = (List<Object>) property.value();
            for (final Object item : list) {
                addProperty(target, DocumentProperty.property(property, item));
            }

            return;
        }

        if (property.value() instanceof Map) {
            final Map<String, Object> map = (Map<String, Object>) property.value();
            map.forEach(
                    (nestedKey, nestedValue) -> addProperty(target, DocumentProperty.property(property, nestedKey, nestedValue))
            );
        }

        target.add(stringify(property));
    }
}
