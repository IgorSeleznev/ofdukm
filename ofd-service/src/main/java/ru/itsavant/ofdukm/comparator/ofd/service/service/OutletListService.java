package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.outlet.OutletListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.outlet.OutletListResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet.OutletListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.outlet.OutletListResponseJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.OutletListRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class OutletListService {

    private final OutletListRestService restService = new OutletListRestService();

    public OutletListResponse outletList(final OutletListRequest request) {
        try {
            final OutletListResponseJson response = restService.outletList(
                    ofdContext().modelMapper().map(request, OutletListRequestJson.class)
            );

            final OutletListResponse domainResponse = ofdContext().modelMapper().map(response, OutletListResponse.class).setReportDate("");

            return domainResponse;
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
