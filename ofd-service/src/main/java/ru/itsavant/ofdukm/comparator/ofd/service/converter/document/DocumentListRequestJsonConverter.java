package ru.itsavant.ofdukm.comparator.ofd.service.converter.document;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentListRequestJson;

public class DocumentListRequestJsonConverter implements Converter<DocumentListRequest, DocumentListRequestJson> {

    @Override
    public DocumentListRequestJson convert(final DocumentListRequest source) {
        return new DocumentListRequestJson()
                .setFn(source.getFn())
                .setPageNumber(source.getPageNumber())
                .setSessionToken(source.getSessionToken())
                .setShift(source.getShift());
    }
}
