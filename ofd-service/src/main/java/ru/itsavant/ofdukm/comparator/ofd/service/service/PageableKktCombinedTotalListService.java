package ru.itsavant.ofdukm.comparator.ofd.service.service;

import com.google.common.collect.ImmutableList;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.outlet.OutletListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.outlet.OutletListRequest;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.common.constants.PageSizeConstants.MAX_SMALL_LIST_PAGE_SIZE;
import static ru.itsavant.ofdukm.comparator.common.counter.Counter.basedByZero;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class PageableKktCombinedTotalListService {

    private final OutletListService outletListService = new OutletListService();
    private final KktCombinedListService kktListService = new KktCombinedListService();

    private final Counter outletPageCounter = basedByZero();
    private final List<OutletListItem> outlets = newArrayList();
    private final List<KktCombined> collected = newArrayList();

    public List<KktCombined> kktList() {

        final List<KktCombined> result = newArrayList();

        if (collected.size() > 0) {
            for (int i = 0; i < collected.size(); i++) {
                result.add(collected.get(i));
                if (result.size() == MAX_SMALL_LIST_PAGE_SIZE) {
                    collected.subList(0, MAX_SMALL_LIST_PAGE_SIZE).clear();
                    return result;
                }
            }
        }

        if (outlets.size() == 0) {
            outlets.clear();
            outlets.addAll(
                    outletListService.outletList(
                            new OutletListRequest()
                                    .setPageSize(MAX_SMALL_LIST_PAGE_SIZE)
                                    .setSessionToken(ofdContext().sessionToken())
                                    .setPageNumber(outletPageCounter.current())
                    ).getRecords()
            );
            outletPageCounter.next();
        }

        final Counter outletCounter = basedByZero();

        for (OutletListItem outlet : outlets) {
            final List<KktCombined> kkts = kktListService.kktList(outlet.getId());
            for (KktCombined kkt : kkts) {
                if (result.size() < MAX_SMALL_LIST_PAGE_SIZE) {
                    result.add(kkt);
                } else {
                    collected.add(kkt);
                }
            }

            if (result.size() == MAX_SMALL_LIST_PAGE_SIZE) {
                break;
            }

            outletCounter.next();
        }

        outlets.subList(0, outletCounter.current()).clear();

        outletPageCounter.next();

        return result;
    }
}
