package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.shift.ShiftListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.ShiftListRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class ShiftListService {

    private final ShiftListRestService restService = new ShiftListRestService();

    public ShiftListResponse shiftList(final ShiftListRequest request) {
        try {
            return ofdContext().modelMapper().map(
                    restService.shiftList(
                            ofdContext().modelMapper().map(request, ShiftListRequestJson.class)
                    ), ShiftListResponse.class
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
