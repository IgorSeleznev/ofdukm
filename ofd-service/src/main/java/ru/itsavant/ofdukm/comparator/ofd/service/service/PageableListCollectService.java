package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.common.concurrent.Managable;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.response.OfdResponse;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class PageableListCollectService {

    private final PageableListApplyService applyService;

    public PageableListCollectService(final Managable manager) {
        this.applyService = new PageableListApplyService(manager);
    }

    public <R> List<R> collect(final PageableServiceFunction<OfdResponse<R>> serviceFunction) {
        final List<R> result = newArrayList();
        applyService.apply(serviceFunction, result::addAll);

        return result;
    }
}
