package ru.itsavant.ofdukm.comparator.ofd.service.converter.document;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentInfoRequest;
import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentInfoRequestJson;

public class DocumentInfoRequestJsonConverter implements Converter<DocumentInfoRequest, DocumentInfoRequestJson> {

    @Override
    public DocumentInfoRequestJson convert(final DocumentInfoRequest source) {

        return new DocumentInfoRequestJson()
                .setFd(source.getFd())
                .setFn(source.getFn())
                .setSessionToken(source.getSessionToken());
    }
}
