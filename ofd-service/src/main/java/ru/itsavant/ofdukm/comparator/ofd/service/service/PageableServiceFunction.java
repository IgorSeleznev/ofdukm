package ru.itsavant.ofdukm.comparator.ofd.service.service;

public interface PageableServiceFunction<R> {

    R list(final int pageNumber, final int pageSize);
}
