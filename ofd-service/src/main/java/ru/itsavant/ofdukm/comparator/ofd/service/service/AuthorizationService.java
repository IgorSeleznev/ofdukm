package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.authorization.AuthorizationRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.authorization.AuthorizationResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization.AuthorizationRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization.IntegratorAuthorizationRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.AuthorizationRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class AuthorizationService {

    private final AuthorizationRestService restService = new AuthorizationRestService();

    public String authorize(final AuthorizationRequest authorizationRequest) {
        try {
            final AuthorizationResponse response =
                    ofdContext().modelMapper().map(
                            restService.authorize(
                                    new IntegratorAuthorizationRequestJson()
                                            .setIntegratorId(ofdContext().integratorId())
                                            .setAuthorization(
                                                    new AuthorizationRequestJson()
                                                            .setLogin(authorizationRequest.getLogin())
                                                            .setPassword(authorizationRequest.getPassword())
                                            )
                            ), AuthorizationResponse.class
                    );

            return response.getSessionToken();
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
