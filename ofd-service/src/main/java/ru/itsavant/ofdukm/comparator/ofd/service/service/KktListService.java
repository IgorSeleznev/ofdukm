package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktListResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.KktListRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class KktListService {

    private final KktListRestService restService = new KktListRestService();

    public KktListResponse kktList(final KktListRequest request) {
        try {
            return ofdContext().modelMapper().map(
                    restService.kktList(
                            ofdContext().modelMapper().map(request, KktListRequestJson.class)
                    ), KktListResponse.class
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
