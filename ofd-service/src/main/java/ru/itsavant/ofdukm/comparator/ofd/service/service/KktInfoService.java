package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktInfo;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktInfoRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.KktInfoRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class KktInfoService {

    private final KktInfoRestService restService = new KktInfoRestService();

    public KktInfo kktInfo(final KktInfoRequest request) {
        try {
            return ofdContext().modelMapper().map(
                    restService.kktInfo(
                            ofdContext().modelMapper().map(request, KktInfoRequestJson.class)
                    ), KktInfo.class
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
