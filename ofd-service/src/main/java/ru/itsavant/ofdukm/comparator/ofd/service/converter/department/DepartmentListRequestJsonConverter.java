package ru.itsavant.ofdukm.comparator.ofd.service.converter.department;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ofd.model.department.DepartmentListRequest;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.department.DepartmentListRequestJson;

public class DepartmentListRequestJsonConverter implements Converter<DepartmentListRequest, DepartmentListRequestJson> {

    @Override
    public DepartmentListRequestJson convert(final DepartmentListRequest source) {
        return new DepartmentListRequestJson()
                .setPageNumber(source.getPageNumber())
                .setSessionToken(source.getSessionToken());
    }
}
