package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.document.DocumentListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.DocumentListRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class DocumentListService {

    private final DocumentListRestService restService = new DocumentListRestService();

    public DocumentListResponse documentList(final DocumentListRequest request) {
        try {
            return ofdContext().modelMapper().map(
                    restService.documentList(
                            ofdContext().modelMapper().map(request, DocumentListRequestJson.class)
                    ), DocumentListResponse.class
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
