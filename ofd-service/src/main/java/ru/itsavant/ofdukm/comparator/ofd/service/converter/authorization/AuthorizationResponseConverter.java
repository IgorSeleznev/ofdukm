package ru.itsavant.ofdukm.comparator.ofd.service.converter.authorization;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ofd.model.authorization.AuthorizationResponse;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.authorization.AuthorizationResponseJson;

public class AuthorizationResponseConverter implements Converter<AuthorizationResponseJson, AuthorizationResponse> {

    @Override
    public AuthorizationResponse convert(final AuthorizationResponseJson source) {
        return new AuthorizationResponse()
                .setSessionToken(source.getSessionToken());
    }
}
