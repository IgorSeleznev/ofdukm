package ru.itsavant.ofdukm.comparator.ofd.service.manager;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.common.value.WrappedValue;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static ru.itsavant.ofdukm.comparator.common.value.WrappedValue.*;

public class DocumentWrappedValueConverter implements Converter<Map<String, Object>, Map<String, WrappedValue>> {

    public Map<String, WrappedValue> convert(final Map<String, Object> source) {
        final Map<String, WrappedValue> target = newHashMap();
        source.forEach((key, value) -> {
            target.put(key, wrap(value));
        });

        return target;
    }

    private WrappedValue wrap(final Object value) {
        if (value != null) {
            if (value instanceof String) {
                return stringValue(String.valueOf(value));
            }
            if (value instanceof Integer) {
                return integerValue((Integer) value);
            }
            if (value instanceof Double) {
                return doubleValue((Double) value);
            }
            if (value instanceof Boolean) {
                return booleanValue((Boolean) value);
            }
            if (value instanceof List) {
                final List<WrappedValue> wrappedValues = newArrayList();
                final List<Object> valueList = (List<Object>) value;
                valueList.forEach(
                        nestedValue -> wrappedValues.add(wrap(nestedValue))
                );

                return listValue(wrappedValues);
            }
            if (value instanceof Map) {
                final Map<String, WrappedValue> wrappedMap = newHashMap();
                final Map<String, Object> valueMap = (Map<String, Object>) value;
                valueMap.forEach(
                        (key, nestedValue) -> {
                            wrappedMap.put(key, wrap(nestedValue));
                        }
                );

                return mapValue(wrappedMap);
            }
        }
        return emptyValue();
    }
}
