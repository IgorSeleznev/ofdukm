package ru.itsavant.ofdukm.comparator.ofd.service.context;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.modelmapper.ModelMapper;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.AuthorizationRestService;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.OfdRestService;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class OfdServiceContext {

    private static final OfdServiceContext CONTEXT = new OfdServiceContext();

    public static OfdServiceContext ofdContext() {
        return CONTEXT;
    }

    private OfdServiceContext() {

    }

    private String sessionToken;
    private String integratorId;
    private String login;
    private String password;

    private ModelMapper modelMapper = new ModelMapper();
}
