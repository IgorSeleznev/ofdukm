package ru.itsavant.ofdukm.comparator.ofd.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktFnListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktFnList;
import ru.itsavant.ofdukm.comparator.ofd.rest.json.kkt.KktFnListRequestJson;
import ru.itsavant.ofdukm.comparator.ofd.rest.service.KktFnListRestService;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class KktFnListService {

    private final KktFnListRestService restService = new KktFnListRestService();

    public KktFnList kktFnList(final KktFnListRequest request) {
        try {
            return ofdContext().modelMapper().map(
                    restService.kktFnList(
                            ofdContext().modelMapper().map(request, KktFnListRequestJson.class)
                    ), KktFnList.class
            );
        } catch (final Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
