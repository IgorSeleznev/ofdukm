package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmShiftListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmShiftFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShiftRequest;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmShiftService {

    public List<UkmShift> shift(final UkmShiftRequest request) {
        final UkmShiftListDao dao = new UkmShiftListDao();
        return dao.list(
                new DaUkmShiftFindRequest()
                        .setCashId(request.getCashId())
                        .setShiftId(request.getShiftId())
                        .setStartDate(request.getStartDate())
                        .setFinishDate(request.getFinishDate())
        ).stream()
                .map(
                        daShift -> ukmContext().modelMapper().map(daShift, UkmShift.class)
                )
                .collect(toList());
    }
}
