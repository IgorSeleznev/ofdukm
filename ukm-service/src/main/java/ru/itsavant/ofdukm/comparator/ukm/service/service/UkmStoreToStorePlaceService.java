package ru.itsavant.ofdukm.comparator.ukm.service.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmExportConverterDao;

import java.util.HashMap;
import java.util.Map;

public class UkmStoreToStorePlaceService {

    private final UkmExportConverterDao dao = new UkmExportConverterDao();
    private final XmlMapper xmlMapper = new XmlMapper();

    public UkmStoreToStorePlaceService() {
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public Map<String, String> storeToStorePlaceMap() {
        try {
            final String configuration = dao.converterConfiguration();
            final Map<String, String> result = new HashMap<>();
            final Map<String, Object> map = xmlMapper.readValue(configuration, new TypeReference<HashMap<String, Object>>() {
            });
            //final Map<String, Object> boostSerialization = (Map<String, Object>) map.get("boost_serialization");
            final Map<String, Object> params = (Map<String, Object>) map.get("params");
            final Map<String, Object> cashlines = (Map<String, Object>) params.get("cashlines");
            cashlines.forEach((key, value) -> {
                        if (key.toLowerCase().startsWith("item_")) {
                            final Map<String, Object> parametersMap = (Map<String, Object>) value;
                            result.put(
                                    parametersMap.get("first").toString(),
                                    parametersMap.get("second").toString()
                            );
                        }
                    }
            );
            return result;
        } catch (final Exception exception) {
            throw new RuntimeException("Couldn`t to deserialize ukm export converter parameters when try to get store to store place match parameters");
        }
    }
}
