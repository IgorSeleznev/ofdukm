package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.model.UkmPayment;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmContextPaymentsService {

    private final UkmPaymentListService service = new UkmPaymentListService();

    public void paymentsToContext() {
        final List<UkmPayment> payments = service.payments(ukmContext().store().getStoreId());

        payments.forEach(
                payment -> ukmContext().payments().put(payment.getIsCashe(), payment)
        );
    }
}
