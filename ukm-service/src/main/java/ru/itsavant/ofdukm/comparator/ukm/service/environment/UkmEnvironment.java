package ru.itsavant.ofdukm.comparator.ukm.service.environment;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class UkmEnvironment {

    private String storeId;
    private String cashId;
}
