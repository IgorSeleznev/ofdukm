package ru.itsavant.ofdukm.comparator.ukm.service.service;

import java.util.List;

public interface OfdListSaveService<T> {

    void save(final List<T> data);
}
