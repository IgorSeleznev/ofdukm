package ru.itsavant.ofdukm.comparator.ukm.service.context;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.modelmapper.ModelMapper;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ukm.model.*;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.service.configuration.DaConfiguration;
import ru.itsavant.ofdukm.comparator.ukm.da.configuration.DataSourceConfiguration;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

@Getter
@Setter
@Accessors(fluent = true)
public class UkmServiceContext {

    private static final UkmServiceContext CONTEXT = new UkmServiceContext();

    private UkmServiceContext() {

    }

    public static UkmServiceContext ukmContext() {
        return CONTEXT;
    }

    public void configure(final DaConfiguration configuration) {
        ukmContext().configuration = configuration;
        daContext().initConnection(
                new DataSourceConfiguration()
                        .setDriverName("com.mysql.jdbc.Driver")
                        //.setUrl(String.format("jdbc:mysql://%s:%d/ukmserver?autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=cp1251", configuration.host(), configuration.port()))
                        .setUrl(String.format("jdbc:mysql://%s:%d/ukmserver?autoReconnect=true&useSSL=false", configuration.getHost(), configuration.getPort()))
                        .setUsername(configuration.getUsername())
                        .setPassword(configuration.getPassword())
                        .setCharset(configuration.getCharset())
                        .setUseUnicode(configuration.isUseUnicode())
        );
    }

    private DaConfiguration configuration;
    private UkmFindRequest findRequest;
    private UkmStore store;
    private UkmPos ukmPos;
    private KktCombined kktCombined;
    private UkmNomenclature nomenclature;
    private UkmPricelist pricelist;
    private String exportShiftId;
    private String storePlaceId;
    private Map<UkmPaymentType, UkmPayment> payments = newHashMap();

    private ModelMapper modelMapper = new ModelMapper();

}
