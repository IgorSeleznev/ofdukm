package ru.itsavant.ofdukm.comparator.ukm.service.service;

import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProduct;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static ru.itsavant.ofdukm.comparator.ofd.model.constants.OfdDocumentPropertyKeyConstants.*;

@Setter
@Accessors(chain = true, fluent = true)
public class OfdReceiptItemBuilder {

    private UkmFindRequest request;
    private DocumentProduct product;
    private DocumentCombined documentCombined;

    public OfdReceiptItem build() {
        final OfdReceiptItem receiptItem = new OfdReceiptItem();
        product.properties().forEach(
                productProperty -> {
                    if (OFD_DOCUMENT_ITEM_PRICE_KEY.equals(productProperty.code())) {
                        receiptItem.setPrice(String.valueOf(productProperty.value()));
                    }
                    if (OFD_DOCUMENT_ITEM_QUANTITY_KEY.equals(productProperty.code())) {
                        receiptItem.setQuantity(String.valueOf(productProperty.value()));
                    }
                    if (OFD_DOCUMENT_ITEM_SUM_KEY.equals(productProperty.code())) {
                        receiptItem.setAmount(String.valueOf(productProperty.value()));
                    }
                    if (OFD_DOCUMENT_ITEM_NAME_KEY.equals(productProperty.code())) {
                        receiptItem.setItemName(String.valueOf(productProperty.value()));
                    }
                    if (OFD_DOCUMENT_ITEM_CODE_KEY.equals(productProperty.code())) {
                        receiptItem.setItemCode(String.valueOf(productProperty.value()));
                    }
                    receiptItem.setCashId(request.getCashId());
                    receiptItem.setStoreId(request.getStoreId());
                    receiptItem.setNumberInDocument(
                            productProperty.numberOfNestedItemInDocument()
                    );
                    receiptItem.setFd(String.valueOf(documentCombined.getHead().getFdNumber()));
                    receiptItem.setFn(documentCombined.getHead().getFnFactoryNumber());
                }
        );

        return receiptItem;
    }
}
