package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmCashierCodeDao;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmCashierCodeService {

    private UkmCashierCodeDao codeDao = new UkmCashierCodeDao();

    public String cashierCode(final String cashierName) {
        return codeDao.cashierCode(cashierName, Integer.valueOf(ukmContext().store().getStoreId()));
    }
}
