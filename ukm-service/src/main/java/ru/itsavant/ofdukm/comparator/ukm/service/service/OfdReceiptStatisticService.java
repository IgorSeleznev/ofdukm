package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ListSqlDao;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceipt;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class OfdReceiptStatisticService {

    private final ListSqlDao<OfdReceiptStatistic> headerDao = new ListSqlDao<>();
    private final ListSqlDao<OfdReceiptItem> itemDao = new ListSqlDao<>();

    public List<OfdReceiptStatistic> statistics(final String kktShift, final UkmPos ukmPos) {
        final List<OfdReceiptStatistic> receipts = headerDao.list(
                new SqlParametrizedQuery()
                        .sql("select " +
                                "store_id,           \n" +
                                "cash_id,            \n" +
                                "ukm_shift,          \n" +
                                "kkt_shift,          \n" +
                                "fn,                 \n" +
                                "fd,                 \n" +
                                "global_number,      \n" +
                                "local_number,       \n" +
                                "cash_name,          \n" +
                                "cashier_name,       \n" +
                                "insert_record_date, \n" +
                                "receipt_date,       \n" +
                                "total_amount,       \n" +
                                "items_count,        \n" +
                                "document_type,      \n" +
                                "cashless_amount     \n" +
                                "from ofdukm.receipt_header\n" +
                                "where kkt_shift = ?")
                        .parameters(
                                newArrayList(kktShift)
                        ),
                (resultSet, rowNum) -> new OfdReceiptStatistic()
                        .setHeader(
                                new OfdReceipt()
                                        .setAmount(resultSet.getString(13))
                                        .setCashId(resultSet.getString(2))
                                        .setCashier(resultSet.getString(10))
                                        .setCashName(resultSet.getString(9))
                                        .setFd(resultSet.getString(6))
                                        .setFn(resultSet.getString(5))
                                        .setKktShiftNumber(resultSet.getString(4))
                                        .setNumberInShift(Integer.valueOf(resultSet.getString(8)))
                                        .setReceiptDate(resultSet.getString(12))
                                        .setProductCount(Integer.valueOf(resultSet.getString(14)))
                                        .setType(resultSet.getString(15))
                                        .setUkmShiftNumber(resultSet.getString(3))
                                        .setStoreId(resultSet.getString(1))
                                        .setCashlessAmount(resultSet.getString(16))
                        )
                        .setUkmPos(ukmPos)
        );

        receipts.forEach(
                receipt -> receipt.setItems(
                        itemDao.list(
                                new SqlParametrizedQuery()
                                        .sql(
                                                "select store_id, \n" +
                                                        "r.cash_id, \n" +
                                                        "r.fn, \n" +
                                                        "r.fd, \n" +
                                                        "r.number_in_document, \n" +
                                                        "r.item_name, \n" +
                                                        "r.price, \n" +
                                                        "r.quantity, \n" +
                                                        "r.amount, \n" +
                                                        "ifnull(i.id, '') as item_code \n" +
                                                        "from ofdukm.receipt_item r left join ukmserver.trm_in_items i on i.name = r.item_name \n" +
                                                        "where r.fn = ? and r.fd = ? and ifnull(i.nomenclature_id, ?) = ? \n" +
                                                        "  and ifnull(i.deleted, 0) = 0 \n" +
                                                        "order by r.number_in_document"
                                                //TODO:добавить налоги???
                                        ).parameters(
                                        newArrayList(
                                                receipt.getHeader().getFn(),
                                                receipt.getHeader().getFd(),
                                                ukmContext().nomenclature().getNomenclatureId(),
                                                ukmContext().nomenclature().getNomenclatureId()
                                        )
                                ),
                                (resultSet, rowNum) ->
                                        new OfdReceiptItem()
                                                .setStoreId(resultSet.getString(1))
                                                .setCashId(resultSet.getString(2))
                                                .setFn(resultSet.getString(3))
                                                .setFd(resultSet.getString(4))
                                                .setNumberInDocument(Integer.valueOf(resultSet.getString(5)))
                                                .setItemName(resultSet.getString(6))
                                                .setPrice(String.valueOf(resultSet.getBigDecimal(7)))
                                                .setQuantity(String.valueOf(resultSet.getBigDecimal(8)))
                                                .setAmount(String.valueOf(resultSet.getBigDecimal(9)))
                                                .setItemCode(resultSet.getString(10))
                        )
                )
        );

        return receipts;
    }
}
