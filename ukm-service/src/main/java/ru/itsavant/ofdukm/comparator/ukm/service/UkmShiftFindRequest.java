package ru.itsavant.ofdukm.comparator.ukm.service;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UkmShiftFindRequest {

    private String storeId;
    private String cashId;
    private String shiftId;
    private String startDate;
    private String finishDate;
}
