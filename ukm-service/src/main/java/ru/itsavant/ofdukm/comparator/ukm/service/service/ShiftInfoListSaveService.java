package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfo;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create.OfdShiftCreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert.OfdShiftListSaveDao;

import java.util.List;

public class ShiftInfoListSaveService implements OfdListSaveService<ShiftInfo> {

    @Override
    public void save(List<ShiftInfo> data) {
        new OfdShiftCreateTableDao().create();
        new OfdShiftListSaveDao().save(data);
    }
}
