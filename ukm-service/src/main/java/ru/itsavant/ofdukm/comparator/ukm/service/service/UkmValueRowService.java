package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.pair.ValueRow;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.CollectResultSetHandler;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.CollectSqlDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.service.service.UkmValueRowNormalizer.normalize;

public class UkmValueRowService {

    public void sortRows() {
        final List<ValueRow> rows = new CollectSqlDao<ValueRow>()
                .collect(
                        new SqlParametrizedQuery()
                                .sql("select id, value from ukmserver.local_server_param")
                                .parameters(newArrayList()),
                        new CollectResultSetHandler<ValueRow>() {
                            @Override
                            public boolean needNewInstance(ValueRow currentInstance, ResultSet resultSet) throws SQLException {
                                return true;
                            }

                            @Override
                            public ValueRow newInstance(ResultSet resultSet) throws SQLException {
                                return new ValueRow(resultSet.getString(1), resultSet.getString(2));
                            }

                            @Override
                            public ValueRow applyResultSet(ValueRow currentInstance, ResultSet resultSet) throws SQLException {
                                return new ValueRow(resultSet.getString(1), resultSet.getString(2));
                            }
                        }
                );
        final UkmLookupPairService lookupPairService = new UkmLookupPairService();
        for (final ValueRow valueRow : rows) {
            if (lookupPairService.validator().getLeft().equals(normalize(valueRow.getLeft())) && lookupPairService.validator().getRight().equals(normalize(valueRow.getRight()))) {
                return;
            }
        }

        throw new UkmRowException();
    }
}
