package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmPaymentListDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPayment;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmPaymentListService {

    private final UkmPaymentListDao listDao = new UkmPaymentListDao();

    public List<UkmPayment> payments(final String storeId) {
        return listDao.list(storeId)
                .stream()
                .map(payment -> ukmContext().modelMapper().map(payment, UkmPayment.class))
                .collect(Collectors.toList());
    }
}
