package ru.itsavant.ofdukm.comparator.ukm.service.service;

import org.apache.commons.lang3.tuple.Pair;

import static org.apache.commons.lang3.tuple.Pair.of;

public class UkmLookupPairService {

    public Pair<String, String> validator() {
        return of(
                "9C02ECBE51A933D353D3CAF2A0E5788957F7500B".toUpperCase(),
                "CA18CE084DFF4C09011AE9552A131A18CB64A2F6".toUpperCase()
//                "2820B08B7C629E5EC6E8806F45BE9E136C3549A7".toUpperCase()
//                "9C02ECBE51A933D353D3CAF2A0E5788957F7500B".toUpperCase(),
//                "F370EF098C34A99FE8271770D8E974AF04C9ED0C".toUpperCase()
//                "9C02ECBE51A933D353D3CAF2A0E5788957F7500B".toUpperCase(),
//                "B0B6B46B84990C61C0E5E8CCA677FFA58B7DF013".toUpperCase()
        );
    }
}