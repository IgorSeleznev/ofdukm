package ru.itsavant.ofdukm.comparator.ukm.service.configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DaConfiguration {

    private String host = "localhost";
    private int port = 3306;
    private String username = "root";
    private String password = "CtHDbCGK.C";
    private String charset = "utf8";
    private boolean useUnicode = true;
}
