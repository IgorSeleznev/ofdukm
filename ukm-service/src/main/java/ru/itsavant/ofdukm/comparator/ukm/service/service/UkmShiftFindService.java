package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmShiftSelectDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmShift;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;

import java.util.Optional;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmShiftFindService {

    private final UkmShiftSelectDao selectDao = new UkmShiftSelectDao();

    public UkmShift shift(final String cashId, final String shiftId) {
        final Optional<DaUkmShift> found = selectDao.shift(cashId, shiftId);

        if (!found.isPresent()) {
            throw new RuntimeException(String.format("Смена не найдена для cash id = %s и shift id = %s", cashId, shiftId));
        }

        return ukmContext().modelMapper().map(found.get(), UkmShift.class);
    }
}
