package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create.*;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert.*;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class DocumentCombinedListSaveService implements OfdListSaveService<DocumentCombined> {

    private final UkmReceiptHeaderBuilder receiptHeaderBuilder = new UkmReceiptHeaderBuilder();
    private final OfdReceiptItemListCollector itemListCollector = new OfdReceiptItemListCollector();

    @Override
    public void save(final List<DocumentCombined> data) {
        System.out.println("OfdDocumentHeadCreateDao");
        new OfdDocumentHeadCreateDao().create();
        System.out.println("OfdDocumentHeadSaveDao");
        new OfdDocumentHeadSaveDao().save(data);

        System.out.println("OfdDocumentPropertyCreateDao");
        new OfdDocumentPropertyCreateDao().create();
        System.out.println("OfdDocumentPropertySaveDao");
        new OfdDocumentPropertySaveDao().save(data);

        System.out.println("OfdDocumentProductCreateDao");
        new OfdDocumentProductCreateDao().create();
        System.out.println("OfdDocumentProductSaveDao");
//        data.subList(1, data.size() - 1).clear();
        new OfdDocumentProductListSaveDao().save(data);

        System.out.println("OfdDocumentProductCreateDao");
        new OfdReceiptHeaderCreateDao().create();
        System.out.println("OfdReceiptHeaderSaveDao");
        new OfdReceiptHeaderSaveDao().save(
                data.stream()
                .map(document -> receiptHeaderBuilder.build(ukmContext().findRequest(), document))
                .collect(toList())
        );

        System.out.println("OfdDocumentProductCreateDao");
        new OfdReceiptItemCreateDao().create();
        System.out.println("OfdReceiptItemSaveDao");
        new OfdReceiptItemSaveDao().save(
                itemListCollector.collect(ukmContext().findRequest(), data)
        );
    }
}