package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class OfdReceiptItemListCollector {

    private final OfdReceiptItemBuilder receiptItemBuilder = new OfdReceiptItemBuilder();

    public List<OfdReceiptItem> collect(final UkmFindRequest request, final List<DocumentCombined> documents) {
        final List<OfdReceiptItem> items = newArrayList();

        documents.forEach(
                document -> document.getProducts().forEach(
                        product -> items.add(
                                receiptItemBuilder
                                        .product(product)
                                        .request(request)
                                        .documentCombined(document)
                                        .build()
                        )
                )
        );

        return items;
    }
}
