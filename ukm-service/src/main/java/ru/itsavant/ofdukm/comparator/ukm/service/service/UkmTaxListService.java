package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmTaxListDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmTax;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmTaxListService {

    private final UkmTaxListDao listDao = new UkmTaxListDao();

    public List<UkmTax> taxes(final long NomenclatureId) {
        return listDao.taxes(NomenclatureId)
                .stream()
                .map(tax -> ukmContext().modelMapper().map(tax, UkmTax.class))
                .collect(toList());
    }
}
