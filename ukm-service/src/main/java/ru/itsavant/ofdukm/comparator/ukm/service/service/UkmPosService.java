package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmPosSelectDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmPosService {

    public UkmPos pos(final String storeId, final String cashId) {
        return ukmContext().modelMapper().map(
                new UkmPosSelectDao().pos(storeId, cashId), UkmPos.class
        );
    }
}
