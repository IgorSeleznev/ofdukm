package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create.OfdFiscalStorageCreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create.OfdKktInfoCreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert.OfdKktFnListSaveDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert.OfdKktListSaveDao;

import java.util.List;

public class KktCombinedListSaveService implements OfdListSaveService<KktCombined> {

    @Override
    public void save(List<KktCombined> data) {
        System.out.println("OfdKktInfoCreateTableDao");
        new OfdKktInfoCreateTableDao().create();
        System.out.println("OfdFiscalStorageCreateTableDao");
        new OfdFiscalStorageCreateTableDao().create();
        System.out.println("OfdKktListSaveDao");
        new OfdKktListSaveDao().save(data);
        System.out.println("OfdKktFnListSaveDao");
        new OfdKktFnListSaveDao().save(data);
    }
}
