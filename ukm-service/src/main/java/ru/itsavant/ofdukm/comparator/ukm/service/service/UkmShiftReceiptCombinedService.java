package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmShiftReceiptHeaderSelectDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmShiftReceiptItemSelectDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmReceiptCombined;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmReceiptCombined;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmShiftReceiptCombinedService {

    private final UkmShiftReceiptHeaderSelectDao headerSelectDao = new UkmShiftReceiptHeaderSelectDao();
    private final UkmShiftReceiptItemSelectDao itemSelectDao = new UkmShiftReceiptItemSelectDao();

    public List<UkmReceiptCombined> receipts(final String shiftId, final String cashId) {
        final List<DaUkmReceiptCombined> daReceipts = headerSelectDao.list(shiftId, cashId);
        daReceipts.forEach(
                itemSelectDao::items
        );

        return daReceipts.stream()
                .map(
                        daReceipt -> ukmContext().modelMapper().map(daReceipt, UkmReceiptCombined.class)
                )
                .collect(Collectors.toList());
    }
}
