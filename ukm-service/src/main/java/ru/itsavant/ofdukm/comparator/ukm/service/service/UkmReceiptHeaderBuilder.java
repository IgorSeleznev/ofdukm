package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceipt;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

public class UkmReceiptHeaderBuilder {

    public OfdReceipt build(final UkmFindRequest ukmFindRequest, final DocumentCombined document) {
                return new OfdReceipt()
                        .setStoreId(ukmFindRequest.getStoreId())
                        .setCashId(ukmFindRequest.getCashId())
                        .setFn(document.getHead().getFnFactoryNumber())
                        .setFd(String.valueOf(document.getHead().getFdNumber()))
                        .setKktShiftNumber(String.valueOf(document.getHead().getShiftNumber()))

                        .setUkmShiftNumber(ukmFindRequest.getShiftId())
                        .setCashName(ukmFindRequest.getCashName())
                        .setCashier(document.getHead().getCashier())
                        .setAmount(String.valueOf(document.getHead().getSum()))
                        .setNumberInShift(document.getHead().getNumberInShift())

                        .setReceiptDate(document.getHead().getDateTime())
                        .setProductCount(document.getProducts().size())
                        .setNds0Sum(String.valueOf(document.getHead().getNds0Sum()))
                        .setNds10(String.valueOf(document.getHead().getNds10()))
                        .setNds18(String.valueOf(document.getHead().getNds18()))

                        .setNds20(String.valueOf(document.getHead().getNds20()))
                        .setNdsC10(String.valueOf(document.getHead().getNdsC10()))
                        .setNdsC18(String.valueOf(document.getHead().getNdsC18()))
                        .setNdsC20(String.valueOf(document.getHead().getNdsC20()))
                        .setType(document.getHead().getAccountingType())
                        .setCashlessAmount(String.valueOf(document.getHead().getElectronic()));
    }
}
