package ru.itsavant.ofdukm.comparator.ukm.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmShiftListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmShiftFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmShiftListService {

    private final UkmShiftListDao selectDao = new UkmShiftListDao();

    public List<UkmShift> list(final UkmShiftFindRequest request) {
        return selectDao.list(
                ukmContext().modelMapper().map(request, DaUkmShiftFindRequest.class)
        ).stream()
                .map(daUkmShift -> ukmContext().modelMapper().map(daUkmShift, UkmShift.class))
                .collect(Collectors.toList());
    }
}
