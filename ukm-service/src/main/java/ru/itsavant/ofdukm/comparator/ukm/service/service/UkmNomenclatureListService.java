package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmNomenclatureListDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmNomenclature;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmNomenclatureListService {

    private final UkmNomenclatureListDao listDao = new UkmNomenclatureListDao();

    public List<UkmNomenclature> list() {
        return listDao.list()
                .stream()
                .map(ukmNomenclature -> ukmContext().modelMapper().map(ukmNomenclature, UkmNomenclature.class))
                .collect(Collectors.toList());
    }
}
