package ru.itsavant.ofdukm.comparator.ukm.service.service;

import org.apache.commons.codec.digest.DigestUtils;

public class UkmValueRowNormalizer {

    public static String normalize(final String source) {
        return DigestUtils.sha1Hex(source).toUpperCase();
    }
}
