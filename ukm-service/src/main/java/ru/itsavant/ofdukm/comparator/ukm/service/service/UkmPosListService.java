package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmPosListDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmPosListService {

    private final UkmPosListDao listDao = new UkmPosListDao();

    public List<UkmPos> list(final String storeId) {
        return listDao.list(storeId)
                .stream()
                .map(ukmPos -> ukmContext().modelMapper().map(ukmPos, UkmPos.class))
                .collect(Collectors.toList());
    }
}
