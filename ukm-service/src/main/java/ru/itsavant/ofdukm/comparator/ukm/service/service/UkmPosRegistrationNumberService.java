package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmPosRegistrationNumberDao;

import java.util.Optional;

public class UkmPosRegistrationNumberService {

    private final UkmPosRegistrationNumberDao numberDao = new UkmPosRegistrationNumberDao();

    public String number(final String cashId, final String startDate, final String finishDate) {
        final Optional<String> result = numberDao.registrationNumber(cashId, startDate, finishDate);

        if (result.isPresent()) {
            return result.get();
        }

        throw new RuntimeException("Внимание! Не найдена ни одна смена для указанной кассы. Не удалось определить регистрационный номер для ККМ.");
    }
}
