package ru.itsavant.ofdukm.comparator.ukm.service.service;

import org.modelmapper.ModelMapper;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.select.OfdShiftListAmountDao;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class OfdShiftListAmountService {

    private final OfdShiftListAmountDao dao = new OfdShiftListAmountDao();

    public List<OfdShiftAmount> list() {
        final ModelMapper modelMapper = ukmContext().modelMapper();
        return dao.list().stream()
                .map(item -> modelMapper.map(item, OfdShiftAmount.class))
                .collect(Collectors.toList());
    }
}
