package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmPricelistListDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPricelist;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmPricelistListService {

    private final UkmPricelistListDao listDao = new UkmPricelistListDao();

    public List<UkmPricelist> list(final long nomenclatureId) {
        return listDao.list(nomenclatureId)
                .stream()
                .map(ukmPricelist -> ukmContext().modelMapper().map(ukmPricelist, UkmPricelist.class))
                .collect(Collectors.toList());
    }
}
