package ru.itsavant.ofdukm.comparator.ukm.service.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm.UkmStoreListDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmStore;

import java.util.List;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmStoreListService {

    private final UkmStoreListDao listDao = new UkmStoreListDao();

    public List<UkmStore> list() {
        return listDao.list()
                .stream().map(
                        item -> ukmContext().modelMapper().map(item, UkmStore.class)
                ).collect(Collectors.toList());
    }
}
