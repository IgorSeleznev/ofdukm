package ru.itsavant.ofdukm.comparator.manual.test.application.main;

import ru.itsavant.ofdukm.comparator.ofd.model.authorization.AuthorizationRequest;
import ru.itsavant.ofdukm.comparator.ofd.service.service.AuthorizationService;
import ru.itsavant.ofdukm.comparator.transport.search.SearchPeriodValue;
import ru.itsavant.ofdukm.comparator.transport.search.SearchRequest;
import ru.itsavant.ofdukm.comparator.transport.search.SearchService;
import ru.itsavant.ofdukm.comparator.transport.transport.destination.DefaultTransportDestinationProgressEventHandler;
import ru.itsavant.ofdukm.comparator.ukm.service.configuration.DaConfiguration;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class Main {

    public static void main(final String... args) throws InterruptedException {
        ofdContext().integratorId("B10AE21C-9086-4EA4-969D-D8A9F87DF801");
        ofdContext().login("i.u.seleznev@yandex.ru");
        ofdContext().password("Ofiko08");
//        ukmContext().configure(
//                new DaConfiguration()
//                        .host("127.0.0.1")
//                        .username("root")
//                        .password("CtHDbCGK.C")
//        );
        final String token = new AuthorizationService()
                .authorize(
                        new AuthorizationRequest()
                                .setLogin(ofdContext().login())
                                .setPassword(ofdContext().password())
                );
        ofdContext().sessionToken(token);
        final SearchService searchService = new SearchService();

        ukmContext().configure(
                new DaConfiguration()
                        .setHost("127.0.0.1")
                        .setUsername("root")
                        .setPassword("CtHDbCGK.C")
                        .setUseUnicode(true)
                        .setCharset("utf8")
        );


        searchService.search(
                new SearchRequest()
                        .cashId("1001001")
                        .storeId("1001")
                        .period(
                                new SearchPeriodValue.Builder()
                                        .from("2019-01-18T12:01:01")
                                        .to("2019-01-18T21:00:00")
                        ),
                new DefaultTransportDestinationProgressEventHandler()
        );

//        final List<OutletListItem> outlets = new OutletListService().outletList(
//                new OutletListRequest()
//                .setSessionToken(ofdContext().sessionToken())
//        ).getRecords();

//        final Transport<KktCombined, KktCombined> transport = new Transport<>(new KktListTransportSource());
//
//        transport.transport(new KktCombinedTransportDestination());
//
//        while(transport.isRunning()) {
//            Thread.sleep(1000);
//        }

//        System.out.println(outlets);

//        final List<KktListItem> new KktListService().kktList(
//                new KktListRequest()
//                .setSessionToken(ofdContext().sessionToken())
//                .setOutletId()
//        )

//        final Transport<DocumentCombined, DocumentCombined> documentTransport = new Transport<>(new DocumentListTransportSource("8710000101496844", "335"));
//
//        documentTransport.transport(new DocumentCombinedTransportDestination());
//
//        while(documentTransport.isRunning()) {
//            Thread.sleep(1000);
//        }

        System.out.println("done");
    }
}