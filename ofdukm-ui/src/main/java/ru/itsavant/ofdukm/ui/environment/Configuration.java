package ru.itsavant.ofdukm.ui.environment;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ukm.service.configuration.DaConfiguration;

@Getter
@Setter
@Accessors(chain = true)
public class Configuration {

    private DaConfiguration database;
    private long pricelistId;
    private long nomenclatureId;
    private String integratorId;
    private String ofdLogin;
    private String ofdPassword;
}
