package ru.itsavant.ofdukm.ui.form.table;

import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;

import java.util.List;

import static ru.itsavant.ofdukm.ui.form.table.ShiftTableViewHelper.stringColumn;

public class ShiftTableFactory {

    public TableView<OfdShiftAmount> tableInstance(final List<OfdShiftAmount> data) {
        final TableView<OfdShiftAmount> table = new TableView<>(ShiftTableViewHelper.getObservableList(data));

        TableView.TableViewSelectionModel<OfdShiftAmount> tableSelectionModel = table.getSelectionModel();
        tableSelectionModel.setSelectionMode(SelectionMode.SINGLE);

        table.getColumns().addAll(
                stringColumn("shift", "Смена"),
                stringColumn("fn", "Фискальный аппарат"),
                stringColumn("amount", "Сумма")
        );

        // Set the stringColumn resize policy to constrained resize policy
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // Set the Placeholder for an empty tableInstance
        table.setPlaceholder(new Label("Нет данных."));

        return table;
    }
}