package ru.itsavant.ofdukm.ui.version;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true, fluent = true)
public class ReleaseVersion {

    private final String major = "1";
    private final String minor = "0";
    private final String release = "3";
    private final String build = "35";

    public String prettify() {
        return major() + "." + minor() + "." + release() + " сборка " + build();
    }
}
