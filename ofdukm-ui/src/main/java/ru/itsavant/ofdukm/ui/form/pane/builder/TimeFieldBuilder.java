package ru.itsavant.ofdukm.ui.form.pane.builder;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.util.regex.Pattern;

public class TimeFieldBuilder {

    public static TextField build() {
        final TextField textField = new TextField();

//        textField.setOnKeyTyped(event -> {
//            String typedCharacter = event.getCharacter();
//            event.consume();
//
//            if (typedCharacter.matches("\\d*")) {
//                String currentText =
//                        textField.getText().replaceAll("\\.","").replace(",", "");
//                long longVal = Long.parseLong(currentText.concat(typedCharacter));
//                textField.setText(new DecimalFormat("#,##0").format(longVal));
//            }
//        });
//
//
        final char separatorChar = ':';
        final Pattern pattern = Pattern.compile("[0-9" + separatorChar + "]*");
        textField.setTextFormatter(new TextFormatter<>(content -> {
            StringBuilder builder = new StringBuilder();
            if (content.getControlText().length() == 0) {
                builder.append("00" + separatorChar  + "00");
            }
            if (content.getControlText().length() == 1) {
                builder.append(content.getControlText() + "0" + separatorChar + "00");
            }
            if (!content.isContentChange()) {
                return content; // no need for modification, if only the selection changes
            }
            final String newText = content.getControlNewText();
            if (newText.isEmpty()) {
                return content;
            }
            if (!pattern.matcher(newText).matches()) {
                return null; // invalid change
            }

            // invert everything before the range
            int suffixCount = content.getControlText().length() - content.getRangeEnd();
            int digits = suffixCount - suffixCount / 3;

            // insert seperator just before caret, if necessary
            if (digits % 2 == 0 && digits > 0 && suffixCount % 3 != 0) {
                builder.append(separatorChar);
            }

            // add the rest of the digits in reversed order
            for (int i = content.getRangeStart() + content.getText().length() - 1; i >= 0; i--) {
                char letter = newText.charAt(i);
                if (Character.isDigit(letter)) {
                    builder.append(letter);
                    digits++;
                    if (digits % 2 == 0) {
                        builder.append(separatorChar);
                    }
                }
            }

            // remove seperator char, if added as last char
            if (digits % 2 == 0) {
                builder.deleteCharAt(builder.length() - 1);
            }
            builder.reverse();
            int length = builder.length();

            // replace with modified text
            content.setRange(0, content.getRangeEnd());
            content.setText(builder.toString());
            content.setCaretPosition(length);
            content.setAnchor(length);

            return content;
        }));
        return textField;
    }

}
