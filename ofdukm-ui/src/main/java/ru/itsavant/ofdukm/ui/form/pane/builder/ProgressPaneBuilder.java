package ru.itsavant.ofdukm.ui.form.pane.builder;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;

import static ru.itsavant.ofdukm.ui.environment.Environment.environment;
import static ru.itsavant.ofdukm.ui.environment.ProgressBarType.COMMON;
import static ru.itsavant.ofdukm.ui.environment.ProgressBarType.CURRENT;

public class ProgressPaneBuilder {


    public Node build() {
        final GridPane commonPane = new GridPane();
        final GridPane progressPane = new GridPane();
        final GridPane infoPane = new GridPane();

        // Set the hgap and vgap properties
//        infoPane.setHgap(10);
//        infoPane.setVgap(5);
        infoPane.setVisible(true);

//        commonPane.setHgap(10);
//        commonPane.setVgap(5);
        commonPane.setVisible(true);

//        progressPane.setHgap(10);
//        progressPane.setVgap(5);
        progressPane.setVisible(true);

        final Label firstLabel = new Label("Поиск расхождений");

        Label storeLabel = new Label();
        Label cashLabel = new Label();
        Label startDateLabel = new Label();
        Label finishDateLabel = new Label();

        infoPane.add(firstLabel, 0, 0);
        infoPane.add(new Label("Магазин"), 0, 1);

        final Label delimeter = new Label("");
        delimeter.setMinHeight(firstLabel.heightProperty().doubleValue());

        infoPane.add(delimeter, 0, 5);

        final Label label = new Label("Идет обращение в ОФД и поиск чеков по указанным параметрам...");
        final Label labelTitle = new Label("");

        environment().inLoad().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                if (environment().searchFilter() != null && environment().searchFilter().getUkmStore() != null) {
                    storeLabel.setText(environment().searchFilter().getUkmStore().getName());
                }
                infoPane.add(new Label("Касса"), 0, 2);
                if (environment().searchFilter() != null && environment().searchFilter().getUkmPos() != null) {
                    cashLabel.setText(environment().searchFilter().getUkmPos().getName());
                }
                infoPane.add(new Label("Дата с"), 0, 3);
                if (environment().searchFilter() != null) {
                    final String datePresentation = environment().searchFilter().getStartDate().substring(8, 10) + "." +
                            environment().searchFilter().getStartDate().substring(5, 7) + "." +
                            environment().searchFilter().getStartDate().substring(0, 4) + " " +
                            environment().searchFilter().getStartDate().substring(11);
                    startDateLabel.setText(datePresentation);
                }
                infoPane.add(new Label("по"), 0, 4);
                if (environment().searchFilter() != null) {
                    final String datePresentation = environment().searchFilter().getFinishDate().substring(8, 10) + "." +
                            environment().searchFilter().getFinishDate().substring(5, 7) + "." +
                            environment().searchFilter().getFinishDate().substring(0, 4) + " " +
                            environment().searchFilter().getFinishDate().substring(11);
                    finishDateLabel.setText(datePresentation);
                }
            }
            //searchPane.setVisible(!newValue);
            //progressPane.setVisible(newValue);
        });

        infoPane.add(storeLabel, 1, 1);
        infoPane.add(cashLabel, 1, 2);
        infoPane.add(startDateLabel, 1, 3);
        infoPane.add(finishDateLabel, 1, 4);



        final ProgressBar currentProgressBar = new ProgressBar(50);
        final ProgressBar commonProgressBar = new ProgressBar(50);

        commonProgressBar.setProgress(0);
        currentProgressBar.setProgress(0);

        commonProgressBar.setProgress(0);
        currentProgressBar.setProgress(0);

        infoPane.add(labelTitle, 0, 8);
        infoPane.add(label, 0, 7);


        infoPane.add(currentProgressBar, 0, 9);
        //infoPane.add(commonProgressBar, 0, 4);
//        progressPane.minHeight(100);
        commonProgressBar.setVisible(true);
        currentProgressBar.setVisible(true);

        commonPane.add(progressPane, 0, 0);
        commonPane.add(infoPane, 0, 1);

        environment().progress().get(COMMON).addListener(
                (observable, oldValue, newValue) -> {
                    try {
                        commonProgressBar.setProgress(environment().progress().get(COMMON).get() / 100D);
                        label.setText("Идет обращение в ОФД и поиск чеков по указанным параметрам... " + environment().progress().get(COMMON).getValue().toString() + "%");
                    } catch (final Throwable throwable) {

                    }
                }
        );

        environment().progressTitles().get(COMMON).addListener(
                (observable, oldValue, newValue) -> {
                    try {
                        labelTitle.setText(environment().progressTitles().get(COMMON).getValue());
                    } catch (final Throwable throwable) {

                    }
                }
        );

        environment().progress().get(CURRENT).addListener(
                (observable, oldValue, newValue) -> {
                    try {
                        currentProgressBar.setProgress(environment().progress().get(CURRENT).get() / 100D);
                        label.setText("Идет обращение в ОФД и поиск чеков по указанным параметрам... " + environment().progress().get(CURRENT).getValue().toString() + "%");
                    } catch (final Throwable throwable) {

                    }
                }
        );

        environment().progressTitles().get(CURRENT).addListener(
                (observable, oldValue, newValue) -> {
                    try {
                        labelTitle.setText(environment().progressTitles().get(CURRENT).getValue());
                    } catch (final Throwable throwable) {

                    }
                }
        );

        return infoPane;
    }
}
