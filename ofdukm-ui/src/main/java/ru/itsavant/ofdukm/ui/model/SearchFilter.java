package ru.itsavant.ofdukm.ui.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmStore;

@Getter
@Setter
@Accessors(chain = true)
public class SearchFilter {

    private UkmStore ukmStore;
    private OfdShiftAmount ofdShiftAmount;
    private UkmPos ukmPos;
    private String shiftNumber;
    private String startDate;
    private String finishDate;
}
