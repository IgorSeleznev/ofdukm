package ru.itsavant.ofdukm.ui.handler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import ru.itsavant.ofdukm.comparator.transport.search.SearchResult;
import ru.itsavant.ofdukm.ui.service.repair.RepairServiceStarter;
import ru.itsavant.ofdukm.ui.service.search.SearchServiceStarter;

import java.util.List;
import java.util.function.Consumer;

public class RepairStartHandler implements EventHandler<ActionEvent> {

    private final RepairServiceStarter serviceStarter = new RepairServiceStarter();

    private final Consumer<Void> repairFinishHandler;

    public RepairStartHandler(final Consumer<Void> repairFinishHandler) {
        this.repairFinishHandler = repairFinishHandler;
    }

    @Override
    public void handle(final ActionEvent event) {
        serviceStarter.start(
                repairFinishHandler
        );
    }
}
