package ru.itsavant.ofdukm.ui.environment;

import javafx.beans.property.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;
import ru.itsavant.ofdukm.comparator.common.util.MapUtil;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ukm.model.*;
import ru.itsavant.ofdukm.ui.model.SearchFilter;
import ru.itsavant.ofdukm.ui.service.InProgressListener;
import ru.itsavant.ofdukm.ui.version.ReleaseVersion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;
import static ru.itsavant.ofdukm.ui.environment.ProgressBarType.COMMON;
import static ru.itsavant.ofdukm.ui.environment.ProgressBarType.CURRENT;

@Getter
@Setter
@Accessors(fluent = true)
public class Environment {

    private static final Environment ENVIRONMENT = new Environment();

    public static Environment environment() {
        return ENVIRONMENT;
    }

    private Environment() {

    }

    private List<UkmStore> stores = newArrayList();
    private List<OfdShiftAmount> shiftAmounts = newArrayList();
    private String exportShiftId = "";
    private List<UkmNomenclature> nomenclatures = newArrayList();
    private final Map<String, List<UkmPos>> ukmPosMap = newHashMap();
    private final Map<Long, List<UkmPricelist>> ukmPricelistMap = newHashMap();
    private List<ShiftListItem> shifts;
    private Map<String, String> storeToStorePlaceMap = new HashMap<>();

    private final ReleaseVersion releaseVersion = new ReleaseVersion();

    private final Map<ProgressBarType, IntegerProperty> progress =
            new MapUtil.Builder<ProgressBarType, IntegerProperty>()
                    .entry(COMMON, new SimpleIntegerProperty(0))
                    .entry(CURRENT, new SimpleIntegerProperty(0))
                    .build();

    private final Map<ProgressBarType, StringProperty> progressTitles =
            new MapUtil.Builder<ProgressBarType, StringProperty>()
                    .entry(COMMON, new SimpleStringProperty(""))
                    .entry(CURRENT, new SimpleStringProperty(""))
                    .build();

    private final InProgressListener inProgressListener = new InProgressListener();
    private final ObjectProperty<ErrorMessage> errorMessage = new SimpleObjectProperty<>(null);

    private Configuration configuration;
    private ConcurrentLinkedQueue<ErrorMessage> errorMessages = new ConcurrentLinkedQueue<>();

    public void sdfsdf() {
        ofdContext().integratorId("B10AE21C-9086-4EA4-969D-D8A9F87DF801");
        ofdContext().login("i.u.seleznev@yandex.ru");
        ofdContext().password("Ofiko08");

    }

    private BooleanProperty inLoad = new SimpleBooleanProperty(false);

    private SearchFilter searchFilter = new SearchFilter();

}
