package ru.itsavant.ofdukm.ui.form.pane.builder;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmStore;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmContextPaymentsService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

@Slf4j
public class SearchFormPaneBuilder {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private final UkmContextPaymentsService paymentsService = new UkmContextPaymentsService();

    private final TextField ukmShiftField = new TextField();
    private final DatePicker dateField = new DatePicker();
    private final TextField timeFromField = new TextField("00:00");
    private final TextField timeToField = new TextField("23:59");
    private final TextField exportShiftField = new TextField();
    //private final TextField storePlaceField = new TextField();
    //private final Label storePlaceLabel = new Label();
    //private final ComboBox<UkmStore> storePlaceComboBox = new ComboBox<>();

    public Node build(
            final ComboBox<OfdShiftAmount> shiftAmountBox,
            final EventHandler<ActionEvent> searchButtonEvent,
            final EventHandler<ActionEvent> exportButtonEvent
    ) {
        final GridPane pane = new GridPane();

        // Set the hgap and vgap properties
        pane.setLayoutX(10);
        pane.setLayoutY(10);
        pane.setHgap(10);
        pane.setVgap(5);

        final ComboBox<UkmStore> ukmStoreBox = new ComboBox<>();
        final ComboBox<UkmPos> ukmPosBox = new ComboBox<>();

        ukmStoreBox.setConverter(
                new StringConverter<UkmStore>() {
                    @Override
                    public String toString(final UkmStore store) {
                        return store.getName();
                    }

                    @Override
                    public UkmStore fromString(final String title) {
                        final List<UkmStore> stores = environment().stores().stream().filter(
                                predicate ->
                                        predicate.getName().equals(title)

                        ).collect(toList());
                        if (stores.size() > 0) {
                            return stores.get(0);
                        }
                        throw new IllegalStateException("Выбранный магазин не найден в списке, загруженном из базы данных");
                    }
                }
        );

        shiftAmountBox.setConverter(
                new StringConverter<OfdShiftAmount>() {
                    @Override
                    public String toString(final OfdShiftAmount shiftAmount) {
                        return shiftAmount.getShift();
                    }

                    @Override
                    public OfdShiftAmount fromString(final String shiftId) {
                        final List<OfdShiftAmount> stores = environment().shiftAmounts().stream().filter(
                                predicate ->
                                        predicate.getShift().equals(shiftId)

                        ).collect(toList());
                        if (stores.size() > 0) {
                            return stores.get(0);
                        }
                        throw new IllegalStateException("Выбранная смена ОФД не найдена в списке, загруженном из базы данных");
                    }
                }
        );

        ukmPosBox.setConverter(
                new StringConverter<UkmPos>() {
                    @Override
                    public String toString(final UkmPos pos) {
                        return pos.getName();
                    }

                    @Override
                    public UkmPos fromString(final String title) {
                        if (ukmContext().store() == null) {
                            return new UkmPos();
                        }
                        final List<UkmPos> stores = environment().ukmPosMap().get(ukmContext().store().getStoreId()).stream().filter(
                                predicate ->
                                        predicate.getName().equals(title)

                        ).collect(toList());
                        if (stores.size() > 0) {
                            return stores.get(0);
                        }
                        throw new IllegalStateException("Выбранная касса не найдена в списке, загруженном из базы данных");
                    }
                }
        );

        pane.setVisible(true);

        // Add the TextFields to the Pane
        pane.add(new Label("ФИЛЬТР:"), 0, 0);
        pane.add(new Label("Магазин:"), 0, 1);
        pane.add(ukmStoreBox, 1, 1);
        pane.add(new Label("Касса:"), 0, 2);
        pane.add(ukmPosBox, 1, 2);
        pane.add(new Label("Смена:"), 0, 3);
        pane.add(ukmShiftField, 1, 3);
        pane.add(new Label("Дата:"), 2, 2);
        pane.add(dateField, 3, 2);
        pane.add(new Label("Время с:"), 2, 3);
        pane.add(timeFromField, 3, 3);
        pane.add(new Label("Время по:"), 2, 4);
        pane.add(timeToField, 3, 4);

        ukmStoreBox.setVisibleRowCount(15);
        ukmPosBox.setVisibleRowCount(10);
        shiftAmountBox.setVisibleRowCount(5);

        ukmStoreBox.valueProperty().addListener(
                (observable, oldValue, newValue) -> {
                    environment().searchFilter().setUkmStore(newValue);
                    paymentsService.paymentsToContext();
                }
        );

        shiftAmountBox.valueProperty().addListener(
                (observable, oldValue, newValue) -> environment().searchFilter().setOfdShiftAmount(newValue)
        );

        ukmPosBox.valueProperty().addListener(
                (observable, oldValue, newValue) -> environment().searchFilter().setUkmPos(newValue)
        );

        ukmShiftField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().searchFilter().setShiftNumber(newValue)
        );

        exportShiftField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().exportShiftId(newValue)
        );

        //if (environment().stores().size() == 1) {
        //    storePlaceLabel.setText(
        //            environment().stores().stream()
        //                    .filter(ukmStore -> ukmStore.getStoreId().equals(ukmContext().storePlaceId()))
        //                    .collect(Collectors.toList())
        //                    .get(0)
        //                    .getName()
        //    );
        //} else {
        //    storePlaceComboBox.setPromptText("Место хранения");
        //    storePlaceComboBox.setValue(environment().stores().get(0));
        //    final AtomicBoolean userAction = new AtomicBoolean(true);
        //    storePlaceComboBox.valueProperty().addListener(
        //            (observable, oldValue, newValue) -> {
        //
        //                // Если значение устанавливается не этим слушателем
        //                //if (userAction.compareAndSet(true, false)) {
        //                    Platform.runLater(() -> {
        //                        // Возвращаем ранее выбранный элемент в список (для возможности повторного выбора)
        //                        if (oldValue != null) storePlaceComboBox.getItems().add(oldValue);
        //                        if (newValue != null) {
        //                            // Убираем выбранный элемент из списка (что бы не отображался)
        //                            storePlaceComboBox.getItems().remove(newValue);
        //                            // устанавливаем значение не связанное со списком
        //                            storePlaceComboBox.setValue(newValue);
        //                        }
        //                        // Отмечаем завершение работы слушателя
        //                        userAction.lazySet(true);
        //                    });
        //                //}
        //            }
        //    );
        //}

        //storePlaceField.textProperty().addListener(
        //        (observable, oldValue, newValue) -> ukmContext().storePlaceId(newValue)
        //);

        timeFromField.textProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (defaultIfNull(environment().searchFilter().getStartDate(), "").length() >= 10) {
                        environment().searchFilter().setStartDate(
                                environment().searchFilter().getStartDate().substring(0, 10) + "T" + newValue
                        );
                    } else {
                        environment().searchFilter().setStartDate(
                                simpleDateFormat.format(new Date()) + "T" + newValue
                        );
                    }
                    if (newValue.length() < 8) {
                        environment().searchFilter().setStartDate(
                                environment().searchFilter().getStartDate() + ":00"
                        );
                    }
                    System.out.println(environment().searchFilter().getStartDate());
                    System.out.println(environment().searchFilter().getFinishDate());
                }
        );

        timeToField.textProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (defaultIfNull(environment().searchFilter().getFinishDate(), "").length() >= 10) {
                        environment().searchFilter().setFinishDate(
                                environment().searchFilter().getFinishDate().substring(0, 10) + "T" + newValue
                        );
                    } else {
                        environment().searchFilter().setFinishDate(
                                simpleDateFormat.format(new Date()) + "T" + newValue
                        );
                    }
                    if (newValue.length() < 8) {
                        environment().searchFilter().setFinishDate(
                                environment().searchFilter().getFinishDate() + ":59"
                        );
                    }
                    System.out.println(environment().searchFilter().getStartDate());
                    System.out.println(environment().searchFilter().getFinishDate());
                }
        );

        dateField.getEditor().textProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (defaultIfNull(newValue, "").length() < 10) {
                        return;
                    }
                    final String newDate = newValue.substring(6, 10) + "-" + newValue.substring(3, 5) + "-" + newValue.substring(0, 2);
                    if (defaultIfNull(environment().searchFilter().getStartDate(), "").length() >= 19) {
                        environment().searchFilter().setStartDate(
                                newDate + "T" + environment().searchFilter().getStartDate().substring(11)
                        );
                    } else {
                        environment().searchFilter().setStartDate(
                                newDate + "T00:00:00"
                        );
                    }
                    if (defaultIfNull(environment().searchFilter().getFinishDate(), "").length() >= 19) {
                        environment().searchFilter().setFinishDate(
                                newDate + "T" + environment().searchFilter().getFinishDate().substring(11)
                        );
                    } else {
                        environment().searchFilter().setFinishDate(
                                newDate + "T23:59:59"
                        );
                    }
                    System.out.println(environment().searchFilter().getStartDate());
                    System.out.println(environment().searchFilter().getFinishDate());
                }
        );

        // Create the Add Button and add Event-Handler
        final Button searchButton = new Button("Найти расхождения");
        final Button exportButton = new Button("ЭКСПОРТ");
        searchButton.setOnAction(searchButtonEvent);
        exportButton.setOnAction(exportButtonEvent);

        // Add the Add Button to the GridPane
        pane.add(searchButton, 3, 6);
        pane.add(new Label("ЭКСПОРТ"), 0, 7);
        pane.add(exportButton, 3, 10);
        pane.add(new Label("смена УКМ для вставки"), 1, 8);
        pane.add(exportShiftField, 2, 8);
        //pane.add(new Label("номер места хранения"), 1, 9);
        //if (environment().stores().size() > 1) {
        //    pane.add(storePlaceLabel, 2, 9);
        //} else {
        //    pane.add(storePlaceComboBox, 2, 9);
        //}
        //pane.add(storePlaceField, 2, 9);
        pane.add(new Label("смена ОФД для экспорта"), 1, 10);
        pane.add(shiftAmountBox, 2, 10);

        environment().inLoad().addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() != oldValue.booleanValue() && !newValue) {
                ukmStoreBox.setItems(
                        FXCollections.observableArrayList(environment().stores())
                );
            }
        });

        ukmStoreBox.getSelectionModel().selectedIndexProperty().addListener((observableValue, oldValue, newValue) -> {
            ukmContext().store((ukmStoreBox.getItems().get((Integer) newValue)));
            ukmContext().storePlaceId(
                    environment().storeToStorePlaceMap().get(
                            (ukmStoreBox.getItems().get((Integer) newValue)).getStoreId()
                    )
            );
            ukmPosBox.setItems(
                    FXCollections.observableArrayList(environment().ukmPosMap().get(ukmStoreBox.getItems().get((Integer) newValue).getStoreId()))
            );
//            try {
//                Files.write(
//                        Paths.get("debug.log"),
//                        String.format("UKM-POS-BOX HAS RECIEVE THESE KASSAs for storeId = %s and storePlace = %s:", ukmContext().store().getStoreId(), ukmContext().storePlaceId()).getBytes(StandardCharsets.UTF_8),
//                        CREATE, WRITE
//                );
//            } catch (IOException e) {
//                //e.printStackTrace();
//            }
//            ukmPosBox.getItems().forEach(
//                    item -> {
//                        try {
//                            Files.write(
//                                    Paths.get("debug.log"),
//                                    String.format("%s, %s", item.getStoreId(), item.getCashId()).getBytes(StandardCharsets.UTF_8),
//                                    CREATE, WRITE
//                            );
//                        } catch (IOException e) {
//                            //e.printStackTrace();
//                        }
//                    }
//            );
        });

        return pane;
    }
}