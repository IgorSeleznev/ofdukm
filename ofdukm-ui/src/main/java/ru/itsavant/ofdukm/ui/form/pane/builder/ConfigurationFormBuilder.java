package ru.itsavant.ofdukm.ui.form.pane.builder;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmNomenclature;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPricelist;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmStore;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.common.safety.SafetyTask.safety;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class ConfigurationFormBuilder {

    private final TextField hostField = new TextField();
    private final TextField portField = new TextField();
    private final TextField usernameField = new TextField();
    private final PasswordField passwordField = new PasswordField();
    private final TextField charsetField = new TextField();

    private final TextField integratorIdField = new TextField();
    private final TextField ofdLoginField = new TextField();
    private final PasswordField ofdPasswordField = new PasswordField();

    public Node build(
            final EventHandler<ActionEvent> saveButtonEvent
    ) {
        final GridPane pane = new GridPane();

        pane.setLayoutX(10);
        pane.setLayoutY(10);
        pane.setHgap(10);
        pane.setVgap(5);

        final ChoiceBox<UkmNomenclature> ukmNomenclatureBox = new ChoiceBox<>();
        final ChoiceBox<UkmPricelist> ukmPricelistBox = new ChoiceBox<>();

        ukmNomenclatureBox.setConverter(
                new StringConverter<UkmNomenclature>() {
                    @Override
                    public String toString(final UkmNomenclature nomenclature) {
                        return nomenclature.getName();
                    }

                    @Override
                    public UkmNomenclature fromString(final String title) {
                        final List<UkmNomenclature> stores = environment().nomenclatures().stream().filter(
                                predicate ->
                                        predicate.getName().equals(title)

                        ).collect(toList());
                        if (stores.size() > 0) {
                            return stores.get(0);
                        }
                        throw new IllegalStateException("Выбранная номенклатура не найдена в списке, загруженном из базы данных");
                    }
                }
        );

        ukmPricelistBox.setConverter(
                new StringConverter<UkmPricelist>() {
                    @Override
                    public String toString(final UkmPricelist pricelist) {
                        return pricelist.getName();
                    }

                    @Override
                    public UkmPricelist fromString(final String title) {
                        if (ukmContext().nomenclature() == null) {
                            return new UkmPricelist();
                        }
                        final List<UkmPricelist> pricelists = environment().ukmPricelistMap().get(ukmContext().nomenclature().getNomenclatureId()).stream().filter(
                                predicate ->
                                        predicate.getName().equals(title)

                        ).collect(toList());
                        if (pricelists.size() > 0) {
                            return pricelists.get(0);
                        }
                        throw new IllegalStateException("Выбраннй прайслист не найден в списке, загруженном из базы данных");
                    }
                }
        );

        final CheckBox useUnicodeCheckbox = new CheckBox("Использовать UNICODE");

        pane.setVisible(true);

        pane.add(new Label("Номенклатура:"), 0, 0);
        pane.add(ukmNomenclatureBox, 1, 0);
        pane.add(new Label("Прайслист:"), 0, 1);
        pane.add(ukmPricelistBox, 1, 1);
        pane.add(new Label("Хост:"), 2, 0);
        pane.add(hostField, 3, 0);
        pane.add(new Label("Пользователь"), 2, 1);
        pane.add(usernameField, 3, 1);
        pane.add(new Label("Пароль:"), 2, 2);
        pane.add(passwordField, 3, 2);
        pane.add(new Label("Порт:"), 2, 3);
        pane.add(portField, 3, 3);
//        pane.add(new Label("База данных:"), 2, 1);
//        pane.add(databaseField, 3, 1);
        pane.add(new Label("Кодировка:"), 2, 4);
        pane.add(charsetField, 3, 4);
        pane.add(useUnicodeCheckbox, 2, 5);

        pane.add(new Label("Интегратор-ID:"), 4, 0);
        pane.add(integratorIdField, 5, 0);
        pane.add(new Label("Логин ОФД:"), 4, 1);
        pane.add(ofdLoginField, 5, 1);
        pane.add(new Label("Пароль ОФД:"), 4, 2);
        pane.add(ofdPasswordField, 5, 2);

        useUnicodeCheckbox.selectedProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().getDatabase().setUseUnicode(newValue)
        );

        usernameField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().getDatabase().setUsername(newValue)
        );

        portField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().getDatabase().setPort(Integer.valueOf(newValue))
        );

        passwordField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().getDatabase().setPassword(newValue)
        );

        hostField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().getDatabase().setHost(newValue)
        );

        useUnicodeCheckbox.selectedProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().getDatabase().setUseUnicode(newValue)
        );


        integratorIdField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().setIntegratorId(newValue)
        );

        ofdLoginField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().setOfdLogin(newValue)
        );

        ofdPasswordField.textProperty().addListener(
                (observable, oldValue, newValue) -> environment().configuration().setOfdPassword(newValue)
        );

        final Button applyButton = new Button("Сохранить");
        applyButton.setOnAction(saveButtonEvent);

        // Add the Add Button to the GridPane
        pane.add(applyButton, 3, 6);

        environment().inLoad().addListener((observable, oldValue, newValue) -> {
            if (newValue.booleanValue() != oldValue.booleanValue() && !newValue) {
                ukmNomenclatureBox.setItems(
                        FXCollections.observableArrayList(environment().nomenclatures())
                );

                final List<UkmNomenclature> currentNomenclatures = environment().nomenclatures()
                        .stream()
                        .filter(
                                ukmNomenclature ->
                                        ukmNomenclature.getNomenclatureId() == environment().configuration().getNomenclatureId()
                        ).collect(toList());

                if (currentNomenclatures.size() > 0) {
                    ukmContext().nomenclature(currentNomenclatures.get(0));
                    Platform.runLater(
                            () -> {
                                safety(() -> ukmNomenclatureBox.setValue(ukmContext().nomenclature()));
                                safety(() -> ukmPricelistBox.setItems(
                                        FXCollections.observableArrayList(environment().ukmPricelistMap().get(ukmContext().nomenclature().getNomenclatureId()))
                                        )
                                );
                                safety(() -> ukmNomenclatureBox.getSelectionModel().select(
                                        ukmNomenclatureBox.getItems().stream().filter(
                                                item -> item.getNomenclatureId() == currentNomenclatures.get(0).getNomenclatureId()
                                        ).collect(toList()).get(0)
                                        )
                                );
                            }
                    );
                }

                if (ukmContext().nomenclature() != null) {
                    final List<UkmPricelist> currentPricelists =
                            environment().ukmPricelistMap().get(ukmContext().nomenclature().getNomenclatureId())
                                    .stream()
                                    .filter(
                                            ukmPricelist -> ukmPricelist.getPricelistId() == environment().configuration().getPricelistId()
                                    ).collect(toList()
                            );

                    try {
                        if (currentPricelists.size() > 0) {
                            ukmContext().pricelist(currentPricelists.get(0));
                            ukmPricelistBox.setValue(currentPricelists.get(0));
                            ukmPricelistBox.getSelectionModel().select(
                                    ukmPricelistBox.getItems().stream().filter(
                                            item -> item.getPricelistId() == currentPricelists.get(0).getPricelistId()
                                    ).collect(toList()).get(0)
                            );
                        }
                    } catch (final Throwable throwable) {
                        System.out.println("");
                    }
                }

                hostField.textProperty().setValue(environment().configuration().getDatabase().getHost());
                usernameField.textProperty().setValue(environment().configuration().getDatabase().getUsername());
                passwordField.textProperty().setValue(environment().configuration().getDatabase().getPassword());
                portField.textProperty().setValue(String.valueOf(environment().configuration().getDatabase().getPort()));
                charsetField.textProperty().setValue(environment().configuration().getDatabase().getCharset());
                useUnicodeCheckbox.setSelected(environment().configuration().getDatabase().isUseUnicode());

                integratorIdField.textProperty().setValue(environment().configuration().getIntegratorId());
                ofdLoginField.textProperty().setValue(environment().configuration().getOfdLogin());
                ofdPasswordField.textProperty().setValue(environment().configuration().getOfdPassword());
            }
        });

        ukmNomenclatureBox.getSelectionModel().selectedIndexProperty().addListener((observableValue, oldValue, newValue) -> {
            ukmContext().nomenclature((ukmNomenclatureBox.getItems().get((Integer) newValue)));
            ukmPricelistBox.setItems(
                    FXCollections.observableArrayList(environment().ukmPricelistMap().get(ukmNomenclatureBox.getItems().get((Integer) newValue).getNomenclatureId()))
            );
        });

        return pane;
    }
}
