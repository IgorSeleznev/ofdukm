package ru.itsavant.ofdukm.ui.service.repair;

import ru.itsavant.ofdukm.comparator.repair.service.RepairService;

import java.util.function.Consumer;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class RepairThread implements Runnable {

    private final RepairService repairService = new RepairService();
    private final Consumer<Void> repairFinishHandler;

    public RepairThread(final Consumer<Void> repairFinishHandler) {
        this.repairFinishHandler = repairFinishHandler;
    }

    @Override
    public void run() {
        repairService.repair(environment().shifts(), ukmContext().findRequest());
        repairFinishHandler.accept(null);
    }
}
