package ru.itsavant.ofdukm.ui.form.pane.builder;

import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ru.itsavant.ofdukm.ui.application.StageCloseEventHandler;

import static ru.itsavant.ofdukm.ui.environment.Environment.environment;


public class Root {

    public void show(final Stage stage) {
        stage.setOnCloseRequest(new StageCloseEventHandler());

        final VBox vBox = new VBox();
        vBox.getChildren().addAll(
                new RootFormBuilder().build(stage)
        );

        vBox.setSpacing(5);
        vBox.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" +
                "-fx-border-radius: 5;" +
                "-fx-border-color: blue;");

        final Scene scene = new Scene(vBox);
        stage.setScene(scene);
        stage.setTitle(String.format("Исправление расхождений (версия %s)", environment().releaseVersion().prettify()));

        stage.show();
    }
}
