package ru.itsavant.ofdukm.ui.application;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class StageCloseEventHandler implements EventHandler<WindowEvent> {

    @Override
    public void handle(WindowEvent event) {
        environment().inProgressListener().stop();
        Platform.exit();
        System.exit(0);
    }
}
