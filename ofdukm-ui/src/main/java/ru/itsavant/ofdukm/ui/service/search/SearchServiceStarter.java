package ru.itsavant.ofdukm.ui.service.search;


import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.transport.search.SearchResult;

import java.util.List;
import java.util.function.Consumer;

public class SearchServiceStarter {

    private final SearchInitEnvironmentValidator environmentValidator = new SearchInitEnvironmentValidator();
    private final SearchEnvironmentSetter environmentSetter = new SearchEnvironmentSetter();

    public void start(final Consumer<List<ShiftListItem>> searchFinishHandler) {
        if (!environmentValidator.valid()) {
            return;
        }
        environmentSetter.setEnvironment();

        new Thread(
                new SearchThread(searchFinishHandler)
        ).start();
    }
}
