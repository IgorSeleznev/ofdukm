package ru.itsavant.ofdukm.ui.environment;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigurationSaver {

    private final String filename;

    public ConfigurationSaver(final String filename) {
        this.filename = filename;
    }

    public void save(final Configuration configuration) {
        final Path path = Paths.get(filename);
        try {
            final String json = new ObjectMapper()
                    .writer(new DefaultPrettyPrinter()
                    .withObjectIndenter(new DefaultIndenter().withLinefeed("\n")))
                    .writeValueAsString(configuration);
            Files.write(path, json.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка сохранения файла конфигурации!");
        }
    }
}
