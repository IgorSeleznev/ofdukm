package ru.itsavant.ofdukm.ui.service.search;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class SearchInitEnvironmentValidator {

    public boolean valid() {
        try {
            if (environment().searchFilter().getUkmPos() == null) {
                final Alert alert = new Alert(Alert.AlertType.WARNING, "Пожалуйста, укажите кассу", ButtonType.CLOSE);
                alert.showAndWait();
                throw new IllegalStateException("Не указана касса");
            }
            if (environment().searchFilter().getUkmStore() == null) {
                final Alert alert = new Alert(Alert.AlertType.WARNING, "Пожалуйста, укажите торговую точку", ButtonType.CLOSE);
                alert.showAndWait();
                throw new IllegalStateException("Не указана торговая точка");
            }
            if (defaultIfNull(environment().searchFilter().getShiftNumber(), "").length() != 0) {
                return true;
            }
            if (defaultIfNull(environment().searchFilter().getStartDate(), "").length() < 19) {
                final Alert alert = new Alert(Alert.AlertType.WARNING, "Дата или время начала периода не указаны или указаны неверно", ButtonType.CLOSE);
                alert.showAndWait();
                throw new IllegalStateException("Не указана дата начала периода");
            }
            if (defaultIfNull(environment().searchFilter().getFinishDate(), "").length() < 19) {
                final Alert alert = new Alert(Alert.AlertType.WARNING, "Дата или время окончания периода не указаны или указаны неверно", ButtonType.CLOSE);
                alert.showAndWait();
                throw new IllegalStateException("Не указана дата окончания периода");
            }
            return true;
        } catch (final IllegalStateException exception) {
            return false;
        }
    }
}
