package ru.itsavant.ofdukm.ui.handler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;
import ru.itsavant.ofdukm.ui.service.export.ExportServiceStarter;

import java.io.File;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class ExportStartHandler implements EventHandler<ActionEvent> {

    private final Consumer<Void> repairFinishHandler;
    private final Stage stage;
    private final Supplier<OfdShiftAmount> shiftAmountSupplier;

    public ExportStartHandler(final Supplier<OfdShiftAmount> shiftAmountSupplier,
                              final Consumer<Void> repairFinishHandler,
                              final Stage stage) {
        this.repairFinishHandler = repairFinishHandler;
        this.stage = stage;
        this.shiftAmountSupplier = shiftAmountSupplier;
    }

    @Override
    public void handle(final ActionEvent event) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        final File selectedDirectory = directoryChooser.showDialog(stage);

        if (selectedDirectory != null) {
            new ExportServiceStarter().start(
                    shiftAmountSupplier.get(),
                    repairFinishHandler,
                    selectedDirectory.getAbsolutePath() + "\\"
            );
        }
    }
}