package ru.itsavant.ofdukm.ui.service.search;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.transport.search.*;
import ru.itsavant.ofdukm.comparator.transport.transport.destination.DefaultTransportDestinationProgressEventHandler;

import java.util.List;
import java.util.function.Consumer;

import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class SearchThread implements Runnable {

    private final SearchService searchService = new SearchService();
    private final SearchPrepareService prepareService = new SearchPrepareService();
    private final Consumer<List<ShiftListItem>> searchFinishHandler;

    public SearchThread(final Consumer<List<ShiftListItem>> searchFinishHandler) {
        this.searchFinishHandler = searchFinishHandler;
    }

    @Override
    public void run() {
        try {
            prepareService.prepare();
            searchService.onFinished(searchFinishHandler).search(
                    new SearchRequest()
                            .shiftId(environment().searchFilter().getShiftNumber())
                            .storeId(environment().searchFilter().getUkmStore().getStoreId())
                            .cashId(environment().searchFilter().getUkmPos().getCashId())
                            .period(
                                    new SearchPeriodValue.Builder()
                                            .from(environment().searchFilter().getStartDate())
                                            .to(environment().searchFilter().getFinishDate())

                            ),
                    new DefaultTransportDestinationProgressEventHandler()
            );
        } catch (final Throwable throwable) {
            environment().inLoad().setValue(false);
        }
    }
}
