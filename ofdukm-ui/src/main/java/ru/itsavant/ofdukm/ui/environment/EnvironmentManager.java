package ru.itsavant.ofdukm.ui.environment;

import lombok.extern.slf4j.Slf4j;
import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;
import ru.itsavant.ofdukm.comparator.ukm.service.service.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;
import static ru.itsavant.ofdukm.comparator.common.context.CommonContext.commonContext;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

@Slf4j
public class EnvironmentManager {

    private final UkmPosListService ukmPosListService = new UkmPosListService();
    private final UkmPricelistListService ukmPricelistListService = new UkmPricelistListService();
    private final UkmStoreToStorePlaceService storePlaceListService = new UkmStoreToStorePlaceService();

    public void initialize() {
        environment().configuration(
                new ConfigurationLoader("ofdukm-comparator.json").load()
        );
        final Thread thread = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            environment().inLoad().setValue(true);
                            environment().inProgressListener().start();
                            commonContext().messageQueue().push(
                                    new ProgressMessage()
                                            .messageSource(this.getClass())
                                            .current(0)
                                            .size(10)
                                            .title("Загрузка данных...")
                            );
                            environment().stores().clear();

                            ukmContext().configure(
                                    environment().configuration().getDatabase()
                            );

                            environment().stores().addAll(
                                    new UkmStoreListService().list()
                            );

                            environment().storeToStorePlaceMap().putAll(
                                    storePlaceListService.storeToStorePlaceMap()
                            );


                            environment().nomenclatures(
                                    new UkmNomenclatureListService().list()
                            );

                            ukmContext().storePlaceId(
                                    environment().storeToStorePlaceMap().get(
                                            environment().stores().get(0).getStoreId()
                                    )
                            );

                            environment().stores().forEach(
                                    ukmStore -> environment()
                                            .ukmPosMap()
                                            .put(
                                                    ukmStore.getStoreId(),
                                                    ukmPosListService.list(ukmStore.getStoreId())
                                            )
                            );

//                            environment().ukmPosMap().forEach(
//                                    (key, value) -> {
//                                        try {
//                                            Files.write(
//                                                    Paths.get("debug.log"),
//                                                    String.format("%s -> %s", key, value).getBytes(StandardCharsets.UTF_8),
//                                                    CREATE, WRITE
//                                            );
//                                        } catch (IOException e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                            );

                            environment().nomenclatures().forEach(
                                    ukmPricelist -> environment()
                                            .ukmPricelistMap()
                                            .put(
                                                    ukmPricelist.getNomenclatureId(),
                                                    ukmPricelistListService.list(ukmPricelist.getNomenclatureId())
                                            )
                            );

                            commonContext().messageQueue().push(
                                    new ProgressMessage()
                                            .messageSource(this.getClass())
                                            .current(1)
                                            .size(10)
                                            .title("Список магазинов загружен")
                            );
                        } catch (final Throwable throwable) {
                            environment().errorMessage().setValue(
                                    ErrorMessage.builder()
                                            .title("Ошибка загрузки данных")
                                            .exception(throwable)
                                            .message("Во время загрузки данных из УКМ произошла ошибка")
                                            .build()
                            );
                        } finally {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            environment().inLoad().setValue(false);
                        }
                    }
                }
        );
        thread.start();
    }
}