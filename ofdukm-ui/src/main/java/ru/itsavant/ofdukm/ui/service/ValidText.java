package ru.itsavant.ofdukm.ui.service;

class ValidText {

    static String valid() {
        return "Операция недоступна.\n" +
                "Лицензия УКМ-сервера не подходит для данной копии программы.\n" +
                "Пожалуйста, обратитесь к своему поставщику данной программы \n" +
                "для получения копии, собранной для Вашего сервера УКМ.";
    }

    static String title() {
        return "Ошибка лицензии.";
    }
}
