package ru.itsavant.ofdukm.ui.service.export;

import ru.itsavant.ofdukm.comparator.repair.service.ExportService;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;

import java.util.function.Consumer;
import java.util.stream.Collectors;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class ExportThread implements Runnable {

    private final ExportService exportService;
    private final Consumer<Void> exportFinishHandler;
    private final OfdShiftAmount shiftAmount;

    public ExportThread(final OfdShiftAmount shiftAmount,
                        final Consumer<Void> repairFinishHandler,
                        final String directoryPath
    ) {
        this.exportFinishHandler = repairFinishHandler;
        this.exportService = new ExportService(directoryPath);
        this.shiftAmount = shiftAmount;
    }

    @Override
    public void run() {
        try {
            if (shiftAmount != null) {
                exportService.export(
                        environment().shifts().stream().filter(
                                item -> String.valueOf(item.getShiftNumber()).equals(shiftAmount.getShift())
                        ).collect(Collectors.toList()),
                        ukmContext().findRequest()
                );
            } else {
                exportService.export(environment().shifts(), ukmContext().findRequest());
            }
            exportFinishHandler.accept(null);
        } catch (final Throwable throwable) {
            environment().inLoad().setValue(false);
        }
    }
}
