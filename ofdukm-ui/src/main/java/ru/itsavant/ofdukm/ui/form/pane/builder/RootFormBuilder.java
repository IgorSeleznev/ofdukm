package ru.itsavant.ofdukm.ui.form.pane.builder;

import ch.qos.logback.classic.Logger;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import lombok.val;
import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;
import ru.itsavant.ofdukm.comparator.common.logger.LoggerUtil;
import ru.itsavant.ofdukm.comparator.common.numeric.Rubbles;
import ru.itsavant.ofdukm.comparator.common.stacktrace.StackTracePrinter;
import ru.itsavant.ofdukm.comparator.transport.search.SearchOfdStatisticsService;
import ru.itsavant.ofdukm.comparator.transport.search.SearchStatistic;
import ru.itsavant.ofdukm.comparator.transport.search.SearchUkmStatisticsService;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;
import ru.itsavant.ofdukm.comparator.ukm.service.service.OfdShiftListAmountService;
import ru.itsavant.ofdukm.ui.environment.ConfigurationSaver;
import ru.itsavant.ofdukm.ui.form.table.FoundTableFactory;
import ru.itsavant.ofdukm.ui.form.table.ShiftTableFactory;
import ru.itsavant.ofdukm.ui.handler.ExportStartHandler;
import ru.itsavant.ofdukm.ui.handler.SearchStartHandler;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.common.context.CommonContext.commonContext;
import static ru.itsavant.ofdukm.comparator.common.numeric.Rubbles.rubbles;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class RootFormBuilder {

    private final Logger logger;

    public RootFormBuilder() {
        this.logger = LoggerUtil.createLoggerFor("ru.itsavant.ofdukm.ui.form.pane.builder.RootFormBuilder", "ofdukm-comparator.log");
    }

    public List<Node> build(final Stage stage) {
        final GridPane rootPane = new GridPane();

        // Set the hgap and vgap properties
        rootPane.setHgap(10);
        rootPane.setVgap(5);

        final ComboBox<OfdShiftAmount> shiftAmountBox = new ComboBox<>();

        final GridPane commonPane = new GridPane();
        commonPane.setHgap(10);
        commonPane.setVgap(5);
        commonPane.setLayoutX(10);
        commonPane.setLayoutY(10);

        final List<Tab> tabs = new ArrayList<>();
        tabs.add(new Tab());
        tabs.get(0).setText("Расхождения");
        tabs.add(new Tab());
        tabs.get(1).setText("Настройки");

        // Set the hgap and vgap properties
        rootPane.setHgap(10);
        rootPane.setVgap(5);

        final TabPane tabPane = new TabPane(tabs.get(0), tabs.get(1));

        final GridPane searchPane = new GridPane();
        searchPane.setHgap(10);
        searchPane.setVgap(5);

        tabs.get(0).setContent(commonPane);
        tabs.get(1).setContent(
                new ConfigurationFormBuilder().build(
                        event -> new ConfigurationSaver("ofdukm-comparator.json").save(
                                environment().configuration()
                        )
                )
        );

        rootPane.add(tabPane, 0, 0);

        final TableView<SearchStatistic> ofdTable = new FoundTableFactory().tableInstance(newArrayList());
        final TableView<SearchStatistic> ukmTable = new FoundTableFactory().tableInstance(newArrayList());
        final TableView<OfdShiftAmount> shiftsTable = new ShiftTableFactory().tableInstance(newArrayList());

/*
        dao.execute(
                new SqlParametrizedQuery()
                .sql(
                        "update trm_out_shift_open\n" +
                        "set sale = (select sum(ifnull(i.price, 0) * ifnull(i.quantity, 0) + ifnull(i.discount, 0)) from trm_out_receipt_item i, trm_out_receipt_header h where h.cash_id = ? and h.shift_open = ? and h.cash_id = i.cash_id and h.id = i.receipt_header and h.type = 0),\n" +
                        "    sale_fiscal = sale = (select sum(ifnull(i.price, 0) * ifnull(i.quantity, 0) + ifnull(i.discount, 0)) from trm_out_receipt_item i, trm_out_receipt_header h where h.cash_id = ? and h.shift_open = ? and h.cash_id = i.cash_id and h.id = i.receipt_header and h.type = 0),\n" +
                        "    sreturn = ifnull((select sum(ifnull(i.price, 0) * ifnull(i.quantity, 0) + ifnull(i.discount, 0)) from trm_out_receipt_item i, trm_out_receipt_header h where h.cash_id = ? and h.shift_open = ? and h.cash_id = i.cash_id and h.id = i.receipt_header and h.type = 3), 0),\n" +
                        "    sreturn_fiscal = ifnull((select sum(ifnull(i.price, 0) * ifnull(i.quantity, 0) + ifnull(i.discount, 0)) from trm_out_receipt_item i, trm_out_receipt_header h where h.cash_id = ? and h.shift_open = ? and h.cash_id = i.cash_id and h.id = i.receipt_header and h.type = 3), 0)\n" +
                        "where cash_id = ? and id = ?;\n"
                )
                .parameters(
                        newArrayList(

                        )
                )
        );

 */

        val ofdAmountLabel = new Label("сумма чеков в ОФД: 0");
        val ukmAmountLabel = new Label("количество чеков в ОФД: 0");

        val ofdCountLabel = new Label("сумма чеков в УКМ: 0");
        val ukmCountLabel = new Label("количество чеков в УКМ: 0");

        final Node filterPane = new SearchFormPaneBuilder().build(
                shiftAmountBox,
                new SearchStartHandler(
                        (list) -> {
                            try {
                                commonContext().messageQueue().push(
                                        new ProgressMessage()
                                                .messageSource(this.getClass())
                                                .title("Вывод статистики")
                                                .current(100)
                                                .size(100)
                                );

                                environment().shifts(list);

                                environment().inLoad().setValue(false);
                                final List<SearchStatistic> ofdStatistic = new SearchOfdStatisticsService().statictics();
                                final List<SearchStatistic> ukmStatistic = new SearchUkmStatisticsService().statictics();
                                ofdTable.getItems().removeAll();
                                ofdTable.getItems().setAll(ofdStatistic);
                                ukmTable.getItems().removeAll();
                                ukmTable.getItems().setAll(ukmStatistic);

                                final Rubbles ofdRubbles = rubbles(0);
                                ofdStatistic.forEach(item -> ofdRubbles.add(rubbles(item.getAmount())));

                                final Rubbles ukmRubbles = rubbles(0);
                                ukmStatistic.forEach(item -> ukmRubbles.add(rubbles(item.getAmount())));

                                Platform.runLater(() -> {
                                    ukmAmountLabel.setText(String.format("сумма чеков в УКМ: %s", ukmRubbles.toString()));
                                    ukmCountLabel.setText(String.format("количество чеков в УКМ: %d", ukmStatistic.size()));

                                    ofdAmountLabel.setText(String.format("сумма чеков в ОФД: %s", ofdRubbles.toString()));
                                    ofdCountLabel.setText(String.format("количество чеков в ОФД: %d", ofdStatistic.size()));
                                });

                                final List<OfdShiftAmount> shiftAmounts = new OfdShiftListAmountService().list();
                                shiftsTable.getItems().removeAll();
                                shiftsTable.getItems().setAll(shiftAmounts);
                                environment().shiftAmounts().clear();
                                environment().shiftAmounts().addAll(shiftAmounts);
                                shiftAmountBox.getItems().clear();
                                shiftAmountBox.getItems().addAll(shiftAmounts);

                            } catch (final Throwable throwable) {
                                logger.error("Произошла ошибка в процессе поиска расхождений: {}", throwable.getMessage());
                                logger.error(StackTracePrinter.print(throwable));
                                commonContext().errorsQueue().add(
                                        ErrorMessage.builder().message("Ошибка вывода результата поиска на экран: " + throwable.getMessage()).title("Ошибка").exception(throwable).build()
                                );
                            }
                        }
                ),
                new ExportStartHandler(
                        () -> environment().searchFilter().getOfdShiftAmount(),
                        nullValue -> environment().inLoad().setValue(false),
                        stage
                )
        );

        final Node progressPane = new ProgressPaneBuilder().build();

        final GridPane tablesPane = new GridPane();

        final Label ofdTableTitleLabel = new Label("Найдено в ОФД");
        ofdTableTitleLabel.setFont(new Font("Arial", 14));

        tablesPane.add(ofdTableTitleLabel, 0, 0);
        tablesPane.add(ofdTable, 0, 1);
        tablesPane.add(ofdAmountLabel, 0, 2);
        tablesPane.add(ofdCountLabel, 0, 3);

        final Label ukmTableTitleLabel = new Label("Найдено в УКМ");
        ukmTableTitleLabel.setFont(new Font("Arial", 14));

        tablesPane.add(ukmTableTitleLabel, 0, 4);
        tablesPane.add(ukmTable, 0, 5);
        tablesPane.add(ukmAmountLabel, 0, 6);
        tablesPane.add(ukmCountLabel, 0, 7);

        searchPane.add(tablesPane, 0, 0);
        searchPane.add(filterPane, 1, 0);
        tablesPane.add(shiftsTable, 0, 8);

        searchPane.setVisible(true);
        progressPane.setVisible(false);

        commonPane.add(searchPane, 0, 0);
        commonPane.add(progressPane, 0, 0);

        final List<Node> nodes = new ArrayList<>();
        nodes.add(rootPane);

        environment().inLoad().addListener((observable, oldValue, newValue) -> {
            //searchPane.setDisable(newValue);
            searchPane.setVisible(!newValue);
            progressPane.setVisible(newValue);
        });

        return nodes;
    }
}
