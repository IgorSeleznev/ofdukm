package ru.itsavant.ofdukm.ui.environment;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class ConfigurationLoader {

    private final String filename;

    public ConfigurationLoader(final String filename) {
        this.filename = filename;
    }

    public Configuration load() {
        final Path path = Paths.get(filename);
        try {
//            return new Configuration()
//                    .setNomenclatureId(1)
//                    .setPricelistId(1)
//                    .setDatabase(
//                            new DaConfiguration()
//                                    .charset("utf8")
//                                    .host("localhost")
//                                    .username("root")
//                                    .password("CtHDbCGK.C")
//                                    .port(3306)
//                                    .useUnicode(true)
//                    );
            return new ObjectMapper().readValue(
                    new String(Files.readAllBytes(path)),
                    Configuration.class
            );
        } catch (IOException exception) {
            exception.printStackTrace();
            environment().errorMessage()
                    .setValue(
                            ErrorMessage.builder()
                            .message("При загрузке файла с настройками произошла ошибка")
                            .title("Ошибка загрузки конфигурации")
                            .exception(exception)
                            .build()
                    );
            throw new RuntimeException("Отсутствует или не соответствует формату файл конфигурации!");
        }
    }
}