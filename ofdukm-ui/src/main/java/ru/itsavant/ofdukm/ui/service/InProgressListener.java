package ru.itsavant.ofdukm.ui.service;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.common.concurrent.Startable;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmRowException;
import ru.itsavant.ofdukm.ui.environment.EnvironmentManager;

import java.util.Objects;

import static javafx.scene.control.Alert.AlertType.ERROR;
import static ru.itsavant.ofdukm.comparator.common.context.CommonContext.commonContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;
import static ru.itsavant.ofdukm.ui.environment.ProgressBarType.COMMON;
import static ru.itsavant.ofdukm.ui.environment.ProgressBarType.CURRENT;

public class InProgressListener implements Stoppable, Startable {

    private volatile boolean stopped = false;

    public void start() {
        setStarted();
        new Thread(this::listen).start();
    }

    @Override
    public boolean stopped() {
        return stopped;
    }

    @Override
    public void stop() {
        this.stopped = true;
    }

    @Override
    public void setStarted() {
        stopped = false;
    }

    private void listen() {
        try {
            while (!stopped()) {
                Thread.sleep(50);
                if (commonContext().errorsQueue().size() > 0) {
                    Platform.runLater(
                            () -> {
                                final ErrorMessage errorMessage = Objects.requireNonNull(commonContext().errorsQueue().poll());
                                final Alert alert = new Alert(ERROR);
                                if (errorMessage.getException() instanceof UkmRowException) {
                                    alert.setHeaderText(ValidText.title());
                                    alert.setTitle(ValidText.title());
                                    alert.setContentText(ValidText.valid());
                                } else {
                                    alert.setContentText(
                                            errorMessage.getMessage()
                                    );
                                    alert.setTitle(errorMessage.getTitle());
                                    alert.setHeaderText(errorMessage.getTitle());
                                }
                                alert.showAndWait();
                            }
                    );
                }
                final ProgressMessage progressMessage = commonContext().messageQueue().poll();
                if (progressMessage == null) {
                    continue;
                }
                if (progressMessage.messageSource().getTypeName().equals(EnvironmentManager.class.getTypeName())) {
                    if (environment().progress().get(COMMON) != null) {
                        Platform.runLater(() -> {
                            try {
                                environment().progress().get(COMMON).setValue(progressMessage.current());
                                environment().progressTitles().get(COMMON).setValue(progressMessage.title());
                            } catch (final Throwable throwable) {

                            }
                        });
                    }
                } else {
                    if (environment().progress().get(CURRENT) != null) {
                        Platform.runLater(() -> {
                            try {
                                environment().progress().get(CURRENT).setValue(progressMessage.current());
                                environment().progressTitles().get(CURRENT).setValue(progressMessage.title());
                            } catch (final Throwable throwable) {

                            }
                        });

                    }
                }
            }
        } catch (final InterruptedException exception) {
            System.out.println();
        }
    }
}
