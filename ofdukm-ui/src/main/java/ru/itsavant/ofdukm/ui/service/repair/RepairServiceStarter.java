package ru.itsavant.ofdukm.ui.service.repair;

import java.util.function.Consumer;

import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class RepairServiceStarter {

    public void start(final Consumer<Void> repairFinishHandler) {
        environment().inLoad().setValue(true);
        new Thread(
                new RepairThread(repairFinishHandler)
        ).start();
    }
}
