package ru.itsavant.ofdukm.ui.handler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.transport.search.SearchResult;
import ru.itsavant.ofdukm.ui.service.search.SearchServiceStarter;

import java.util.List;
import java.util.function.Consumer;

public class SearchStartHandler implements EventHandler<ActionEvent> {

    private final SearchServiceStarter serviceStarter = new SearchServiceStarter();

    private final Consumer<List<ShiftListItem>> searchFinishHandler;

    public SearchStartHandler(final Consumer<List<ShiftListItem>> onFinishHandler) {
        this.searchFinishHandler = onFinishHandler;
    }

    @Override
    public void handle(final ActionEvent event) {
        serviceStarter.start(
                searchFinishHandler
        );
    }
}
