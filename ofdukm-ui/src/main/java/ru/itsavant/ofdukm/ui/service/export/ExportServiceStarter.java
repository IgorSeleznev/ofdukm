package ru.itsavant.ofdukm.ui.service.export;

import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;

import java.util.function.Consumer;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class ExportServiceStarter {

    public void start(final OfdShiftAmount shiftAmount,
                      final Consumer<Void> exportFinishHandler,
                      final String directoryPath
    ) {
        environment().inLoad().setValue(true);
        ukmContext().exportShiftId(environment().exportShiftId());
        new Thread(
                new ExportThread(shiftAmount, exportFinishHandler, directoryPath)
        ).start();
    }
}
