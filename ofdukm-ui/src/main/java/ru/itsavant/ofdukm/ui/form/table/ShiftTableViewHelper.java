package ru.itsavant.ofdukm.ui.form.table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdShiftAmount;

import java.util.List;

public class ShiftTableViewHelper {

    public static ObservableList<OfdShiftAmount> getObservableList(final List<OfdShiftAmount> data) {
        return FXCollections.observableArrayList(data);
    }

    public static TableColumn<OfdShiftAmount, String> stringColumn(final String code, final String title) {
        final TableColumn<OfdShiftAmount, String> column = new TableColumn<>(title);
        final PropertyValueFactory<OfdShiftAmount, String> cellValueFactory = new PropertyValueFactory<>(code);
        column.setCellValueFactory(cellValueFactory);
        return column;
    }
}
