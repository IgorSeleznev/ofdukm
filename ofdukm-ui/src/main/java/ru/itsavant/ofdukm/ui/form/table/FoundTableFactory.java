package ru.itsavant.ofdukm.ui.form.table;

import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import ru.itsavant.ofdukm.comparator.transport.search.SearchStatistic;

import java.util.List;

import static ru.itsavant.ofdukm.ui.form.table.FoundTableViewHelper.stringColumn;

public class FoundTableFactory {

    public TableView<SearchStatistic> tableInstance(final List<SearchStatistic> data) {
        final TableView<SearchStatistic> table = new TableView<>(FoundTableViewHelper.getObservableList(data));

        TableView.TableViewSelectionModel<SearchStatistic> tableSelectionModel = table.getSelectionModel();
        tableSelectionModel.setSelectionMode(SelectionMode.SINGLE);

        table.getColumns().addAll(
                stringColumn("shift", "Смена"),
                stringColumn("date", "Дата чека"),
                stringColumn("amount", "Сумма"),
                stringColumn("count", "Позиций в чеке"),
                stringColumn("quantity", "Количество")
        );

        // Set the stringColumn resize policy to constrained resize policy
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // Set the Placeholder for an empty tableInstance
        table.setPlaceholder(new Label("Нет данных."));

        return table;
    }
}
