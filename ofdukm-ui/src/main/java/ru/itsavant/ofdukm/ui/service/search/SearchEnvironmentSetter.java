package ru.itsavant.ofdukm.ui.service.search;

import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;
import static ru.itsavant.ofdukm.ui.environment.Environment.environment;

public class SearchEnvironmentSetter {

    public void setEnvironment() {
        environment().inLoad().setValue(true);
        ukmContext().findRequest(
                new UkmFindRequest()
                        .setShiftId(environment().searchFilter().getShiftNumber())
                        .setCashName(environment().searchFilter().getUkmPos().getName())
                        .setCashId(environment().searchFilter().getUkmPos().getCashId())
                        .setStoreId(environment().searchFilter().getUkmStore().getStoreId())
                        .setStartDate(environment().searchFilter().getStartDate().replace("T", " "))
                        .setFinishDate(environment().searchFilter().getFinishDate().replace("T", " "))
        );
        ofdContext().integratorId(environment().configuration().getIntegratorId());
        ofdContext().login(environment().configuration().getOfdLogin());
        ofdContext().password(environment().configuration().getOfdPassword());
    }
}
