package ru.itsavant.ofdukm.ui.main;

import ch.qos.logback.classic.Logger;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import ru.itsavant.ofdukm.ui.environment.EnvironmentManager;
import ru.itsavant.ofdukm.ui.form.pane.builder.Root;

public class MainForm extends Application
{
    private static final Logger LOGGER = (Logger)LoggerFactory.getLogger("ofdukm-ui");

    public static void main(final String... args) {
        Application.launch(args);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void start(final Stage stage) {

        LOGGER.info("ПРИЛОЖЕНИЕ ЗАПУЩЕНО.");
        new EnvironmentManager().initialize();
        new Root().show(stage);
    }
}
