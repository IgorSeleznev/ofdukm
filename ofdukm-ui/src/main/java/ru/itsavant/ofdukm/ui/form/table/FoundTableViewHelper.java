package ru.itsavant.ofdukm.ui.form.table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.itsavant.ofdukm.comparator.transport.search.SearchStatistic;

import java.util.List;

public class FoundTableViewHelper {

    public static ObservableList<SearchStatistic> getObservableList(final List<SearchStatistic> data) {
        return FXCollections.observableArrayList(data);
    }

    public static TableColumn<SearchStatistic, String> stringColumn(final String code, final String title) {
        final TableColumn<SearchStatistic, String> column = new TableColumn<>(title);
        final PropertyValueFactory<SearchStatistic, String> cellValueFactory = new PropertyValueFactory<>(code);
        column.setCellValueFactory(cellValueFactory);
        return column;
    }
}
