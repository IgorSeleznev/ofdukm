package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

import org.springframework.core.annotation.AliasFor;

import static ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation.EnumConvertType.STRING;

public @interface EnumConversion {

    @AliasFor("type")
    EnumConvertType value() default STRING;

    @AliasFor("parameters")
    EnumConvertType type() default STRING;
}
