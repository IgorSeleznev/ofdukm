package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmStore;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmStoreListDao {

    private static final String UKM_STORE_LIST_SQL = "select store_id, name, enterprise_name, address \n" +
            "from %s.trm_in_store \n" +
            "where deleted = 0 \n" +
            "order by name asc\n";

    public List<DaUkmStore> list() {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        return jdbcTemplate.query(
                String.format(UKM_STORE_LIST_SQL, UKM_DATABASE_NAME),
                (resultSet, rowNum) -> new DaUkmStore()
                        .setStoreId(String.valueOf(resultSet.getInt(1)))
                        .setName(resultSet.getString(2))
                        .setEnterpriseName(resultSet.getString(3))
                        .setAddress(resultSet.getString(4))
        );
    }
}
