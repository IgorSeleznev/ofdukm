package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;


@Getter
@Setter
@Builder
@Accessors(chain = true)
@AllArgsConstructor
public class CreateTableQuery {

    private String schema;
    private boolean ifNotExist = false;
    private String tablename;
    private List<CreateTableFieldDescription> fields;
    private String postFieldsSqlExpression;
}
