package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public @interface RelationFields {

    String[] thisFields() default {};
    String[] thoseFields() default {};
}
