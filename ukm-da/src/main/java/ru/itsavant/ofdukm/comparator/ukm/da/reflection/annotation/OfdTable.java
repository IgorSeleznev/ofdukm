package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface OfdTable {

    @AliasFor("name")
    String value() default "";

    @AliasFor("parameters")
    String name() default "";

    String[] postDescriptions() default {};

    String schema() default "";

    boolean dropIfExist() default false;

    boolean checkExist() default false;

    String checkExistSql() default "";

    String dropIfExistSql() default "";

    boolean temporaryTable() default false;

    Index[] indexes() default {};

    Relation[] relations() default {};
}
