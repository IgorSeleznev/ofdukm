package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public enum InsertationBehaviorType {

    EAGER_CHILD,
    ALL_PARENT_FIRST
}
