package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_RECEIPT_ITEM_TABLENAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class OfdReceiptItemSaveDao {

    public void save(final List<OfdReceiptItem> receiptItems) {
        final JdbcTemplate template = daContext().jdbcTemplate();

        try {

            if (receiptItems.size() == 0) {
                return;
            }

            template.execute("begin");

            final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            var dao = new InsertListDao<OfdReceiptItem>(template);
            dao.insert(
                    InsertListQuery.<OfdReceiptItem>builder()
                            .data(receiptItems)
                            .insertHeadSql(
                                    String.format(
                                    "insert into %s.%s (" +
                                            "store_id, " +
                                            "cash_id, " +
                                            "insert_record_date, " +
                                            "fn, " +
                                            "fd, " +

                                            "number_in_document, " +
                                            "item_name, " +
                                            "price, " +
                                            "quantity, " +
                                            "amount" +
                                            ")",
                                            OFD_DATABASE_NAME,
                                            OFD_RECEIPT_ITEM_TABLENAME
                                    )
                            )
                            .parameterMapper(
                                    receiptItem -> {
                                        final List<Object> list = newArrayList();
                                        list.add(receiptItem.getStoreId());
                                        list.add(receiptItem.getCashId());
                                        list.add(formatter.format(new Date()));
                                        list.add(receiptItem.getFn());
                                        list.add(receiptItem.getFd());

                                        list.add(receiptItem.getNumberInDocument());
                                        list.add(receiptItem.getItemName());
                                        list.add(Double.valueOf(receiptItem.getPrice()) / 100);
                                        list.add(Double.valueOf(receiptItem.getQuantity()));
                                        list.add(Double.valueOf(receiptItem.getAmount()) / 100);
//                                        items.add(BigDecimal.valueOf(Integer.valueOf(receiptItem.getPrice())).divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_EVEN));
//                                        items.add(BigDecimal.valueOf(Integer.valueOf(receiptItem.getQuantity())).divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_EVEN));
//                                        items.add(BigDecimal.valueOf(Integer.valueOf(receiptItem.getAmount())).divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_EVEN));

                                        return list;
                                    }
                            )
                            .build()
            );

            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("rollback");
        }
    }
}
