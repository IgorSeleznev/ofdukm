package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTable;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldDescription;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableQuery;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_FISCAL_STORAGE_TABLENAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.VARCHAR;

public class OfdFiscalStorageCreateTableDao implements CreateTable {

    @Override
    public void create() {
        final CreateTableDao createTableDao = new CreateTableDao();
        createTableDao.create(
                CreateTableQuery.builder()
                        .ifNotExist(true)
                        .schema(OFD_DATABASE_NAME)
                        .tablename(OFD_FISCAL_STORAGE_TABLENAME)
                        .fields(
                                newArrayList(
                                        CreateTableFieldDescription.builder()
                                                .name("kkt_registration_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("kkt_factory_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fn").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fn_status").fieldType(VARCHAR).notNull(false).size(32)
                                                .build()
                                )
                        )
                        .build()
        );
    }
}
