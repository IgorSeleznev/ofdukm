package ru.itsavant.ofdukm.comparator.ukm.da.configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentPropertiesUkmMapping {

    private Map<String, String> properties;
}
