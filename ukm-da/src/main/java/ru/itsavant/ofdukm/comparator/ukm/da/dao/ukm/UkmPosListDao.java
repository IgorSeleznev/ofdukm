package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPos;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmPosListDao {

    private static final String UKM_POS_LIST_SQL =
            "select p.store_id, p.cash_id, p.name\n" +
                    "from %s.trm_in_pos p\n" +
                    "where p.deleted = 0\n" +
                    "  and p.store_id = ?;\n";

    public List<DaUkmPos> list(final String storeId) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        final List<DaUkmPos> posList = jdbcTemplate.query(
                String.format(UKM_POS_LIST_SQL, UKM_DATABASE_NAME),
                statement -> statement.setString(1, storeId),
                (resultSet, rowNum) -> new DaUkmPos()
                        .setCashId(resultSet.getString(2))
                        .setStoreId(resultSet.getString(1))
                        .setName((resultSet.getString(3)))
//                        .setRegistrationNumber(resultSet.getString(4))
//                        .setFactoryNumber(resultSet.getString(5))
        );

        return posList;
    }
}
