package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTable;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldDescription;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableQuery;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.*;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.*;

public class OfdReceiptHeaderCreateDao implements CreateTable {

    @Override
    public void create() {
        final CreateTableDao createTableDao = new CreateTableDao();
        createTableDao.create(
                CreateTableQuery.builder()
                        .ifNotExist(true)
                        .schema(OFD_DATABASE_NAME)
                        .tablename(OFD_RECEIPT_HEADER_TABLENAME)
                        .fields(
                                newArrayList(
//                                        "store_id, " +
                                        CreateTableFieldDescription.builder()
                                                .name("store_id").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "cash_id, " +
                                        CreateTableFieldDescription.builder()
                                                .name("cash_id").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "ukm_shift, " +
                                        CreateTableFieldDescription.builder()
                                                .name("ukm_shift").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "kkt_shift, " +
                                        CreateTableFieldDescription.builder()
                                                .name("kkt_shift").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "fn, " +
                                        CreateTableFieldDescription.builder()
                                                .name("fn").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//
//                                                "fd, " +
                                        CreateTableFieldDescription.builder()
                                                .name("fd").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "global_number, " +
                                        CreateTableFieldDescription.builder()
                                                .name("global_number").fieldType(BIGINT)
                                                .build(),
//                                                "local_number, " +
                                        CreateTableFieldDescription.builder()
                                                .name("local_number").fieldType(BIGINT)
                                                .build(),
//                                                "cash_name, " +
                                        CreateTableFieldDescription.builder()
                                                .name("cash_name").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "cashier_name, " +
                                        CreateTableFieldDescription.builder()
                                                .name("cashier_name").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//
//                                                "insert_record_date, " +
                                        CreateTableFieldDescription.builder()
                                                .name("insert_record_date").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "receipt_date, " +
                                        CreateTableFieldDescription.builder()
                                                .name("receipt_date").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "total_amount, " +
                                        CreateTableFieldDescription.builder()
                                                .name("total_amount").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "items_count, " +
                                        CreateTableFieldDescription.builder()
                                                .name("items_count").fieldType(INTEGER)
                                                .build(),
//                                                "document_type " +
                                        CreateTableFieldDescription.builder()
                                                .name("document_type").fieldType(VARCHAR).notNull(false).size(64)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("cashless_amount").fieldType(VARCHAR).notNull(false).size(255)
                                                .build()
                                )
                        )
                        .build()
        );
    }
}
