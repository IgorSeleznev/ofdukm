package ru.itsavant.ofdukm.comparator.ukm.da.dao;

import ch.qos.logback.classic.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.common.logger.LoggerUtil;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class SafeDatabaseCreator {

    public void safeCreate() {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        try {
            jdbcTemplate.execute("create database if not exists ofdukm charset UTF8");
        } catch (final Throwable throwable) {
            final Logger logger = LoggerUtil.createLoggerFor("ru.itsavant.ofdukm.comparator.ukm.da.dao.SafeDatabaseCreator", "ofdukm-comparator.log");
            logger.warn("Создание базы данных ofdukm завершилось с сообщением: {}", throwable.getMessage());
        }
    }
}
