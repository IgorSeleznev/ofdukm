package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.BatchInsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_KKT_TABLENAME;

public class OfdKktListSaveDao {

    public void save(final List<KktCombined> kktInfoList) {
        final JdbcTemplate template = UkmDatasourceContext.daContext().jdbcTemplate();
        try {
            template.execute("begin");
            var dao = new BatchInsertListDao<KktCombined>(template);
            dao.insert(
                    InsertListQuery.<KktCombined>builder()
                    .data(kktInfoList)
                    .insertHeadSql(
                            String.format(
                            "insert into %s.%s (" +
                                    "outlet_id, outlet_code, name, registration_number, factory_number, " +
                                    "model_name, fiscal_number, fiscal_reg_date, fiscal_duration, shift_status, " +
                                    "cashdesk_state, cashdesk_end_date, fiscal_state, fiscal_end_date, last_document_state, " +
                                    "last_document_date" +
                                    ")",
                                    OFD_DATABASE_NAME,
                                    OFD_KKT_TABLENAME
                            )
                    )
                    .parameterMapper(
                            kkt -> {
                                final List<Object> list = newArrayList();
                                list.add(kkt.getInfo().getCashdesk().getOutlet().getId());
                                list.add(kkt.getInfo().getCashdesk().getOutlet().getCode());
                                list.add(kkt.getInfo().getCashdesk().getName());
                                list.add(kkt.getInfo().getCashdesk().getKktRegNumber());
                                list.add(kkt.getInfo().getCashdesk().getKktFactoryNumber());

                                list.add(kkt.getInfo().getCashdesk().getKktModelName());
                                list.add(kkt.getInfo().getCashdesk().getFnFactoryNumber());
                                list.add(kkt.getInfo().getCashdesk().getFnRegDateTime());
                                list.add(kkt.getInfo().getCashdesk().getFnDuration());
                                list.add(kkt.getInfo().getCashdesk().getShiftStatus());

                                list.add(kkt.getInfo().getCashdesk().getCashdeskState());
                                list.add(kkt.getInfo().getCashdesk().getCashdeskEndDateTime());
                                list.add(kkt.getInfo().getCashdesk().getFnState());
                                list.add(kkt.getInfo().getCashdesk().getFnEndDateTime());
                                list.add(kkt.getInfo().getCashdesk().getLastDocumentState());

                                list.add(kkt.getInfo().getCashdesk().getLastDocumentDateTime());

                                return list;
                            }
                    )
                    .build()
            );
            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("commit");
            throw new RuntimeException(throwable);
        }
    }
}
