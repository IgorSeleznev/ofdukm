package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.CollectResultSetHandler;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.CollectSqlDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmTax;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class UkmTaxListDao {

    public List<DaUkmTax> taxes(final long nomenclatureId) {
/*
  nomenclature_id int default 0 not null,
  id int default 0 not null,
  tax_id int default 0 not null,
  percent varchar(20) default '0' not null,
  fp_code varchar(20) null,
  version int default 0 not null,
  deleted tinyint(1) default 0 not null,

 */
        final CollectSqlDao<DaUkmTax> collectSqlDao = new CollectSqlDao<>();
        return collectSqlDao.collect(
                new SqlParametrizedQuery()
                        .sql("select t.id, t.name, tg.percent, t.priority, tg.id \n" +
                                "from ukmserver.trm_in_taxes t, ukmserver.trm_in_taxgroup tg \n" +
                                "where tg.tax_id = t.id \n" +
                                "  and t.nomenclature_id = tg.nomenclature_id and t.deleted = 0 and tg.deleted = 0 \n" +
                                "  and t.nomenclature_id = ?"
                        )
                        .parameters(newArrayList(nomenclatureId)),
                new CollectResultSetHandler<DaUkmTax>() {
                    @Override
                    public boolean needNewInstance(final DaUkmTax currentInstance, final ResultSet resultSet) {
                        return true;
                    }

                    @Override
                    public DaUkmTax newInstance(final ResultSet resultSet) {
                        return new DaUkmTax();
                    }

                    @Override
                    public DaUkmTax applyResultSet(final DaUkmTax currentInstance, final ResultSet resultSet) throws SQLException {
                        return currentInstance
                                .setId(resultSet.getLong(1))
                                .setName(resultSet.getString(2))
                                .setPriority(resultSet.getShort(4))
                                .setRate(resultSet.getString(3))
                                .setGroupId(resultSet.getLong(5));
                    }
                }
        );
    }
}
