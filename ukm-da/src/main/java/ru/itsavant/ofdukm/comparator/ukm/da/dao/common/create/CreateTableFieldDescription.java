package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Builder
@Accessors(chain = true)
@AllArgsConstructor
public class CreateTableFieldDescription {

    private String name;
    private boolean notNull = false;
    private String defaultValue = "null";
    private CreateTableFieldType fieldType;
    private int size = 0;
    private int precision = 0;
}
