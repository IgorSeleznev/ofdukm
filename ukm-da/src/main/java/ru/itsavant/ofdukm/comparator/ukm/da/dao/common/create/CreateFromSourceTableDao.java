package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

import org.springframework.jdbc.core.JdbcTemplate;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class CreateFromSourceTableDao {

    private static final String CREATE_TABLE_COPY_SQL = "" +
            "create table if not extsts %s.%s as select * from %s.%s limit 0";

    private static final String ALTER_TABLE_INSERT_DATE_COLUMN_SQL = "" +
            "alter table %s.%s add column record_insert_date datetime first";

    public void create(final String sourceTablename) {
        final String createSql = String.format(
                CREATE_TABLE_COPY_SQL,
                OFD_DATABASE_NAME,
                sourceTablename,
                UKM_DATABASE_NAME,
                sourceTablename
        );

        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        jdbcTemplate.execute(createSql);

        final String alterSql = String.format(
                ALTER_TABLE_INSERT_DATE_COLUMN_SQL,
                OFD_DATABASE_NAME,
                sourceTablename
        );

        jdbcTemplate.execute(alterSql);
    }
}
