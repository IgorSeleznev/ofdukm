package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ListSqlDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmReceiptCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmReceiptItem;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class UkmShiftReceiptHeaderSelectDao {

    private static final String UKM_HEADER_LIST_SQL =
            "select h.shift_open, h.id, h.cash_id, h.date as header_date, f.date as footer_date, sum(i.price * i.quantity + i.discount), count(*), sum(i.total_quantity) \n" +
                    "from ukmserver.trm_out_receipt_header h, \n" +
                    "     ukmserver.trm_out_receipt_footer f, \n" +
                    "     ukmserver.trm_out_receipt_item i \n" +
                    "where i.cash_id = h.cash_id \n" +
                    "  and i.receipt_header = h.id \n" +
                    "  and f.id = h.id and f.cash_id = h.cash_id \n" +
                    "  and h.shift_open = ? \n" +
                    "  and h.cash_id = ? \n" +
                    "  and h.type in (0, 3) \n" +
                    "group by h.shift_open, h.id, h.cash_id, h.date, f.date";

    private final ListSqlDao<DaUkmReceiptCombined> receiptDao = new ListSqlDao<>();

    public List<DaUkmReceiptCombined> list(final String shiftId, final String cashId) {
        return receiptDao.list(
                new SqlParametrizedQuery()
                        .sql(UKM_HEADER_LIST_SQL)
                        .parameters(
                                newArrayList(shiftId, cashId)
                        ),
                (resultSet, rowNum) -> new DaUkmReceiptCombined()
                        .setAmount(resultSet.getString(6))
                        .setCashId(cashId)
                        .setHeaderDate(resultSet.getString(4))
                        .setFooterDate(resultSet.getString(5))
                        .setReceiptId(resultSet.getLong(2))
        );
    }
}
