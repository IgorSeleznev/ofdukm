package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;

import java.sql.SQLException;
import java.util.List;

import static java.util.stream.IntStream.rangeClosed;

public class InsertListDao<T> implements Stoppable {

    private JdbcTemplate jdbcTemplate;
    private volatile boolean stopped;

    public InsertListDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final InsertListQuery<T> query) {
        final List<T> data = query.data();

        for (final T item : data) {
            if (stopped) {
                break;
            }

            final StringBuilder resultSql = new StringBuilder();

            final List<Object> fields = query.parameterMapper().apply(data.get(0));

            final StringBuilder insertValuesSqlBuilder = new StringBuilder();
            rangeClosed(1, fields.size())
                    .forEach(field -> {
                        if (insertValuesSqlBuilder.length() > 0) {
                            insertValuesSqlBuilder.append(", ");
                        }
                        insertValuesSqlBuilder.append("?");
                    });
            insertValuesSqlBuilder.insert(0, "(").append(")");

            final String insertValuesSql = insertValuesSqlBuilder.toString();

            resultSql.append(query.insertHeadSql()).append(" values ");
            resultSql.append(insertValuesSql);

            System.out.println("will start with sql..." + resultSql.toString());

            try {
                jdbcTemplate.update(resultSql.toString(), ps -> {
                    final List<Object> values = query.parameterMapper().apply(item);
                    final Counter fieldCounter = Counter.of(1);
                    for (Object value : values) {
                        try {
                            ps.setObject(fieldCounter.currentAndNext(), value);
                        } catch (final SQLException sqlException) {
                            throw new RuntimeException(sqlException);
                        }
                    }
                });
            } catch (final Throwable throwable) {
                throwable.printStackTrace();
                throw new RuntimeException(throwable);
            }
        }
        System.out.println("sql completed.");
    }

    @Override
    public void stop() {
        this.stopped = true;
    }

    @Override
    public boolean stopped() {
        return this.stopped;
    }
}
