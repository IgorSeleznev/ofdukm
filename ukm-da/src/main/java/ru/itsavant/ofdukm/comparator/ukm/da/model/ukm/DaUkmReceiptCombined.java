package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Getter
@Setter
@Accessors(chain = true)
public class DaUkmReceiptCombined {

    private long receiptId;
    private String cashId;
    private String headerDate;
    private String footerDate;
    private String amount;
    private List<DaUkmReceiptItem> items = newArrayList();
}
