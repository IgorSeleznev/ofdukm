package ru.itsavant.ofdukm.comparator.ukm.da.configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DataSourceConfiguration {

    private String driverName = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://localhost:3306/ukmserver";
    private String charset;
    private boolean useUnicode = false;
    private String username;
    private String password;
}
