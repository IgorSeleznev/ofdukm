package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.of;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ValueDao<T> {

    public Optional<T> get(final SqlParametrizedQuery query, final RowMapper<T> mapper) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        final List<T> result = jdbcTemplate.query(
                query.sql(),
                parameterStatement -> {
                    final Counter counter = new Counter(1);
                    query.parameters().forEach(
                            value -> {
                                try {
                                    parameterStatement.setObject(counter.currentAndNext(), value);
                                } catch (final SQLException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                    );
                },
                mapper
        );

        if (result.size() == 0) {
            return Optional.empty();
        }

        return of(result.get(0));
    }
}
