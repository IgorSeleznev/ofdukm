package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

public class CreateTableSqlBuilder {

    public String build(final CreateTableQuery query) {

        final StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("CREATE TABLE ");
        if (query.isIfNotExist()) {
            queryBuilder.append("IF NOT EXISTS ");
        }
        if (query.getSchema() != null && !"".equals(query.getSchema())) {
            queryBuilder.append(query.getSchema() + ".");
        }
        queryBuilder
                .append(query.getTablename())
                .append(" (\n");
        final StringBuilder fieldsDescription = new StringBuilder();
        query.getFields().forEach(field -> {
            if (fieldsDescription.length() > 0) {
                fieldsDescription.append(",\n");
            }
            fieldsDescription
                    .append(field.getName() + " ")
                    .append(field.getFieldType());
            if (field.getSize() > 0) {
                fieldsDescription.append("(" + field.getSize());
                if (field.getPrecision() > 0) {
                    fieldsDescription.append(", " + field.getPrecision());
                }
                fieldsDescription.append(")");
            }
            if (field.isNotNull()) {
                fieldsDescription.append(" not null");
            }
            if (field.getDefaultValue() != null && !"".equals(field.getDefaultValue())) {
                fieldsDescription.append(String.format(" default %s", field.getDefaultValue()));
            }
        });

        queryBuilder.append(fieldsDescription.toString());

        queryBuilder.append("\n)\n");
        if (query.getPostFieldsSqlExpression() != null && !"".equals(query.getPostFieldsSqlExpression())) {
            queryBuilder.append(" " + query.getPostFieldsSqlExpression());
        } else {
            queryBuilder.append(" engine = InnoDB");
        }

        return queryBuilder.toString();
    }
}
