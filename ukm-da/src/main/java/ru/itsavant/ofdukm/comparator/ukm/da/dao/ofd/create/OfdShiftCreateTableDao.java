package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTable;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldDescription;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableQuery;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_SHIFT_TABLENAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.INTEGER;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.VARCHAR;

public class OfdShiftCreateTableDao implements CreateTable {

    @Override
    public void create() {
        final CreateTableDao createTableDao = new CreateTableDao();
        createTableDao.create(
                CreateTableQuery.builder()
                        .ifNotExist(true)
                        .schema(OFD_DATABASE_NAME)
                        .tablename(OFD_SHIFT_TABLENAME)
                        .fields(
                                newArrayList(
                                        CreateTableFieldDescription.builder()
                                                .name("fiscal_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("shift_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("open_date").fieldType(VARCHAR).notNull(false).size(32)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("close_date").fieldType(VARCHAR).notNull(false).size(32)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("receipt_count").fieldType(INTEGER).notNull(true).defaultValue("0")
                                                .build()
                                )
                        )
                        .build()
        );
    }
}
