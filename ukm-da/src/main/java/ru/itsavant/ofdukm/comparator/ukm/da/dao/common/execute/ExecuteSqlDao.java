package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.execute;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ExecuteSqlDao {

    public void execute(final SqlParametrizedQuery query) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        if (query.parameters().size() > 0) {
            jdbcTemplate.update(query.sql(),
                    ps -> {
                        for (int i = 0; i < query.parameters().size(); i++) {
                            ps.setObject(i + 1, query.parameters().get(i));
                        }
                    });
        } else {
            jdbcTemplate.execute(query.sql());
        }
    }
}
