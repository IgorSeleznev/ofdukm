package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

import org.springframework.jdbc.core.JdbcTemplate;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class CreateTableCopyDao {

    public void copy(final String sourceTablename, final String copyTablename) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        try {
            jdbcTemplate.execute("begin");
            jdbcTemplate.execute(
                    String.format(
                            "create table if not exists %s engine = InnoDB as select * from %s limit 0",
                            copyTablename,
                            sourceTablename
                    )
            );
            jdbcTemplate.execute("commit");
        } catch (final Throwable throwable) {
            throwable.printStackTrace();
            jdbcTemplate.execute("rollback");
        }
    }
}
