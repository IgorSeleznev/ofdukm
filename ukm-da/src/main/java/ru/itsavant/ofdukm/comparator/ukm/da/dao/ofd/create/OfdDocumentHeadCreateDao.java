package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTable;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldDescription;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableQuery;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.*;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.VARCHAR;

public class OfdDocumentHeadCreateDao implements CreateTable {

    @Override
    public void create() {
        final CreateTableDao createTableDao = new CreateTableDao();
        createTableDao.create(
                CreateTableQuery.builder()
                        .ifNotExist(true)
                        .schema(OFD_DATABASE_NAME)
                        .tablename(OFD_DOCUMENT_HEAD_TABLENAME)
                        .fields(
                                newArrayList(
                                        CreateTableFieldDescription.builder()
                                                .name("factory_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fpd").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fd_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("shift_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("number_in_shift").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),

                                        CreateTableFieldDescription.builder()
                                                .name("date_time").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("accounting_type").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("document_type").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("cash").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("cashier_name").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),

                                        CreateTableFieldDescription.builder()
                                                .name("electronic").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("documentSum").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("taxation_system").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("nds0_sum").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("nds10").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),

                                        CreateTableFieldDescription.builder()
                                                .name("nds18").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("nds20").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("nds_c10").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("nds_c18").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("nds_c20").fieldType(VARCHAR).notNull(false).size(255)
                                                .build()
                                )
                        )
                        .build()
        );
    }
}