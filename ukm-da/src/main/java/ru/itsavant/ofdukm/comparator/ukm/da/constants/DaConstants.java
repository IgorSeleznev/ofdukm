package ru.itsavant.ofdukm.comparator.ukm.da.constants;

public interface DaConstants {

    String OFD_DATABASE_NAME = "ofdukm";
    String UKM_DATABASE_NAME = "ukmserver";

    String OFD_KKT_TABLENAME = "kkt";
    String OFD_FISCAL_STORAGE_TABLENAME = "fn";

    String OFD_SHIFT_TABLENAME = "shift";

    String OFD_DOCUMENT_HEAD_TABLENAME = "document_head";
    String OFD_DOCUMENT_PROPERTY_TABLENAME = "document_property";
    String OFD_DOCUMENT_PRODUCT_PROPERTY_TABLENAME = "document_product_property";

    String OFD_RECEIPT_HEADER_TABLENAME = "receipt_header";
    String OFD_RECEIPT_ITEM_TABLENAME = "receipt_item";
}
