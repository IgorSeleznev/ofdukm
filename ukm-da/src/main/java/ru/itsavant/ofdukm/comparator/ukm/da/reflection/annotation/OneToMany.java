package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public @interface OneToMany {

    String[] thisFields() default {};
    String[] thoseFields() default {};
    boolean useNamesFromTableFields() default false;
}
