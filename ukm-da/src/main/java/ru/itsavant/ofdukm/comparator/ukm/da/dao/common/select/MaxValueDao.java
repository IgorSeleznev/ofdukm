package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select;

import com.google.common.base.Strings;
import org.springframework.jdbc.core.JdbcTemplate;

import static java.lang.String.format;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class MaxValueDao {

    private static final String MAX_VALUE_SQL = "select ifnull(max(%s), 0) as max_value from %s.%s";

    private final String tablename;
    private final String fieldname;
    private final String condition;

    public MaxValueDao(final String tablename, final String fieldname) {
        this(tablename, fieldname, null);
    }

    public MaxValueDao(final String tablename, final String fieldname, final String condition) {
        this.tablename = tablename;
        this.fieldname = fieldname;
        this.condition = condition;
    }

    public long max() {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        final StringBuilder sql = new StringBuilder(format(MAX_VALUE_SQL, fieldname, UKM_DATABASE_NAME, tablename));

        if (!Strings.isNullOrEmpty(condition)) {
            sql.append(" where ").append(condition);
        }

        return jdbcTemplate.query(
                sql.toString(),
                resultSet -> {
                    if (resultSet.next()) {
                        return resultSet.getLong(1);
                    }
                    throw new RuntimeException("Max ID not found for query " + sql.toString());
                }
        );
    }
}
