package ru.itsavant.ofdukm.comparator.ukm.da.context;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.configuration.DataSourceConfiguration;
import ru.itsavant.ofdukm.comparator.ukm.da.configuration.DocumentPropertiesUkmMapping;
import ru.itsavant.ofdukm.comparator.ukm.da.environment.DatabaseEnvironment;

import javax.sql.DataSource;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class UkmDatasourceContext {

    private static final UkmDatasourceContext CONTEXT = new UkmDatasourceContext();

    private UkmDatasourceContext() {

    }

    public static UkmDatasourceContext daContext() {
        return CONTEXT;
    }

    public DataSource newDataSource(final DataSourceConfiguration dataSourceConfiguration) {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setPoolName("ukmConnectionPool");
        hikariConfig.setConnectionTestQuery("select 1");
        hikariConfig.setDriverClassName(dataSourceConfiguration.getDriverName());
        hikariConfig.setJdbcUrl(dataSourceConfiguration.getUrl());
        hikariConfig.setUsername(dataSourceConfiguration.getUsername());
        hikariConfig.setPassword(dataSourceConfiguration.getPassword());

        if (dataSourceConfiguration.getCharset() != null) {
            hikariConfig.addDataSourceProperty("characterEncoding", dataSourceConfiguration.getCharset());
            hikariConfig.addDataSourceProperty("useUnicode", String.valueOf(dataSourceConfiguration.isUseUnicode()));
        }

        return new HikariDataSource(hikariConfig);
    }

    public JdbcTemplate newJdbcTemplate(final DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    public void initConnection(final DataSourceConfiguration configuration) {
        dataSource = newDataSource(configuration);
        jdbcTemplate = newJdbcTemplate(dataSource);
    }

    private JdbcTemplate jdbcTemplate;
    private DataSource dataSource;
    private DataSourceConfiguration dataSourceConfiguration;
    private DatabaseEnvironment databaseEnvironment;
    private DocumentPropertiesUkmMapping documentPropertiesMapping;
}
