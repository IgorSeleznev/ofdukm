package ru.itsavant.ofdukm.comparator.ukm.da.model.ofd.kkt;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktFn;

@Getter
@Setter
@Accessors(chain = true)
public class KktFnRecord {

    private String kktRegNumber;
    private String kktFactoryNumber;
    private KktFn fn;
}
