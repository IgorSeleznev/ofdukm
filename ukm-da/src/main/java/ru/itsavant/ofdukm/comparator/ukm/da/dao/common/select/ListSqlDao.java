package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ListSqlDao<T> {

    public List<T> list(final SqlParametrizedQuery query, final RowMapper<T> mapper) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        return jdbcTemplate.query(
                query.sql(),
                parameterStatement -> {
                    final Counter counter = new Counter(1);
                    query.parameters().forEach(
                            value -> {
                                try {
                                    parameterStatement.setObject(counter.currentAndNext(), value);
                                } catch (final SQLException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                    );
                },
                mapper
        );
    }
}
