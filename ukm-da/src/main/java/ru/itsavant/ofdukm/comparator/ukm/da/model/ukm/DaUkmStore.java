package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DaUkmStore {

    private String storeId;
    private String name;
    private String enterpriseName;
    private String address;
}
