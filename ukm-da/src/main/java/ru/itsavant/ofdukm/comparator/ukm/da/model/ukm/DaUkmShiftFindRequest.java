package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DaUkmShiftFindRequest {

    private String storeId;
    private String cashId;
    private String shiftId;
    private String startDate;
    private String finishDate;
}
