package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProperty;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.BatchInsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.reducing;
import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DOCUMENT_PROPERTY_TABLENAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class OfdDocumentPropertySaveDao {

    public void save(final List<DocumentCombined> documentList) {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final JdbcTemplate template = daContext().jdbcTemplate();

        try {
            final List<DocumentProperty> properties = documentList
                    .stream()
                    .flatMap(
                            documentCombined -> documentCombined.getProperty().stream()
                    )
                    .collect(toList());

            if (properties.size() == 0) {
                return;
            }

            template.execute("begin");

            var dao = new BatchInsertListDao<DocumentProperty>(template);

            final String sql = String.format(
                    "insert into %s.%s (" +
                            "record_date," +
                            "fn, " +
                            "fd, " +
                            "item_name, " +
                            "property_code, " +
                            "property_value" +
                            ")",
                    OFD_DATABASE_NAME,
                    OFD_DOCUMENT_PROPERTY_TABLENAME
            );

            dao.insert(
                    InsertListQuery.<DocumentProperty>builder()
                            .data(properties)
                            .insertHeadSql(sql)
                            .parameterMapper(
                                    property -> {
                                        final List<Object> list = newArrayList();
                                        list.add(formatter.format(new Date()));
                                        list.add(property.fn());
                                        list.add(property.fd());
                                        list.add(property.itemName());
                                        list.add(property.code());
                                        list.add(property.value());

                                        return list;
                                    }
                            )
                            .build()
            );

            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("rollback");
            throwable.printStackTrace();
        }
    }
}
