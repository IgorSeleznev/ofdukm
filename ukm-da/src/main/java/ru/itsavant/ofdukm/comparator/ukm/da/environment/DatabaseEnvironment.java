package ru.itsavant.ofdukm.comparator.ukm.da.environment;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DatabaseEnvironment {

    private String ukmserverDatabase;
    private String ofdDatabase;
}
