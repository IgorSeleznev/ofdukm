package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ValueDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmShift;

import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;

public class UkmShiftSelectDao {

    private static final String SHIFT_LIST_SQL =
            "select so.cash_id, so.id, so.date as open_date,\n" +
                    "       sc.date as close_date, sc.kkm_shift_number,\n" +
                    "       sc.kkm_registration_number, kkt_FN_Number, kkt_fiscalDocNumber\n" +
                    "from %s.trm_out_shift_open so\n" +
                    "       left join %s.trm_out_shift_close sc on so.cash_id = sc.cash_id and so.id = sc.id\n" +
                    "where so.cash_id = ? and so.id = ? \n";

    public Optional<DaUkmShift> shift(final String cashId, final String shiftId) {
        final ValueDao<DaUkmShift> valueDao = new ValueDao<>();
        return valueDao.get(
                new SqlParametrizedQuery()
                        .sql(
                                String.format(
                                        SHIFT_LIST_SQL,
                                        UKM_DATABASE_NAME,
                                        UKM_DATABASE_NAME
                                )
                        )
                        .parameters(
                                newArrayList(cashId, shiftId)
                        ),
                (resultSet, rowNum) -> new DaUkmShift()
                        .setCashId(resultSet.getString(1))
                        .setShiftNumber(resultSet.getString(2))
                        .setOpenDate(resultSet.getString(3))
                        .setCloseDate(resultSet.getString(4))
                        .setKkmShiftNumber(resultSet.getString(5))

                        .setKkmRegNumber(resultSet.getString(6))
                        .setKktFnNumber(resultSet.getString(7))
                        .setKktFiscalDocNumber(resultSet.getString(8))
        );
    }
}
