package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public @interface InsertationBehavior {

    boolean eagerChild() default true;
}
