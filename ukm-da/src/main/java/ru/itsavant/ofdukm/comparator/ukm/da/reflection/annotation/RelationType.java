package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public enum RelationType {

    ONE_TO_ONE,
    ONE_TO_MANY,
    MANY_TO_ONE,
    MANY_TO_MANY;
}
