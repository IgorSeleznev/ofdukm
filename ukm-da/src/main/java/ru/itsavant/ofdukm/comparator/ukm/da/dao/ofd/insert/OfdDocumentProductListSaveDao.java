package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import lombok.val;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentProperty;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.BatchInsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class OfdDocumentProductListSaveDao {

    public void save(final List<DocumentCombined> documentList) {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final JdbcTemplate template = daContext().jdbcTemplate();

        try {
            final List<DocumentProperty> properties = documentList
                    .stream()
                    .flatMap(
                            documentCombined -> documentCombined.getProducts().stream()
                    )
                    .flatMap(
                            product -> product.properties().stream()
                    )
                    .collect(toList());

            if (properties.size() == 0) {
                return;
            }

            template.execute("begin");

            val dao = new BatchInsertListDao<DocumentProperty>(template);
            dao.insert(
                    InsertListQuery.<DocumentProperty>builder()
                            .data(properties)
                            .insertHeadSql(
                                    "insert into ofdukm.document_product_property (" +
                                            "record_date, " +
                                            "fn, " +
                                            "fd, " +
                                            "item_name, " +
                                            "number_in_document, " +

                                            "property_code, " +
                                            "property_value" +
                                            ")"
                            )
                            .parameterMapper(
                                    property -> {
                                        final List<Object> list = newArrayList();
                                        list.add(formatter.format(new Date()));
                                        list.add(property.fn());
                                        list.add(property.fd());
                                        list.add(property.itemName());
                                        list.add(property.numberOfNestedItemInDocument());

                                        list.add(property.code());
                                        list.add(property.value());

                                        return list;
                                    }
                            )
                            .build()
            );

            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("rollback");
            throwable.printStackTrace();
        }
    }
}
