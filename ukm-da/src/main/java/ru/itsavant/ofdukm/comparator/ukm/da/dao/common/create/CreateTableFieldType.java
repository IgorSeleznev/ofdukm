package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

public enum CreateTableFieldType {

    BIGINT("BIGINT"),
    INTEGER("INT"),
    DOUBLE("DOUBLE"),
    DECIMAL("DECIMAL"),
    VARCHAR("VARCHAR"),
    TEXT("TEXT");

    private String sqlExpression;

    CreateTableFieldType(final String sqlExpression) {
        this.sqlExpression = sqlExpression;
    }

    public String sqlExpression() {
        return this.sqlExpression;
    }
}
