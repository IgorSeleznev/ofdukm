package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

public enum DaUkmPaymentType {

    CASH,
    CASHLESS
}
