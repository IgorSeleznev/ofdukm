package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTable;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldDescription;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableQuery;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DOCUMENT_PRODUCT_PROPERTY_TABLENAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.INTEGER;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.VARCHAR;

public class OfdDocumentProductCreateDao implements CreateTable {

    @Override
    public void create() {
        final CreateTableDao createTableDao = new CreateTableDao();
        createTableDao.create(
                CreateTableQuery.builder()
                        .ifNotExist(true)
                        .schema(OFD_DATABASE_NAME)
                        .tablename(OFD_DOCUMENT_PRODUCT_PROPERTY_TABLENAME)
                        .fields(
                                newArrayList(
                                        CreateTableFieldDescription.builder()
                                                .name("record_date").fieldType(VARCHAR).notNull(false).size(64)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fn").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fd").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("item_name").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("number_in_document").fieldType(INTEGER).notNull(false)
                                                .build(),

                                        CreateTableFieldDescription.builder()
                                                .name("property_code").fieldType(VARCHAR).notNull(false).size(1024)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("property_value").fieldType(VARCHAR).notNull(false).size(16384)
                                                .build()
                                )
                        )
                        .build()
        );
    }
}
