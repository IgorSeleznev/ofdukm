package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

import org.springframework.jdbc.core.JdbcTemplate;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class CreateTableDao {

    public void create(final CreateTableQuery query) {
        final String sql = new CreateTableSqlBuilder().build(query);
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        jdbcTemplate.execute(sql);
    }
}
