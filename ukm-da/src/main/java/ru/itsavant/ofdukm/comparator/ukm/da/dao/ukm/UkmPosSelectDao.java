package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.exception.UkmPosNotFoundException;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPos;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmPosSelectDao {

    private static final String UKM_POS_SQL =
            "select p.store_id, p.cash_id, p.name, p.number\n" +
                    "from %s.trm_in_pos p\n" +
                    "where p.deleted = 0\n" +
                    "  and p.store_id = ?\n" +
                    "  and p.cash_id = ?\n" +
                    "limit 1;\n";

    public DaUkmPos pos(final String storeId, final String cashId) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        final List<DaUkmPos> ukmPosList = jdbcTemplate.query(
                String.format(UKM_POS_SQL, UKM_DATABASE_NAME),
                statement -> {
                    statement.setString(1, storeId);
                    statement.setString(2, cashId);
                },
                (resultSet, rowNum) -> new DaUkmPos()
                        .setCashId(resultSet.getString(2))
                        .setStoreId(resultSet.getString(1))
                        .setName((resultSet.getString(3)))
                        .setNumber(String.valueOf(resultSet.getInt(4)))
        );

        if (ukmPosList.size() == 0) {
            throw new UkmPosNotFoundException();
        }

        return ukmPosList.get(0);
    }
}
