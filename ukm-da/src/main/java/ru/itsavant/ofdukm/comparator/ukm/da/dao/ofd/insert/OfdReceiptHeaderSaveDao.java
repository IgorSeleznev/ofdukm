package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.BatchInsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceipt;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_RECEIPT_HEADER_TABLENAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class OfdReceiptHeaderSaveDao {

    public void save(final List<OfdReceipt> receipts) {
        final JdbcTemplate template = daContext().jdbcTemplate();

        try {
            template.execute("begin");

            final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            var dao = new BatchInsertListDao<OfdReceipt>(template);
            dao.insert(
                    InsertListQuery.<OfdReceipt>builder()
                            .data(receipts)
                            .insertHeadSql(
                                    String.format(
                                            "insert into %s.%s (" +
                                                    "store_id, " +
                                                    "cash_id, " +
                                                    "ukm_shift, " +
                                                    "kkt_shift, " +
                                                    "fn, " +

                                                    "fd, " +
                                                    "global_number, " +
                                                    "local_number, " +
                                                    "cash_name, " +
                                                    "cashier_name, " +

                                                    "insert_record_date, " +
                                                    "receipt_date, " +
                                                    "total_amount, " +
                                                    "items_count, " +
                                                    "document_type, " +
                                                    "cashless_amount " +
                                                    ")",
                                            OFD_DATABASE_NAME,
                                            OFD_RECEIPT_HEADER_TABLENAME
                                    )
                            )
                            .parameterMapper(
                                    receipt -> {
                                        final List<Object> list = newArrayList();
                                        list.add(receipt.getStoreId());
                                        list.add(receipt.getCashId());
                                        list.add(receipt.getUkmShiftNumber());
                                        list.add(receipt.getKktShiftNumber());
                                        list.add(receipt.getFn());
                                        list.add(receipt.getFd());

                                        list.add(0);

                                        list.add(receipt.getNumberInShift());
                                        list.add(receipt.getCashName());
                                        list.add(receipt.getCashier());

                                        list.add(formatter.format(new Date()));

                                        list.add(receipt.getReceiptDate());
                                        list.add(BigDecimal.valueOf(Long.valueOf(receipt.getAmount())).divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_EVEN));

                                        list.add(receipt.getProductCount());
                                        list.add(receipt.getType());

                                        list.add(
                                                BigDecimal.valueOf(Long.valueOf(receipt.getCashlessAmount())).divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_EVEN)
                                        );

                                        return list;
                                    }
                            )
                            .build()
            );

            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("rollback");
            throwable.printStackTrace();
        }
    }
}
