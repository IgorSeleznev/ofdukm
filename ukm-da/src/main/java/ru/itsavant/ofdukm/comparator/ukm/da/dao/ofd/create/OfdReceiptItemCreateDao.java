package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTable;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldDescription;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableQuery;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.*;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.*;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.VARCHAR;

public class OfdReceiptItemCreateDao implements CreateTable {

    @Override
    public void create() {
        final CreateTableDao createTableDao = new CreateTableDao();
        createTableDao.create(
                CreateTableQuery.builder()
                        .ifNotExist(true)
                        .schema(OFD_DATABASE_NAME)
                        .tablename(OFD_RECEIPT_ITEM_TABLENAME)
                        .fields(
                                newArrayList(
//                                        "store_id, " +
                                        CreateTableFieldDescription.builder()
                                                .name("store_id").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "cash_id, " +
                                        CreateTableFieldDescription.builder()
                                                .name("cash_id").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "insert_record_date, " +
                                        CreateTableFieldDescription.builder()
                                                .name("insert_record_date").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "fn, " +
                                        CreateTableFieldDescription.builder()
                                                .name("fn").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "fd, " +
                                        CreateTableFieldDescription.builder()
                                                .name("fd").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//
//                                                "number_in_document, " +
                                        CreateTableFieldDescription.builder()
                                                .name("number_in_document").fieldType(INTEGER).size(11)
                                                .build(),
//                                                "item_name, " +
                                        CreateTableFieldDescription.builder()
                                                .name("item_name").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
//                                                "price, " +
                                        CreateTableFieldDescription.builder()
                                                .name("price").fieldType(DECIMAL).size(20).precision(4)
                                                .build(),
//                                                "quantity, " +
                                        CreateTableFieldDescription.builder()
                                                .name("quantity").fieldType(DECIMAL).size(20).precision(4)
                                                .build(),
//                                                "amount" +
                                        CreateTableFieldDescription.builder()
                                                .name("amount").fieldType(DECIMAL).size(20).precision(4)
                                                .build()
                                )
                        )
                        .build()
        );
    }
}
