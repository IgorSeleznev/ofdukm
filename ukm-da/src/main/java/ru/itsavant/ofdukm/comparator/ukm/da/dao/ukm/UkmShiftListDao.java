package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import com.google.common.base.Strings;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.exception.UkmShiftNotFound;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmShift;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmShiftFindRequest;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmShiftListDao {

    private static final String UKM_SHIFT_SELECT_SQL =
            "select so.cash_id, so.id, so.date as open_date,\n" +
                    "       sc.date as close_date, sc.kkm_shift_number,\n" +
                    "       sc.kkm_registration_number, kkt_FN_Number, kkt_fiscalDocNumber\n" +
                    "from %s.trm_out_shift_open so\n" +
                    "       left join %s.trm_out_shift_close sc on so.cash_id = sc.cash_id and so.id = sc.id\n" +
                    "where so.cash_id = ? \n";

    private static final String UKM_SHIFT_SELECT_DATE_CONDITION =
            "  and ? <= sc.date\n" +
                    "  and ? >= so.date\n";

    private static final String UKM_SHIFT_SELECT_SHIFT_CONDITION = "  and so.id = ?\n";

    public List<DaUkmShift> list(final DaUkmShiftFindRequest query) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        final StringBuilder sqlBuilder = new StringBuilder(UKM_SHIFT_SELECT_SQL);
        if (Strings.isNullOrEmpty(query.getShiftId())) {
            sqlBuilder.append(UKM_SHIFT_SELECT_DATE_CONDITION);
        } else {
            sqlBuilder.append(UKM_SHIFT_SELECT_SHIFT_CONDITION);
        }

        final String sql = sqlBuilder.toString();

        final List<DaUkmShift> ukmShifts = jdbcTemplate.query(
                String.format(
                        sql,
                        UKM_DATABASE_NAME,
                        UKM_DATABASE_NAME
                ),
                statement -> {
                    statement.setString(1, query.getCashId());
                    if (Strings.isNullOrEmpty(query.getShiftId())) {
                        statement.setString(2, query.getStartDate());
                        statement.setString(3, query.getFinishDate());
                    } else {
                        statement.setString(2, query.getShiftId());
                    }
                },
                (resultSet, rowNum) -> new DaUkmShift()
                        .setCashId(resultSet.getString(1))
                        .setShiftNumber(resultSet.getString(2))
                        .setOpenDate(resultSet.getString(3))
                        .setCloseDate(resultSet.getString(4))
                        .setKkmShiftNumber(resultSet.getString(5))

                        .setKkmRegNumber(resultSet.getString(6))
                        .setKktFnNumber(resultSet.getString(7))
                        .setKktFiscalDocNumber(resultSet.getString(8))
        );

        if (ukmShifts == null) {
            throw new UkmShiftNotFound();
        }

        return ukmShifts;
    }
}
