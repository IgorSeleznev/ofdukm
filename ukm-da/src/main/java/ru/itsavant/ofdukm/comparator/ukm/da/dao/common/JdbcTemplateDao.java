package ru.itsavant.ofdukm.comparator.ukm.da.dao.common;

import org.springframework.jdbc.core.JdbcTemplate;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public abstract class JdbcTemplateDao {

    protected JdbcTemplate jdbcTemplate() {
        return daContext().jdbcTemplate();
    }
}
