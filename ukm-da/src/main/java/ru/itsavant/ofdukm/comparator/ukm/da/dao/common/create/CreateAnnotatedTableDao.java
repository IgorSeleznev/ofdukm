package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create;

import ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation.OfdTable;

import java.lang.annotation.Annotation;

public class CreateAnnotatedTableDao {

    public void create(final Class<?> klass) {
        final Annotation tableAnotation = klass.getAnnotation(OfdTable.class);
        if (tableAnotation == null) {
            throw new RuntimeException("Class has not annotation for table describe");
        }
    }
}
