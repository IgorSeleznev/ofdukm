package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.BatchInsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ofd.kkt.KktFnRecord;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_FISCAL_STORAGE_TABLENAME;

public class OfdKktFnListSaveDao {

    public void save(final List<KktCombined> kktList) {
        final JdbcTemplate template = UkmDatasourceContext.daContext().jdbcTemplate();
        try {
            template.execute("begin");
            final List<KktFnRecord> storages = new KktFnRecordListConverter().convert(kktList);

            var dao = new BatchInsertListDao<KktFnRecord>(template);
            dao.insert(
                    InsertListQuery.<KktFnRecord>builder()
                            .data(storages)
                            .insertHeadSql(
                                    String.format(
                                            "insert into %s.%s (" +
                                                    "kkt_registration_number, " +
                                                    "kkt_factory_number, " +
                                                    "fn, " +
                                                    "fn_status" +
                                                    ")",
                                            OFD_DATABASE_NAME,
                                            OFD_FISCAL_STORAGE_TABLENAME
                                    )
                            )
                            .parameterMapper(
                                    storage -> {
                                        final List<Object> list = newArrayList();
                                        list.add(storage.getKktRegNumber());
                                        list.add(storage.getKktFactoryNumber());
                                        list.add(storage.getFn().getFn());
                                        list.add(storage.getFn().getStatusFn());

                                        return list;
                                    }
                            )
                            .build()
            );

            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("rollback");
        }
    }
}
