package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DaUkmReceiptItem {

    private long itemId;
    private String itemCode;
    private String itemName;
    private String localNumber;
    private String price;
    private String quantity;
    private String discount;
    private String amount;
    private String total;
}
