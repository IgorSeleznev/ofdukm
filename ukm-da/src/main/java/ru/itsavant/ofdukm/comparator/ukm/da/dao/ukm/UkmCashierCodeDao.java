package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmCashierCodeDao {

    public String cashierCode(final String cashierName, final int storeId) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        final List<String> result = jdbcTemplate.query(
                "select id from trm_in_users where store_id = ? and name = ? and deleted = 0",
                (statement) -> {
                    statement.setInt(1, storeId);
                    statement.setString(2, cashierName);
                },
                (resultSet, index) -> resultSet.getString(1)
        );

        if (result.size() > 0) {
            return result.get(0);
        }

        throw new RuntimeException("Для указанного имени кассира не найдено пользователя в таблице trm_in_users");
    }
}
