package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ofd.kkt.KktFnRecord;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class KktFnRecordListConverter implements Converter<List<KktCombined>, List<KktFnRecord>> {

    @Override
    public List<KktFnRecord> convert(List<KktCombined> source) {
        final List<KktFnRecord> target = newArrayList();

        for (final KktCombined combined : source) {
            combined.getKktFn().forEach(
                    fn -> target.add(
                            new KktFnRecord()
                                    .setKktRegNumber(combined.getInfo().getCashdesk().getKktRegNumber())
                                    .setKktFactoryNumber(combined.getInfo().getCashdesk().getFnFactoryNumber())
                                    .setFn(fn)
                    ));
        };

        return target;
    }
}
