package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert;

import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;

import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import static java.util.stream.IntStream.rangeClosed;

public class BatchInsertListDao<T> {

    private static final int MAX_STEP_SIZE = 1000;

    private JdbcTemplate jdbcTemplate;
    private volatile boolean stopped;

    public BatchInsertListDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final InsertListQuery<T> query) {
        final List<T> data = query.data();

        final Counter totalCounter = new Counter(1);
        final Counter rowInStepCounter = new Counter(1);

        while (!stopped) {
            final int step = data.size() - totalCounter.current() + 1 > MAX_STEP_SIZE ? MAX_STEP_SIZE : data.size() - totalCounter.current() + 1;
            rowInStepCounter.reset();
            final StringBuilder resultSql = new StringBuilder();

            if (step == 0) {
                break;
            }

            final List<Object> fields = query.parameterMapper().apply(data.get(0));

            final StringBuilder insertValuesSqlBuilder = new StringBuilder();
            rangeClosed(1, fields.size())
                    .forEach(field -> {
                        if (insertValuesSqlBuilder.length() > 0) {
                            insertValuesSqlBuilder.append(", ");
                        }
                        insertValuesSqlBuilder.append("?");
                    });
            insertValuesSqlBuilder.insert(0, "(").append(")");
            final String insertValuesSql = insertValuesSqlBuilder.toString();

            rangeClosed(rowInStepCounter.current(), rowInStepCounter.current() + step - 1)
                    .forEach(index -> {
                                if (resultSql.length() > 0) {
                                    resultSql.append(", ");
                                } else {
                                    resultSql.append(query.insertHeadSql()).append(" values ");
                                }
                                resultSql.append(insertValuesSql);
                            }
                    );

            System.out.println("will start with sql..." + resultSql.toString());

            try {
                jdbcTemplate.update(resultSql.toString(), ps -> {
                    final int startIndex = rowInStepCounter.current();
                    for (int index = startIndex; index < startIndex + step; index++) {
                        var item = data.get(index - 1);
                        final List<Object> list = query.parameterMapper().apply(item);
                        final Counter fieldCounter = new Counter(1);
                        list.forEach(field -> {
                            try {
                                if (field == null) {
                                    ps.setNull((rowInStepCounter.current() - 1) * list.size() + fieldCounter.current(), Types.NULL);
                                } else {
                                    ps.setObject((rowInStepCounter.current() - 1) * list.size() + fieldCounter.current(), String.valueOf(field));
                                }
//                                System.out.print((rowInStepCounter.current() - 1) * items.size() + fieldCounter.current());
//                                System.out.println(" = " + String.valueOf(field));
                                fieldCounter.next();
                            } catch (SQLException e) {
                                throw new RuntimeException(e);
                            }
                        });
                        totalCounter.next();
                        rowInStepCounter.next();
                    }
                });
            } catch (final Throwable throwable) {
                throwable.printStackTrace();
                throw new RuntimeException(throwable);
            }
            System.out.println("sql completed.");
        }
    }
}
