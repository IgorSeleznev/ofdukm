package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.create;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTable;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldDescription;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableQuery;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.*;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.INTEGER;
import static ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableFieldType.VARCHAR;

public class OfdKktInfoCreateTableDao implements CreateTable {

    @Override
    public void create() {
        final CreateTableDao createTableDao = new CreateTableDao();
        createTableDao.create(
                CreateTableQuery.builder()
                        .ifNotExist(true)
                        .schema(OFD_DATABASE_NAME)
                        .tablename(OFD_KKT_TABLENAME)
                        .fields(
                                newArrayList(
                                        CreateTableFieldDescription.builder()
                                                .name("outlet_id").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("outlet_code").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("name").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("registration_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("factory_number").fieldType(INTEGER).notNull(true).defaultValue("0")
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("model_name").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fiscal_number").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fiscal_reg_date").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fiscal_duration").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("shift_status").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("cashdesk_state").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("cashdesk_end_date").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fiscal_state").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("fiscal_end_date").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("last_document_state").fieldType(VARCHAR).notNull(false).size(255)
                                                .build(),
                                        CreateTableFieldDescription.builder()
                                                .name("last_document_date").fieldType(VARCHAR).notNull(false).size(255)
                                                .build()
                                )
                        )
                        .build()
        );
    }
}
