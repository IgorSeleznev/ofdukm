package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmNomenclature;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPayment;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPaymentType;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;
import static ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPaymentType.CASH;
import static ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPaymentType.CASHLESS;

public class UkmPaymentListDao {

    private static final String UKM_PAYMENT_LIST_SQL =
            "select p.id, p.name\n" +
                    "from %s.trm_in_payments p\n" +
                    "where p.deleted = 0 and p.store_id = ?";

    public List<DaUkmPayment> list(final String storeId) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        return jdbcTemplate.query(
                String.format(UKM_PAYMENT_LIST_SQL, UKM_DATABASE_NAME),
                statement -> statement.setString(1, storeId),
                (resultSet, rowNum) -> new DaUkmPayment()
                        .setId(resultSet.getLong(1))
                        .setName((resultSet.getString(2)))
                        .setIsCashe(
                                //resultSet.getShort(3) != 0
                                resultSet.getLong(1) == 0//.equals("Наличные")
                                        ? CASH
                                        : CASHLESS
                        )
        );
    }
}
