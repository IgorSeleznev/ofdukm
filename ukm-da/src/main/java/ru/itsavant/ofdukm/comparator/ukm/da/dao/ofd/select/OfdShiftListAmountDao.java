package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.select;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ofd.shift.DaOfdShiftAmount;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class OfdShiftListAmountDao {

    private static final String OFD_SHIFT_LIST_AMOUNT_SQL =
            "select kkt_shift, fn, sum(case when document_type = 'Income' then total_amount when document_type = 'IncomeReturn' then -1 * total_amount else 0 end) \n" +
                    "from %s.receipt_header \n" +
                    "group by kkt_shift, fn;";

    public List<DaOfdShiftAmount> list() {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        return jdbcTemplate.query(
                String.format(OFD_SHIFT_LIST_AMOUNT_SQL, OFD_DATABASE_NAME),
                (resultSet, rowNum) -> new DaOfdShiftAmount()
                        .setShift(resultSet.getString(1))
                        .setFn((resultSet.getString(2)))
                        .setAmount((resultSet.getString(3)))
        );
    }
}
