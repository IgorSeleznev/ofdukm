package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DaUkmPos {

    private String storeId;
    private String cashId;
    private String name;
    private String number;
    private String registrationNumber;
    private String factoryNumber;
}
