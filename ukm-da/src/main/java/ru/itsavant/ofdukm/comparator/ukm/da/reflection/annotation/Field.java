package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

import org.springframework.core.annotation.AliasFor;

public @interface Field {

    @AliasFor("name")
    String value();

    @AliasFor("name")
    String name();
}
