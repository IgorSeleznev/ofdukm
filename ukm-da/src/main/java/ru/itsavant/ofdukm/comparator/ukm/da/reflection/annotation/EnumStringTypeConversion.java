package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface EnumStringTypeConversion {

    String enumValue();
    String fieldValue();
}
