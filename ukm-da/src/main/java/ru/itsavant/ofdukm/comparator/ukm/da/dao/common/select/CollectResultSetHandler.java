package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface CollectResultSetHandler<T> {

    boolean needNewInstance(final T currentInstance, final ResultSet resultSet) throws SQLException;

    T newInstance(final ResultSet resultSet) throws SQLException;

    T applyResultSet(final T currentInstance, final ResultSet resultSet) throws SQLException;
}
