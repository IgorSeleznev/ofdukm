package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ValueDao;

import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;

public class UkmPosRegistrationNumberDao {

    private static final String SHIFTS_LIST_FOR_REGISTRATION_NUMBER_SQL = "select *\n" +
            "from (\n" +
            "       select 1 as order_number, id, date, kkm_registration_number, kkm_model_name, kkm_serial_number\n" +
            "       from trm_out_shift_close sc\n" +
            "       where cash_id = ?\n" +
            "         and date > ?\n" +
            "       order by date\n" +
            "       limit 1\n" +
            "     ) later\n" +
            "union all\n" +
            "select *\n" +
            "from (\n" +
            "       select 0 as order_number, id, date, kkm_registration_number, kkm_model_name, kkm_serial_number\n" +
            "       from trm_out_shift_close sc\n" +
            "       where cash_id = ?\n" +
            "         and date < ?\n" +
            "       order by date desc\n" +
            "       limit 1\n" +
            "     ) early\n" +
            "\n" +
            "order by order_number desc\n" +
            "limit 1;";

    private final ValueDao<String> valueDao = new ValueDao<>();

    public Optional<String> registrationNumber(final String cashId, final String startDate, final String finishDate) {
        return valueDao.get(
                new SqlParametrizedQuery()
                        .sql(SHIFTS_LIST_FOR_REGISTRATION_NUMBER_SQL)
                        .parameters(
                                newArrayList(
                                        cashId,
                                        startDate,
                                        cashId,
                                        finishDate
                                )
                        ),
                (resultSet, rowNum) -> resultSet.getString(4)
        );
    }
}
