package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPos;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmPricelist;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmPricelistListDao {

    private static final String UKM_PRICELIST_LIST_SQL =
            "select p.nomenclature_id, p.pricelist_id, p.name\n" +
                    "from %s.trm_in_pricelist p\n" +
                    "where p.deleted = 0\n" +
                    "  and p.nomenclature_id = ? \n" +
                    "  and active = 1 and ifnull(date_from, now()) <= now() and ifnull(date_to, now()) >= now();\n";

    public List<DaUkmPricelist> list(final long nomenclatureId) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        return jdbcTemplate.query(
                String.format(UKM_PRICELIST_LIST_SQL, UKM_DATABASE_NAME),
                statement -> statement.setLong(1, nomenclatureId),
                (resultSet, rowNum) -> new DaUkmPricelist()
                        .setNomenclatureId(resultSet.getLong(1))
                        .setPricelistId(resultSet.getLong(2))
                        .setName((resultSet.getString(3)))
        );
    }
}
