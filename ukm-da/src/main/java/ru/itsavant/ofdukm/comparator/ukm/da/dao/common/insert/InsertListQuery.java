package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.function.Function;

@Getter
@Setter
@Builder
@AllArgsConstructor
@Accessors(chain = true, fluent = true)
public class InsertListQuery<T> {

//    private String tablename;
//    private String schema;
    private String insertHeadSql;
    private Function<T, List<Object>> parameterMapper;
    private List<T> data;
}
