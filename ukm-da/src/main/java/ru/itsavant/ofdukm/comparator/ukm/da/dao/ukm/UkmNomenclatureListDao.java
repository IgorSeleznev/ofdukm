package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmNomenclature;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmNomenclatureListDao {

    private static final String UKM_NOMENCLATURE_LIST_SQL =
            "select n.nomenclature_id, n.name\n" +
                    "from %s.trm_in_nomenclature n\n" +
                    "where n.deleted = 0";

    public List<DaUkmNomenclature> list() {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        return jdbcTemplate.query(
                String.format(UKM_NOMENCLATURE_LIST_SQL, UKM_DATABASE_NAME),
                (resultSet, rowNum) -> new DaUkmNomenclature()
                        .setNomenclatureId(resultSet.getLong(1))
                        .setName((resultSet.getString(2)))
        );
    }
}
