package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class SqlParametrizedQuery {

    private String sql;
    private List<Object> parameters;
}
