package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class CollectSqlDao<T> {

    public List<T> collect(final SqlParametrizedQuery query, final CollectResultSetHandler<T> handler) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        return jdbcTemplate.query(
                query.sql(),
                (statement) -> {
                    for (int i = 1; i <= query.parameters().size(); i++) {
                    final Object value = query.parameters().get(i - 1);
                        statement.setObject(i, value);
                    }
                },
                new ResultSetExtractor<List<T>>() {
                    private final List<T> resultList = newArrayList();

                    @Override
                    public List<T> extractData(final ResultSet resultSet) throws SQLException, DataAccessException {
                        while (resultSet.next()) {
                            if (resultList.size() == 0
                                    || handler.needNewInstance(resultList.get(resultList.size() - 1), resultSet)) {
                                resultList.add(handler.newInstance(resultSet));
                            }
                            handler.applyResultSet(resultList.get(resultList.size() - 1), resultSet);
                        }

                        return resultList;
                    }
                }
        );
    }
}
