package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert;

import org.springframework.jdbc.core.JdbcTemplate;

import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class InsertTableCopyDao {

    public void copy(final String sourceTablename, final String targetTablename) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        try {
            jdbcTemplate.execute("begin");
            jdbcTemplate.execute(String.format("insert into %s select now(), t.* from %s t", sourceTablename, targetTablename));
            jdbcTemplate.execute("commit");
        } catch (final Throwable throwable) {
            jdbcTemplate.execute("rollback");
        }
    }
}
