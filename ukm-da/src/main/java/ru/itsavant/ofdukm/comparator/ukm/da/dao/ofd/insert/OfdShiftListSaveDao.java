package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;


import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfo;
import ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.BatchInsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_SHIFT_TABLENAME;

public class OfdShiftListSaveDao {

    public void save(final List<ShiftInfo> shiftList) {
        final JdbcTemplate template = UkmDatasourceContext.daContext().jdbcTemplate();
        try {
            template.execute("begin");
            var dao = new BatchInsertListDao<ShiftInfo>(template);
            dao.insert(
                    InsertListQuery.<ShiftInfo>builder()
                            .data(shiftList)
                            .insertHeadSql(
                                    String.format(
                                            "insert into %s.%s (\n" +
                                                    "    fiscal_number, \n" +
                                                    "    shift_number, \n" +
                                                    "    cashier, \n" +
                                                    "    open_date, \n" +
                                                    "    close_date, \n" +
                                                    "    open_fd, \n" +
                                                    "    close_fd\n" +
                                                    ")",
                                            OFD_DATABASE_NAME,
                                            OFD_SHIFT_TABLENAME
                                    )
                            )
                            .parameterMapper(
                                    shift -> {
                                        final List<Object> list = newArrayList();
                                        list.add(shift.getFnFactoryNumber());
                                        list.add(shift.getShiftNumber());
                                        list.add(shift.getCashier());
                                        list.add(shift.getOpenDateTime());
                                        list.add(shift.getCloseDateTime());
                                        list.add(shift.getOpenFdNumber());
                                        list.add(shift.getCloseFdNumber());

                                        return list;
                                    }
                            )
                            .build()
            );
            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("rollback");
        }
    }
}
