package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public @interface RelationTable {

    Class<?> type() default Void.class;
    String name() default "";
    RelationFields[] thisRelation() default {};
    RelationFields[] thoseRelation() default {};
}
