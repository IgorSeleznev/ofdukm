package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class DaOfdReceipt {

    private String storeId;
    private String cashId;
    private String fn;
    private String fd;
    private String kktShiftNumber;

    private String ukmShiftNumber;
    private String cashier;
    private String amount;
    private String nds0Sum;
    private String nds10;

    private String nds18;
    private String nds20;
    private String ndsC10;
    private String ndsC18;

    private String ndsC20;
    private String type;
    private List<DaOfdReceiptItem> items;
}
