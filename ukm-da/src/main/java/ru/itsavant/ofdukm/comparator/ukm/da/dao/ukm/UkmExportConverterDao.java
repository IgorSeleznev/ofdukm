package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class UkmExportConverterDao {

    private static final String UKM_EXPORT_CONVERTER_SQL =
            "select c.cnv_pr_xml\n" +
                    "from %s.srv_converters c\n" +
                    "where c.deleted = 0\n" +
                    "  and c.cnv_code = ?;\n";

    private static final String UKM_CONVERTER_CODE = "ExpRptXml";

    public String converterConfiguration() {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();
        final List<String> result = jdbcTemplate.query(
                String.format(UKM_EXPORT_CONVERTER_SQL, UKM_DATABASE_NAME),
                statement -> statement.setString(1, UKM_CONVERTER_CODE),
                (resultSet, rowNum) -> resultSet.getString(1)
        );
        if (result.size() == 0) {
            throw new RuntimeException("Export converter hasn`t found in ukmserver database in table srv_converters");
        }
        return result.get(0);
    }
}
