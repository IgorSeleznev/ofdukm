package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public enum EnumConvertType {

    NUMBER,
    STRING
}
