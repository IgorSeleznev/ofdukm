package ru.itsavant.ofdukm.comparator.ukm.da.dao.ukm;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ListSqlDao;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmReceiptCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.model.ukm.DaUkmReceiptItem;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class UkmShiftReceiptItemSelectDao {

    private static final String UKM_ITEM_LIST_SQL =
            "select i.id, i.item, i.name, i.price, i.quantity, i.discount, (i.price * i.quantity + i.discount) as amount, i.total, i.total_quantity \n" +
                    "from ukmserver.trm_out_receipt_item i \n" +
                    "where i.cash_id = ? and i.receipt_header = ? \n" +
                    "  order by i.id\n";

    private final ListSqlDao<DaUkmReceiptItem> itemDao = new ListSqlDao<>();

    public List<DaUkmReceiptItem> items(final DaUkmReceiptCombined receipt) {
        return itemDao.list(
                new SqlParametrizedQuery()
                        .sql(UKM_ITEM_LIST_SQL)
                        .parameters(
                                newArrayList(
                                        receipt.getCashId(),
                                        receipt.getReceiptId()
                                )
                        ),
                (resultSet, rowNum) -> new DaUkmReceiptItem()
                        .setItemId(resultSet.getLong(1))
                        .setItemCode(resultSet.getString(2))
                        .setItemName(resultSet.getString(3))
                        .setPrice(resultSet.getString(4))
                        .setQuantity(resultSet.getString(5))
                        .setDiscount(resultSet.getString(6))
                        .setAmount(resultSet.getString(7))
                        .setTotal(resultSet.getString(8))
        );
    }
}
