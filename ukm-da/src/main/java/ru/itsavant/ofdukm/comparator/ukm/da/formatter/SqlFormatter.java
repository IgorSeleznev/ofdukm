package ru.itsavant.ofdukm.comparator.ukm.da.formatter;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;

public class SqlFormatter {

    public static String format(final String sql) {
        return ukm(ofd(sql));
    }

    public static String ukm(final String sql) {
        return sql.replaceAll("${ukm}", UKM_DATABASE_NAME);
    }

    public static String ofd(final String sql) {
        return sql.replaceAll("${ofd}", OFD_DATABASE_NAME);
    }
}
