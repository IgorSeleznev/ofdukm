package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DaUkmNomenclature {

    private long nomenclatureId;
    private String name;
}
