package ru.itsavant.ofdukm.comparator.ukm.da.model.ukm;

import lombok.Getter;

@Getter
public class DaSingleResult<T> {

    private final T value;

    public static <T> DaSingleResult<T> of(final T value) {
        return new DaSingleResult<>(value);
    }

    private DaSingleResult(final T value) {
        this.value = value;
    }
}
