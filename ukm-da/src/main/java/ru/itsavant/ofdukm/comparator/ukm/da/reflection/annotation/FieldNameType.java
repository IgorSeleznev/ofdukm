package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

public enum FieldNameType {

    CLASS_PROPERTY_NAME,
    TABLE_FIELD_NAME
}
