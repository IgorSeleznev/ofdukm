package ru.itsavant.ofdukm.comparator.ukm.da.dao.common.truncate;

import org.springframework.jdbc.core.JdbcTemplate;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class TruncateDao {

    public void truncate(final String tablename) {

        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        try {
            jdbcTemplate.execute("begin");
            jdbcTemplate.execute(String.format("truncate table %s", tablename));
            jdbcTemplate.execute("commit");
        } catch (final Throwable throwable) {
            jdbcTemplate.execute("rollback");
        }

    }
}
