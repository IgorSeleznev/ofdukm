package ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation;

import org.springframework.core.annotation.AliasFor;

import static ru.itsavant.ofdukm.comparator.ukm.da.reflection.annotation.RelationType.ONE_TO_MANY;

public @interface Relation {

    @AliasFor("toTable")
    Class<?> value() default Void.class;

    RelationType type() default ONE_TO_MANY;

    RelationTable relationTable() default @RelationTable();

    @AliasFor("parameters")
    Class<?> toTable() default Void.class;

    Class<?> byTable() default Void.class;

    String byTableName() default "";

    String toTableName() default "";

    String[] fromClassField() default "";

    String[] toClassField() default "";

    String[] fromTableField() default "";

    String[] toTableField() default "";
}
