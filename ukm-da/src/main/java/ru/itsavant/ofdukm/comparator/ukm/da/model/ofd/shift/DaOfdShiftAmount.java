package ru.itsavant.ofdukm.comparator.ukm.da.model.ofd.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DaOfdShiftAmount {

    private String shift;
    private String fn;
    private String amount;
}
