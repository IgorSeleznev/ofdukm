package ru.itsavant.ofdukm.comparator.ukm.da.dao.ofd.insert;

import lombok.var;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.BatchInsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DOCUMENT_HEAD_TABLENAME;

public class OfdDocumentHeadSaveDao {

    public void save(final List<DocumentCombined> documentList) {
        final JdbcTemplate template = UkmDatasourceContext.daContext().jdbcTemplate();
        try {
            template.execute("begin");
            var dao = new BatchInsertListDao<DocumentCombined>(template);
            dao.insert(
                    InsertListQuery.<DocumentCombined>builder()
                            .data(documentList)
                            .insertHeadSql(
                                    String.format(
                                            "insert into %s.%s (" +
                                                    "factory_number, " +
                                                    "fpd, " +
                                                    "fd_number, " +
                                                    "shift_number, " +
                                                    "number_in_shift, " +

                                                    "date_time, " +
                                                    "accounting_type, " +
                                                    "document_type, " +
                                                    "cash, " +
                                                    "cashier_name, " +

                                                    "electronic, " +
                                                    "documentSum, " +
                                                    "taxation_system, " +
                                                    "nds0_sum, " +
                                                    "nds10, " +

                                                    "nds18, " +
                                                    "nds20, " +
                                                    "nds_c10, " +
                                                    "nds_c18, " +
                                                    "nds_c20" +
                                                    ")",
                                            OFD_DATABASE_NAME,
                                            OFD_DOCUMENT_HEAD_TABLENAME
                                    )
                            )
                            .parameterMapper(
                                    document -> {
                                        final List<Object> list = newArrayList();

                                        list.add(document.getHead().getFnFactoryNumber());
                                        list.add(document.getHead().getFpd());
                                        list.add(document.getHead().getFdNumber());
                                        list.add(document.getHead().getShiftNumber());
                                        list.add(document.getHead().getNumberInShift());

                                        list.add(document.getHead().getDateTime());
                                        list.add(document.getHead().getAccountingType());
                                        list.add(document.getHead().getDocumentType());
                                        list.add(document.getHead().getCash());
                                        list.add(document.getHead().getCashier());

                                        list.add(document.getHead().getElectronic());
                                        list.add(document.getHead().getSum());
                                        list.add(document.getHead().getTaxationSystem());
                                        list.add(document.getHead().getNds0Sum());
                                        list.add(document.getHead().getNds10());

                                        list.add(document.getHead().getNds18());
                                        list.add(document.getHead().getNds20());
                                        list.add(document.getHead().getNdsC10());
                                        list.add(document.getHead().getNdsC18());
                                        list.add(document.getHead().getNdsC20());

                                        return list;
                                    }
                            )
                            .build()
            );
            template.execute("commit");
        } catch (final Throwable throwable) {
            template.execute("rollback");
            throwable.printStackTrace();
        }
    }
}
