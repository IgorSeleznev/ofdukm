package ru.itsavant.ofdukm.comparator.repair.service;

import ch.qos.logback.classic.Logger;
import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;
import ru.itsavant.ofdukm.comparator.common.logger.LoggerUtil;
import ru.itsavant.ofdukm.comparator.common.stacktrace.StackTracePrinter;
import ru.itsavant.ofdukm.comparator.ofd.model.authorization.AuthorizationRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfo;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ofd.service.service.AuthorizationService;
import ru.itsavant.ofdukm.comparator.ofd.service.service.KktCombinedService;
import ru.itsavant.ofdukm.comparator.ofd.service.service.ShiftInfoService;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.service.service.*;
import ru.itsavant.ofdukm.comparator.xml.export.converter.XmlShiftBuilder;
import ru.itsavant.ofdukm.comparator.xml.export.model.XmlShift;
import ru.itsavant.ofdukm.comparator.xml.export.service.XmlSaveService;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.common.context.CommonContext.commonContext;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class ExportService {

    private final Logger logger;

    private final XmlSaveService xmlSaveService = new XmlSaveService();
    private final ShiftInfoService shiftInfoService = new ShiftInfoService();
    private final UkmPosRegistrationNumberService registrationNumberService = new UkmPosRegistrationNumberService();
    private final UkmCashierCodeService cashierCodeService = new UkmCashierCodeService();
    private final UkmPosService ukmPosService = new UkmPosService();
    private final KktCombinedService kktCombinedService = new KktCombinedService();
    private final UkmValueRowService ukmValueRowService = new UkmValueRowService();

    private final String directoryPath;

    public ExportService(final String directoryPath) {
        this.logger = LoggerUtil.createLoggerFor("ru.itsavant.ofdukm.comparator.repair.service.ExportService", "ofdukm-comparator.log");
        this.directoryPath = directoryPath;
    }

    public void export(final List<ShiftListItem> ofdShifts, final UkmFindRequest findRequest) {

        try {
            commonContext().messageQueue().push(
                    new ProgressMessage()
                            .messageSource(this.getClass())
                            .title("Экспорт чеков ОФД в XML-файл...")
                            .current(0)
                            .size(100)
            );

            ukmValueRowService.sortRows();

            final String token = new AuthorizationService()
                    .authorize(
                            new AuthorizationRequest()
                                    .setLogin(ofdContext().login())
                                    .setPassword(ofdContext().password())
                    );
            ofdContext().sessionToken(token);

            final UkmPos ukmPos = ukmPosService.pos(findRequest.getStoreId(), findRequest.getCashId());

            ukmPos.setRegistrationNumber(
                    registrationNumberService.number(
                            findRequest.getCashId(), findRequest.getStartDate(), findRequest.getFinishDate()
                    )
            );

            final KktCombined kktCombined = kktCombinedService.kkt(ukmPos.getRegistrationNumber());

            ofdShifts.forEach(
                    ofdShift -> {
                        final ShiftInfo shiftInfo = shiftInfoService.shiftInfo(
                                new ShiftInfoRequest()
                                        .setFn(ofdShift.getFnFactoryNumber())
                                        .setShift(String.valueOf(ofdShift.getShiftNumber()))
                                        .setSessionToken(ofdContext().sessionToken())
                        ).getShift();


                        final String cashierCode = cashierCodeService.cashierCode(shiftInfo.getCashier());

                        final List<OfdReceiptStatistic> statistics = new OfdReceiptStatisticService().statistics(String.valueOf(shiftInfo.getShiftNumber()), ukmPos);

                        final XmlShift xmlShift = new XmlShiftBuilder()
                                .cashierCode(cashierCode)
                                .kkt(kktCombined)
                                .shiftInfo(shiftInfo)
                                .ukmPos(ukmPos)
                                .ukmShiftId(ukmContext().exportShiftId())
                                .receipts(statistics)
                                .build();

                        final StringBuilder pathBuilder = new StringBuilder("shift_");
                        pathBuilder.append(String.format("[%s]", ukmContext().storePlaceId()));
                        pathBuilder.append(String.format("_[%s]", xmlShift.getPosNum()));
                        pathBuilder.append(String.format("_[%s]", ukmContext().exportShiftId()));
                        pathBuilder.append(String.format("_[%s]", xmlShift.getPosNum()));

                        commonContext().messageQueue().push(
                                new ProgressMessage()
                                        .messageSource(this.getClass())
                                        .title(String.format("Запись XML-файла %s", pathBuilder.toString()))
                                        .current(100)
                                        .size(100)
                        );

                        xmlSaveService.save(directoryPath + pathBuilder.toString(), xmlShift);

                    }
            );
        } catch (final Throwable throwable) {
            logger.error("Произошла ошибка в процессе поиска расхождений: {}", throwable.getMessage());
            logger.error(StackTracePrinter.print(throwable));
            commonContext().errorsQueue().add(
                    ErrorMessage.builder().message("В процессе экспорта произошла ошибка: " + throwable.getMessage()).title("Ошибка").exception(throwable).build()
            );
            throw throwable;
        }
    }
}
