package ru.itsavant.ofdukm.comparator.repair.service.shift;

import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktInfo;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.outlet.OutletListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.outlet.OutletListRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.ofd.service.service.*;
import ru.itsavant.ofdukm.comparator.repair.service.insert.StatisticService;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;

public class ShiftStatisticService implements StatisticService<ShiftListItem, ShiftStatistic> {

    private final ShiftInfoService infoService = new ShiftInfoService();
    private final KktInfoService kktInfoService = new KktInfoService();
    private final DocumentListService documentListService = new DocumentListService();

    public ShiftStatistic statistic(final ShiftListItem shift) {
        final ShiftStatistic statistic = new ShiftStatistic();

        statistic
                .setShiftHead(shift)
                .setShiftInfo(
                        infoService.shiftInfo(
                                new ShiftInfoRequest()
                                        .setSessionToken(ofdContext().sessionToken())
                                        .setShift(String.valueOf(shift.getShiftNumber()))
                                        .setFn(shift.getFnFactoryNumber())
                        ).getShift()
                );

        final KktInfo kktInfo = kktInfoService.kktInfo(
                new KktInfoRequest()
                        .setSessionToken(ofdContext().sessionToken())
                        .setFn(shift.getFnFactoryNumber())
        );

        statistic.setKkt(kktInfo);

        statistic.setAmount(
                documentListService.documentList(
                        new DocumentListRequest()
                                .setSessionToken(ofdContext().sessionToken())
                                .setShift(String.valueOf(shift.getShiftNumber()))
                                .setFn(statistic.getShiftInfo().getFnFactoryNumber())
                ).getRecords().stream().mapToLong(
                        DocumentListItem::getSum
                ).sum()
        );

        return statistic;
    }
}
