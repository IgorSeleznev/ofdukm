package ru.itsavant.ofdukm.comparator.repair.service.exception;

public class UkmItemNotFoundException extends RuntimeException {

    private final String ukmItemName;

    public UkmItemNotFoundException(final String ukmItemName) {
        super(String.format("Товар с именем %s не найден в справочнике", ukmItemName));
        this.ukmItemName = ukmItemName;
    }

    public String getUkmItemName() {
        return this.ukmItemName;
    }
}
