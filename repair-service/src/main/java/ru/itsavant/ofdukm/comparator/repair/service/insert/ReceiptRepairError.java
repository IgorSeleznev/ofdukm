package ru.itsavant.ofdukm.comparator.repair.service.insert;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class ReceiptRepairError {

    private OfdReceiptStatistic receipt;
    private Throwable throwable;
}
