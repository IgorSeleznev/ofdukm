package ru.itsavant.ofdukm.comparator.repair.service.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class UkmArchivableTable {

    private String tablename;
    private String archiveCondition;
}
