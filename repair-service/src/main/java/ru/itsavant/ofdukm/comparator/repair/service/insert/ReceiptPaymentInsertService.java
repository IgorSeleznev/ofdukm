package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType;

import java.math.BigDecimal;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.common.numeric.Rubbles.rubbles;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;
import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASH;
import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASHLESS;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class ReceiptPaymentInsertService {

    private final static String TRM_OUT_RECEIPT_PAYMENT_INSERT_SQL = "" +
            "insert into ukmserver.trm_out_receipt_payment (\n" +

            "cash_id, \n" +
            "id, \n" +
            "receipt_header, \n" +
            "payment_id, \n" +
            "payment_name, \n" +

            "amount, \n" +
            "amount_with_change, \n" +
            "cookies, \n" +
            "link, \n" +
            "card_number, \n" +

            "auth_code, \n" +
            "ref_number, \n" +
            "is_cash, \n" +
            "kkt_payment_form, \n" +
            "version, \n" +

            "deleted) ";


    public Long insertPayment(
            final OfdReceiptStatistic statistic,
            final String cashId,
            final UkmPaymentType ukmPaymentType
    ) {
        final long paymentId = new MaxValueDao("trm_out_receipt_payment", "id").max() + 1;
        final InsertListDao<OfdReceiptStatistic> dao = new InsertListDao<>(daContext().jdbcTemplate());

        final String amount = CASHLESS.equals(ukmPaymentType)
                ? statistic.getHeader().getCashlessAmount()
                : rubbles(statistic.getHeader().getAmount())
                .subtract(
                        rubbles(statistic.getHeader().getCashlessAmount())
                ).toString();

        dao.insert(
                new InsertListQuery<>(
                        TRM_OUT_RECEIPT_PAYMENT_INSERT_SQL,
                        receipt -> newArrayList(
                                cashId,//"cash_id, \n" +
                                paymentId,//"id, \n" +
                                statistic.getReceiptId(),//"receipt_header, \n" +
                                ukmContext().payments().get(ukmPaymentType),//TODO: обязательно сделать конфигурируемым тип платежа//"payment_id, \n" +
                                "Наличные",//Обязательно подтягивать наименование платежа из настроек//"payment_name, \n" +
                                //

                                amount,//"amount, \n" +
                                amount,//"amount_with_change, \n" +
                                "", //"cookies, \n" +
                                null, //"link, \n" +
                                null, //"card_number, \n" +
                                //
                                null, //"auth_code, \n" +
                                null, //"ref_number, \n" +
                                CASH.equals(ukmPaymentType), //"is_cash, \n" +
                                0, //"kkt_payment_form, \n" +
                                0, //"version, \n" +
                                //
                                0//"deleted) values (\n" +
                        ),
                        ImmutableList.of(statistic)
                )
        );
        return paymentId;
    }
}
