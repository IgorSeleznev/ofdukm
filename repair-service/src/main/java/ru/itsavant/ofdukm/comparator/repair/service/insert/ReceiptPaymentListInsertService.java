package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASH;
import static ru.itsavant.ofdukm.comparator.ukm.model.UkmPaymentType.CASHLESS;

public class ReceiptPaymentListInsertService implements RepairInsertService<OfdReceiptStatistic> {

    private final ReceiptPaymentInsertService paymentInsertService = new ReceiptPaymentInsertService();

    @Override
    public void insert(final OfdReceiptStatistic statistic, final UkmFindRequest findRequest) {
        if (Integer.valueOf(statistic.getHeader().getCashlessAmount()) != 0) {
            statistic.getPaymentIds().add(
                    paymentInsertService.insertPayment(statistic, findRequest.getCashId(), CASHLESS)
            );
        }
        if (!Integer.valueOf(statistic.getHeader().getCashlessAmount()).equals(Integer.valueOf(statistic.getHeader().getAmount()))) {
            statistic.getPaymentIds().add(
                    paymentInsertService.insertPayment(statistic, findRequest.getCashId(), CASH)
            );
        }
    }
}
