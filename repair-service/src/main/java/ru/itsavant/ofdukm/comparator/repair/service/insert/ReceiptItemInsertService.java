package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertValueDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertValueQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmItem;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.common.counter.Counter.basedByZero;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class ReceiptItemInsertService implements RepairInsertService<OfdReceiptStatistic> {

    private final UkmItemService ukmItemService = new UkmItemService();

    @Override
    public void insert(final OfdReceiptStatistic statistic, final UkmFindRequest findRequest) {
        final String sql = "" +
                "insert into ukmserver.trm_out_receipt_item (\n" +

                "cash_id, \n" +
                "id, \n" +
                "receipt_header, \n" +
                "var, \n" +
                "item, \n" +

                "name, \n" +
                "var_quantity, \n" +
                "var_tare, \n" +
                "quantity, \n" +
                "total_quantity, \n" +

                "price, \n" +
                "min_price, \n" +
                "blocked_discount, \n" +
                "total, \n" +
                "discount, \n" +

                "account_discount, \n" +
                "stock_id, \n" +
                "stock_name, \n" +
                "measurement, \n" +
                "measurement_precision, \n" +

                "classif, \n" +
                "type, \n" +
                "weight, \n" +
                "input, \n" +
                "tax, \n" +

                "country, \n" +
                "link_item, \n" +
                "parent_item, \n" +
                "position, \n" +
                "remain, \n" +

                "pricelist, \n" +
                "cancellation_reason_id, \n" +
                "seller_name, \n" +
                "seller_code, \n" +
                "blocked_add, \n" +

                "version, \n" +
                "deleted) ";

        final Counter itemPosition = basedByZero();

        statistic.getItems().forEach(
                receiptItem -> {
                    final long itemId = new MaxValueDao("trm_out_receipt_item", "id").max() + 1;

                    receiptItem.setUkmReceiptItemId(itemId);

                    final UkmItem ukmItem = ukmItemService.ukmItem(receiptItem.getItemName());

                    final InsertValueDao<OfdReceiptItem> dao = new InsertValueDao<>(daContext().jdbcTemplate());
                    dao.insert(
                            InsertValueQuery.<OfdReceiptItem>builder()
                                    .insertHeadSql(sql)
                                    .parameterMapper(
                                            receipt -> newArrayList(
                                                    findRequest.getCashId(),//"cash_id, \n" +
                                                    itemId,//"id, \n" +
                                                    statistic.getReceiptId(),//"receipt_header, \n" +
                                                    "", //"var, \n" +
                                                    ukmItem.getCode(),//"item, \n" +
                                                    //
                                                    receiptItem.getItemName(),//"name, \n" +
                                                    1, //"var_quantity, \n" +
                                                    0,//"var_tare, \n" +
                                                    receiptItem.getQuantity(),//"quantity, \n" +
                                                    receiptItem.getQuantity(),//"total_quantity, \n" +
                                                    //
                                                    receiptItem.getPrice(),//"price, \n" +
                                                    receiptItem.getPrice(),//"min_price, \n" +
                                                    0, //"blocked_discount, \n" +
                                                    receiptItem.getAmount(),//"total, \n" +
                                                    0, //"discount, \n" +
                                                    //
                                                    0, //"account_discount, \n" +
                                                    0, //"stock_id, \n" +
                                                    "", //"stock_name, \n" +
                                                    ukmItem.getMeasurement(),//"measurement, \n" +
                                                    ukmItem.getMeasurementPrecision(),//"measurement_precision, \n" +
                                                    //
                                                    ukmItem.getClassif(),//"classif, \n" +
                                                    0,//"type, \n" +//TODO:убедиться, что тип действительно должен быть 0, есть еще 1, 2 и 3
                                                    ukmItem.getWeight(),//"weight, \n" +
                                                    2,//"input, \n" +//TODO:везде 2, но почему?
                                                    ukmItem.getTaxId(),//"tax, \n" +//
                                                    //
                                                    ukmItem.getCountry(),//"country, \n" +
                                                    null, //"link_item, \n" +
                                                    null,//"parent_item, \n" +
                                                    itemPosition.currentAndNext(),//"position, \n" +
                                                    null,//"remain, \n" +
                                                    //
                                                    ukmContext().pricelist().getPricelistId(), //TODO: прайслист берем только из конфигурации! у каждого товара есть все прайслисты, а в базе два разных прайлиста, для магазаинов и для франчайзи//"pricelist, \n" +
                                                    null, //"cancellation_reason_id, \n" +
                                                    "",//"seller_name, \n" +
                                                    "",//"seller_code, \n" +
                                                    0,//"blocked_add, \n" +
                                                    //
                                                    0,//"version, \n" +
                                                    0//"deleted) values (\n" +
                                            )

                                    ).value(receiptItem)
                                    .build()
                    );
                }
        );
    }
}
