package ru.itsavant.ofdukm.comparator.repair.service.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class UkmPaymentStatistics {

    private String cashId;
    private String shiftId;
    private String paymentId;
    private String paymentName;
    private String amount;
}
