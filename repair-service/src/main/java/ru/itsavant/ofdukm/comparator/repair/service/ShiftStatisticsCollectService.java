package ru.itsavant.ofdukm.comparator.repair.service;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.repair.service.shift.ShiftCompareStatistic;
import ru.itsavant.ofdukm.comparator.ukm.service.service.OfdReceiptStatisticService;

public class ShiftStatisticsCollectService {

    private final UkmShiftCandidateService shiftCandidateService = new UkmShiftCandidateService();
    private final OfdReceiptStatisticService receiptStatisticService = new OfdReceiptStatisticService();

    public ShiftCompareStatistic statistics(final ShiftListItem ofdShift, final UkmShiftsInfo shiftsInfo) {
        final ShiftCompareStatistic closestStatistics = new ShiftCompareStatistic();
        closestStatistics.ofdReceipts(
                receiptStatisticService.statistics(String.valueOf(ofdShift.getShiftNumber()), shiftsInfo.ukmPos())
        );

        return shiftCandidateService.candidate(ofdShift, shiftsInfo, closestStatistics);
    }
}
