package ru.itsavant.ofdukm.comparator.repair.service.shift;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmReceiptCombined;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmShiftReceiptCombinedService;

import java.util.List;

public class ShiftsCompareService {

    private final UkmShiftReceiptCombinedService ukmReceiptService = new UkmShiftReceiptCombinedService();

    public ShiftCompareStatistic compare(final ShiftListItem ofdShift, final UkmShift ukmShift, final List<OfdReceiptStatistic> ofdReceipts) {

        final ShiftCompareStatistic statistic = new ShiftCompareStatistic().ukmShift(ukmShift);

        if (ofdShift.getOpenDateTime().replace("T", " ").compareTo(ukmShift.getCloseDate()) > 0
        || ofdShift.getCloseDateTime().replace("T", " ").compareTo(ukmShift.getOpenDate()) < 0) {
            return statistic;
        }

        statistic.datesCrossed(true);

        final List<UkmReceiptCombined> ukmReceipts = ukmReceiptService.receipts(ukmShift.getShiftNumber(), ukmShift.getCashId());

        return statistic.ukmReceipts(ukmReceipts);
    }
}
