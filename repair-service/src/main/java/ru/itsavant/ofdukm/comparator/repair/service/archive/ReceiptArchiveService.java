package ru.itsavant.ofdukm.comparator.repair.service.archive;

import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

public interface ReceiptArchiveService {

    void archive(final UkmFindRequest ukmFindRequest);
}
