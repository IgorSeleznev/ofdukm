package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertValueDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertValueQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmTax;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmTaxListService;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class ReceiptTaxInsertService implements RepairInsertService<OfdReceiptStatistic> {

    private static final String INSERT_TAX_SQL = "insert into ukmserver.trm_out_receipt_tax ( \n" +
            "cash_id, \n" +
            "id, \n" +
            "taxgroup_id, \n" +
            "tax_id, \n" +
            "name, \n" +
            "percent, \n" +
            "priority, \n" +
            "amount, \n" +
            "version, \n" +
            "deleted) \n" +
            "select h.cash_id, ?, tg.id, t.id, t.name, tg.percent, t.priority, sum(i.price * i.quantity + i.discount) * (? / 100), 0, 0 \n" +
            "from ukmserver.trm_out_receipt_header h,  \n" +
            "     ukmserver.trm_out_receipt_item i,  \n" +
            "     ukmserver.trm_in_items ii,  \n" +
            "     ukmserver.trm_in_taxes t,  \n" +
            "     ukmserver.trm_in_taxgroup tg \n" +
            "where h.cash_id = i.cash_id and h.id = i.receipt_header  \n" +
            "  and i.item = ii.id and ii.tax = t.id and tg.tax_id = t.id and t.id = ? and ii.nomenclature_id = ? \n" +
            "  and ii.nomenclature_id = t.nomenclature_id and t.nomenclature_id = tg.nomenclature_id \n" +
            "  and h.id = ? and h.cash_id = ? and ii.deleted = 0 and tg.deleted = 0 and t.deleted = 0 \n" +
            "group by h.cash_id, tg.id, t.id, t.name, tg.percent, t.priority";

    private static final String INSERT_ITEM_TAX_SQL = "insert into trm_out_receipt_item_tax (cash_id, id, receipt_item, receipt_tax, version, deleted) \n" +
            "select i.cash_id, ?, i.id, ?, 0, 0 \n" +
            "from ukmserver.trm_out_receipt_item i, trm_in_items ii, trm_in_taxes tt \n" +
            "where i.cash_id = ? and i.receipt_header = ? and i.id = ? and ii.deleted = 0 and tt.deleted = 0 \n" +
            "  and i.item = ii.id and tt.id = ii.tax and ii.nomenclature_id = tt.nomenclature_id and ii.nomenclature_id = ?";


    private final UkmTaxListService taxListService = new UkmTaxListService();
    private final MaxValueDao maxValueDao = new MaxValueDao("trm_out_receipt_tax", "id");

    @Override
    public void insert(final OfdReceiptStatistic source, final UkmFindRequest findRequest) {
/*
  cash_id
  id
  taxgroup_id
  tax_id
  name
  percent
  priority tinyint null,
  amount
  version
  deleted

 */

        final List<UkmTax> taxes = taxListService.taxes(ukmContext().nomenclature().getNomenclatureId());
        final InsertValueDao<UkmTax> insertValueDao = new InsertValueDao<>(daContext().jdbcTemplate());
        final InsertValueDao<OfdReceiptItem> itemInsertValueDao = new InsertValueDao<>(daContext().jdbcTemplate());

        taxes.forEach(
                tax -> {
                    //TODO: вставить налог
                    final long taxId = maxValueDao.max() + 1;
                    insertValueDao.insert(
                            InsertValueQuery.<UkmTax>builder()
                                    .insertHeadSql(INSERT_TAX_SQL)
                                    .parameterMapper(
                                            ukmTax -> newArrayList(
                                                    taxId,
                                                    Integer.valueOf(tax.getRate().replace("%", "")),
                                                    ukmTax.getId(),
                                                    ukmContext().nomenclature().getNomenclatureId(),
                                                    source.getReceiptId(),
                                                    findRequest.getCashId()
                                            )
                                    ).value(tax)
                                    .build()
                    );
                    source.getItems().forEach(
                            item -> {
                                final long itemId = new MaxValueDao("trm_out_receipt_item_tax", "id").max() + 1;
                                itemInsertValueDao.insert(
                                        InsertValueQuery.<OfdReceiptItem>builder()
                                                .insertHeadSql(INSERT_ITEM_TAX_SQL)
                                                .parameterMapper(
                                                        receiptItem -> newArrayList(
                                                                itemId,
                                                                taxId,
                                                                findRequest.getCashId(),
                                                                source.getReceiptId(),
                                                                item.getUkmReceiptItemId(),
                                                                ukmContext().nomenclature().getNomenclatureId()
                                                        )
                                                )
                                                .value(item)
                                                .build()
                                );
                            }
                    );
                }
        );
    }
}
