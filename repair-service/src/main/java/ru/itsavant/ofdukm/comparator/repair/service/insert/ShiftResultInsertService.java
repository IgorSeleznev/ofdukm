package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ShiftResultInsertService implements RepairInsertService<ShiftStatistic> {
    @Override
    public void insert(final ShiftStatistic statistic, final UkmFindRequest findRequest) {
/*
  cash_id
  id
  receipts_sale_ukm
  receipts_return_ukm
  receipts_cancel_ukm
  receipts_sale_kkm
  receipts_return_kkm
  receipts_cancel_kkm
  insertion_ukm
  withdrawal_ukm
  insertion_kkm
  withdrawal_kkm
  sale_kkm
  version
  deleted

 */
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        final InsertListDao<ShiftStatistic> insertShiftOpenDao = new InsertListDao<>(jdbcTemplate);

        final long shiftId = new MaxValueDao("trm_out_shift_open", "id").max() + 1;
        final long shiftVersion = new MaxValueDao("trm_out_shift_open", "version").max() + 1;

        insertShiftOpenDao.insert(
                InsertListQuery.<ShiftStatistic>builder().insertHeadSql(
                        "insert into trm_out_shift_open ( \n" +
                                "  cash_id, \n" +
                                "  id,  \n" +
                                "  receipts_sale_ukm,  \n" +
                                "  receipts_return_ukm,  \n" +
                                "  receipts_cancel_ukm,  \n" +
                                "  receipts_sale_kkm,  \n" +
                                "  receipts_return_kkm,  \n" +
                                "  receipts_cancel_kkm,  \n" +
                                "  insertion_ukm,  \n" +
                                "  withdrawal_ukm,  \n" +
                                "  insertion_kkm,  \n" +
                                "  withdrawal_kkm,  \n" +
                                "  sale_kkm,  \n" +
                                "  version,  \n" +
                                "  deleted) "
                ).parameterMapper(
                        shiftStatistic -> newArrayList(
                                findRequest.getCashId(), statistic.getUkmShiftId(), statistic.getReceiptCount(), statistic.getShiftInfo().getOpenDateTime(),
                                statistic.getAmount(), 0, 0, 0, statistic.getAmount(),
                                0, shiftVersion, 0
                        )
                ).data(ImmutableList.of())
                        .build()
        );
    }
}
