package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;

public class LoginFromShiftService {

    private final ValueDao<Integer> valueDao = new ValueDao<>();

    public int login(final UkmFindRequest request) {
        final Optional<Integer> login = valueDao.get(
                new SqlParametrizedQuery()
                        .sql(
                                "select login from trm_out_shift_open where id = ? and cash_id = ?"
                        ).parameters(
                        newArrayList(
                                request.getShiftId(),
                                request.getCashId()
                        )
                ),
                (resultSet, rowNum) -> resultSet.getInt(1)
        );

        if (!login.isPresent()) {
            throw new IllegalStateException("Shift in UKM has not login and cashier is unknown");
        }

        return login.get();
    }
}
