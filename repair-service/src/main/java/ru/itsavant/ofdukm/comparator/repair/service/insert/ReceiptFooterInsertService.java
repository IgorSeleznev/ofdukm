package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ReceiptFooterInsertService implements RepairInsertService<OfdReceiptStatistic> {

    @Override
    public void insert(final OfdReceiptStatistic statistic, final UkmFindRequest findRequest) {
        final String sql = "" +
                "insert into ukmserver.trm_out_receipt_footer (\n" +

                "cash_id, \n" +
                "id, \n" +
                "result, \n" +
                "date, \n" +
                "version, \n" +

                "deleted) ";


        final InsertListDao<OfdReceiptStatistic> dao = new InsertListDao<>(daContext().jdbcTemplate());
        dao.insert(
                new InsertListQuery<>(
                        sql,
                        receipt -> newArrayList(
                                findRequest.getCashId(),//"cash_id, ",
                                receipt.getReceiptId(),//"id, ",
                                0,//"result, ",
                                receipt.getHeader().getReceiptDate(),//"date, ",
                                0,//"version, ",

                                0//"deleted ",
                        ),
                        ImmutableList.of(statistic)
                )
        );
    }
}
