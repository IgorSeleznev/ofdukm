package ru.itsavant.ofdukm.comparator.repair.service.shift;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.repair.service.insert.*;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmShiftFindService;

public class ShiftRepairChainManager {

    private final UkmShiftFindService ukmShiftByIdService = new UkmShiftFindService();
    private final IterableInsertManager<ShiftListItem, ShiftStatistic> shiftChainManager = new IterableInsertManager<>(
            new ShiftStatisticService()
    );

    public UkmShift repair(final ShiftListItem ofdShift, final UkmFindRequest findRequest) {
        shiftChainManager
                .withService(new LoginInsertService())
                .withService(new LogoutInsertService())
                .withService(new ShiftOpenInsertService())
                .withService(new ShiftCloseInsertService())
                .withService(new ShiftResultInsertService());

        shiftChainManager.insert(ofdShift, findRequest);

        return ukmShiftByIdService.shift(findRequest.getCashId(), findRequest.getShiftId());
    }
}
