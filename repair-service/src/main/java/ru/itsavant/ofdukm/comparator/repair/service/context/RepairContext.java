package ru.itsavant.ofdukm.comparator.repair.service.context;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.repair.service.insert.ReceiptRepairError;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class RepairContext {

    private static final RepairContext CONTEXT = new RepairContext();

    private RepairContext() {

    }

    public static RepairContext repairContext() {
        return CONTEXT;
    }

    private List<ReceiptRepairError> repairErrors = newArrayList();
}
