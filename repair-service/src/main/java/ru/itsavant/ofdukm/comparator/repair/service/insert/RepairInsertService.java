package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

public interface RepairInsertService<T> {

    void insert(final T source, final UkmFindRequest findRequest);
}
