package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.repair.service.exception.UkmItemNotFoundException;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmItem;

import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmItemService {

    public UkmItem ukmItem(final String name) {
        final Optional<UkmItem> ukmItem = new ValueDao<UkmItem>().get(
                new SqlParametrizedQuery()
                        .sql(
                                "select i.id, i.name, i.measure, i.tax, tg.percent as tax_percent, i.measprec, i.classif \n" +
                                        "from trm_in_items i, trm_in_taxgroup tg \n" +
                                        "where tg.id = i.tax and i.nomenclature_id = ? and i.name = ? and i.deleted = 0 and tg.deleted = 0 \n" +
                                        "  and tg.nomenclature_id = i.nomenclature_id"
                        )
                        .parameters(
                                newArrayList(
                                        ukmContext().nomenclature().getNomenclatureId(),
                                        name
                                )
                        ),
                (resultSet, rowNum) -> new UkmItem()
                        .setCode(resultSet.getString(1))
                        .setName(resultSet.getString(2))
                        .setMeasurement(resultSet.getString(3))
                        .setTaxId(resultSet.getLong(4))
                        .setTaxRate(resultSet.getString(5))
                        .setMeasurementPrecision(resultSet.getString(6))
                        .setClassif(resultSet.getString(7))
        );

        if (ukmItem.isPresent()) {
            return ukmItem.get();
        }

        throw new UkmItemNotFoundException(name);
    }
}
