package ru.itsavant.ofdukm.comparator.repair.service.archive;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.execute.ExecuteSqlDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.OFD_DATABASE_NAME;
import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.UKM_DATABASE_NAME;

public class ReceiptItemArchiveService implements ReceiptArchiveService {

    private static final String ARCHIVE_RECEIPT_ITEM_SQL =
            "insert into %s.trm_out_receipt_header \n" +
                    "select now(), h. \n*" +
                    "from %s.trm_out_receipt_header h \n" +
                    "inner %s.trm_out_receipt_item i \n" +
                    "   on h.id = i.receipt_header and h.cash_id = i.cash_id \n" +
                    "where h.cash_id = ? \n" +
                    "  and h.type in (0, 3) \n" +
                    "  and h.date between ? and ? \n";


    @Override
    public void archive(final UkmFindRequest ukmFindRequest) {
        final ExecuteSqlDao executeDao = new ExecuteSqlDao();
        executeDao.execute(
                new SqlParametrizedQuery()
                        .sql(
                                String.format(
                                        ARCHIVE_RECEIPT_ITEM_SQL,
                                        OFD_DATABASE_NAME,
                                        UKM_DATABASE_NAME,
                                        UKM_DATABASE_NAME
                                )
                        )
                        .parameters(
                                newArrayList(
                                        ukmFindRequest.getCashId(),
                                        ukmFindRequest.getStartDate(),
                                        ukmFindRequest.getFinishDate()
                                )
                        )
        );
    }
}
