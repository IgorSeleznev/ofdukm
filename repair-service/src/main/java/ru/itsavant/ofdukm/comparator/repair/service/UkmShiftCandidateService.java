package ru.itsavant.ofdukm.comparator.repair.service;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.repair.service.shift.ShiftCompareStatistic;
import ru.itsavant.ofdukm.comparator.repair.service.shift.ShiftRepairChainManager;
import ru.itsavant.ofdukm.comparator.repair.service.shift.ShiftsCompareService;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;

public class UkmShiftCandidateService {

    private final ShiftRepairChainManager shiftRepairService = new ShiftRepairChainManager();
    private final ShiftsCompareService shiftsCompareService = new ShiftsCompareService();

    public ShiftCompareStatistic candidate(final ShiftListItem ofdShift, final UkmShiftsInfo shiftsInfo, final ShiftCompareStatistic statistics) {
        for (final UkmShift ukmShift : shiftsInfo.ukmShifts()) {
            final ShiftCompareStatistic compareStatistic = shiftsCompareService.compare(ofdShift, ukmShift, statistics.ofdReceipts());
            if (compareStatistic.receiptsEqualCount() >= statistics.receiptsEqualCount()
                    && compareStatistic.datesCrossed()) {
                statistics.ukmShift(ukmShift)
                        .datesCrossed(compareStatistic.datesCrossed())
                        .receiptsEqualCount(compareStatistic.receiptsEqualCount());
            }
        }
        if (statistics.ukmShift() == null) {
            final UkmShift repairedShift = shiftRepairService.repair(ofdShift, shiftsInfo.findRequest());
            statistics.ukmShift(repairedShift);
        }

        return statistics;
    }
}
