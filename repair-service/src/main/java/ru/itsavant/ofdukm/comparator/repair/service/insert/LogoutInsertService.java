package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class LogoutInsertService implements RepairInsertService<ShiftStatistic> {

    public void insert(final ShiftStatistic statistic, final UkmFindRequest findRequest) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        final long logoutVersion = new MaxValueDao("trm_out_logout", "version").max() + 1;

        final InsertListDao<ShiftStatistic> insertLogoutDao = new InsertListDao<>(jdbcTemplate);
        insertLogoutDao.insert(
                InsertListQuery.<ShiftStatistic>builder().insertHeadSql(
                        "insert into trm_out_logout (cash_id, id, date, version, deleted) "
                ).parameterMapper(
                        item -> newArrayList(
                                findRequest.getCashId(), statistic.getLoginId(), statistic.getShiftInfo().getCloseDateTime(), logoutVersion, 0
                        )
                ).data(ImmutableList.of(statistic))
                        .build()
        );
    }
}
