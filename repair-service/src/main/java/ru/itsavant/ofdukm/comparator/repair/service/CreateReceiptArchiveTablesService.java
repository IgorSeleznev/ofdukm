package ru.itsavant.ofdukm.comparator.repair.service;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateFromSourceTableDao;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.repair.service.constants.ReceiptTableConstants.RECEIPT_TABLENAMES;

public class CreateReceiptArchiveTablesService {

    private final CreateFromSourceTableDao createDao = new CreateFromSourceTableDao();

    public void createArchive() {
        RECEIPT_TABLENAMES.forEach(createDao::create);
    }
}
