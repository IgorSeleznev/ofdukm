package ru.itsavant.ofdukm.comparator.repair.service;

import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.service.UkmShiftFindRequest;
import ru.itsavant.ofdukm.comparator.ukm.service.UkmShiftListService;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmPosService;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class UkmShiftsInfoGetter {

    private final UkmShiftListService shiftListService = new UkmShiftListService();
    private final UkmPosService ukmPosService = new UkmPosService();

    public UkmShiftsInfo get(final UkmFindRequest findRequest) {
        final UkmPos ukmPos = ukmPosService.pos(findRequest.getStoreId(), findRequest.getCashId());

        //TODO:собрать список смен УКМ
        final List<UkmShift> ukmShifts = shiftListService.list(
                new UkmShiftFindRequest()
                        .setCashId(ukmContext().findRequest().getCashId())
                        .setStartDate(ukmContext().findRequest().getStartDate())
                        .setFinishDate(ukmContext().findRequest().getFinishDate())
                        .setStoreId(ukmContext().findRequest().getStoreId())
                        .setShiftId(null)
        );

        return new UkmShiftsInfo()
                .ukmPos(ukmPos)
                .ukmShifts(ukmShifts);
    }
}
