package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ReceiptHeaderInsertService implements RepairInsertService<OfdReceiptStatistic> {

    private final LoginFromShiftService loginService = new LoginFromShiftService();

    @Override
    public void insert(final OfdReceiptStatistic statistic, final UkmFindRequest findRequest) {
        final String sql = "" +
                "insert into ukmserver.trm_out_receipt_header (\n" +

                "cash_id, \n" +
                "id, \n" +
                "global_number, \n" +
                "local_number, \n" +
                "type, \n" +

                "stock_id, \n" +
                "stock_name, \n" +
                "client, \n" +
                "card, \n" +
                "login, \n" +

                "shift_open, \n" +
                "date, \n" +
                "pos, \n" +
                "pos_name, \n" +
                "sale_type, \n" +

                "customer_address, \n" +
                "version, \n" +
                "deleted)";


        final long receiptId = new MaxValueDao("trm_out_receipt_header", "id").max() + 1;
        final long localNumber = new MaxValueDao(
                "trm_out_receipt_header",
                "id",
                "cash_id = " + findRequest.getCashId() + " and shift_open = " + findRequest.getShiftId()
        ).max() + 1;

        final int login = loginService.login(findRequest);

        final InsertListDao<OfdReceiptStatistic> dao = new InsertListDao<>(daContext().jdbcTemplate());
        dao.insert(
                new InsertListQuery<>(
                        sql,
                        receipt -> newArrayList(
                                findRequest.getCashId(),//"cash_id, ",
                                receiptId,//"id, ",
                                0,//"global_number, ",
                                localNumber,//"local_number, ",
                                0,//"type, ",

                                0,//"stock_id, ",
                                null,//"stock_name, ",
                                null,//"client, ",
                                null,//"card, ",
                                login,// "login, ",

                                findRequest.getShiftId(),//"shift_open, ",
                                receipt.getHeader().getReceiptDate(),//"date, ",
                                receipt.getUkmPos().getNumber(),//TODO: заменить на получение порядкового номера кассы //"pos, ",
                                findRequest.getCashName(),//"pos_name, ",
                                0,//"sale_type, ",

                                null,//"customer_address, ",
                                0,//"version ",
                                0//deleted
                        ),
                        ImmutableList.of(statistic)
                )
        );

        statistic.setReceiptId(receiptId);
        statistic.setLogin(login);
    }
}
