package ru.itsavant.ofdukm.comparator.repair.service.constants;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public interface ReceiptTableConstants {

    List<String> RECEIPT_TABLENAMES = newArrayList(
            "trm_out_receipt_header",
            "trm_out_receipt_item",
            "trm_out_receipt_footer",
            "trm_out_receipt_subtotal",
            "trm_out_receipt_tax",
            "trm_out_receipt_item_tax",
            "trm_out_receipt_payment",
            "trm_out_shift_result",
            "trm_out_shift_result_payment"
    );
}
