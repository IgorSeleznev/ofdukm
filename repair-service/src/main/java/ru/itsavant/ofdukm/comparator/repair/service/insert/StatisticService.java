package ru.itsavant.ofdukm.comparator.repair.service.insert;

public interface StatisticService<TSource, TStatistic> {

    TStatistic statistic(final TSource source);
}
