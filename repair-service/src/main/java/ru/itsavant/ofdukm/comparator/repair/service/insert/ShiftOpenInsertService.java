package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ShiftOpenInsertService implements RepairInsertService<ShiftStatistic> {

    public void insert(final ShiftStatistic statistic, final UkmFindRequest findRequest) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        final InsertListDao<ShiftStatistic> insertShiftOpenDao = new InsertListDao<>(jdbcTemplate);

        final long shiftId = new MaxValueDao("trm_out_shift_open", "id").max() + 1;
        final long shiftVersion = new MaxValueDao("trm_out_shift_open", "version").max() + 1;

        insertShiftOpenDao.insert(
                InsertListQuery.<ShiftStatistic>builder().insertHeadSql(
                        "insert into trm_out_shift_open (cash_id, id, number, login, date, \n" +
                                "sale, sreturn, cancel, cancel_return, sale_fiscal, \n" +
                                "sreturn_fiscal, version, deleted) "
                ).parameterMapper(
                        shiftStatistic -> newArrayList(
                                findRequest.getCashId(), shiftId, shiftId, statistic.getShiftInfo().getOpenDateTime(),
                                statistic.getAmount(), 0, 0, 0, statistic.getAmount(),
                                0, shiftVersion, 0
                        )
                ).data(ImmutableList.of())
                        .build()
        );

        findRequest.setShiftId(String.valueOf(shiftId));
        statistic.setUkmShiftId(shiftId);
    }
}
