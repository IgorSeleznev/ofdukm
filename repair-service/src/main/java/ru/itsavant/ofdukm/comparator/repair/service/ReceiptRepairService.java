package ru.itsavant.ofdukm.comparator.repair.service;

import ru.itsavant.ofdukm.comparator.repair.service.insert.ReceiptRepairChainManager;
import ru.itsavant.ofdukm.comparator.repair.service.shift.ReceiptsCompareSubtractor;
import ru.itsavant.ofdukm.comparator.repair.service.shift.ShiftCompareStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

public class ReceiptRepairService {

    private final ReceiptRepairChainManager receiptRepairService = new ReceiptRepairChainManager();
    private final ReceiptsCompareSubtractor substractor = new ReceiptsCompareSubtractor();

    public void repair(final ShiftCompareStatistic statistics, final UkmFindRequest findRequest) {
        receiptRepairService.repair(
                substractor.compareAndSubstract(statistics.ofdReceipts(), statistics.ukmReceipts()),
                findRequest
        );
    }
}
