package ru.itsavant.ofdukm.comparator.repair.service.shift;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmReceiptCombined;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class ShiftCompareStatistic {

    private int receiptsEqualCount = 0;
    private boolean datesCrossed = false;
    private UkmShift ukmShift = null;
    private List<OfdReceiptStatistic> ofdReceipts = newArrayList();
    private List<UkmReceiptCombined> ukmReceipts = newArrayList();
}
