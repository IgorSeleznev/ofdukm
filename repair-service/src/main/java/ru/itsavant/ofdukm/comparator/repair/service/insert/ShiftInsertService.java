package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

public interface ShiftInsertService {

    void insert(final ShiftStatistic statistic, final UkmFindRequest findRequest);
}
