package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.repair.service.exception.UkmItemNotFoundException;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static ru.itsavant.ofdukm.comparator.repair.service.context.RepairContext.repairContext;

public class ReceiptRepairChainManager {

    private final ShiftResultPaymentInsertService shiftResultPaymentInsertService = new ShiftResultPaymentInsertService();

    private final IterableInsertManager<OfdReceiptStatistic, OfdReceiptStatistic> receiptChainManager =
            new IterableInsertManager<>(
                    statistic -> statistic
            );

    //TODO: header, footer
    public void repair(final List<OfdReceiptStatistic> data, final UkmFindRequest findRequest) {
        receiptChainManager
                .withService(new ReceiptHeaderInsertService())
                .withService(new ReceiptFooterInsertService())
                .withService(new ReceiptPaymentListInsertService())
                .withService(new ReceiptItemInsertService())
                .withService(new ReceiptTaxInsertService());

        final List<ReceiptRepairError> errors = newArrayList();
        final Set<String> notFoundItems = newHashSet();

        for (final OfdReceiptStatistic ofdReceipt : data) {
            try {
                receiptChainManager.insert(ofdReceipt, findRequest);
            } catch (final Throwable throwable) {
                errors.add(new ReceiptRepairError().receipt(ofdReceipt).throwable(throwable));
                if (throwable instanceof UkmItemNotFoundException) {
                    notFoundItems.add(((UkmItemNotFoundException) throwable).getUkmItemName());
                }
            }
        }

        repairContext().repairErrors(errors);

        shiftResultPaymentInsertService.insert(findRequest);
    }
}
