package ru.itsavant.ofdukm.comparator.repair.service;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class UkmShiftsInfo {

    private List<UkmShift> ukmShifts;
    private UkmPos ukmPos;
    private UkmFindRequest findRequest;
}
