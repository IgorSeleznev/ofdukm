package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ShiftCloseInsertService implements RepairInsertService<ShiftStatistic> {

    public void insert(final ShiftStatistic statistic, final UkmFindRequest findRequest) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        final long shiftVersion = new MaxValueDao("trm_out_shift_close", "version").max() + 1;

        final InsertListDao<ShiftStatistic> insertShiftCloseDao = new InsertListDao<>(jdbcTemplate);

        insertShiftCloseDao.insert(
                InsertListQuery.<ShiftStatistic>builder().insertHeadSql(
                        "insert into trm_out_shift_close (" +
                                "  cash_id ,\n" +
                                "  id ,\n" +
                                "  login ,\n" +
                                "  date ,\n" +
                                "  kkm_shift_number ,\n" +
                                "  kkm_serial_number ,\n" +
                                "  kkm_registration_number ,\n" +
                                "  kkm_owner_number ,\n" +
                                "  eklz_number ,\n" +
                                "  eklz_date_activate ,\n" +
                                "  eklz_fast_full ,\n" +
                                "  kkm_model_name ,\n" +
                                "  kkt_lifePhase ,\n" +
                                "  kkt_shiftState ,\n" +
                                "  kkt_NeedChangeFN ,\n" +
                                "  kkt_EndingResourceFN ,\n" +
                                "  kkt_OverflowFN ,\n" +
                                "  kkt_LongWaitOFD ,\n" +
                                "  kkt_FN_Number ,\n" +
                                "  kkt_fiscalDocNumber ,\n" +
                                "  kkt_lifeTime ,\n" +
                                "  kkt_status ,\n" +
                                "  kkt_ffd_version ,\n" +
                                "  kkt_ofdQueueLength ,\n" +
                                "  kkt_firstQueueDocNumber ,\n" +
                                "  kkt_firstQueueDocDateTime ,\n" +
                                "  version ,\n" +
                                "  deleted ) "
                ).parameterMapper(
                        shiftStatistic -> newArrayList(
                                findRequest.getCashId(), statistic.getUkmShiftId(), statistic.getLoginId(), statistic.getShiftInfo().getCloseDateTime(), statistic.getShiftInfo().getShiftNumber(),
                                null, null, null, null, null,
                                null, null, null, statistic.getShiftInfo().getState().toString(), null,
                                null, null, null, statistic.getShiftInfo().getFnFactoryNumber(), statistic.getShiftInfo().getCloseFdNumber(), null,
                                statistic.getKkt().getCashdesk().getCashdeskState(), null, null, null, null,
                                null, shiftVersion, 0
                        )
                ).data(ImmutableList.of())
                        .build()
        );
    }
}
