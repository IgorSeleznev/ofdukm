package ru.itsavant.ofdukm.comparator.repair.service;

import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.List;

public class RepairService {

    private final ShiftStatisticsCollectService statisticsCollectService = new ShiftStatisticsCollectService();
    private final UkmShiftsInfoGetter shiftsInfoGetter = new UkmShiftsInfoGetter();
    private final ReceiptRepairService receiptRepairService = new ReceiptRepairService();

    public void repair(final List<ShiftListItem> ofdShifts, final UkmFindRequest findRequest) {

        final UkmShiftsInfo shiftsInfo = shiftsInfoGetter.get(findRequest);

        //TODO:пройтись по сменам ОФД
        for (final ShiftListItem ofdShift : ofdShifts) {
            //TODO:для каждой смены ОФД вставить в смену УКМ те чеки, которые не были найдены в УКМ
            receiptRepairService.repair(
                    statisticsCollectService.statistics(ofdShift, shiftsInfo),
                    findRequest
            );
        }
    }
}
