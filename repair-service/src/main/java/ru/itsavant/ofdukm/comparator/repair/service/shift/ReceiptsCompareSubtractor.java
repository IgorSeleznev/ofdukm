package ru.itsavant.ofdukm.comparator.repair.service.shift;

import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptItem;
import ru.itsavant.ofdukm.comparator.ukm.model.OfdReceiptStatistic;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmReceiptCombined;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmReceiptItem;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.common.counter.Counter.basedByZero;
import static ru.itsavant.ofdukm.comparator.common.numeric.NumericUtil.roundStringRubbles;
import static ru.itsavant.ofdukm.comparator.common.numeric.NumericUtil.stringRubbles;

public class ReceiptsCompareSubtractor {

    public List<OfdReceiptStatistic> compareAndSubstract(final List<OfdReceiptStatistic> ofdReceipts, final List<UkmReceiptCombined> ukmReceipts) {
        final List<OfdReceiptStatistic> difference = newArrayList();
        ofdReceipts.forEach(
                ofdReceipt -> {
                    boolean ukmReceiptFound = false;
                    for (final UkmReceiptCombined ukmReceipt : ukmReceipts) {
                        if (Objects.equals(
                                roundStringRubbles(ofdReceipt.getHeader().getAmount()),
                                stringRubbles(ukmReceipt.getAmount())
                        ) && ofdReceipt.getItems().size() == ukmReceipt.getItems().size()) {
                            final Counter itemsEquals = basedByZero();
                            final Set<String> checked = newHashSet();
                            for (final OfdReceiptItem item : ofdReceipt.getItems()) {
                                for (final UkmReceiptItem ukmItem : ukmReceipt.getItems()) {
                                    if (item.getItemName().equals(ukmItem.getItemName())
                                            && Objects.equals(roundStringRubbles(item.getPrice()), stringRubbles(ukmItem.getPrice()))
                                            && Objects.equals(Double.valueOf(item.getQuantity()), Double.valueOf(ukmItem.getQuantity()))
                                            && !checked.contains(ukmItem.getLocalNumber())) {
                                        checked.add(ukmItem.getLocalNumber());
                                        itemsEquals.next();

                                        break;
                                    }
                                }
                            }
                            if (itemsEquals.current() == ofdReceipt.getItems().size()) {
                                ukmReceiptFound = true;
                            }
                        }
                    }
                    if (!ukmReceiptFound) {
                        difference.add(ofdReceipt);
                    }
                }
        );

        return difference;
    }
}
