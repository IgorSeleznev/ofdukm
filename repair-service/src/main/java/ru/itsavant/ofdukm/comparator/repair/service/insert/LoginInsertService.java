package ru.itsavant.ofdukm.comparator.repair.service.insert;

import com.google.common.collect.ImmutableList;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftStatistic;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertListQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class LoginInsertService implements RepairInsertService<ShiftStatistic> {

    public void insert(final ShiftStatistic statistic, final UkmFindRequest findRequest) {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        final long loginVersion = new MaxValueDao("trm_out_login", "version").max() + 1;
        final long loginId = new MaxValueDao("trm_out_login", "id").max() + 1;

        final Optional<Long> userId = new ValueDao<Long>().get(
                new SqlParametrizedQuery()
                        .sql("select id from trm_in_users where store_id = ? and deleted = 0 and name = ?")
                        .parameters(
                                newArrayList(
                                        Integer.valueOf(findRequest.getStoreId()),
                                        statistic.getShiftInfo().getCashier()
                                )
                        ),
                (resultSet, rowNum) -> resultSet.getLong(1)
        );

        final InsertListDao<ShiftStatistic> insertLoginDao = new InsertListDao<>(jdbcTemplate);
        insertLoginDao.insert(
                InsertListQuery.<ShiftStatistic>builder().insertHeadSql(
                        "insert into trm_out_login (cash_id, id, user_id, date, user_name, \n" +
                                "version, deleted) "
                ).parameterMapper(
                        item -> newArrayList(
                                findRequest.getCashId(), loginId, userId.get(), statistic.getShiftInfo().getOpenDateTime(), statistic.getShiftInfo().getCashier(),
                                loginVersion, 0
                        )
                ).data(ImmutableList.of(statistic))
                        .build()
        );

        statistic.setLoginId(loginId);
    }
}
