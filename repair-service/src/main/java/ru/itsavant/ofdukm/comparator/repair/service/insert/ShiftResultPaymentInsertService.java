package ru.itsavant.ofdukm.comparator.repair.service.insert;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.repair.service.model.UkmPaymentStatistics;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertValueDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertValueQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.CollectResultSetHandler;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.CollectSqlDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.MaxValueDao;
import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class ShiftResultPaymentInsertService {

    public void insert(final UkmFindRequest findRequest) {
/*
  "cash_id, \n" +
  "id, \n" +
  "payment_id, \n" +
  "payment_name, \n" +
  "is_cash, \n" +
  "is_local, \n" +
  "sale_ukm, \n" +
  "return_ukm, \n" +
  "sale_kkm, \n" +
  "return_kkm, \n" +
  "version, \n" +
  "deleted, \n" +
 */

        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();

        final CollectSqlDao<UkmPaymentStatistics> collectSqlDao = new CollectSqlDao<>();

        final List<UkmPaymentStatistics> paymentStatistics = collectSqlDao.collect(
                new SqlParametrizedQuery()
                        .sql("select p.cash_id, h.shift_open, p.payment_id, \n" +
                                "       sum(p.amount) \n" +
                                "from ukmserver.trm_out_receipt_payment p, ukmserver.trm_out_receipt_header h \n" +
                                "where p.cash_id = h.cash_id and p.receipt_header = h.id and h.shift_open = ? and h.cash_id = ? \n" +
                                "group by p.cash_id, h.shift_open, p.payment_id"
                        )
                        .parameters(
                                newArrayList(
                                        findRequest.getShiftId(),
                                        findRequest.getCashId()
                                )
                        ),
                new CollectResultSetHandler<UkmPaymentStatistics>() {
                    @Override
                    public boolean needNewInstance(final UkmPaymentStatistics currentInstance, final ResultSet resultSet) throws SQLException {
                        return true;
                    }

                    @Override
                    public UkmPaymentStatistics newInstance(final ResultSet resultSet) throws SQLException {
                        return new UkmPaymentStatistics();
                    }

                    @Override
                    public UkmPaymentStatistics applyResultSet(final UkmPaymentStatistics currentInstance, ResultSet resultSet) throws SQLException {
                        return currentInstance
                                .cashId(resultSet.getString(1))
                                .shiftId(resultSet.getString(2))
                                .paymentId(resultSet.getString(3))
                                .amount(resultSet.getString(4));
                    }
                }
        );

//        final long id = new MaxValueDao("trm_out_shift_result_payments", "id").max() + 1;

        final InsertValueDao<UkmPaymentStatistics> insertDao = new InsertValueDao<>(jdbcTemplate);

        jdbcTemplate.execute("delete from ukmserver.trm_out_shift_result_payments where cash_id = " + findRequest.getCashId() + " and id = " + findRequest.getShiftId());

        paymentStatistics.forEach(
                payment -> {
                    insertDao.insert(
                            InsertValueQuery.<UkmPaymentStatistics>builder().build().insertHeadSql(
                                    "insert into ukmserver.trm_out_shift_result_payments ( \n" +
                                            "  cash_id, \n" +
                                            "  id, \n" +
                                            "  payment_id, \n" +
                                            "  payment_name, \n" +
                                            "  is_cash, \n" +

                                            "  is_local, \n" +
                                            "  sale_ukm, \n" +
                                            "  return_ukm, \n" +
                                            "  sale_kkm, \n" +
                                            "  return_kkm, \n" +

                                            "  version, \n" +
                                            "  deleted) \n" +
                                            "select ?, ?, ?, ?, 0, \n" +
                                            "        0, ?, 0, ?, 0, \n" +
                                            "        0, 0 \n" +
                                            "from trm_in_payments \n" +
                                            "where store_id = ? and id = ? and deleted = 0"
                            ).parameterMapper(
                                    paymentInfo -> newArrayList(
                                            paymentInfo.cashId(),
                                            paymentInfo.shiftId(),
                                            paymentInfo.paymentId(),
                                            paymentInfo.paymentName(),
                                            paymentInfo.amount(),
                                            paymentInfo.amount(),
                                            findRequest.getStoreId(),
                                            paymentInfo.paymentId()
                                    )
                            ).value(payment)
                    );
                }
        );
    }
}
