package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class IterableInsertManager<TSource, TStatistic> {

    private final StatisticService<TSource, TStatistic> statisticService;

    private List<RepairInsertService<TStatistic>> services = newArrayList();

    public IterableInsertManager(final StatisticService<TSource, TStatistic> statisticService) {
        this.statisticService = statisticService;
    }

    public IterableInsertManager<TSource, TStatistic> withService(final RepairInsertService<TStatistic> service) {
        services.add(service);
        return this;
    }

    public void insert(final TSource source, final UkmFindRequest findRequest) {
        final TStatistic statistic = statisticService.statistic(source);

        for (final RepairInsertService<TStatistic> service : services) {
            service.insert(statistic, findRequest);
        }
    }
}
