package ru.itsavant.ofdukm.comparator.repair.service.insert;

import ru.itsavant.ofdukm.comparator.ukm.model.environment.UkmFindRequest;

import java.util.List;

public interface ReceiptInsertService<T> {

    void insert(final UkmFindRequest request, final List<T> data);
}
