package ru.itsavant.ofdukm.comparator.pair;

import lombok.Value;
import lombok.experimental.Accessors;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Value
@Accessors(fluent = true)
public class Pair {

    private List<String> pair = newArrayList(
            "9C02ECBE51A933D353D3CAF2A0E5788957F7500B",
            "B0B6B46B84990C61C0E5E8CCA677FFA58B7DF013"
    );
}
