package ru.itsavant.ofdukm.comparator.pair;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ValueRow {

    private final String left;
    private final String right;
}
