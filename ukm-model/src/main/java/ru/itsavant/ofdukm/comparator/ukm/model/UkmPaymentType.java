package ru.itsavant.ofdukm.comparator.ukm.model;

public enum UkmPaymentType {

    CASH,
    CASHLESS
}
