package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class OfdReceiptStatistic {

    private long receiptId;
    private OfdReceipt header;
    private List<OfdReceiptItem> items;
    private UkmPos ukmPos;
    private long shiftId;
    private List<Long> paymentIds;
    private long login;
    private Double totalQuantity;
    private int receiptCount;
}
