package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UkmShiftRequest {

    private String storeId;
    private String cashId;
    private String shiftId;
    private String startDate;
    private String finishDate;
}
