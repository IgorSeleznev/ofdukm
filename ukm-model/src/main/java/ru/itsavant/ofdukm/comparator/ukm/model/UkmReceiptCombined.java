package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Getter
@Setter
@Accessors(chain = true)
public class UkmReceiptCombined {

    private long receiptId;
    private String cashId;
    private String headerDate;
    private String footerDate;
    private String amount;
    private List<UkmReceiptItem> items = newArrayList();
}
