package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UkmReceiptKkmRecord {

    private String storeId;
    private String cashId;
    private String regNumber;
    private String factoryNumber;
    private String name;
}
