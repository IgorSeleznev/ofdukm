package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class OfdReceipt {

    private String storeId;
    private String cashId;
    private String ukmShiftNumber;
    private String kktShiftNumber;
    private String fn;

    private String fd;
    private long numberInShift;
    private String cashName;
    private String cashier;
    private String receiptDate;

    private String amount;
    private String cashlessAmount;
    private int productCount;
    private String nds0Sum;
    private String nds10;

    private String nds18;
    private String nds20;
    private String ndsC10;
    private String ndsC18;
    private String ndsC20;

    private String type;
}
