package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UkmShift {

    private String cashId;
    private String shiftNumber;
    private String openDate;
    private String closeDate;
    private String kkmShiftNumber;

    private String kkmRegNumber;
    private String kktFnNumber;
    private String kktFiscalDocNumber;
}
