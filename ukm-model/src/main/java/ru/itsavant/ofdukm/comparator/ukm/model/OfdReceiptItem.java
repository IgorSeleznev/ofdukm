package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class OfdReceiptItem {

    private String storeId;
    private String cashId;
    private String fn;
    private String fd;
    private int numberInDocument;

    private String itemCode;
    private String itemName;
    private String price;
    private String quantity;
    private String nds0;

    private String nds10;
    private String nds18;
    private String nds20;
    private String amount;
    private Long ukmReceiptItemId;
}
