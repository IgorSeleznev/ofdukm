package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UkmItem {

    private String code;
    private String name;
    private String measurement;
    private String measurementPrecision;
    private String classif;

    private long taxId;
    private String taxRate;
    private String weight;
    private String country;
}
