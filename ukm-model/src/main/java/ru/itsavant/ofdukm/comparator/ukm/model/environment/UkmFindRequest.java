package ru.itsavant.ofdukm.comparator.ukm.model.environment;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UkmFindRequest {

    private String storeId;
    private String cashId;
    private String cashName;
    private String shiftId;
    private String startDate;
    private String finishDate;
}
