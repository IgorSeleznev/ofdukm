package ru.itsavant.ofdukm.comparator.ukm.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UkmReceiptItem {

    private long itemId;
    private String itemCode;
    private String itemName;
    private String localNumber;
    private String price;
    private String quantity;
    private String discount;
    private String amount;
    private String total;
}
