package ru.itsavant.ofdukm.comparator.transport.transport.destination;

import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.transport.TransportData;
import ru.itsavant.ofdukm.comparator.ukm.service.service.DocumentCombinedListSaveService;

public class DocumentCombinedTransportDestination implements TransportDestination<DocumentCombined> {

    private Stoppable manipulator;
    private final DocumentCombinedListSaveService saveService = new DocumentCombinedListSaveService();
    private TransportDestinationProgressEventHandler progressEventHandler;

    @Override
    public void write(final TransportData<DocumentCombined> transportData) {
        if (manipulator.stopped()) {
            return;
        }
        System.out.println("start write documents items in destination...");
        saveService.save(transportData.getData());
    }

    @Override
    public DocumentCombinedTransportDestination stopManipulator(final Stoppable manipulator) {
        this.manipulator = manipulator;
        return this;
    }

    @Override
    public void progressCallback(final ProgressMessage progressMessage) {
        progressEventHandler.handle(progressMessage);
    }

    public TransportDestinationProgressEventHandler getProgressEventHandler() {
        return progressEventHandler;
    }

    public DocumentCombinedTransportDestination setProgressEventHandler(TransportDestinationProgressEventHandler progressEventHandler) {
        this.progressEventHandler = progressEventHandler;
        return this;
    }
}
