package ru.itsavant.ofdukm.comparator.transport;

import lombok.Value;

import java.util.List;

@Value
public class TransportData<T> {

    private List<T> data;

    public static <T> TransportData<T> data(final List<T> data) {
        return new TransportData<>(data);
    }
}
