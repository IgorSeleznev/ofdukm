package ru.itsavant.ofdukm.comparator.transport.transport.destination;

import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;

public interface TransportDestinationProgressEventHandler {

    void handle(final ProgressMessage progressMessage);
}
