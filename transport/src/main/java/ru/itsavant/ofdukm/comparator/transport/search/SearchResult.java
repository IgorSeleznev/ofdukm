package ru.itsavant.ofdukm.comparator.transport.search;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class SearchResult {

    private String ofdShift;
    private String ofdDate;
    private String ofdAmount;
    private String ofdCount;
    private String ofdQuantity;

    private String ukmShift;
    private String ukmDate;
    private String ukmAmount;
    private String ukmCount;
    private String ukmQuantity;
}
