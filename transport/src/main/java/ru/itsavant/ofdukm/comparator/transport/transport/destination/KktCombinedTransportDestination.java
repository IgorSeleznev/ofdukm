package ru.itsavant.ofdukm.comparator.transport.transport.destination;

import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.transport.Manipulated;
import ru.itsavant.ofdukm.comparator.transport.TransportData;
import ru.itsavant.ofdukm.comparator.ukm.service.service.KktCombinedListSaveService;

public class KktCombinedTransportDestination implements TransportDestination<KktCombined> {

    private Stoppable manipulator;
    private final KktCombinedListSaveService saveService = new KktCombinedListSaveService();
    private TransportDestinationProgressEventHandler progressEventHandler;

    @Override
    public void write(final TransportData<KktCombined> transportData) {
        if (manipulator.stopped()) {
            return;
        }
        System.out.println("start write kkt items in destination...");
        saveService.save(transportData.getData());
    }

    @Override
    public Manipulated stopManipulator(final Stoppable manipulator) {
        this.manipulator = manipulator;
        return this;
    }

    @Override
    public void progressCallback(final ProgressMessage progressMessage) {
        progressEventHandler.handle(progressMessage);
    }

    public TransportDestinationProgressEventHandler getProgressEventHandler() {
        return progressEventHandler;
    }

    public KktCombinedTransportDestination setProgressEventHandler(TransportDestinationProgressEventHandler progressEventHandler) {
        this.progressEventHandler = progressEventHandler;
        return this;
    }
}
