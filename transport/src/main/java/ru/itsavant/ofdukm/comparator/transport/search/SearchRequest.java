package ru.itsavant.ofdukm.comparator.transport.search;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Optional;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class SearchRequest {

    private String storeId;
    private String cashId;
    private String shiftId;
    private SearchPeriodValue period;
}
