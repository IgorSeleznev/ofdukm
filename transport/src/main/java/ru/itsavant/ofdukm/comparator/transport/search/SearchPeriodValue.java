package ru.itsavant.ofdukm.comparator.transport.search;

import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)
public class SearchPeriodValue {

    private String startDate;
    private String finishDate;

    private SearchPeriodValue() {

    }

    public static class Builder {

        private String startDate;

        public Builder from(final String startDate) {
            this.startDate = startDate;
            return this;
        }

        public SearchPeriodValue to(final String finishDate) {
            return new SearchPeriodValue()
                    .setStartDate(startDate)
                    .setFinishDate(finishDate);
        }
    }

    private SearchPeriodValue setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    private SearchPeriodValue setFinishDate(String finishDate) {
        this.finishDate = finishDate;
        return this;
    }
}
