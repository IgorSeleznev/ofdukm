package ru.itsavant.ofdukm.comparator.transport.transport.source;

import ru.itsavant.ofdukm.comparator.transport.Manipulated;
import ru.itsavant.ofdukm.comparator.transport.TransportData;

public interface TransportSource<T> extends Manipulated {

    TransportData<T> read();

    boolean hasNext();
}
