package ru.itsavant.ofdukm.comparator.transport.search;

public class SearchParameter {

    private final String value;

    public static SearchParameter of(final String value) {
        return new SearchParameter(value);
    }

    public static SearchParameter parameter(final String value) {
        return new SearchParameter(value);
    }

    public static SearchParameter empty() {
        return new SearchParameter(null);
    }

    private SearchParameter(final String value) {
        this.value = value;
    }

    public boolean isEmpty() {
        return value == null;
    }

    public String value() {
        return value;
    }

    public String valueOr(final String defaultValue) {
        if (isEmpty()) {
            return defaultValue;
        }

        return value;
    }
}
