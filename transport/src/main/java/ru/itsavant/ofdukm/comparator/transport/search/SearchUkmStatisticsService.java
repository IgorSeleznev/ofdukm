package ru.itsavant.ofdukm.comparator.transport.search;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ListSqlDao;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class SearchUkmStatisticsService {

    private static final String UKM_STATISTIC_SQL =
            "select h.shift_open, h.id, h.cash_id, h.date, sum(i.price * i.quantity + i.discount) as amount, count(*) as item_count, sum(i.total_quantity) as quantity \n" +
                    "from ukmserver.trm_out_receipt_header h, \n" +
                    "     trm_out_receipt_item i \n" +
                    "where i.cash_id = h.cash_id \n" +
                    "  and i.receipt_header = h.id \n" +
                    "  and h.date between ? and ? \n" +
                    "  and h.cash_id = ? \n" +
                    "  and h.type in (0, 3) \n" +
                    "group by h.shift_open, h.id, h.cash_id, h.date\n";

    private final ListSqlDao<SearchStatistic> dao = new ListSqlDao<>();

    public List<SearchStatistic> statictics() {
        return dao.list(
                new SqlParametrizedQuery()
                        .sql(UKM_STATISTIC_SQL)
                        .parameters(
                                newArrayList(
                                        ukmContext().findRequest().getStartDate(),
                                        ukmContext().findRequest().getFinishDate(),
                                        ukmContext().findRequest().getCashId()
                                )
                        ),
                (resultSet, rowNum) -> new SearchStatistic()
                        .setShift(resultSet.getString(1))
                        .setReceiptId(resultSet.getLong(2))
                        .setDate(resultSet.getString(4))
                        .setAmount(resultSet.getString(5))
                        .setCount(resultSet.getString(6))
                        .setQuantity(resultSet.getString(7))
        );
    }
}
