package ru.itsavant.ofdukm.comparator.transport.search;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ShiftTableResult {

    private String shift;
    private String fn;
    private String amount;
}
