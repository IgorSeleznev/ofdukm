package ru.itsavant.ofdukm.comparator.transport.search;

import com.google.common.base.Strings;

public class SearchRequestValidator {

    public void validate(final SearchRequest searchRequest) {
        if (Strings.isNullOrEmpty(searchRequest.storeId())) {
            throw new IllegalArgumentException("Argument storeId is required.");
        }
        if (Strings.isNullOrEmpty(searchRequest.cashId())) {
            throw new IllegalArgumentException("Argument cashId is required.");
        }
        if (Strings.isNullOrEmpty(searchRequest.shiftId())
                && searchRequest.period() == null) {
            throw new IllegalArgumentException("Must be specified one of arguments: items number or dates period.");
        }
    }
}
