package ru.itsavant.ofdukm.comparator.transport.transport.destination;

import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.transport.Manipulated;
import ru.itsavant.ofdukm.comparator.transport.TransportData;

public interface TransportDestination<T> extends Manipulated {

    void write(final TransportData<T> transportData);

    void progressCallback(final ProgressMessage progressMessage);
}
