package ru.itsavant.ofdukm.comparator.transport;

public interface TransportDataSourceCollector<T> {

    public void collect(final TransportData<T> data);
}
