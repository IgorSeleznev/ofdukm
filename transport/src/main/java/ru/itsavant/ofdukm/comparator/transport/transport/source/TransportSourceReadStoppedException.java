package ru.itsavant.ofdukm.comparator.transport.transport.source;

public class TransportSourceReadStoppedException extends RuntimeException {

    public TransportSourceReadStoppedException() {
        super("Read operation from source was stopped");
    }
}
