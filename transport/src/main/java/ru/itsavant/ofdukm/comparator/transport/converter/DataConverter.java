package ru.itsavant.ofdukm.comparator.transport.converter;

import java.util.List;

public interface DataConverter<S, T> {

    List<T> convert(final List<S> data);
}
