package ru.itsavant.ofdukm.comparator.transport;

public interface TransportEvent {

    void event(final TransportContext transportContext);
}
