package ru.itsavant.ofdukm.comparator.transport.search;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class SearchStatistic {

    private String shift;
    private Long receiptId;
    private String date;
    private String amount;
    private String count;
    private String quantity;
}