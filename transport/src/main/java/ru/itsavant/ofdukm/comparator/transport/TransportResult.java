package ru.itsavant.ofdukm.comparator.transport;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class TransportResult {

    private TransportContext transportContext;
}
