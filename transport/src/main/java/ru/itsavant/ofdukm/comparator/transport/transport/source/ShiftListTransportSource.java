package ru.itsavant.ofdukm.comparator.transport.transport.source;

import com.google.common.collect.ImmutableList;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfo;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftInfoRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListRequest;
import ru.itsavant.ofdukm.comparator.ofd.service.manager.ShiftListParameters;
import ru.itsavant.ofdukm.comparator.ofd.service.service.ShiftInfoService;
import ru.itsavant.ofdukm.comparator.ofd.service.service.ShiftListService;
import ru.itsavant.ofdukm.comparator.transport.Manipulated;
import ru.itsavant.ofdukm.comparator.transport.TransportData;
import ru.itsavant.ofdukm.comparator.transport.TransportSourceReadFinishedException;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static ru.itsavant.ofdukm.comparator.common.constants.PageSizeConstants.MAX_SMALL_LIST_PAGE_SIZE;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;
import static ru.itsavant.ofdukm.comparator.transport.TransportData.data;

public class ShiftListTransportSource {
    //implements TransportSource<ShiftInfo> {

//    private Stoppable manipulator;
//    private final ShiftListService shiftListManager = new ShiftListService();
//    private final ShiftInfoService shiftInfoService = new ShiftInfoService();
//    private final ShiftListRequest request;
//
//    public ShiftListTransportSource(final ShiftListRequest request) {
//        this.request = request;
//    }
//
//    @Override
//    public TransportData<ShiftInfo> read() {
//        if (manipulator.stopped()) {
//            throw new TransportSourceReadFinishedException();
//        }
//        if (request.getPageSize() == 0) {
//            return data(ImmutableList.of());
//        }
//        final List<ShiftListItem> items = shiftListManager.shiftList(
//                new ShiftListParameters()
//        );
//        request.setPageNumber(request.getPageNumber() + 1);
//        request.setPageSize(items.size() < MAX_SMALL_LIST_PAGE_SIZE ? 0 : MAX_SMALL_LIST_PAGE_SIZE);
//
//        final List<ShiftInfo> result = newArrayList();
//        items.forEach(
//                shift ->
//                        result.add(
//                                shiftInfoService.shiftInfo(
//                                        new ShiftInfoRequest()
//                                                .setFn(shift.getFnFactoryNumber())
//                                                .setShift(String.valueOf(shift.getShiftNumber()))
//                                                .setSessionToken(ofdContext().sessionToken())
//                                )
//                                        .getShift()
//                        ));
//
//        return data(result);
//    }
//
//    @Override
//    public boolean hasNext() {
//        return request.getPageSize() == MAX_SMALL_LIST_PAGE_SIZE;
//    }
//
//    @Override
//    public Manipulated stopManipulator(Stoppable manipulator) {
//        this.manipulator = manipulator;
//        return this;
//    }
}
