package ru.itsavant.ofdukm.comparator.transport.destination;

import java.util.List;

public interface Destination<T> {

    void save(final List<T> data);
}
