package ru.itsavant.ofdukm.comparator.transport.search;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.insert.InsertTableCopyDao;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.*;

public class SearchResultLogService {

    private final InsertTableCopyDao copyDao = new InsertTableCopyDao();
    
    public void log() {
        copyDao.copy(OFD_DOCUMENT_HEAD_TABLENAME, OFD_DATABASE_NAME + ".log_" + OFD_DOCUMENT_HEAD_TABLENAME);
        copyDao.copy(OFD_DOCUMENT_PROPERTY_TABLENAME, OFD_DATABASE_NAME + ".log_" + OFD_DOCUMENT_PROPERTY_TABLENAME);
        copyDao.copy(OFD_DOCUMENT_PRODUCT_PROPERTY_TABLENAME, OFD_DATABASE_NAME + ".log_" + OFD_DOCUMENT_PRODUCT_PROPERTY_TABLENAME);
        copyDao.copy(OFD_RECEIPT_HEADER_TABLENAME, OFD_DATABASE_NAME + ".log_" + OFD_RECEIPT_HEADER_TABLENAME);
        copyDao.copy(OFD_RECEIPT_ITEM_TABLENAME, OFD_DATABASE_NAME + ".log_" + OFD_RECEIPT_ITEM_TABLENAME);
    }
}
