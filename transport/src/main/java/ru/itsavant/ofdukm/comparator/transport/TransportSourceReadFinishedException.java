package ru.itsavant.ofdukm.comparator.transport;

public class TransportSourceReadFinishedException extends RuntimeException {

    public TransportSourceReadFinishedException() {
        super("Read source has finished to read data");
    }
}
