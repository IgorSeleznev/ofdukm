package ru.itsavant.ofdukm.comparator.transport.transport.source;

import lombok.val;
import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.Counts;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombinedResponse;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentListRequest;
import ru.itsavant.ofdukm.comparator.ofd.service.service.DocumentCombinedListService;
import ru.itsavant.ofdukm.comparator.ofd.service.service.DocumentInfoService;
import ru.itsavant.ofdukm.comparator.transport.Manipulated;
import ru.itsavant.ofdukm.comparator.transport.TransportData;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static ru.itsavant.ofdukm.comparator.common.constants.PageSizeConstants.MAX_LARGE_LIST_PAGE_SIZE;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;
import static ru.itsavant.ofdukm.comparator.transport.TransportData.data;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

public class DocumentListTransportSource implements TransportSource<DocumentCombined> {

    private Stoppable stoppable;
    private final DocumentCombinedListService documentService = new DocumentCombinedListService();
    private final String fn;
    private final String shift;
    private Counts counts = new Counts();
    private boolean wasRead = false;
    private int pageNumber;

    public DocumentListTransportSource(final String fn, final String shift) {
        this.fn = fn;
        this.shift = shift;
        this.pageNumber = 1;
    }

    @Override
    public TransportData<DocumentCombined> read() {
        if (!hasNext()) {
            return data(emptyList());
        }
        if (stoppable.stopped()) {
            throw new TransportSourceReadStoppedException();
        }

        final DocumentCombinedResponse result = documentService.documents(
                new DocumentListRequest()
                        .setFn(fn)
                        .setShift(shift)
                        .setSessionToken(ofdContext().sessionToken())
                        .setPageNumber(pageNumber)
                        .setPageSize(MAX_LARGE_LIST_PAGE_SIZE)
        );

        wasRead = true;

        counts = result.getCounts();
        pageNumber++;

        return data(
                result.getRecords()
                        .stream()
                        .filter(
                                documentCombined ->
                                        documentCombined.getHead().getDateTime().replace("T", " ").compareTo(ukmContext().findRequest().getStartDate()) >= 0
                                                && documentCombined.getHead().getDateTime().replace("T", " ").compareTo(ukmContext().findRequest().getFinishDate()) <= 0
                        ).collect(Collectors.toList())
        );
    }

    @Override
    public boolean hasNext() {
        return !wasRead || counts.getRecordInResponceCount() == MAX_LARGE_LIST_PAGE_SIZE;
    }

    @Override
    public Manipulated stopManipulator(final Stoppable stoppable) {
        this.stoppable = stoppable;
        this.documentService.stoppable(stoppable);
        return this;
    }
}
