package ru.itsavant.ofdukm.comparator.transport;

import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.common.converter.Converter;
import ru.itsavant.ofdukm.comparator.transport.transport.destination.TransportDestination;
import ru.itsavant.ofdukm.comparator.transport.transport.source.TransportSource;

public class Transport<TSource, TDestination> implements Stoppable {

//    private final Thread thread = new Thread(this::process);
    private TransportDataSourceCollector<TSource> dataCollector;
    private TransportSource<TSource> source;
    private TransportDestination<TDestination> destination;
    private Converter<TransportData<TSource>, TransportData<TDestination>> transportConverter = data -> (TransportData<TDestination>) data;
    private volatile boolean stopped = false;
    private volatile boolean finished = false;
    private volatile boolean isRunning = false;

    public Transport(final TransportSource<TSource> source) {
        this.source = source;
        this.source.stopManipulator(this);
    }

    public Transport(final TransportSource<TSource> source, final TransportDataSourceCollector<TSource> dataCollector) {
        this(source);
        this.dataCollector = dataCollector;
    }

    public TransportDataSourceCollector<TSource> getDataCollector() {
        return dataCollector;
    }

    public Transport setDataCollector(TransportDataSourceCollector<TSource> dataCollector) {
        this.dataCollector = dataCollector;
        return this;
    }

    public void transport(final TransportDestination<TDestination> destination) {
        this.isRunning = true;
        this.destination = destination;
        this.destination.stopManipulator(this);
//        if (thread.isAlive()) {
//            throw new RuntimeException("Transport process has already started.");
//        }
//        if (finished || stopped) {
//            throw new RuntimeException("That process process has done already");
//        }

//        thread.start();
        process();
    }

    @Override
    public boolean stopped() {
        return stopped;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public void stop() {
        this.stopped = true;
    }

    private void process() {
        isRunning = true;
        do {
            try {

                destination.write(
                        transportConverter.convert(
                                collect(source.read())
                        )
                );
            } catch (final Throwable throwable) {
                isRunning = false;
                finished = true;
                throw new RuntimeException(throwable);

            }
        } while (source.hasNext() && !stopped);
        isRunning = false;
        finished = true;
    }

    private TransportData<TSource> collect(final TransportData<TSource> data) {
        if (dataCollector != null) {
            dataCollector.collect(data);
        }
        return data;
    }
}
