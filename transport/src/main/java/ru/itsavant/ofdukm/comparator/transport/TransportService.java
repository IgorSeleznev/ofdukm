package ru.itsavant.ofdukm.comparator.transport;

import ru.itsavant.ofdukm.comparator.transport.transport.source.TransportSource;

public interface TransportService<TSource, TDestination> {

    TransportService source(final TransportSource<TSource> transportSource);
    TransportService destination(final TransportSource<TDestination> transportDestination);

    void start();
    void commit();
    void rollback();

    int transport();

    boolean hasNext();
}
