package ru.itsavant.ofdukm.comparator.transport.transport.source;

import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.service.service.PageableKktCombinedTotalListService;
import ru.itsavant.ofdukm.comparator.transport.Manipulated;
import ru.itsavant.ofdukm.comparator.transport.TransportData;
import ru.itsavant.ofdukm.comparator.transport.TransportSourceReadFinishedException;

import java.util.List;

import static ru.itsavant.ofdukm.comparator.common.constants.PageSizeConstants.MAX_SMALL_LIST_PAGE_SIZE;
import static ru.itsavant.ofdukm.comparator.transport.TransportData.data;

public class KktListTransportSource implements TransportSource<KktCombined> {

    private Stoppable manipulator;

    private final PageableKktCombinedTotalListService totalListService = new PageableKktCombinedTotalListService();

    private int receivedRecourdsCount = 0;

    @Override
    public TransportData<KktCombined> read() {

        if (manipulator.stopped()) {
            throw new TransportSourceReadFinishedException();
        }

        final List<KktCombined> result = totalListService.kktList();

        receivedRecourdsCount = result.size();

        return data(result);
    }

    @Override
    public boolean hasNext() {
        return receivedRecourdsCount == MAX_SMALL_LIST_PAGE_SIZE;
    }

    @Override
    public Manipulated stopManipulator(Stoppable manipulator) {
        this.manipulator = manipulator;
        return this;
    }
}
