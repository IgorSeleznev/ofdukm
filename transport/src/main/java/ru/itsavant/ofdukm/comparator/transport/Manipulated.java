package ru.itsavant.ofdukm.comparator.transport;

import ru.itsavant.ofdukm.comparator.common.concurrent.Stoppable;

public interface Manipulated {

    Manipulated stopManipulator(final Stoppable manipulator);
}
