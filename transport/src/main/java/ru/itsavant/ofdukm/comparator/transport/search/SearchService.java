package ru.itsavant.ofdukm.comparator.transport.search;

import ch.qos.logback.classic.Logger;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.slf4j.LoggerFactory;
import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;
import ru.itsavant.ofdukm.comparator.common.counter.Counter;
import ru.itsavant.ofdukm.comparator.common.error.ErrorMessage;
import ru.itsavant.ofdukm.comparator.common.logger.LoggerUtil;
import ru.itsavant.ofdukm.comparator.common.stacktrace.StackTracePrinter;
import ru.itsavant.ofdukm.comparator.ofd.model.authorization.AuthorizationRequest;
import ru.itsavant.ofdukm.comparator.ofd.model.document.DocumentCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktCombined;
import ru.itsavant.ofdukm.comparator.ofd.model.kkt.KktFn;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListItem;
import ru.itsavant.ofdukm.comparator.ofd.model.shift.ShiftListRequest;
import ru.itsavant.ofdukm.comparator.ofd.service.service.AuthorizationService;
import ru.itsavant.ofdukm.comparator.ofd.service.service.KktCombinedService;
import ru.itsavant.ofdukm.comparator.ofd.service.service.ShiftListService;
import ru.itsavant.ofdukm.comparator.transport.Transport;
import ru.itsavant.ofdukm.comparator.transport.transport.destination.DocumentCombinedTransportDestination;
import ru.itsavant.ofdukm.comparator.transport.transport.destination.TransportDestinationProgressEventHandler;
import ru.itsavant.ofdukm.comparator.transport.transport.source.DocumentListTransportSource;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.SafeDatabaseCreator;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmPos;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShift;
import ru.itsavant.ofdukm.comparator.ukm.model.UkmShiftRequest;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmPosRegistrationNumberService;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmPosService;
import ru.itsavant.ofdukm.comparator.ukm.service.service.UkmShiftService;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.stream.Collectors.toList;
import static ru.itsavant.ofdukm.comparator.common.constants.PageSizeConstants.MAX_SMALL_LIST_PAGE_SIZE;
import static ru.itsavant.ofdukm.comparator.common.context.CommonContext.commonContext;
import static ru.itsavant.ofdukm.comparator.ofd.service.context.OfdServiceContext.ofdContext;
import static ru.itsavant.ofdukm.comparator.ukm.service.context.UkmServiceContext.ukmContext;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class SearchService {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(SearchService.class);

    private final SearchRequestValidator requestValidator = new SearchRequestValidator();
    private final UkmPosRegistrationNumberService registrationNumberService = new UkmPosRegistrationNumberService();
    private final SafeDatabaseCreator databaseCreator = new SafeDatabaseCreator();

    private final Logger logger;

    private Consumer<List<ShiftListItem>> onFinished;

    public SearchService() {
        this.logger = LoggerUtil.createLoggerFor("ru.itsavant.ofdukm.comparator.transport.search.SearchService", "ofdukm-comparator.log");
    }

    public void search(final SearchRequest searchRequest, final TransportDestinationProgressEventHandler progressEventHandler) {
        logger.info("ЗАПУЩЕНО ПОЛУЧЕНИЕ ДАННЫХ ИЗ ОФД");

        try {
            databaseCreator.safeCreate();

            requestValidator.validate(searchRequest);

            commonContext().messageQueue().push(
                    new ProgressMessage()
                            .messageSource(this.getClass())
                            .title("Получение кассы из УКМ")
                            .current(0)
                            .size(100)
            );

            //TODO: получить кассу из УКМ
            logger.info("ПОЛУЧЕНИЕ ДАННЫХ О КАССЕ ИЗ УКМ");
            final UkmPos ukmPos = new UkmPosService().pos(searchRequest.storeId(), searchRequest.cashId());

            ukmContext().ukmPos(ukmPos);

            commonContext().messageQueue().push(
                    new ProgressMessage()
                            .messageSource(this.getClass())
                            .title("Поиск смен из УКМ за период")
                            .current(10)
                            .size(100)
            );

            //TODO: получить смены из укм по дате или по идентификатору
            logger.info("ПОЛУЧЕНИЕ СМЕН ПО КАССЕ ЗА ПЕРИОД.Store id = {}, cash id = {}, период с {} по {}.{}",
                    searchRequest.storeId(),
                    searchRequest.cashId(),
                    searchRequest.period().getStartDate(),
                    searchRequest.period().getFinishDate(),
                    isNullOrEmpty(searchRequest.shiftId()) ? "" : " Указана смена = " + searchRequest.shiftId()
            );
            final List<UkmShift> ukmShifts = new UkmShiftService().shift(
                    new UkmShiftRequest()
                            .setStoreId(searchRequest.storeId())
                            .setCashId(searchRequest.cashId())
                            .setShiftId(searchRequest.shiftId())
                            .setStartDate(searchRequest.period().getStartDate())
                            .setFinishDate(searchRequest.period().getFinishDate())
            );

            if (ukmShifts.size() > 0) {
                if (isNullOrEmpty(searchRequest.period().getStartDate())) {
                    String startDate = "2999-12-31 23:59:59";
                    for (final UkmShift ukmShift : ukmShifts) {
                        if (!isNullOrEmpty(ukmShift.getOpenDate()) && startDate.compareTo(ukmShift.getOpenDate()) >= 0) {
                            startDate = ukmShift.getOpenDate();
                        }
                    }
                    ukmContext().findRequest().setStartDate(startDate);
                }

                if (isNullOrEmpty(searchRequest.period().getFinishDate())) {
                    String finishDate = "1970-01-01 00:00:00";
                    for (final UkmShift ukmShift : ukmShifts) {
                        if (!isNullOrEmpty(ukmShift.getOpenDate()) && finishDate.compareTo(ukmShift.getCloseDate()) <= 0) {
                            finishDate = ukmShift.getCloseDate();
                        }
                    }
                    ukmContext().findRequest().setFinishDate(finishDate);
                }
                ukmPos.setRegistrationNumber(ukmShifts.get(0).getKkmRegNumber());
            } else {
                ukmPos.setRegistrationNumber(
                        registrationNumberService.number(
                                searchRequest.cashId(), searchRequest.period().getStartDate(), searchRequest.period().getFinishDate()
                        )
                );
            }

            ukmContext().findRequest()
                    .setStoreId(ukmPos.getStoreId())
                    .setCashId(ukmPos.getCashId())
                    .setCashName(ukmPos.getName());

            commonContext().messageQueue().push(
                    new ProgressMessage()
                            .messageSource(this.getClass())
                            .title("Авторизация в ОФД")
                            .current(20)
                            .size(100)
            );

            logger.info("АВТОРИЗАЦИЯ В ОФД ДЛЯ ПОЛЬЗОВАТЕЛЯ {}", ofdContext().login());
            final String token = new AuthorizationService()
                    .authorize(
                            new AuthorizationRequest()
                                    .setLogin(ofdContext().login())
                                    .setPassword(ofdContext().password())
                    );
            ofdContext().sessionToken(token);

            logger.info("ПОЛУЧЕНИЕ ДАННЫХ О КАССЕ ИЗ ОФД  для регистрационного номера {}", ukmPos.getRegistrationNumber());
            commonContext().messageQueue().push(
                    new ProgressMessage()
                            .messageSource(this.getClass())
                            .title("Получение данных по кассе из ОФД")
                            .current(30)
                            .size(100)
            );

            //TODO: получить фискальные накопители по кассе из офд
            final KktCombined kkt = new KktCombinedService().kkt(ukmPos.getRegistrationNumber());

            ukmContext().kktCombined(kkt);

            final List<ShiftListItem> shifts = newArrayList();

            //TODO: получить смены для фискальных накопителей из офд по дате
            logger.info("ПОЛУЧЕНИЕ СМЕН ДЛЯ ФИСКАЛЬНЫХ НАКОПИТЕЛЕЙ ИЗ ОФД");
            final Map<String, KktFn> fnMap = newHashMap();
            kkt.getKktFn().forEach(
                    fn -> fnMap.put(fn.getFn(), fn)
            );

            fnMap.forEach(
                    (key, value) ->
                            shifts.addAll(
                                    new ShiftListService().shiftList(
                                            new ShiftListRequest()
                                                    .setFn(value.getFn())
                                                    .setBegin(searchRequest.period().getStartDate())
                                                    .setEnd(searchRequest.period().getFinishDate())
                                                    .setSessionToken(ofdContext().sessionToken())
                                                    .setPageNumber(1)
                                                    .setPageSize(MAX_SMALL_LIST_PAGE_SIZE)
                                    ).getRecords()
                            )
            );

            //TODO: скопировать смены и чеки из офд в укм

            final int step = Math.round((60 / shifts.size()) / 4);
            final Counter stepCounter = Counter.basedByZero();

            logger.info("НАЧАТО КОПИРОВАНИ ЧЕКОВ ИЗ ОФД В СЛУЖЕБНЫЕ ТАБЛИЦЫ");
            for (int i = 0; i < shifts.size(); i++) {
                final ShiftListItem ofdShift = shifts.get(i);
                final List<UkmShift> ukmShiftsFiltered = ukmShifts.stream().filter(
                        ukmShift -> ukmShift.getKkmShiftNumber().equals(String.valueOf(ofdShift.getShiftNumber()))
                ).collect(toList());
                ukmContext().findRequest().setShiftId(null);
                if (ukmShiftsFiltered.size() > 0) {
                    ukmContext().findRequest().setShiftId(ukmShiftsFiltered.get(0).getShiftNumber());
                }
                final Transport<DocumentCombined, DocumentCombined> transport = new Transport<>(
                        new DocumentListTransportSource(ofdShift.getFnFactoryNumber(), String.valueOf(ofdShift.getShiftNumber()))
                );

                commonContext().messageQueue().push(
                        new ProgressMessage()
                                .messageSource(this.getClass())
                                .title("Авторизация в ОФД для получения смены")
                                .current(40 + stepCounter.currentAndNext() * step)
                                .size(100)
                );

                logger.info("ЗАПУЩЕНО КОПИРОВАНИЕ ЧЕКОВ ИЗ ОФД ДЛЯ СМЕНЫ {}", ofdShift.getShiftNumber());
                final String renewToken = new AuthorizationService()
                        .authorize(
                                new AuthorizationRequest()
                                        .setLogin(ofdContext().login())
                                        .setPassword(ofdContext().password())
                        );
                commonContext().messageQueue().push(
                        new ProgressMessage()
                                .messageSource(this.getClass())
                                .title("Авторизация в ОФД")
                                .current(40)
                                .size(100)
                );

                final int shiftIndex = i + 1;

                commonContext().messageQueue().push(
                        new ProgressMessage()
                                .messageSource(this.getClass())
                                .title("Начато получение чеков из ОФД для смены (" + shiftIndex + " из " + shifts.size() + ")...")
                                .current(40 + stepCounter.currentAndNext() * step)
                                .size(100)
                );
                ofdContext().sessionToken(renewToken);
                transport.transport(
                        new DocumentCombinedTransportDestination()
                                .setProgressEventHandler(progressEventHandler)
                                .stopManipulator(transport)
                );

                commonContext().messageQueue().push(
                        new ProgressMessage()
                                .messageSource(SearchService.class)
                                .title("Скопированы чеки из смены " + ofdShift.getShiftNumber() + " из фискального накопителя " + ofdShift.getFnFactoryNumber() + ".")
                                .size(shifts.size())
                                .current(i)
                );

                try {
                    while (transport.isRunning() && !commonContext().stopped()) {
                        Thread.sleep(100);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                commonContext().messageQueue().push(
                        new ProgressMessage()
                                .messageSource(this.getClass())
                                .title("Получение статистики по чекам из УКМ")
                                .current(80)
                                .size(100)
                );

                commonContext().messageQueue().push(
                        new ProgressMessage()
                                .messageSource(this.getClass())
                                .title("Статистика успешно собрана.")
                                .current(40 + stepCounter.currentAndNext() * step)
                                .size(100)
                );

            }
            if (onFinished != null) {
                onFinished.accept(shifts);
            }

            logger.info("ЗАПУЩЕН ПОДСЧЕТ СТАТИСТИКИ ПО НАЙДЕННЫМ ЧЕКАМ ИЗ ОФД И УКМ");
        } catch (final Throwable throwable) {
            logger.error("Произошла ошибка в процессе поиска расхождений: {}", throwable.getMessage());
            logger.error(StackTracePrinter.print(throwable));
            System.out.println("Произошла ошибка в процессе поиска расхождений: " + throwable.getMessage());

            commonContext().errorsQueue().add(
                    ErrorMessage.builder().message("В процессе поиска произошла ошибка: " + throwable.getMessage()).title("Ошибка").exception(throwable).build()
            );
            throw throwable;
        }
    }
}