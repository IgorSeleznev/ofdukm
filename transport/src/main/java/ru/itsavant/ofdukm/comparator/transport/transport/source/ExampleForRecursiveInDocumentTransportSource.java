package ru.itsavant.ofdukm.comparator.transport.transport.source;

import static com.google.common.collect.Lists.newArrayList;

public class ExampleForRecursiveInDocumentTransportSource {//implements TransportSource<DocumentCombined> {

//    private Stoppable manipulator;
//    private final DocumentListManager documentListManager = new DocumentListManager();
//    private final DocumentInfoService documentInfoService = new DocumentInfoService();
//    private final String fn;
//    private final String fd;
//
//    public ExampleForRecursiveInDocumentTransportSource(final String fn, String fd) {
//        this.fn = fn;
//        this.fd = fd;
//    }
//
//    @Override
//    public TransportData<DocumentCombined> read() {
//        if (!hasNext()) {
//            return data(emptyList());
//        }
//        if (manipulator.stopped()) {
//            throw new TransportSourceReadStoppedException();
//        }
//        if (!documentListManager.isStarted()) {
//            documentListManager.start(fn, shiftNumber);
//        }
//
//
//        final List<DocumentCombined> result = newArrayList();
//
//        final List<DocumentListItem> items = documentListManager.next();
//        for (var head : items) {
//            if (manipulator.stopped()) {
//                throw new TransportSourceReadStoppedException();
//            }
//
//            result.add(
//                    new DocumentCombined()
//                            .setHead(head)
//                            .setProperty(
//                                    documentInfoService.documentInfo(
//                                            new DocumentInfoRequest()
//                                                    .setFn(fn)
//                                                    .setFd(String.valueOf(head.getFdNumber()))
//                                                    .setSessionToken(ofdContext().sessionToken())
//                                    )
//                            )
//            );
//        }
//
//        return data(result);
//    }
//
//    @Override
//    public boolean hasNext() {
//        return documentListManager.hasNext() && !manipulator.stopped();
//    }
//
//    @Override
//    public Manipulated stopManipulator(final Stoppable manipulator) {
//        this.manipulator = manipulator;
//        return this;
//    }
}
