package ru.itsavant.ofdukm.comparator.transport.search;

import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.query.SqlParametrizedQuery;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.select.ListSqlDao;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class SearchOfdStatisticsService {

    public List<SearchStatistic> statictics() {
        return new ListSqlDao<SearchStatistic>().list(
                new SqlParametrizedQuery()
                        .sql("select h.kkt_shift, h.fd, h.fn, h.receipt_date, h.total_amount, sum(i.price * i.quantity) as amount, count(*) as items_count, sum(i.quantity) as quantity_sum \n" +
                                "from ofdukm.receipt_header h, ofdukm.receipt_item i \n" +
                                "where h.fn = i.fn and h.fd = i.fd \n" +
                                "group by h.kkt_shift, h.fd, h.fn, h.receipt_date, h.total_amount")
                        .parameters(
                                newArrayList(

                                )
                        ),
                (resultSet, rowNum) -> new SearchStatistic()
                        .setDate(resultSet.getString(4))
                        .setAmount(resultSet.getString(5))
                        .setCount(resultSet.getString(7))
                        .setQuantity(resultSet.getString(8))
                        .setShift(resultSet.getString(1))
                        .setReceiptId(resultSet.getLong(2))
        );
    }
}
