package ru.itsavant.ofdukm.comparator.transport.transport.destination;

import ru.itsavant.ofdukm.comparator.common.concurrent.ProgressMessage;

import static ru.itsavant.ofdukm.comparator.common.context.CommonContext.commonContext;

public class DefaultTransportDestinationProgressEventHandler implements TransportDestinationProgressEventHandler {

    @Override
    public void handle(final ProgressMessage progressMessage) {
        commonContext().messageQueue().push(progressMessage);
    }
}
