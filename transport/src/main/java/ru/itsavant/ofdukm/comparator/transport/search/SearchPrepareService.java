package ru.itsavant.ofdukm.comparator.transport.search;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.create.CreateTableCopyDao;
import ru.itsavant.ofdukm.comparator.ukm.da.dao.common.truncate.TruncateDao;

import static ru.itsavant.ofdukm.comparator.ukm.da.constants.DaConstants.*;
import static ru.itsavant.ofdukm.comparator.ukm.da.context.UkmDatasourceContext.daContext;

public class SearchPrepareService {

    private final CreateTableCopyDao createTableCopyDao = new CreateTableCopyDao();
    private final TruncateDao truncateDao = new TruncateDao();
    private final String[] tablenames = new String[]{
            OFD_DOCUMENT_HEAD_TABLENAME,
            OFD_DOCUMENT_PROPERTY_TABLENAME,
            OFD_DOCUMENT_PRODUCT_PROPERTY_TABLENAME,
            OFD_RECEIPT_HEADER_TABLENAME,
            OFD_RECEIPT_ITEM_TABLENAME
    };

    public void prepare() {
        final JdbcTemplate jdbcTemplate = daContext().jdbcTemplate();


        for (final String tablename : tablenames) {
            truncateDao.truncate(OFD_DATABASE_NAME + "." + tablename);
            createTableCopyDao.copy(OFD_DATABASE_NAME + "." + tablename, OFD_DATABASE_NAME + ".log_" + tablename);
            try {
                jdbcTemplate.execute(
                        String.format(
                                "alter table %s.log_%s add column log_date DATETIME first",
                                OFD_DATABASE_NAME,
                                tablename
                        )
                );
            } catch (final Throwable throwable) {
                System.out.println("Таблица уже существует, добавлять колонку больше не нужно.");
            }
        }
    }
}
